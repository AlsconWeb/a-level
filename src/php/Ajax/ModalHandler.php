<?php
/**
 * Ajax modal handler.
 *
 * @package iwpdev/alevel.
 */

namespace Alevel\Ajax;

use Alevel\Email\AdminEmail;
use Alevel\Helpers\Helper;
use Alevel\Pipedrive\API\PipedriveApiCrm;
use Exception;
use GuzzleHttp\Client;
use JsonException;
use libphonenumber\PhoneNumberUtil;
use WP_Query;

/**
 * ModalHandler class file.
 */
class ModalHandler {
	/**
	 * Action and nonce become teacher.
	 */
	public const NEW_TEACHER = 'alv_become_teacher';

	/**
	 * Action and nonce course review.
	 */
	public const NEW_REVIEW = 'alv_course_review';

	/**
	 * Action and nonce register on event.
	 */
	public const REGISTER_ON_EVENT = 'alv_register_on_event';

	/**
	 * Action and nonce register on course.
	 */
	public const      REGISTER_ON_COURSE = 'alv_register_on_course';

	/**
	 * Action and nonce register on partner.
	 */
	public const REGISTER_PARTNERS = 'alv_register_partner';

	/**
	 * Action and nonce course payment form.
	 */
	public const COURSE_PAYMENT_FORM = 'alv_course_payment';

	/**
	 * Action and nonce review regular.
	 */
	public const REVIEW_REGULAR_ACTION = 'alv_review_regular';

	/**
	 * Action and nonce call back modal.
	 */
	public const CALL_BACK_MODAL_ACTION = 'alv_call_back_modal';

	/**
	 * Get course teachers action and nonce.
	 */
	public const GET_COURSE_TEACHERS = 'alv_get_course_teachers';

	/**
	 * Telegram bot token.
	 */
	private const TG_TOKEN = '5427192791:AAHsfKKSr33iGxZBr8JIdTKSaNFDdBOjXk4';

	/**
	 * Telegram chat id.
	 */
	public const TG_CHAT_ID = '-544652317';

	/**
	 * ModalHandler construct.
	 */
	public function __construct() {
		add_action( 'wp_ajax_' . self::NEW_TEACHER, [ $this, 'become_teacher' ] );
		add_action( 'wp_ajax_nopriv_' . self::NEW_TEACHER, [ $this, 'become_teacher' ] );

		add_action( 'wp_ajax_' . self::NEW_REVIEW, [ $this, 'new_review' ] );
		add_action( 'wp_ajax_nopriv_' . self::NEW_REVIEW, [ $this, 'new_review' ] );

		add_action( 'wp_ajax_' . self::REGISTER_ON_EVENT, [ $this, 'register_on_event' ] );
		add_action( 'wp_ajax_nopriv_' . self::REGISTER_ON_EVENT, [ $this, 'register_on_event' ] );

		add_action( 'wp_ajax_' . self::REGISTER_ON_COURSE, [ $this, 'register_on_course' ] );
		add_action( 'wp_ajax_nopriv_' . self::REGISTER_ON_COURSE, [ $this, 'register_on_course' ] );

		add_action( 'wp_ajax_' . self::REGISTER_PARTNERS, [ $this, 'register_on_partner' ] );
		add_action( 'wp_ajax_nopriv_' . self::REGISTER_PARTNERS, [ $this, 'register_on_partner' ] );

		add_action( 'wp_ajax_' . self::COURSE_PAYMENT_FORM, [ $this, 'payment_form' ] );
		add_action( 'wp_ajax_nopriv_' . self::COURSE_PAYMENT_FORM, [ $this, 'payment_form' ] );

		add_action( 'wp_ajax_' . self::REVIEW_REGULAR_ACTION, [ $this, 'review_regular_modal' ] );
		add_action( 'wp_ajax_nopriv_' . self::REVIEW_REGULAR_ACTION, [ $this, 'review_regular_modal' ] );

		add_action( 'wp_ajax_' . self::CALL_BACK_MODAL_ACTION, [ $this, 'call_back_modal' ] );
		add_action( 'wp_ajax_nopriv_' . self::CALL_BACK_MODAL_ACTION, [ $this, 'call_back_modal' ] );

		add_action( 'wp_ajax_' . self::GET_COURSE_TEACHERS, [ $this, 'get_course_teachers_checkboxes' ] );
		add_action( 'wp_ajax_nopriv_' . self::GET_COURSE_TEACHERS, [ $this, 'get_course_teachers_checkboxes' ] );
	}

	/**
	 * Become teacher ajax handler.
	 *
	 * @return void
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public function become_teacher(): void {
		$nonce = ! empty( $_POST['nonce'] ) ? filter_var( wp_unslash( $_POST['nonce'] ), FILTER_SANITIZE_STRING ) : false;

		if ( ! wp_verify_nonce( $nonce, self::NEW_TEACHER ) ) {
			wp_send_json_error( [ 'message' => __( 'Не врiрний одноразовий код', 'alevel' ) ] );
		}

		$first_name = ! empty( $_POST['first_name'] ) ? filter_var( wp_unslash( $_POST['first_name'] ), FILTER_SANITIZE_STRING ) : null;
		$last_name  = ! empty( $_POST['last_name'] ) ? filter_var( wp_unslash( $_POST['last_name'] ), FILTER_SANITIZE_STRING ) : null;
		$email      = ! empty( $_POST['email'] ) ? filter_var( wp_unslash( $_POST['email'] ), FILTER_SANITIZE_EMAIL ) : null;
		$phone      = ! empty( $_POST['phone'] ) ? filter_var( wp_unslash( $_POST['phone'] ), FILTER_SANITIZE_NUMBER_INT ) : null;

		$company       = ! empty( $_POST['company'] ) ? filter_var( wp_unslash( $_POST['company'] ), FILTER_SANITIZE_STRING ) : '';
		$position      = ! empty( $_POST['position'] ) ? filter_var( wp_unslash( $_POST['position'] ), FILTER_SANITIZE_STRING ) : '';
		$linkedin_link = ! empty( $_POST['linkedin'] ) ? filter_var( wp_unslash( $_POST['linkedin'] ), FILTER_SANITIZE_URL ) : '';
		$description   = ! empty( $_POST['message'] ) ? esc_html( filter_var( wp_unslash( $_POST['message'] ), FILTER_SANITIZE_STRING ) ) : '';
		$course_name   = ! empty( $_POST['course_name'] ) ? filter_var( wp_unslash( $_POST['course_name'] ), FILTER_SANITIZE_STRING ) : '';
		$course_id     = ! empty( $_POST['course_id'] ) ? filter_var( wp_unslash( $_POST['course_id'] ), FILTER_SANITIZE_NUMBER_INT ) : 1;
		$pipe          = Helper::is_basic_programiong( $course_id ) ? 2 : 1;

		$data = [
			'title'       => __( 'Нова заявка стати викладачем', 'alevel' ),
			'first_name'  => $first_name,
			'last_name'   => $last_name,
			'email'       => $email,
			'phone'       => $phone,
			'company'     => $company,
			'position'    => $position,
			'linkedin'    => $linkedin_link,
			'description' => $description,
			'course_name' => $course_name,
			'pipeline_id' => $pipe,
			'form_name'   => 'new_teacher',
		];

		$pipe     = new PipedriveApiCrm();
		$response = $pipe->create_lead( $data );

		if ( isset( $response['error'] ) ) {
			wp_send_json_error(
				[
					'message' => __( 'Виникла помилка спробуйте пізніше або зв\'яжіться з нами по емейлу', 'alevel' ),
					'error'   => $response['error'],
				]
			);
		}

		$data['form_name'] = 'tg_body_theaters';
		$tg_response       = self::send_to_tg_channel( $data, self::TG_CHAT_ID );

		if ( $response['success'] ) {
			wp_send_json_success(
				[
					'message' => __( 'Все добре!' ),
					'send_tg' => $tg_response,
					'events'  => Helper::get_event_to_madal_thnas(),
				]
			);
		}
	}

	/**
	 * Send to telegram channel.
	 *
	 * @param array  $data       Data array message.
	 * @param string $channel_id Channel id.
	 *
	 * @return array|bool[]
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public static function send_to_tg_channel( array $data, string $channel_id ): array {
		try {
			$client = new Client(
				[
					'base_uri' => 'https://api.telegram.org',
				]
			);

			$bot_token = self::TG_TOKEN;
			$chat_id   = $channel_id;
			$crm_api   = new PipedriveApiCrm();

			$description_candidate = $crm_api->generate_body_message( $data );
			$response              = $client->request(
				'GET',
				"/bot$bot_token/sendMessage",
				[
					'query' => [
						'chat_id'    => $chat_id,
						'text'       => $description_candidate,
						'parse_mode' => 'Markdown',
					],
				]
			);

			$body     = $response->getBody();
			$arr_body = json_decode( $body, false, 512, JSON_THROW_ON_ERROR );

			if ( $arr_body->ok ) {
				return [ 'success' => true ];
			}

			return [ 'error' => $arr_body ];

		} catch ( Exception $e ) {
			return [ 'error' => $e->getMessage() ];
		}
	}

	/**
	 * New review ajax handler.
	 *
	 * @return void
	 * @throws JsonException
	 */
	public function new_review(): void {
		$nonce = ! empty( $_POST['nonce'] ) ? filter_var( wp_unslash( $_POST['nonce'] ), FILTER_SANITIZE_STRING ) : false;

		if ( ! wp_verify_nonce( $nonce, self::NEW_REVIEW ) ) {
			wp_send_json_error( [ 'message' => __( 'Не врiрний одноразовий код', 'alevel' ) ] );
		}

		$email      = ! empty( $_POST['email'] ) ? filter_var( wp_unslash( $_POST['email'] ), FILTER_SANITIZE_EMAIL ) : '';
		$first_name = ! empty( $_POST['firs_name'] ) ? filter_var( wp_unslash( $_POST['firs_name'] ), FILTER_SANITIZE_STRING ) : '';
		$last_name  = ! empty( $_POST['last_name'] ) ? filter_var( wp_unslash( $_POST['last_name'] ), FILTER_SANITIZE_STRING ) : '';
		$message    = ! empty( $_POST['message'] ) ? filter_var( wp_unslash( $_POST['message'] ), FILTER_SANITIZE_STRING ) : '';
		$phone      = ! empty( $_POST['phone'] ) ? filter_var( wp_unslash( $_POST['phone'] ), FILTER_SANITIZE_STRING ) : '';
		$social     = ! empty( $_POST['social'] ) ? filter_var_array(
			json_decode( wp_unslash( $_POST['social'] ), true, 512, JSON_THROW_ON_ERROR ),
			[
				'facebook'  => [ 'filter' => FILTER_VALIDATE_URL ],
				'instagram' => [ 'filter' => FILTER_VALIDATE_URL ],
				'linkedin'  => [ 'filter' => FILTER_VALIDATE_URL ],
				'behance'   => [ 'filter' => FILTER_VALIDATE_URL ],
				'telegram'  => [ 'filter' => FILTER_VALIDATE_URL ],
			]
		) : [];

		$file = filter_var_array(
			$_FILES,
			[
				'photo' => [
					'filter' => FILTER_UNSAFE_RAW,
					'flags'  => FILTER_FORCE_ARRAY,
				],
			]
		)['photo'];

		if ( empty( $file ) ) {
			wp_send_json_error( [ 'message' => __( 'Фото порожній файл або у недоступному форматі', 'alevel' ) ] );
		}

		$teachers  = ! empty( $_POST['teacher'] ) ? filter_var( wp_unslash( $_POST['teacher'] ), FILTER_SANITIZE_STRING ) : '';
		$teachers  = explode( ',', $teachers );
		$rating    = ! empty( $_POST['star_rating'] ) ? filter_var( wp_unslash( $_POST['star_rating'] ), FILTER_SANITIZE_NUMBER_FLOAT ) : 0;
		$course_id = ! empty( $_POST['course_id'] ) ? filter_var( wp_unslash( $_POST['course_id'] ), FILTER_SANITIZE_NUMBER_INT ) : 0;
		$pipe      = Helper::is_basic_programiong( $course_id ) ? 2 : 1;

		$current_blog_id = get_current_blog_id();
		if ( 1 !== $current_blog_id && is_multisite() ) {
			switch_to_blog( get_main_site_id() );
		}

		$avatar_id = self::upload_photo( $file );

		if ( isset( $avatar_id['error'] ) ) {
			wp_send_json_error( [ 'message' => __( 'Фото не було завантажено на сервер виникла помилка, спробуйте пізніше', 'alevel' ) ] );
		}

		$args = [
			'title'       => __( 'Залишений відгук на курс: ', 'alevel' ) . get_the_title( $course_id ),
			'email'       => $email,
			'full_name'   => $first_name . ' ' . $last_name,
			'first_name'  => $first_name,
			'last_name'   => $last_name,
			'content'     => $message,
			'avatar_id'   => $avatar_id['attachment_id'],
			'phone'       => $phone,
			'social'      => $social,
			'teachers'    => $teachers,
			'rating'      => $rating,
			'course_id'   => $course_id,
			'pipeline_id' => $pipe,
			'form_name'   => 'review_form',
		];

		$insert_review = self::create_review( $args );

		if ( is_multisite() ) {
			switch_to_blog( $current_blog_id );
		}

		$pipe     = new PipedriveApiCrm();
		$response = $pipe->create_lead( $args );

		if ( isset( $insert_review['error'] ) ) {
			wp_send_json_error( [ 'message' => __( 'Відгук не було надіслано', 'alevel' ) ] );
		}

		wp_send_json_success(
			[
				'message'  => __( 'Дякуємо за відгук, від був відправлений на модерацію', 'alevel' ),
				'id'       => $insert_review['post_id'],
				'response' => $response,
			]
		);

	}

	/**
	 * Upload file to media liberty.
	 *
	 * @param array $file file.
	 *
	 * @return array
	 */
	public static function upload_photo( array $file ): array {
		if ( ! function_exists( 'wp_handle_upload' ) ) {
			require_once ABSPATH . 'wp-admin/includes/file.php';
		}

		require_once ABSPATH . 'wp-admin/includes/image.php';

		$overrides = [ 'test_form' => false ];

		$upload_file = wp_handle_upload( $file, $overrides );

		if ( $upload_file && empty( $upload_file['error'] ) ) {
			$attachment_id = wp_insert_attachment(
				[
					'guid'           => $upload_file['url'],
					'post_mime_type' => $upload_file['type'],
					'post_title'     => basename( $upload_file['file'] ),
					'post_content'   => '',
					'post_status'    => 'inherit',
				],
				$upload_file['file']
			);

			if ( is_wp_error( $attachment_id ) || ! $attachment_id ) {
				return [ 'error' => __( 'Файл не був завантажений', 'alevel' ) ];
			}

			wp_update_attachment_metadata(
				$attachment_id,
				wp_generate_attachment_metadata( $attachment_id, $upload_file['file'] )
			);

			return [ 'attachment_id' => $attachment_id ];

		}

		return [ 'error' => __( 'Файл не був завантажений', 'alevel' ) ];
	}

	/**
	 * Create review
	 *
	 * @param array $args Arguents.
	 *
	 * @return array
	 */
	public static function create_review( array $args ): array {
		$post_id = wp_insert_post(
			wp_slash(
				[
					'post_title'   => $args['full_name'],
					'post_content' => $args['content'],
					'post_status'  => 'draft',
					'post_type'    => 'testimonials',
					'post_author'  => 1,
					'ping_status'  => get_option( 'default_ping_status' ),
					'post_parent'  => 0,
					'menu_order'   => 0,
					'meta_input'   => [
						'alv_course_id' => $args['course_id'],
					],
				]
			)
		);

		if ( is_wp_error( $post_id ) ) {
			return [ 'error' => $post_id->get_error_message() ];
		}

		// set post meta.
		Helper::alv_set_teachers_by_review( $args['teachers'], $post_id );
		Helper::alv_set_social_media( $args['social'], $post_id );
		Helper::alv_set_rating( $args['rating'], $post_id );
		Helper::alv_set_phone( $args['phone'], $post_id );
		Helper::alv_set_email( $args['email'], $post_id );
		set_post_thumbnail( $post_id, $args['avatar_id'] );

		return [ 'post_id' => $post_id ];
	}

	/**
	 * Register on event
	 *
	 * @return void
	 */
	public function register_on_event(): void {
		$nonce = ! empty( $_POST['nonce'] ) ? filter_var( wp_unslash( $_POST['nonce'] ), FILTER_SANITIZE_STRING ) : false;

		if ( ! wp_verify_nonce( $nonce, self::REGISTER_ON_EVENT ) ) {
			wp_send_json_error( [ 'message' => __( 'Не врiрний одноразовий код', 'alevel' ) ] );
		}

		$email     = ! empty( $_POST['email'] ) ? filter_var( wp_unslash( $_POST['email'] ), FILTER_SANITIZE_EMAIL ) : '';
		$phone     = ! empty( $_POST['phone'] ) ? filter_var( wp_unslash( $_POST['phone'] ), FILTER_SANITIZE_STRING ) : '';
		$name      = ! empty( $_POST['name'] ) ? filter_var( wp_unslash( $_POST['name'] ), FILTER_SANITIZE_STRING ) : '';
		$event_id  = ! empty( $_POST['event_id'] ) ? filter_var( wp_unslash( $_POST['event_id'] ), FILTER_SANITIZE_NUMBER_INT ) : '';
		$course_id = ! empty( $_POST['course_id'] ) ? filter_var( wp_unslash( $_POST['course_id'] ), FILTER_SANITIZE_NUMBER_INT ) : 0;
		$pipe      = Helper::is_basic_programiong( $course_id ) ? 2 : 1;

		$data = [
			'title'       => __( 'Запис на захід: ', 'alevel' ) . get_the_title( $event_id ),
			'full_name'   => $name,
			'email'       => $email,
			'phone'       => $phone,
			'pipeline_id' => $pipe,
			'form_name'   => 'register_to_event',
		];

		$pipe     = new PipedriveApiCrm();
		$response = $pipe->create_lead( $data );

		AdminEmail::send_request_to_event_register(
			[
				'event_id' => $event_id,
				'name'     => $name,
				'email'    => $email,
				'phone'    => $phone,
			]
		);

		$event = Helper::get_event_to_madal_thnas( $event_id );

		wp_send_json_success( [ 'event' => $event, 'response' => $response ] );
	}

	/**
	 * Register on course.
	 *
	 * @return void
	 */
	public function register_on_course(): void {
		$nonce = ! empty( $_POST['nonce'] ) ? filter_var( wp_unslash( $_POST['nonce'] ), FILTER_SANITIZE_STRING ) : false;
		if ( ! wp_verify_nonce( $nonce, self::REGISTER_ON_COURSE ) ) {
			wp_send_json_error( [ 'message' => __( 'Не врiрний одноразовий код', 'alevel' ) ] );
		}

		$name      = ! empty( $_POST['name'] ) ? filter_var( wp_unslash( $_POST['name'] ), FILTER_SANITIZE_STRING ) : '';
		$email     = ! empty( $_POST['email'] ) ? filter_var( wp_unslash( $_POST['email'] ), FILTER_SANITIZE_EMAIL ) : '';
		$phone     = ! empty( $_POST['phone'] ) ? filter_var( wp_unslash( $_POST['phone'] ), FILTER_SANITIZE_STRING ) : '';
		$course_id = ! empty( $_POST['course'] ) ? filter_var( wp_unslash( $_POST['course'] ), FILTER_SANITIZE_NUMBER_INT ) : '';
		$message   = ! empty( $_POST['message'] ) ? filter_var( wp_unslash( $_POST['message'] ), FILTER_SANITIZE_STRING ) : '';

		if ( empty( $course_id ) ) {
			wp_send_json_error( [ 'message' => __( 'Ви не обрали курс, на який хочете записатися', 'alevel' ) ] );
		}

		$data = [
			'title'       => __( 'Нова заявка консультації на курс', 'alevel' ),
			'first_name'  => $name,
			'email'       => $email,
			'phone'       => $phone,
			'description' => $message,
			'course_name' => get_the_title( $course_id ),
			'form_name'   => 'consultation_on_course',
		];

		$pipe     = new PipedriveApiCrm();
		$response = $pipe->create_lead( $data );

		if ( isset( $response['error'] ) ) {
			wp_send_json_error(
				[
					'message' => __( 'Виникла помилка спробуйте пізніше або зв\'яжіться з нами по емейлу', 'alevel' ),
					'error'   => $response['error'],
				]
			);
		}

		$data['form_name'] = 'tg_body_course';
		$tg_response       = self::send_to_tg_channel( $data, self::TG_CHAT_ID );
		AdminEmail::send_request_to_consultation_course( $data );
		$event = Helper::get_event_to_madal_thnas();

		wp_send_json_success(
			[
				'message' => __( 'Все добре!' ),
				'send_tg' => $tg_response,
				'event'   => $event,
			]
		);

	}

	/**
	 * Register on partners.
	 *
	 * @return void
	 */
	public function register_on_partner(): void {

		$nonce = ! empty( $_POST['partner_nonce'] ) ? filter_var( wp_unslash( $_POST['partner_nonce'] ), FILTER_SANITIZE_STRING ) : false;
		if ( ! wp_verify_nonce( $nonce, self::REGISTER_PARTNERS ) ) {
			wp_send_json_error( [ 'message' => __( 'Не врiрний одноразовий код', 'alevel' ) ] );
		}

		$partner_name      = ! empty( $_POST['partner_name'] ) ? filter_var( wp_unslash( $_POST['partner_name'] ), FILTER_SANITIZE_STRING ) : '';
		$partner_last_mane = ! empty( $_POST['partner_last_mane'] ) ? filter_var( wp_unslash( $_POST['partner_last_mane'] ), FILTER_SANITIZE_STRING ) : '';
		$partner_email     = ! empty( $_POST['partner_email'] ) ? filter_var( wp_unslash( $_POST['partner_email'] ), FILTER_SANITIZE_EMAIL ) : '';
		$partner_phone     = ! empty( $_POST['partner_phone'] ) ? filter_var( wp_unslash( $_POST['partner_phone'] ), FILTER_SANITIZE_STRING ) : '';
		$partner_company   = ! empty( $_POST['partner_company'] ) ? filter_var( wp_unslash( $_POST['partner_company'] ), FILTER_SANITIZE_STRING ) : '';
		$partner_web       = ! empty( $_POST['partner_web'] ) ? filter_var( wp_unslash( $_POST['partner_web'] ), FILTER_SANITIZE_URL ) : '';
		$partner_linkedin  = ! empty( $_POST['partner_linkedin'] ) ? filter_var( wp_unslash( $_POST['partner_linkedin'] ), FILTER_SANITIZE_URL ) : '';
		$partner_message   = ! empty( $_POST['partner_message'] ) ? filter_var( wp_unslash( $_POST['partner_message'] ), FILTER_SANITIZE_STRING ) : '';
		$course_id         = ! empty( $_POST['course_id'] ) ? filter_var( wp_unslash( $_POST['course_id'] ), FILTER_SANITIZE_NUMBER_INT ) : 0;
		$pipe              = Helper::is_basic_programiong( $course_id ) ? 2 : 1;

		$data = [
			'title'             => __( 'Нова заявка стати партнером', 'alevel' ),
			'partner_name'      => $partner_name,
			'first_name'        => $partner_name,
			'last_name'         => $partner_last_mane,
			'partner_last_mane' => $partner_last_mane,
			'email'             => $partner_email,
			'phone'             => $partner_phone,
			'company'           => $partner_company,
			'description'       => $partner_message,
			'partner_web'       => $partner_web,
			'linkedin'          => $partner_linkedin,
			'pipeline_id'       => $pipe,
			'form_name'         => 'register_on_partner',
		];

		$pipe     = new PipedriveApiCrm();
		$response = $pipe->create_lead( $data );

		if ( isset( $response['error'] ) ) {
			wp_send_json_error(
				[
					'message' => __( 'Виникла помилка спробуйте пізніше або зв\'яжіться з нами по емейлу', 'alevel' ),
					'error'   => $response['error'],
				]
			);
		}

		$data['form_name'] = 'tg_body_on_partner';
		self::send_to_tg_channel( $data, self::TG_CHAT_ID );
		AdminEmail::send_request_become_partners( $data );

		$review = Helper::get_partners();

		wp_send_json_success(
			[
				'review' => $review,
			]
		);
	}

	/**
	 * Payment form ajax handler.
	 *
	 * @return void
	 */
	public function payment_form(): void {
		$nonce = ! empty( $_POST['nonce'] ) ? filter_var( wp_unslash( $_POST['nonce'] ), FILTER_SANITIZE_STRING ) : false;
		if ( ! wp_verify_nonce( $nonce, self::COURSE_PAYMENT_FORM ) ) {
			wp_send_json_error( [ 'message' => __( 'Не врiрний одноразовий код', 'alevel' ) ] );
		}

		$payment_from_firs_name          = ! empty( $_POST['payment_from_firs_name'] ) ? filter_var( wp_unslash( $_POST['payment_from_firs_name'] ), FILTER_SANITIZE_STRING ) : '';
		$payment_from_last_name          = ! empty( $_POST['payment_from_last_name'] ) ? filter_var( wp_unslash( $_POST['payment_from_last_name'] ), FILTER_SANITIZE_STRING ) : '';
		$payment_from_phone              = ! empty( $_POST['payment_from_phone'] ) ? filter_var( wp_unslash( $_POST['payment_from_phone'] ), FILTER_SANITIZE_STRING ) : '';
		$payment_from_phone_country_code = ! empty( $_POST['payment_phone_country_code'] ) ? filter_var( wp_unslash( $_POST['payment_phone_country_code'] ), FILTER_SANITIZE_STRING ) : '';
		$payment_from_email              = ! empty( $_POST['payment_from_email'] ) ? filter_var( wp_unslash( $_POST['payment_from_email'] ), FILTER_SANITIZE_EMAIL ) : '';
		$method_payment                  = ! empty( $_POST['method_payment'] ) ? filter_var( wp_unslash( $_POST['method_payment'] ), FILTER_SANITIZE_STRING ) : '';
		$payment_from_count_payment      = ! empty( $_POST['payment_from_count_payment'] ) && '0' !== $_POST['payment_from_count_payment'] ? filter_var( wp_unslash( $_POST['payment_from_count_payment'] ), FILTER_SANITIZE_STRING ) : null;
		$course_name                     = ! empty( $_POST['course_name'] ) ? filter_var( wp_unslash( $_POST['course_name'] ), FILTER_SANITIZE_STRING ) : '';
		$course_id                       = ! empty( $_POST['course_id'] ) ? filter_var( wp_unslash( $_POST['course_id'] ), FILTER_SANITIZE_NUMBER_INT ) : 0;
		$pipe                            = Helper::is_basic_programiong( $course_id ) ? 2 : 1;

		$phone_util = PhoneNumberUtil::getInstance();

		$payment_from_phone_country_code = explode( '-', $payment_from_phone_country_code )[2];

		try {
			$swiss_number_proto = $phone_util->parse( $payment_from_phone, $payment_from_phone_country_code );
		} catch ( \libphonenumber\NumberParseException $e ) {
			wp_send_json_error(
				[
					'message'   => __( 'Введіть правильний номер телефону', 'alevel' ),
					'error_log' => $e->getMessage(),
					'error'     => true,
				]
			);
		}

		if ( ! $phone_util->isValidNumber( $swiss_number_proto ) ) {
			wp_send_json_error(
				[
					'message' => __( 'Введіть правильний номер телефону', 'alevel' ),
					'error'   => true,
				]
			);
		}

		$data = [
			'title'              => __( 'Запит на оплату курсу', 'alevel' ) . ': ' . $course_name,
			'first_name'         => $payment_from_firs_name,
			'last_name'          => $payment_from_last_name,
			'email'              => $payment_from_email,
			'phone'              => $payment_from_phone,
			'method_payment'     => $method_payment,
			'from_count_payment' => $payment_from_count_payment,
			'pipeline_id'        => $pipe,
			'form_name'          => 'course_payment_from',
		];

		$pipe     = new PipedriveApiCrm();
		$response = $pipe->create_lead( $data );

		if ( isset( $response['error'] ) ) {
			wp_send_json_error(
				[
					'message' => __( 'Виникла помилка спробуйте пізніше або зв\'яжіться з нами по емейлу', 'alevel' ),
					'error'   => $response['error'],
				]
			);
		}

		$data['form_name'] = 'tg_course_payment_from';
		self::send_to_tg_channel( $data, self::TG_CHAT_ID );
		AdminEmail::send_request_payment_course( $data );
		$event = Helper::get_event_to_madal_thnas();

		wp_send_json_success(
			[
				'event' => $event,
			]
		);

	}

	/**
	 * Review regular modal ajax.
	 *
	 * @return void
	 */
	public function review_regular_modal(): void {
		$nonce = ! empty( $_POST['nonce'] ) ? filter_var( wp_unslash( $_POST['nonce'] ), FILTER_SANITIZE_STRING ) : false;
		if ( ! wp_verify_nonce( $nonce, self::REVIEW_REGULAR_ACTION ) ) {
			wp_send_json_error( [ 'message' => __( 'Не врiрний одноразовий код', 'alevel' ) ] );
		}

		$review_id = ! empty( $_POST['review_id'] ) ? filter_var( wp_unslash( $_POST['review_id'] ), FILTER_SANITIZE_NUMBER_INT ) : null;

		if ( empty( $review_id ) ) {
			wp_send_json_error( [ 'message' => __( 'Не знайдено цей відгук', 'alevel' ) ] );
		}

		$current_blog_id = get_current_blog_id();
		if ( 1 !== $current_blog_id && is_multisite() ) {
			switch_to_blog( get_main_site_id() );
		}

		$arg = [
			'post_type'      => 'testimonials',
			'post__in'       => [ $review_id ],
			'posts_per_page' => 1,
			'post_status'    => 'publish',
		];

		$review_obj = new WP_Query( $arg );
		if ( $review_obj->have_posts() ) {
			ob_start();
			while ( $review_obj->have_posts() ) {
				$review_obj->the_post();

				$review_social = carbon_get_post_meta( $review_id, 'alv_social_media_reviewer' );
				$course_id     = carbon_get_post_meta( $review_id, 'alv_course_event' )[0]['id'] ?? 0;
				$course_icon   = carbon_get_post_meta( $course_id, 'alv_course_icon' );
				$teachers      = carbon_get_post_meta( $review_id, 'alv_reviewer_teachers' );
				?>
				<div class="modal-content">
					<div class="modal-header" style="background: linear-gradient(82.27deg, #FED68F 0%, #F2773A 100%);">
						<div class="dfr">
							<?php
							if ( has_post_thumbnail( $review_id ) ) {
								the_post_thumbnail( 'alv-teacher-avatar', [ 'class' => 'user' ] );
							} else {
								?>
								<img
										class="user"
										src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/thumbnail-mini-2.png' ); ?>"
										alt="No Avatar">
								<?php
							}
							?>
							<div class="dfc">
								<h5><?php the_title(); ?></h5>
								<?php if ( ! empty( $review_social ) ) { ?>
									<ul class="soc dfr">
										<?php foreach ( $review_social as $social ) { ?>
											<li>
												<a
														target="_blank"
														rel="nofollow"
														class="icon-<?php echo esc_attr( $social['social_name'] ); ?>"
														href="<?php echo esc_url( $social['social_link'] ); ?>"> </a>
											</li>
										<?php } ?>
									</ul>
								<?php } ?>
							</div>
						</div>
						<button class="close" type="button" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
					<div class="modal-body">
						<?php the_content(); ?>
						<hr class="sline">
						<ul>
							<li data-title="<?php esc_html_e( 'Курс:', 'alevel' ); ?>">
								<a href="<?php echo esc_url( get_the_permalink( $course_id ) ); ?>">
									<?php if ( ! empty( $course_icon ) ) { ?>
										<img
												src="<?php echo esc_url( $course_icon ); ?>"
												alt="<?php the_title(); ?> icon">
									<?php } else { ?>
										<img
												src="https://via.placeholder.com/80x80"
												alt="<?php the_title(); ?> no image">
									<?php } ?>
									<p><?php echo esc_html( get_the_title( $course_id ) ); ?></p>
								</a>
							</li>
							<?php if ( ! empty( $teachers ) ) { ?>
								<li data-title="<?php esc_html_e( 'Викладач:', 'alevel' ); ?>">
									<?php foreach ( $teachers as $teacher ) { ?>
										<div class="teacher">
											<a href="<?php echo esc_url( get_the_permalink( $teacher['id'] ) ); ?>">
												<?php
												if ( has_post_thumbnail( $teacher['id'] ) ) {
													echo wp_kses_post( get_the_post_thumbnail( $teacher['id'], 'alv-teacher-avatar' ) );
												} else {
													?>
													<img
															src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/thumbnail-mini-2.png' ); ?>"
															alt="No Avatar">
												<?php } ?>
												<p><?php echo esc_html( get_the_title( $teacher['id'] ) ); ?></p>
											</a>
										</div>
									<?php } ?>
								</li>
							<?php } ?>
						</ul>
						<a class="button" href="#" data-dismiss="modal" aria-hidden="true">
							<?php esc_html_e( 'Закрити', 'alevel' ); ?>
						</a>
					</div>
				</div>
				<?php
			}
			wp_reset_postdata();

			if ( is_multisite() ) {
				switch_to_blog( $current_blog_id );
			}

			wp_send_json_success( [ 'review' => ob_get_clean() ] );
		}

		if ( is_multisite() ) {
			switch_to_blog( $current_blog_id );
		}

		wp_send_json_error( [ 'message' => __( 'Не найден такой отзыв', 'alevel' ) ] );
	}

	/**
	 * Call back modal ajax.
	 *
	 * @return void
	 */
	public function call_back_modal(): void {
		$nonce = ! empty( $_POST['nonce'] ) ? filter_var( wp_unslash( $_POST['nonce'] ), FILTER_SANITIZE_STRING ) : false;
		if ( ! wp_verify_nonce( $nonce, self::CALL_BACK_MODAL_ACTION ) ) {
			wp_send_json_error( [ 'message' => __( 'Не врiрний одноразовий код', 'alevel' ) ] );
		}

		$name      = ! empty( $_POST['name'] ) ? filter_var( wp_unslash( $_POST['name'] ), FILTER_SANITIZE_STRING ) : null;
		$phone     = ! empty( $_POST['phone'] ) ? filter_var( wp_unslash( $_POST['phone'] ), FILTER_SANITIZE_STRING ) : null;
		$date      = ! empty( $_POST['date'] ) ? filter_var( wp_unslash( $_POST['date'] ), FILTER_SANITIZE_STRING ) : null;
		$time      = ! empty( $_POST['time'] ) ? filter_var( wp_unslash( $_POST['time'] ), FILTER_SANITIZE_STRING ) : null;
		$course_id = ! empty( $_POST['course_id'] ) ? filter_var( wp_unslash( $_POST['course_id'] ), FILTER_SANITIZE_NUMBER_INT ) : 0;
		$pipe      = Helper::is_basic_programiong( $course_id ) ? 2 : 1;

		$data_res = [
			'title'       => __( 'Зворотнiй дзвiнок', 'alevel' ) . ': ' . $name,
			'first_name'  => $name,
			'phone'       => $phone,
			'date'        => $date,
			'time'        => $time,
			'pipeline_id' => $pipe,
			'form_name'   => 'call_back_modal',
		];

		$pipe     = new PipedriveApiCrm();
		$response = $pipe->create_lead( $data_res );

		if ( isset( $response['error'] ) ) {
			wp_send_json_error(
				[
					'message' => __( 'Виникла помилка спробуйте пізніше або зв\'яжіться з нами по емейлу', 'alevel' ),
					'error'   => $response['error'],
				]
			);
		}

		$data_res['form_name'] = 'tg_call_back_modal';
		$tg_response           = self::send_to_tg_channel( $data_res, self::TG_CHAT_ID );
		AdminEmail::send_request_call_back( $data_res );

		$event = Helper::get_event_to_madal_thnas();

		wp_send_json_success( [ 'events' => $event ] );
	}

	/**
	 * Returns teachers course in the modal window to leave feedback
	 *
	 * @return void
	 */
	public function get_course_teachers_checkboxes(): void {
		$nonce = ! empty( $_POST['nonce'] ) ? filter_var( wp_unslash( $_POST['nonce'] ), FILTER_SANITIZE_STRING ) : false;
		if ( ! wp_verify_nonce( $nonce, self::GET_COURSE_TEACHERS ) ) {
			wp_send_json_error( [ 'message' => __( 'Не врiрний одноразовий код', 'alevel' ) ] );
		}

		$course_id = ! empty( $_POST['course_id'] ) ? filter_var( wp_unslash( $_POST['course_id'] ), FILTER_SANITIZE_NUMBER_INT ) : null;

		$current_blog_id = get_current_blog_id();
		if ( 1 !== $current_blog_id && is_multisite() ) {
			switch_to_blog( get_main_site_id() );
		}

		$teachers_ids = get_post_meta( $course_id, 'alv_course_teachers', true );

		ob_start();
		if ( ! empty( $teachers_ids ) ) {
			foreach ( $teachers_ids as $teacher ) {
				?>
				<li>
					<?php
					if ( has_post_thumbnail( $teacher ) ) {
						echo get_the_post_thumbnail( $teacher, 'alv-teacher-avatar' );
					} else {
						?>
						<img
								src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/thumbnail-mini-2.png' ); ?>"
								alt="No Avatar">
					<?php } ?>
					<div class="checkbox">
						<input
								id="teacher-<?php echo esc_attr( $teacher ); ?>"
								name="leave-review-teachers[]"
								value="<?php echo esc_attr( $teacher ); ?>" type="checkbox">
						<label
								class="icon-check"
								for="teacher-<?php echo esc_attr( $teacher ); ?>">
							<?php echo esc_html( get_the_title( $teacher ) ); ?>
						</label>
					</div>
				</li>
				<?php
			}
		} else {
			echo '<li>' . esc_html( __( 'Викладачі не вказані для цього курсу', 'alevel' ) ) . '</li>';
		}

		if ( is_multisite() ) {
			switch_to_blog( $current_blog_id );
		}

		wp_send_json_success( [ 'teachers' => ob_get_clean() ] );

	}
}
