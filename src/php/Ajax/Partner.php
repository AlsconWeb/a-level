<?php
/**
 * Partners ajax.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\Ajax;

/**
 * Partner class file.
 */
class Partner {
	/**
	 * Action and nonce partner page.
	 */
	public const PARTNER_TYPE_ACTION = 'alv_partner_type';

	/**
	 * Partner construct.
	 */
	public function __construct() {
		add_action( 'wp_ajax_' . self::PARTNER_TYPE_ACTION, [ $this, 'filter_partner_ajax' ] );
		add_action( 'wp_ajax_nopriv_' . self::PARTNER_TYPE_ACTION, [ $this, 'filter_partner_ajax' ] );
	}

	/**
	 * Partner type ajax.
	 *
	 * @return void
	 */
	public function filter_partner_ajax(): void {
		$nonce = ! empty( $_POST['nonce'] ) ? filter_var( wp_unslash( $_POST['nonce'] ), FILTER_SANITIZE_STRING ) : false;

		if ( ! wp_verify_nonce( $nonce, self::PARTNER_TYPE_ACTION ) ) {
			wp_send_json_error( [ 'message' => __( 'Не врiрний одноразовий код', 'alevel' ) ] );
		}

		$partner_type = ! empty( $_POST['partner_type'] ) ? filter_var( wp_unslash( $_POST['partner_type'] ), FILTER_SANITIZE_STRING ) : null;

		wp_send_json_success(
			[
				'params' => http_build_query(
					[
						'type' => $partner_type,
					],
				),
			]
		);
	}
}

