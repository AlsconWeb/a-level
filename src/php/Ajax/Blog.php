<?php
/**
 * Ajax in blog page.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\Ajax;

use WP_Query;

/**
 * Blog class file.
 */
class Blog {

	public const CATEGORY_ACTION = 'alv_get_category_action';

	/**
	 * Blog construct.
	 */
	public function __construct() {
		add_action( 'wp_ajax_' . self::CATEGORY_ACTION, [ $this, 'alev_get_category' ] );
		add_action( 'wp_ajax_nopriv_' . self::CATEGORY_ACTION, [ $this, 'alev_get_category' ] );
	}

	/**
	 * Get category ajax handler.
	 *
	 * @return void
	 */
	public function alev_get_category(): void {
		$nonce = ! empty( $_POST['nonce'] ) ? filter_var( wp_unslash( $_POST['nonce'] ), FILTER_SANITIZE_STRING ) : false;

		if ( ! wp_verify_nonce( $nonce, self::CATEGORY_ACTION ) ) {
			wp_send_json_error( [ 'message' => __( 'Не врiрний одноразовий код', 'alevel' ) ] );
		}

		$category_slug = ! empty( $_POST['category_slug'] ) ? filter_var( wp_unslash( $_POST['category_slug'] ), FILTER_SANITIZE_STRING ) : null;
		$teacher_id    = ! empty( $_POST['teacher_id'] ) ? filter_var( wp_unslash( $_POST['teacher_id'] ), FILTER_SANITIZE_NUMBER_INT ) : null;

		if ( 'all' === $category_slug ) {
			$arg = [
				'post_type'      => 'post',
				'post_status'    => 'publish',
				'posts_per_page' => 12,
			];
		} else {
			$arg = [
				'post_type'      => 'post',
				'post_status'    => 'publish',
				'posts_per_page' => 12,
				'category_name'  => $category_slug,
			];
		}

		if ( 'history' === $category_slug ) {
			$arg = [
				'post_type'      => 'testimonials',
				'post_status'    => 'publish',
				'posts_per_page' => 12,
				'meta_query'     => [
					[
						'key'   => '_alv_type_review',
						'value' => 'history',
					],
				],
			];
		}

		$blog_post_object = new WP_Query( $arg );

		ob_start();
		if ( $blog_post_object->have_posts() ) {
			while ( $blog_post_object->have_posts() ) {
				$blog_post_object->the_post();
				$article_id = get_the_ID();
				get_template_part( 'template-parts/blog', 'item', [ 'article_id' => $article_id ] );
			}
			wp_reset_postdata();
		} else {
			echo '<h3 class="no-blog-post">' . esc_html( __( 'Немає відгуків про цей курс', 'alevel' ) ) . '</h3>';
		}

		$blog_posts = ob_get_clean();

		wp_send_json_success( [ 'posts' => $blog_posts ] );

	}
}
