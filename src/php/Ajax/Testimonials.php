<?php
/**
 * Testimonials ajax.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\Ajax;

/**
 * Testimonials class file.
 */
class Testimonials {
	/**
	 * Testimonials action name.
	 */
	public const TESTIMONIALS_CATEGORY_ACTION = 'alv_get_testimonials_category_action';

	/**
	 * Testimonials construct.
	 */
	public function __construct() {
		add_action( 'wp_ajax_' . self::TESTIMONIALS_CATEGORY_ACTION, [ $this, 'alv_get_category_testimonials' ] );
		add_action(
			'wp_ajax_nopriv_' . self::TESTIMONIALS_CATEGORY_ACTION,
			[
				$this,
				'alv_get_category_testimonials',
			]
		);

		add_action( 'pre_get_posts', [ $this, 'filter_archive_testimonials' ] );
	}

	/**
	 * Get testimonials type.
	 *
	 * @return array
	 */
	public static function get_testimonials_type(): array {
		return [
			'all'     => __( 'Усi вiдгуки', 'alevel' ),
			'regular' => __( 'Вiдгуки студентiв', 'alevel' ),
			'firm'    => __( 'Вiдгуки компанiй', 'alevel' ),
			'history' => __( 'Iсторії успіху', 'alevel' ),
		];
	}

	/**
	 * Filter testimonials by course.
	 *
	 * @return void
	 */
	public function alv_get_category_testimonials(): void {
		$nonce = ! empty( $_POST['nonce'] ) ? filter_var( wp_unslash( $_POST['nonce'] ), FILTER_SANITIZE_STRING ) : false;

		if ( ! wp_verify_nonce( $nonce, self::TESTIMONIALS_CATEGORY_ACTION ) ) {
			wp_send_json_error( [ 'message' => __( 'Не врiрний одноразовий код', 'alevel' ) ] );
		}

		$course_id   = ! empty( $_POST['courses_id'] ) ? filter_var( wp_unslash( $_POST['courses_id'] ), FILTER_SANITIZE_NUMBER_INT ) : 0;
		$course_type = ! empty( $_POST['course_type'] ) ? filter_var( wp_unslash( $_POST['course_type'] ), FILTER_SANITIZE_STRING ) : null;

		wp_send_json_success(
			[
				'params' => http_build_query(
					[
						'type'      => $course_type,
						'course_id' => $course_id,
					],
				),
			]
		);
	}

	/**
	 * Add testimonials filter filter.
	 *
	 * @param \WP_Query $query
	 *
	 * @return void
	 */
	public function filter_archive_testimonials( \WP_Query $query ): void {
		// phpcs:disable
		// Processing form data without nonce verification.
		if ( $query->is_post_type_archive( 'testimonials' ) && isset( $_GET['type'] ) && $query->is_main_query() ) {

			$type      = ! empty( $_GET['type'] ) ? filter_var( wp_unslash( $_GET['type'] ), FILTER_SANITIZE_STRING ) : 0;
			$course_id = ! empty( $_GET['course_id'] ) ? filter_var( wp_unslash( $_GET['course_id'] ), FILTER_SANITIZE_NUMBER_INT ) : 0;
			$paged     = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

			if ( 0 !== $type || 0 !== $course_id ) {

				if ( 'all' !== $type ) {
					$type_array = [
						'key'   => '_alv_type_review',
						'value' => $type,
					];
				}

				if ( 'all' === $type ) {

					if ( 0 === $course_id ) {
						$type_array = [
							'key'   => '_alv_type_review',
							'value' => [ 'regular', 'history' ],
						];
					}
				}

				if ( 0 !== $course_id && 'firm' !== $type ) {
					$course = [
						[
							'key'   => '_alv_course_event|||0|id',
							'value' => $course_id,
						],
					];
				}

				if ( 'firm' === $type ) {
					$type_firm = [
						'key'     => '_alv_company_no_review',
						'value'   => 'true',
						'compare' => '!=',
					];
				}

				$arg = [
					! empty( $type_array ) ? $type_array : null,
					! empty( $course ) ? $course : null,
					! empty( $type_firm ) ? $type_firm : null,
				];

				if ( 'all' === $type ) {
					$arg['relation'] = 'OR';
				}

				$current_blog_id = get_current_blog_id();
				if ( 1 !== $current_blog_id && is_multisite() ) {
					switch_to_blog( get_main_site_id() );
				}

				$query->set(
					'meta_query',
					$arg
				);

				$max_page_query = new \WP_Query( $query->query_vars );

				if ( $max_page_query->max_num_pages < $paged ) {
					$query->set(
						'paged',
						1
					);
				}

				if ( is_multisite() ) {
					switch_to_blog( $current_blog_id );
				}
			}
		}
		// phpcs:enable
	}

}
