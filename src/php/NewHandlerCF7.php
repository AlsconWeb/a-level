<?php
/**
 * Intercept event handler contact form 7.
 *
 * @package iwpdev/alevel
 */

namespace Alevel;

use Alevel\Ajax\ModalHandler;
use Alevel\Pipedrive\API\PipedriveApiCrm;
use WPCF7_ContactForm;
use WPCF7_Submission;

/**
 * NewHandlerCF7 class file.
 */
class NewHandlerCF7 {
	/**
	 * NewHandlerCF7 construct.
	 */
	public function __construct() {
		add_action( 'wpcf7_before_send_mail', [ $this, 'before_send_email' ] );
	}

	/**
	 * Before send email from contact form 7.
	 *
	 * @param WPCF7_ContactForm $wpcf7 CF7 class.
	 *
	 * @return void
	 */
	public function before_send_email( WPCF7_ContactForm $wpcf7 ): void {
		$submission = WPCF7_Submission::get_instance();
		if ( 331 === $wpcf7->id() ) {
			$posted_data = $submission->get_posted_data();

			if ( empty( $posted_data ) ) {
				return;
			}

			$data_res = [
				'title'       => __( 'Зворотнiй зв’язок', 'alevel' ),
				'first_name'  => $posted_data['name-user'] ?? '',
				'phone'       => $posted_data['tel'] ?? '',
				'email'       => $posted_data['email'] ?? '',
				'description' => $posted_data['message'] ?? '',
				'pipeline_id' => 1,
				'form_name'   => 'call_back_form',
			];

			$pipe     = new PipedriveApiCrm();
			$response = $pipe->create_lead( $data_res );

			if ( isset( $response['error'] ) ) {
				wp_send_json_error(
					[
						'message' => __( 'Виникла помилка спробуйте пізніше або зв\'яжіться з нами по емейлу', 'alevel' ),
						'error'   => $response['error'],
					]
				);
			}

			$data_res['form_name'] = 'tg_call_back_form';
			$tg_response           = ModalHandler::send_to_tg_channel( $data_res, ModalHandler::TG_CHAT_ID );
		}

	}
}
