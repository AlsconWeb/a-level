<?php
/**
 * Add pipedrive api crm.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\Pipedrive\API;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ClientException;
use Pipedrive\APIHelper;
use Pipedrive\Client;
use Pipedrive\Configuration;
use Pipedrive\Controllers\OrganizationsController;

/**
 * PipedriveApiCrm class file.
 */
class PipedriveApiCrm {

	/**
	 * Pipedrive client for PHP based apps
	 *
	 * @var Client $client Client Pipedrive Api.
	 */
	private $client;

	/**
	 * GuzzleHttp Clients.
	 *
	 * @var HttpClient $http_client
	 */
	private $http_client;

	/**
	 * Multisite Name.
	 */
	private const SITE_NAMES = [
		'1'  => '31',
		'2'  => '28',
		'3'  => '29',
		'4'  => '32',
		'5'  => '33',
		'6'  => '30',
		'7'  => '36',
		'8'  => '34',
		'9'  => '37',
		'10' => '35',
	];


	/**
	 * PipedriveApiCrm construct.
	 *
	 * @todo Нужно сделать проверку. на пустое апи.
	 */
	public function __construct() {
		$this->client      = new Client( null, null, null, $this->get_api_key() );
		$this->http_client = new HttpClient();
	}

	/**
	 * Get Pipedrive api key.
	 *
	 * @return string
	 */
	private function get_api_key(): string {
		$api_key = carbon_get_network_option( 1, 'alv_pipedrive_api_key', 'carbon_fields_container_crb_network_container' );

		if ( ! empty( $api_key ) ) {
			return $api_key;
		}

		return '';
	}

	/**
	 * Create lead to pipedrive crm
	 *
	 * @param array $data Data form.
	 *
	 * @return array
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public function create_lead( array $data ) {
		$new_user = new \Pipedrive\Controllers\PersonsController();

		$query_builder = '/leads';
		$query_url     = APIHelper::cleanUrl( Configuration::getBaseUri() . $query_builder );
		$org_id        = 0;
		if ( ! isset( $data['last_name'] ) ) {
			$data['last_name'] = '';
		}

		$name = $data['first_name'] . ' ' . $data['last_name'];

		if ( 'register_on_partner' === $data['form_name'] ) {
			$organization = new OrganizationsController();
			$org_res      = $organization->addAnOrganization(
				[
					'name' => $data['company'],
				]
			);

			if ( $org_res->success ) {
				$person_data['org_id'] = $org_res->data->id;
			}
		}

		$person_data = [
			'name'  => $name,
			'phone' => $data['phone'],
			'email' => $data['email'],
		];

		$responce = $new_user->addAPerson(
			$person_data
		);

		if ( $responce->success ) {
			$id                = $responce->data->id;
			$data              = $this->generate_body_message( $data );
			$data['person_id'] = $id;
			try {
				$res = $this->http_client->request(
					'POST',
					$query_url,
					[
						'json' => $data,
					]
				);
				if ( 201 === $res->getStatusCode() ) {
					return [ 'success' => true ];
				}

				return [ 'error' => $res->getBody() ];
			} catch ( ClientException $e ) {
				return [ 'error' => $e->getMessage() ];
			}
		}
	}

	/**
	 * Generate body message.
	 *
	 * @param array $data form data.
	 *
	 * @return string|array
	 */
	public function generate_body_message( array $data ) {
		$site_name = is_multisite() ? get_blog_details()->blogname : '';
		$site_id   = get_current_blog_id();

		switch ( $data['form_name'] ) {
			case 'new_teacher':
				$body = [
					'title'                                    => __( 'Стати вчителем', 'alevel' ),
					'5609685eb8a79a42c7c3a264f5a7ea723d5a8b0d' => $data['company'] ?? '',
					'0a4ff31aa6fbfcfd50383479fa66765c4d59f003' => $data['position'] ?? '',
					'bdab3c0686d0d4482c6411ac79d383845450fa16' => $data['linkedin'] ?? '',
					'af3a0dd5eb32f7c851bdb3d54cd42fa5712a08ca' => $data['description'] ?? '',
					'2b3dd2a660ac617b43db91c334b9a17c772d08b6' => self::SITE_NAMES[ $site_id ],
				];
				break;
			case 'consultation_on_course':
				$body = [
					'title'                                    => __( 'Консультація на курс:', 'alevel' ) . ' ' . $data['course_name'],
					'02588238debfdf17c74ab4d9c5a11723fd3ffd10' => $data['course_name'] ?? '',
					'af3a0dd5eb32f7c851bdb3d54cd42fa5712a08ca' => $data['description'] ?? '',
					'2b3dd2a660ac617b43db91c334b9a17c772d08b6' => self::SITE_NAMES[ $site_id ],
				];
				break;
			case 'register_on_partner':
				$body = [
					'title'                                    => __( 'Стати партнером', 'alevel' ),
					'5609685eb8a79a42c7c3a264f5a7ea723d5a8b0d' => $data['company'] ?? '',
					'c72fc7ed987bd47d9506c2f621734913d7cd4dbe' => $data['partner_web'] ?? '',
					'bdab3c0686d0d4482c6411ac79d383845450fa16' => $data['linkedin'] ?? '',
					'd21ba247b768bc5f37f60c56b64afb6c0a50782c' => $data['description'] ?? '',
					'2b3dd2a660ac617b43db91c334b9a17c772d08b6' => self::SITE_NAMES[ $site_id ],
				];

				break;
			case 'tg_body_theaters':
				$full_name = $data['first_name'] . ' ' . $data['last_name'] ?? '';

				$body = '*' . $data['title'] . ' ' . $data['course_name'] . '*' . "\n";

				$body .= '*' . __( 'Заявка із сайту:', 'alevel' ) . '* ' . $site_name . "\n";
				$body .= '*' . __( 'Назва компанії:', 'alevel' ) . '* ' . $data['company'] . "\n";
				$body .= '*' . __( 'Ім\'я та прізвище:', 'alevel' ) . '* ' . $full_name . "\n";
				$body .= '*' . __( 'Email:', 'alevel' ) . '* ' . $data['email'] . "\n";
				$body .= '*' . __( 'Телефон:', 'alevel' ) . '* ' . $data['phone'] . "\n";
				$body .= '*' . __( 'Посада в компанії:', 'alevel' ) . '* ' . $data['position'] . "\n";
				$body .= '*' . __( 'Посилання на linkedin профіль:', 'alevel' ) . '* ' . $data['linkedin'] . "\n";
				$body .= '*' . __( 'Супровідне повідомлення:', 'alevel' ) . '* ' . $data['description'];
				break;
			case 'tg_body_course':
				$full_name = $data['first_name'];

				$body = '*' . $data['title'] . ' ' . $data['course_name'] . '*' . "\n";

				$body .= '*' . __( 'Заявка із сайту:', 'alevel' ) . '* ' . $site_name . "\n";
				$body .= '*' . __( 'Ім\'я та прізвище:', 'alevel' ) . '* ' . $full_name . "\n";
				$body .= '*' . __( 'Email:', 'alevel' ) . '* ' . $data['email'] . "\n";
				$body .= '*' . __( 'Телефон:', 'alevel' ) . '* ' . $data['phone'] . "\n";
				$body .= '*' . __( 'Супровідне повідомлення:', 'alevel' ) . '* ' . $data['description'];
				break;
			case 'tg_body_on_partner':
				$name = $data['first_name'];

				$body = '*' . __( 'Назва компанії:', 'alevel' ) . '* ' . $data['company'] . "\n";

				$body .= '*' . __( 'Заявка із сайту:', 'alevel' ) . '* ' . $site_name . "\n";
				$body .= '*' . __( 'Ім\'я та прізвище:', 'alevel' ) . '* ' . $name . "\n";
				$body .= '*' . __( 'Прiзвище представника:', 'alevel' ) . '* ' . $data['partner_last_mane'] . "\n";
				$body .= '*' . __( 'Email:', 'alevel' ) . '* ' . $data['email'] . "\n";
				$body .= '*' . __( 'Телефон:', 'alevel' ) . '* ' . $data['phone'] . "\n";
				$body .= '*' . __( 'Веб сайт компанії:', 'alevel' ) . '* ' . $data['partner_web'] . "\n";
				$body .= '*' . __( 'Посилання на linkedin профіль:', 'alevel' ) . '* ' . $data['linkedin'] . "\n";
				$body .= '*' . __( 'Супровідне повідомлення:', 'alevel' ) . '* ' . $data['description'] . "\n";
				break;
			case 'course_payment_from':
				$body = [
					'title'                                    => $data['title'],
					'c0446871da5d0219129c9474bff5280ce0f64ebd' => $data['method_payment'] ?? '',
					'8c6745bf265640085c332cccfc25bd19c8f15103' => $data['from_count_payment'] ?? '',
					'2b3dd2a660ac617b43db91c334b9a17c772d08b6' => self::SITE_NAMES[ $site_id ],
				];
				break;
			case 'tg_course_payment_from':
				$name = $data['first_name'] . ' ' . $data['last_name'];

				$body = '*' . $data['title'] . '*' . "\n";

				$body .= '*' . __( 'Заявка із сайту:', 'alevel' ) . '* ' . $site_name . "\n";
				$body .= '*' . __( 'Ім\'я та прізвище:', 'alevel' ) . '*' . $name . "\n";
				$body .= '*' . __( 'Email: ', 'alevel' ) . '*' . $data['email'] . "\n";
				$body .= '*' . __( 'Телефон:', 'alevel' ) . '*' . $data['phone'] . "\n";
				$body .= '*' . __( 'Тип оплати за курс:', 'alevel' ) . '*' . $data['method_payment'] . "\n";
				if ( ! empty( $data['from_count_payment'] ) ) {
					$body .= '*' . __( 'Кількість місяців на розстрочку моно банку:', 'alevel' ) . '*' . $data['from_count_payment'] . "\n";
				}
				break;

			case 'call_back_modal':
				$body = [
					'title'                                    => $data['title'],
					'537c9af8c7b28f2fc38605fe8763beaf43f3aab0' => $data['date'] . ' ' . $data['time'],
					'2b3dd2a660ac617b43db91c334b9a17c772d08b6' => self::SITE_NAMES[ $site_id ],
				];

				break;

			case 'tg_call_back_modal':
				$body = $data['title'] . "\n";

				$body .= '*' . __( 'Заявка із сайту:', 'alevel' ) . '* ' . $site_name . "\n";
				$body .= '*' . __( 'Ім\'я та прізвище:', 'alevel' ) . '* ' . $data['first_name'] . "\n";
				$body .= '*' . __( 'Телефон:', 'alevel' ) . '* ' . $data['phone'] . "\n";
				$body .= '*' . __( 'Дата для дзвінка:', 'alevel' ) . '* ' . $data['date'] . "\n";
				$body .= '*' . __( 'Час:', 'alevel' ) . '* ' . $data['time'] . "\n";
				break;

			case 'call_back_form':
				$body = [
					'title'                                    => $data['title'],
					'af3a0dd5eb32f7c851bdb3d54cd42fa5712a08ca' => $data['description'] ?? '',
					'2b3dd2a660ac617b43db91c334b9a17c772d08b6' => self::SITE_NAMES[ $site_id ],
				];
				break;
			case 'tg_call_back_form':
				$body = $data['title'] . "\n";

				$body .= '*' . __( 'Заявка із сайту:', 'alevel' ) . '* ' . $site_name . "\n";
				$body .= '*' . __( 'Ім\'я та прізвище:', 'alevel' ) . '* ' . $data['first_name'] . "\n";
				$body .= '*' . __( 'Телефон:', 'alevel' ) . '* ' . $data['phone'] . "\n";
				$body .= '*' . __( 'Email:', 'alevel' ) . '* ' . $data['email'] . "\n";
				$body .= '*' . __( 'Супровідне повідомлення:', 'alevel' ) . '* ' . $data['description'] . "\n";
				break;
			case 'review_form':

				foreach ( $data['teachers'] as $teacher ) {
					$teachers_name[] = __( 'Викладач:', 'alevel' ) . get_the_title( $teacher ) . "\n";
				}

				$body = [
					'title'                                    => $data['title'],
					'bdab3c0686d0d4482c6411ac79d383845450fa16' => implode( ', ', $data['social'] ) ?? '',
					'0627024f77527302784047c7bfaab81cea39a00b' => get_the_permalink( $data['avatar_id'] ) ?? '',
					'693a679eb3a61dca49f443fcf20aedce2b920abf' => implode( ',', $teachers_name ) ?? '',
					'af3a0dd5eb32f7c851bdb3d54cd42fa5712a08ca' => $data['content'] ?? '',
					'c284c44ccb965b76f2671f0982b0bd27006d60f9' => $data['rating'] ?? '',
					'2b3dd2a660ac617b43db91c334b9a17c772d08b6' => self::SITE_NAMES[ $site_id ],
				];
				break;
			default:
				$body = '';

		}

		return $body;
	}
}
