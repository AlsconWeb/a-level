<?php
/**
 * TeachersCourse template.
 *
 * @package iwpdev/alevel
 */

global $post;

use Alevel\Helpers\Helper;

$teachers           = get_post_meta( $post->ID, 'alv_course_teachers', true );
$all_teachers_count = 0;
$css_class          = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$where_use          = ! empty( $atts['where_use'] ) ? $atts['where_use'] : 'true';
$teacher_paged      = get_query_var( 'paged' ) ?: 1;

$current_blog_id = get_current_blog_id();

if ( 1 !== $current_blog_id && is_multisite() ) {
	switch_to_blog( get_main_site_id() );
	$main_course_id = Helper::get_post_id_in_main_site( $post->post_name, $post->post_type );
} else {
	$main_course_id = $post->ID;
}

if ( 'true' === $where_use ) {
	$arg = [
		'post_type'   => 'teachers',
		'post_status' => 'publish',
		'paged'       => $teacher_paged,
		'meta_query'  => [
			[
				'key'         => '^(_alv_teacher_course\|...*id)',
				'value'       => $main_course_id,
				'compare_key' => 'REGEXP',
				'type'        => 'NUMERIC',
				'compare'     => '=',
			],
		],
	];
} else {
	$arg = [
		'post_type'      => 'teachers',
		'post_status'    => 'publish',
		'paged'          => $teacher_paged,
		'posts_per_page' => $atts['count_output'],
		'order'          => 'DESC',
		'orderby'        => 'rand',
	];
}

$teachers_object = new WP_Query( $arg );
?>
	<div class="teachers dfr <?php echo esc_attr( $css_class ?? '' ); ?>">
		<?php
		$all_teachers_count = $teachers_object->post_count;
		if ( $teachers_object->have_posts() ) {

			while ( $teachers_object->have_posts() ) {
				$teachers_object->the_post();

				$teacher_id = get_the_ID();
				get_template_part(
					'template-parts/teacher',
					'cart',
					[
						'teacher_id' => $teacher_id,
						'course_id'  => $main_course_id,
					]
				);
			}
			wp_reset_postdata();
		} else {
			echo '<h3>' . esc_html( __( 'Педагоги не призначені для цього курсу', 'alevel' ) ) . '</h3>';
		}
		?>
		<?php if ( 'true' === $where_use ) { ?>
			<a class="button" data-toggle="modal" data-target=".become-teacher" href="#">
				<i class="icon-plus"><?php echo esc_html( $atts['text_button'] ?? '' ); ?></i>
			</a>
		<?php } ?>
	</div>
<?php
/**
 * @todo Нужно реализовать пагинацию аяксом когда заполнят
 */

if ( $all_teachers_count > $atts['count_output'] ?? 5 ) {
	?>
	<div class="page_navigation_wrapper">
		<?php
		if ( function_exists( 'wp_pagenavi' ) ) {
			wp_pagenavi( [ 'query' => $teachers_object ] );
		}
		?>
	</div>
	<?php
}
if ( is_multisite() ) {
	switch_to_blog( $current_blog_id );
}
