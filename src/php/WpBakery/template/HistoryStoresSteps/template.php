<?php
/**
 * HistoryStoresSteps template.
 *
 * @package iwpdev/alevel
 */

$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$items     = vc_param_group_parse_atts( $atts['steps'] );
?>
<div class="history-story-steps <?php echo esc_attr( $css_class ); ?>">
	<?php
	if ( ! empty( $items ) ) {
		foreach ( $items as $item ) {
			$image_url = wp_get_attachment_image_url( $item['icon'], 'full' );
			?>
			<div class="history-story dfr">
				<img
						src="<?php echo esc_url( $image_url ); ?>"
						alt="<?php echo esc_html( get_the_title( $item['icon'] ) ); ?>">
				<div class="desc">
					<h3><?php echo esc_html( $item['title'] ); ?></h3>
					<p><?php echo esc_html( $item['description'] ); ?></p>
				</div>
			</div>
			<?php
		}
	}
	?>
</div>
