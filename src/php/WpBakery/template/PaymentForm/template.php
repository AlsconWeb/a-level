<?php
/**
 * Payment Form template.
 *
 * @package iwpdev/alevel
 */

use Alevel\Ajax\ModalHandler;

$css_class                = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$form_type                = $atts['form_type'] ?? null;
$course_date_start        = carbon_get_the_post_meta( 'alv_course_start' );
$time_input               = strtotime( $course_date_start );
$course_available         = carbon_get_the_post_meta( 'alv_course_available_seats' ) ?? '';
$course_date_end_discount = carbon_get_the_post_meta( 'alv_course_end_date_discount' );
$time_input_end_discount  = strtotime( $course_date_end_discount );
$course_title             = get_the_title( get_the_ID() );

?>
<div class="form-wrapper <?php echo esc_attr( $css_class ?? '' ); ?>">
	<?php if ( 'part_payment' === $form_type ) { ?>
		<div class="dfr">
			<div class="description" id="payment-block">
				<h2><?php echo esc_html( $atts['course_one_payment_title'] ?? '' ); ?></h2>
				<h3><?php echo esc_html( $course_title ); ?></h3>
				<hr class="sline mini">
				<ul class="desc">
					<li>
						<?php esc_html_e( 'Початок курсу:', 'alevel' ); ?>
						<span><?php echo esc_html( date_i18n( 'j F', $time_input ) ); ?></span>
					</li>
					<li>
						<?php
						esc_html_e( 'Залишилось:', 'alevel' );

						echo sprintf(
							'<span>%d %s</span>',
							esc_attr( $course_available ),
							esc_html__( 'мiсць', 'alevel' )
						);
						?>
					</li>
				</ul>
				<?php
				echo wp_kses_post( wpautop( $atts['course_one_payment_description'] ) );
				$course_one_payment_description = vc_param_group_parse_atts( $atts['course_ona_payment_description_list'] );

				if ( ! empty( $course_one_payment_description ) ) {
					?>
					<ul class="cost">
						<?php foreach ( $course_one_payment_description as $item ) { ?>
							<li><?php echo wp_kses_post( $item['text'] ); ?></li>
						<?php } ?>
					</ul>
				<?php } ?>
				<?php if ( ! empty( $atts['course_sales_prise'] ) || ! empty( $atts['course_prise'] ) ) { ?>
					<hr class="sline">
					<h3><?php echo esc_html( $atts['sales_title'] ?? '' ); ?></h3>
					<p><?php echo esc_html( $atts['sales_sub_title'] ?? '' ); ?></p>
					<p class="price">
						<?php echo wp_kses_post( $atts['course_prise'] ); ?>
						<span class="sales"><?php echo wp_kses_post( $atts['course_sales_prise'] ); ?></span>
					</p>
					<?php if ( ! empty( $atts['course_sales_banner_title'] ) ) { ?>
						<div class="discount icon-chart"
							 style="background: linear-gradient(82.27deg, #F5CD72 0%, #2C7CD8 100%);">
							<h5><?php echo esc_html( $atts['course_sales_banner_title'] ); ?></h5>
							<p>
								<?php echo esc_html( $atts['course_sales_banner_text'] ); ?>
								<span
										class="countdown_timer"
										data-start="<?php echo esc_attr( gmdate( 'M d, Y H:i:s', $time_input_end_discount ) ); ?>">
								</span>
							</p>
						</div>
					<?php } ?>
				<?php } ?>
			</div>
			<div class="description" id="installment-block">
				<h2><?php echo esc_html( $atts['course_school_installment_title'] ?? '' ); ?></h2>
				<h3><?php echo esc_html( $course_title ); ?></h3>
				<hr class="sline mini">
				<ul class="desc">
					<li>
						<?php esc_html_e( 'Початок курсу:', 'alevel' ); ?>
						<span><?php echo esc_html( date_i18n( 'j F', $time_input ) ); ?></span>
					</li>
					<li>
						<?php
						esc_html_e( 'Залишилось:', 'alevel' );

						echo sprintf(
							'<span>%d %s</span>',
							esc_attr( $course_available ),
							esc_html__( 'мiсць', 'alevel' )
						);
						?>
					</li>
				</ul>
				<?php
				echo wp_kses_post( wpautop( $atts['course_school_installment_description'] ?? '' ) );
				?>
				<ul class="payments">
					<li><?php echo wp_kses_post( $atts['course_school_installment_one_payment'] ?? '' ); ?></li>
					<li><?php echo wp_kses_post( $atts['course_school_installment_second_payment'] ?? '' ); ?></li>
				</ul>
				<?php if ( ! empty( $atts['course_sales_school'] ) || ! empty( $atts['course_prise_school'] ) ) { ?>
					<hr class="sline">
					<h3><?php echo esc_html( $atts['sales_title'] ?? '' ); ?></h3>
					<p><?php echo esc_html( $atts['sales_sub_title'] ?? '' ); ?></p>
					<p class="price">
						<?php echo wp_kses_post( $atts['course_prise_school'] ); ?>
						<span class="sales"><?php echo wp_kses_post( $atts['course_sales_school'] ); ?></span>
					</p>
					<?php if ( empty( $course_date_end_discount ) ) { ?>
						<p class="price full-price">
							<?php echo wp_kses_post( $atts['course_sales_school'] ); ?>
						</p>
					<?php } ?>
					<?php if ( ! empty( $atts['course_sales_banner_title'] ) ) { ?>
						<div class="discount icon-chart"
							 style="background: linear-gradient(82.27deg, #97F6C3 0%, #113D67 100%);">
							<h5><?php echo esc_html( $atts['course_sales_banner_title'] ); ?></h5>
							<p>
								<?php echo esc_html( $atts['course_sales_banner_text'] ); ?>
								<span
										class="countdown_timer"
										data-start="<?php echo esc_attr( gmdate( 'M d, Y H:i:s', $time_input_end_discount ) ); ?>">
								</span>
							</p>
						</div>
					<?php } ?>
				<?php } ?>
			</div>
			<div class="description" id="mono-block">
				<h2><?php echo esc_html( $atts['course_mono_title'] ?? '' ); ?></h2>
				<h3><?php echo esc_html( $course_title ); ?></h3>
				<hr class="sline mini">
				<ul class="desc">
					<li>
						<?php esc_html_e( 'Початок курсу:', 'alevel' ); ?>
						<span><?php echo esc_html( date_i18n( 'j F', $time_input ) ); ?></span>
					</li>
					<li>
						<?php
						esc_html_e( 'Залишилось:', 'alevel' );

						echo sprintf(
							'<span>%d %s</span>',
							esc_attr( $course_available ),
							esc_html__( 'мiсць', 'alevel' )
						);
						?>
					</li>
				</ul>
				<?php
				echo wp_kses_post( wpautop( $atts['course_mono_description'] ?? '' ) );
				?>
				<?php if ( ! empty( $atts['course_sales_mono'] ) ) { ?>
					<hr class="sline">
					<h3><?php echo esc_html( $atts['sales_title'] ?? '' ); ?></h3>
					<p><?php echo esc_html( $atts['sales_sub_title'] ?? '' ); ?></p>
					<p class="price">
						<?php echo wp_kses_post( $atts['course_prise_mono'] ); ?>
						<span class="sales"><?php echo wp_kses_post( $atts['course_sales_mono'] ); ?></span>
					</p>
					<?php if ( empty( $course_date_end_discount ) ) { ?>
						<p class="price full-price">
							<?php echo wp_kses_post( $atts['course_sales_mono'] ); ?>
						</p>
					<?php } ?>
					<?php if ( ! empty( $atts['course_sales_banner_title'] ) ) { ?>
						<div class="discount icon-chart"
							 style="background: linear-gradient(82.27deg, #97F6C3 0%, #113D67 100%);">
							<h5><?php echo esc_html( $atts['course_sales_banner_title'] ); ?></h5>
							<p>
								<?php echo esc_html( $atts['course_sales_banner_text'] ); ?>
								<span
										class="countdown_timer"
										data-start="<?php echo esc_attr( gmdate( 'M d, Y H:i:s', $time_input_end_discount ) ); ?>">
								</span>
							</p>
						</div>
					<?php } ?>
				<?php } ?>
			</div>
			<form id="payment-form" method="post">
				<h3><?php esc_html_e( 'Крок 1. Виберiть спосiб оплати', 'alevel' ); ?></h3>
				<div class="radio-buttons dfr">
					<div class="radio-button">
						<input
								id="payment"
								type="radio"
								value="<?php esc_html_e( 'Сплата за весь курс', 'alevel' ); ?>"
								name="method_payment"
								checked>
						<label class="icon-check" for="payment"></label>
						<h4><?php esc_html_e( 'Сплата за весь курс', 'alevel' ); ?></h4>
						<?php echo wp_kses_post( wpautop( $atts['course_one_payment_text'] ?? '' ) ); ?>
					</div>
					<div class="radio-button">
						<input
								id="installment"
								type="radio"
								value="<?php esc_html_e( 'Розстрочка вiд школи', 'alevel' ); ?>"
								name="method_payment">
						<label class="icon-check" for="installment"></label>
						<h4><?php esc_html_e( 'Розстрочка вiд школи', 'alevel' ); ?></h4>
						<?php echo wp_kses_post( wpautop( $atts['course_school_installment_text'] ?? '' ) ); ?>
					</div>
					<div class="radio-button">
						<input
								id="mono"
								type="radio"
								value="<?php esc_html_e( 'Покупка частинами вiд Моно банк', 'alevel' ); ?>"
								name="method_payment">
						<label class="icon-check" for="mono"></label>
						<h4><?php echo wp_kses_post( __( 'Покупка частинами вiд <br>“Моно банк”', 'alevel' ) ); ?></h4>
						<img
								width="24px"
								height="24.5px"
								src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/mono.svg' ); ?>"
								alt="Mono icon">
						<?php echo wp_kses_post( wpautop( $atts['course_mono_text'] ?? '' ) ); ?>
					</div>
				</div>
				<h3><?php esc_html_e( 'Крок 2. Заповнiть контактнi данi', 'alevel' ); ?></h3>
				<div class="input">
					<label for="payment_from_firs_name" class="hidden"></label>
					<input
							type="text"
							id="payment_from_firs_name"
							name="payment_from_firs_name"
							placeholder="<?php esc_html_e( 'Iм’я', 'alevel' ); ?>"
							required>
				</div>
				<div class="input">
					<label for="payment_from_last_name" class="hidden"></label>
					<input
							type="text"
							name="payment_from_last_name"
							id="payment_from_last_name"
							placeholder="<?php esc_html_e( 'Прiзвище', 'alevel' ); ?>"
							required>
				</div>
				<div class="input">
					<label for="payment_from_phone" class="hidden"></label>
					<input
							type="tel"
							id="payment_from_phone"
							name="payment_from_phone"
							placeholder="<?php esc_html_e( 'Номер телефону', 'alevel' ); ?>"
							required>
				</div>
				<div class="input">
					<label for="payment_from_email" class="hidden"></label>
					<input
							type="email"
							id="payment_from_email"
							name="payment_from_email"
							placeholder="Email"
							required>
				</div>
				<div class="select">
					<label for="payment_from_count_payment" class="hidden"></label>
					<select name="payment_from_count_payment" id="payment_from_count_payment" required>
						<option value="0">
							<?php esc_html_e( 'Виберіть кількість платежів', 'alevel' ); ?>
						</option>
						<option value="<?php esc_html_e( '1 платежі', 'alevel' ); ?>">
							<?php esc_html_e( '1 платіж', 'alevel' ); ?>
						</option>
						<option value="<?php esc_html_e( '2 платежі', 'alevel' ); ?>">
							<?php esc_html_e( '2 платежі', 'alevel' ); ?>
						</option>
						<option value="<?php esc_html_e( '3 платежі', 'alevel' ); ?>">
							<?php esc_html_e( '3 платежі', 'alevel' ); ?>
						</option>
						<option value="<?php esc_html_e( '4 платежі', 'alevel' ); ?>">
							<?php esc_html_e( '4 платежі', 'alevel' ); ?>
						</option>
						<option value="<?php esc_html_e( '5 платежі', 'alevel' ); ?>">
							<?php esc_html_e( '5 платежі', 'alevel' ); ?>
						</option>
						<option value="<?php esc_html_e( '6 платежі', 'alevel' ); ?>">
							<?php esc_html_e( '6 платежі', 'alevel' ); ?>
						</option>
					</select>
				</div>
				<input type="submit" value="<?php esc_html_e( 'Записатись на курс', 'alevel' ); ?>">
				<input type="hidden" name="course_name" id="course_name" value="<?php the_title(); ?>">
				<?php
				wp_nonce_field( ModalHandler::COURSE_PAYMENT_FORM, ModalHandler::COURSE_PAYMENT_FORM );
				echo sprintf(
					'<label class="label-policy" for="payment_policy"><input type="checkbox" id="payment_policy" name="payment_policy" checked><p>%s <a href="%s" target="_blank">%s <br/>%s </a></p></label>',
					esc_html( __( 'Натискаючи на кнопку я погоджуюсь на', 'alevel' ) ),
					esc_url( get_privacy_policy_url() ),
					esc_html( __( 'обробку', 'alevel' ) ),
					esc_html( __( 'персональних даних', 'alevel' ) )
				);
				?>
			</form>
		</div>
		<?php
	}

	if ( null === $form_type ) {
		?>
		<div class="dfr">
			<div class="description active">
				<h2><?php echo esc_html( $atts['course_one_payment_title'] ?? '' ); ?></h2>
				<h3><?php echo esc_html( $course_title ); ?></h3>
				<hr class="sline mini">
				<ul class="desc">
					<li>
						<?php esc_html_e( 'Початок курсу:', 'alevel' ); ?>
						<span><?php echo esc_html( date_i18n( 'j F', $time_input ) ); ?></span>
					</li>
					<li>
						<?php
						esc_html_e( 'Залишилось:', 'alevel' );

						echo sprintf(
							'<span>%d %s</span>',
							esc_attr( $course_available ),
							esc_html__( 'мiсць', 'alevel' )
						);
						?>
					</li>
				</ul>
				<?php if ( ! empty( $atts['course_sales_prise'] ) || ! empty( $atts['course_prise'] ) ) { ?>
					<hr class="sline">
					<h3><?php echo esc_html( $atts['sales_title'] ?? '' ); ?></h3>
					<p><?php echo esc_html( $atts['sales_sub_title'] ?? '' ); ?></p>
					<p class="price">
						<?php echo wp_kses_post( $atts['course_prise'] ?? '' ); ?>
						<span><?php echo wp_kses_post( $atts['course_sales_prise'] ?? '' ); ?></span>
					</p>
				<?php } ?>
			</div>
			<form id="payment-form" method="post">
				<h3><?php esc_html_e( 'Заповнiть контактнi даннi', 'alevel' ); ?></h3>
				<div class="input">
					<label for="payment_from_firs_name" class="hidden"></label>
					<input
							type="text"
							id="payment_from_firs_name"
							name="payment_from_firs_name"
							placeholder="<?php esc_html_e( 'Iм’я', 'alevel' ); ?>"
							required>
				</div>
				<div class="input">
					<label for="payment_from_last_name" class="hidden"></label>
					<input
							type="text"
							name="payment_from_last_name"
							id="payment_from_last_name"
							placeholder="<?php esc_html_e( 'Прiзвище', 'alevel' ); ?>"
							required>
				</div>
				<div class="input">
					<label for="payment_from_phone" class="hidden"></label>
					<input
							type="tel"
							id="payment_from_phone"
							name="payment_from_phone"
							placeholder="<?php esc_html_e( 'Номер телефону', 'alevel' ); ?>"
							required>
				</div>
				<div class="input">
					<label for="payment_from_email" class="hidden"></label>
					<input
							type="email"
							id="payment_from_email"
							name="payment_from_email"
							placeholder="Email"
							required>
				</div>
				<input type="submit" value="<?php esc_html_e( 'Записатись на курс', 'alevel' ); ?>">
				<input type="hidden" name="course_name" id="course_name" value="<?php the_title(); ?>">
				<?php
				wp_nonce_field( ModalHandler::COURSE_PAYMENT_FORM, ModalHandler::COURSE_PAYMENT_FORM );
				echo sprintf(
					'<label for="payment_policy"><input type="checkbox" id="payment_policy" name="payment_policy" checked><p>%s <a href="%s" target="_blank">%s <br/>%s </a></p></label>',
					esc_html( __( 'Натискаючи на кнопку я погоджуюсь на', 'alevel' ) ),
					esc_url( get_privacy_policy_url() ),
					esc_html( __( 'обробку', 'alevel' ) ),
					esc_html( __( 'персональних даних', 'alevel' ) )
				);
				?>
			</form>
		</div>
	<?php } ?>
</div>
