<?php
/**
 * ToolsBlock template.
 *
 * @package iwpdev/alevel
 */

$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$tabs      = vc_param_group_parse_atts( $atts['tabs'] );
?>
<div class="tool-wrapper <?php echo esc_attr( $css_class ?? '' ); ?>">
	<?php if ( ! empty( $tabs ) ) { ?>
		<ul class="dfr">
			<?php
			foreach ( $tabs as $key => $alv_tab ) {
				$tab_anchor   = mb_strtolower( preg_replace( '/([ +,!@#.%$^&*(){}])/', '', $alv_tab['tab_name'] ) );
				$active_class = 0 === $key ? 'active' : '';
				$icon         = ! empty( $alv_tab['icon'] ) ? wp_get_attachment_image_url( $alv_tab['icon'] ) : null;
				?>
				<li class="<?php echo esc_attr( $active_class ); ?>">
					<a href="#<?php echo esc_attr( $tab_anchor ); ?>" data-toggle="tab">
						<?php if ( ! empty( $icon ) ) { ?>
							<img
									src="<?php echo esc_url( $icon ); ?>"
									alt="<?php echo esc_html( get_the_title( $alv_tab['icon'] ) ); ?>">
							<?php
						}
						echo esc_html( $alv_tab['tab_name'] );
						?>
					</a>
				</li>
			<?php } ?>
		</ul>

		<div class="tabs">
			<?php
			foreach ( $tabs as $key => $alv_tab ) {
				$tab_anchor   = mb_strtolower( preg_replace( '/([ +,!@#.%$^&*(){}])/', '', $alv_tab['tab_name'] ) );
				$active_class = 0 === $key ? 'active' : '';
				$icon         = ! empty( $alv_tab['icon'] ) ? wp_get_attachment_image_url( $alv_tab['icon'] ) : null;
				?>
				<div
						class="tab-pane <?php echo esc_attr( $active_class ); ?>"
						id="<?php echo esc_attr( $tab_anchor ); ?>">
					<?php if ( ! empty( $icon ) ) { ?>
						<img
								src="<?php echo esc_url( $icon ); ?>"
								alt="<?php echo esc_html( get_the_title( $alv_tab['icon'] ) ); ?>">
					<?php } ?>
					<div class="desc">
						<h3><?php echo esc_html( $alv_tab['tab_name'] ); ?></h3>
						<?php echo wp_kses_post( wpautop( $alv_tab['tab_description'] ) ) ?? ''; ?>
					</div>
				</div>
			<?php } ?>
		</div>
	<?php } ?>
</div>
