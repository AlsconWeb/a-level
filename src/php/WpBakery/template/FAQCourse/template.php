<?php
/**
 * FAQ Course block template.
 *
 * @package iwpdev/alevel
 */

$css_class  = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$items      = vc_param_group_parse_atts( $atts['tabs'] );
$title_tabs = $atts['title'] ?? '';
$sub_title  = $atts['sub_title'] ?? '';
?>
<div class="dfr <?php echo esc_attr( $css_class ?? null ); ?>">
	<?php if ( ! empty( $items ) ) { ?>
		<div class="left-block">
			<h2 class="title"><?php echo esc_html( $title_tabs ); ?></h2>
			<?php echo wp_kses_post( wpautop( $sub_title ) ); ?>
			<ul class="tab-nav">
				<?php
				foreach ( $items as $key => $item ) {
					$image = wp_get_attachment_image_url( $item['image'] );
					?>
					<li class="<?php echo 0 === $key ? 'active' : ''; ?>">
						<img
								src="<?php echo esc_url( $image ); ?>"
								alt="<?php echo esc_html( get_the_title( $item['image'] ) ); ?>">
						<div class="desc">
							<p><?php echo esc_html( $item['stage_name'] ); ?></p>
							<h3><?php echo esc_html( $item['title'] ); ?></h3>
						</div>
						<a href="#tab-<?php echo esc_attr( $key ); ?>" data-toggle="tab"></a>
					</li>
				<?php } ?>
			</ul>
		</div>
		<div class="right-block">
			<div class="tabs">
				<?php
				foreach ( $items as $key => $item ) {
					$image       = wp_get_attachment_image_url( $item['image'] );
					$description = $item['description'] ?? '';
					?>
					<div
							class="tab-pane <?php echo 0 === $key ? 'active' : ''; ?>"
							id="tab-<?php echo esc_attr( $key ); ?>">
						<img
								src="<?php echo esc_url( $image ); ?>"
								alt="<?php echo esc_html( get_the_title( $item['image'] ) ); ?>">
						<p><?php echo esc_html( $item['stage_name'] ); ?></p>
						<h3><?php echo esc_html( $item['title'] ); ?></h3>
						<?php echo wp_kses_post( wpautop( $description ) ); ?>
					</div>
				<?php } ?>
			</div>
		</div>
	<?php } ?>
</div>