<?php
/**
 * BonusDescription template file.
 *
 * @package iwpdev/alevel
 */

$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$image     = wp_get_attachment_image_url( $atts['image'] );
?>
<div class="dfr <?php echo esc_attr( $css_class ?? null ); ?>">
	<div class="desc">
		<img src="<?php echo esc_url( $image ); ?>" alt="<?php echo esc_html( get_the_title( $atts['image'] ) ); ?>">
		<h3><?php echo esc_html( $atts['title'] ); ?></h3>
		<?php echo wp_kses_post( wpautop( $atts['sub_title'] ) ); ?>
	</div>
	<?php echo wp_kses_post( wpautop( $atts['description'] ) ); ?>
</div>
