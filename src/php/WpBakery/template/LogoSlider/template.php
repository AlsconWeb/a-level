<?php
/**
 * Logo Slider template.
 *
 * @package iwpdev/alevel
 */

$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$logos     = vc_param_group_parse_atts( $atts['logos'] );
shuffle( $logos );

if ( ! empty( $logos ) ) {
	?>
	<div class="logo-slider <?php echo esc_attr( $css_class ?? '' ); ?>">
		<?php
		foreach ( $logos as $logo ) {
			if ( ! empty( $logo['slide_image'] ) ) {
				$logo_url = wp_get_attachment_image_url( $logo['slide_image'], 'full' );
				?>
				<div class="item">
					<a href="<?php echo esc_url( $logo['company_link'] ?? '#' ); ?>" target="_blank" rel="nofollow">
						<img
								src="<?php echo esc_url( $logo_url ); ?>"
								alt="<?php echo esc_attr( get_the_title( $logo['slide_image'] ) ); ?>">
					</a>
				</div>
				<?php
			}
		}
		?>
	</div>
	<?php
}
