<?php
/**
 * YourCVBlock template.
 *
 * @package iwpdev/alevel
 */

$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$icon      = wp_get_attachment_image_url( $atts['icon'] );
?>
<div class="dfr <?php echo esc_attr( $css_class ?? '' ); ?>">
	<div class="left-block">
		<div class="dfr">
			<img src="<?php echo esc_url( $icon ); ?>" alt="<?php echo esc_html( $atts['icon'] ); ?>">
			<div class="desc">
				<p><?php esc_html_e( 'Посада:', 'alevel' ); ?></p>
				<h3><?php echo esc_html( $atts['position'] ); ?></h3>
			</div>
		</div>
	</div>
	<div class="right-block">
		<img src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/Logo_ua.svg' ); ?>" alt="Logo">
		<p>
			<?php esc_html_e( 'Зарплата вiд:', 'alevel' ); ?>
			<span><?php echo esc_html( $atts['salary_from'] ); ?></span>
		</p>
	</div>
	<hr class="sline">
	<div class="left-block">
		<p><?php esc_html_e( 'Графік:', 'alevel' ); ?></p>
		<h5><?php echo esc_html( $atts['work_schedule'] ); ?></h5>
	</div>
	<div class="right-block">
		<p><?php esc_html_e( 'Освіта:', 'alevel' ); ?></p>
		<h5><?php echo esc_html( $atts['education'] ); ?></h5>
	</div>
</div>
