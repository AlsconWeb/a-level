<?php
/**
 * RatingsSchool template.
 *
 * @package iwpdev/alevel
 */

$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$items     = vc_param_group_parse_atts( $atts['items'] );
?>
<div class="ratings dfr <?php echo esc_attr( $css_class ); ?>">
	<?php
	if ( ! empty( $items ) ) {
		foreach ( $items as $item ) {
			?>
			<div class="rating">
				<h2 class="title icon-star"><?php echo esc_html( $item['title'] ); ?></h2>
				<p><?php echo esc_html( $item['description'] ); ?></p>
			</div>
			<?php
		}
	}
	?>
</div>
