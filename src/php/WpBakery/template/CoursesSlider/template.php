<?php
/**
 * Courses Slider template.
 *
 * @package iwpdev/alevel
 */

$arg = [
	'post_type'      => 'courses',
	'post_status'    => 'publish',
	'posts_per_page' => $atts['count_output'] ?? 10,
	'meta_key'       => '_alv_course_start',
	'orderby'        => 'meta_value_num',
	'order'          => 'ASC',
];

$course_slide_obj = new WP_Query( $arg );
$css_class        = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
?>

<div class="courses-slider <?php echo esc_attr( $css_class ); ?>">
	<?php
	if ( $course_slide_obj->have_posts() ) {
		while ( $course_slide_obj->have_posts() ) {
			$course_slide_obj->the_post();
			$course_id = get_the_ID();

			get_template_part( 'template-parts/course-slider', 'item', [ 'course_id' => $course_id ] );

		}

		wp_reset_postdata();
	}
	?>
</div>
