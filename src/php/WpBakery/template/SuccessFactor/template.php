<?php
/**
 * SuccessFactor template.
 *
 * @package iwpdev/alevel
 */

$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$items     = vc_param_group_parse_atts( $atts['items'] );
if ( ! empty( $items ) ) {
	?>
	<ul class="<?php echo esc_attr( $css_class ); ?>">
		<?php foreach ( $items as $item ) { ?>
			<li class="icon-<?php echo esc_attr( $item['icon_name'] ); ?>">
				<strong><?php echo esc_html( $item['title'] ); ?></strong>
				<?php echo esc_html( $item['description'] ); ?>
			</li>
		<?php } ?>
	</ul>
	<?php
}