<?php
/**
 * HistoryImages template.
 *
 * @package iwpdev/alevel
 */

$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$items     = vc_param_group_parse_atts( $atts['images'] );
?>
<div class="history-images <?php echo esc_attr( $css_class ); ?>">
	<?php
	if ( ! empty( $items ) ) {
		foreach ( $items as $item ) {
			$image_url = wp_get_attachment_image_url( $item['image'], 'full' );
			?>
			<img
					src="<?php echo esc_url( $image_url ); ?>"
					alt="<?php echo esc_html( get_the_title( $item['image'] ) ); ?>">
			<?php
		}
	}
	?>
</div>
