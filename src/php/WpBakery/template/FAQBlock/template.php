<?php
/**
 * FAQ block template.
 *
 * @package iwpdev/alevel
 */

$css_class  = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$items      = vc_param_group_parse_atts( $atts['tabs'] );
$title_tabs = $atts['title'] ?? '';
?>
<div class="wrapper <?php echo esc_attr( $css_class ); ?>">
	<div class="left-block">
		<h5 class="title"><?php echo esc_html( $title_tabs ); ?></h5>
		<?php
		if ( ! empty( $items ) ) {
			?>
			<ul class="nav-faq">
				<?php foreach ( $items as $key => $item ) { ?>
					<li class="<?php echo 0 === $key ? 'active' : ''; ?>">
						<a href="#faq-<?php echo esc_attr( $key ); ?>" data-toggle="tab">
							<?php echo esc_html( $item['tab_name'] ); ?>
						</a>
					</li>
				<?php } ?>
			</ul>
		<?php } ?>
	</div>
	<div class="right-block">
		<?php if ( ! empty( $items ) ) { ?>
			<div class="tabs">
				<?php foreach ( $items as $key => $faq_tab ) { ?>
					<div
							class="tab-pane-faq <?php echo 0 === $key ? 'active' : ''; ?>"
							id="faq-<?php echo esc_attr( $key ); ?>">
						<h3 class="title"><?php echo esc_html( $faq_tab['tab_name'] ); ?></h3>
						<?php
						$accordions = vc_param_group_parse_atts( $faq_tab['answers'] );

						if ( ! empty( $accordions ) ) {
							foreach ( $accordions as $accordion ) {
								?>
								<div class="accordion-item">
									<h4 class="title"><?php echo esc_html( $accordion['title'] ); ?></h4>
									<div class="hide">
										<?php echo wp_kses_post( wpautop( $accordion['answers_text'] ) ); ?>
									</div>
								</div>
								<?php
							}
						}
						?>
					</div>
				<?php } ?>
			</div>
		<?php } ?>
	</div>
</div>
