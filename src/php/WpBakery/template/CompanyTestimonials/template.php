<?php
/**
 * CompanyTestimonials template.
 *
 * @package iwpdev/alevel
 */

$current_blog_id = get_current_blog_id();
if ( 1 !== $current_blog_id && is_multisite() ) {
	switch_to_blog( get_main_site_id() );
}

$arg = [
	'post_type'      => 'testimonials',
	'posts_per_page' => $atts['count_output'] ?? 4,
	'orderby'        => 'rand',
	'post_status'    => 'publish',
	'meta_query'     => [
		'relation' => 'AND',
		[
			'key'   => '_alv_type_review',
			'value' => 'firm',
		],
		[
			'key'     => '_alv_company_no_review',
			'value'   => 'true',
			'compare' => '!=',
		],
	],
];

$testimonials = new WP_Query( $arg );
$css_class    = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );

?>
<div class="partners dfr <?php echo esc_attr( $css_class ?? '' ); ?>">
	<?php
	if ( $testimonials->have_posts() ) {
		while ( $testimonials->have_posts() ) {
			$testimonials->the_post();

			$testimonials_id = get_the_ID();
			$review_social   = carbon_get_post_meta( $testimonials_id, 'alv_social_media_reviewer' );
			$teachers        = carbon_get_post_meta( $testimonials_id, 'alv_reviewer_teachers' );
			$course_id       = carbon_get_post_meta( $testimonials_id, 'alv_course_event' );

			get_template_part(
				'template-parts/reviews',
				'firm',
				[
					'testimonials_id' => $testimonials_id,
					'course_id'       => $course_id[0]['id'] ?? null,
				]
			);
		}
		wp_reset_postdata();

		if ( is_multisite() ) {
			switch_to_blog( $current_blog_id );
		}
	}
	?>
</div>
