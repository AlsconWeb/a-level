<?php
/**
 * History Block template.
 *
 * @package iwpdev/alevel
 */

$css_class         = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$history_link      = ! empty( $atts['link'] ) ? vc_build_link( $atts['link'] ) : null;
$history_title     = ! empty( $atts['title'] ) ? $atts['title'] : null;
$history_sub_title = ! empty( $atts['sub_title'] ) ? $atts['sub_title'] : null;
$history_image     = ! empty( $atts['image'] ) ? wp_get_attachment_image_url( $atts['image'], 'full' ) : null;
$history_text      = ! empty( $atts['text'] ) ? $atts['text'] : null;
?>

<div class="history <?php echo esc_attr( $css_class ?? '' ); ?>">
	<h3><?php echo esc_html( $history_title ); ?></h3>
	<p><?php echo esc_html( $history_sub_title ); ?></p>
	<?php if ( ! empty( $history_image ) ) { ?>
		<img
				src="<?php echo esc_url( $history_image ); ?>"
				alt="<?php echo esc_html( get_the_title( $atts['image'] ) ); ?>">
	<?php } ?>
	<h4><?php echo esc_html( $history_text ); ?></h4>
	<a
			class="button"
			href="<?php echo esc_url( $history_link['url'] ); ?>"
			target="<?php echo esc_attr( $history_link['target'] ?? '' ); ?>"
			rel="<?php echo esc_attr( $history_link['rel'] ?? '' ); ?>">
		<?php echo esc_html( $history_link['title'] ); ?>
	</a>
</div>
