<?php
/**
 * AnchorageMenu template.
 *
 * @package iwpdev/alevel
 */

$items     = vc_param_group_parse_atts( $atts['anchorage'] );
$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
?>
<nav class="nav-sections">
	<ul class="menu dfr <?php echo esc_attr( $css_class ?? null ); ?>">
		<?php
		if ( ! empty( $items ) ) {
			foreach ( $items as $key => $item ) {
				?>
				<li class="menu-item">
					<a
							href="#<?php echo esc_attr( $item['anchorage_link'] ); ?>"
							class="menu-item-link <?php echo 0 === $key ? 'active' : ''; ?>" rel="nofollow">
						<?php echo esc_html( $item['anchorage_text'] ); ?>
					</a>
				</li>
				<?php
			}
			?>
			<div class="active-line"></div>
			<?php
		}
		?>
	</ul>
</nav>
