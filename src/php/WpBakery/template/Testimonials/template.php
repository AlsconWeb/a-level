<?php
/**
 * Testimonials template.
 *
 * @package iwpdev/alevel
 */

use Alevel\Helpers\Helper;

global $post;

$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$where_use = ! empty( $atts['where_use'] ) ? $atts['where_use'] : 'true';

$current_blog_id = get_current_blog_id();

if ( 1 !== $current_blog_id && is_multisite() ) {
	switch_to_blog( get_main_site_id() );
	$main_course_id = Helper::get_post_id_in_main_site( $post->post_name, $post->post_type );
} else {
	$main_course_id = $post->ID;
}

if ( 'true' === $where_use ) {
	$arg = [
		'post_type'      => 'testimonials',
		'posts_per_page' => $atts['count_output'] ?? 7,
		'post_status'    => 'publish',
		'orderby'        => 'rand',
		'meta_query'     => [
			'relation' => 'AND',
			[
				'key'   => '_alv_course_event|||0|id',
				'value' => $main_course_id,
			],
		],
	];
} else {
	$arg = [
		'post_type'      => 'testimonials',
		'posts_per_page' => $atts['count_output'] ?? 4,
		'post_status'    => 'publish',
		'orderby'        => 'rand',
		'meta_query'     => [
			[
				'key'   => '_alv_type_review',
				'value' => ! empty( $atts['type_review'] ) ? $atts['type_review'] : 'regular',
			],
		],
	];
}

$testimonials_query = new WP_Query( $arg );
?>
	<div class="reviews-items dfr <?php echo esc_attr( $css_class ?? '' ); ?>">
		<?php
		if ( $testimonials_query->have_posts() ) {
			while ( $testimonials_query->have_posts() ) {
				$testimonials_query->the_post();
				$testimonials_id = get_the_ID();
				$review_social   = carbon_get_post_meta( $testimonials_id, 'alv_social_media_reviewer' );
				$teachers        = carbon_get_post_meta( $testimonials_id, 'alv_reviewer_teachers' );
				$course_id       = ! empty( carbon_get_post_meta( $testimonials_id, 'alv_course_event' ) ) ? carbon_get_post_meta( $testimonials_id, 'alv_course_event' )[0]['id'] : null;

				get_template_part(
					'template-parts/reviews',
					'item',
					[
						'testimonials_id' => $testimonials_id,
						'course_id'       => $course_id,
					]
				);
			}
			wp_reset_postdata();

		} else {
			?>
			<h4 style="text-align: left"
				class="vc_custom_heading title">
				<?php esc_html_e( 'Відгуків про цей крусі ще ні ти можеш стати першим! Додай свій відгук це важливо для нас.', 'alevel' ); ?>
			</h4>
			<?php
		}
		if ( 'true' === $where_use ) {
			?>
			<div class="buttons">
				<?php if ( $testimonials_query->post_count > 7 ) { ?>
					<a class="button" href="<?php echo esc_url( $atts['more_button_link'] ?? '#' ); ?>">
						<i class="icon-ellipsis"><?php echo esc_html( $atts['more_button'] ?? '' ); ?></i>
					</a>
				<?php } ?>
				<?php if ( 'true' === $where_use ) { ?>
					<a
							class="button <?php echo $testimonials_query->post_count > 7 ? '' : 'one_button'; ?>"
							data-toggle="modal" data-target=".leave-review"
							href="<?php echo esc_url( $atts['add_link_button'] ?? '#' ); ?>">
						<i class="icon-plus"><?php echo esc_html( $atts['add_text_button'] ?? '' ); ?></i>
					</a>
				<?php } ?>
			</div>
		<?php } ?>
	</div>
<?php
if ( is_multisite() ) {
	switch_to_blog( $current_blog_id );
}
