<?php
/**
 * Course Program Accordion template.
 *
 * @package iwpdev/alevel
 */

$course_module = vc_param_group_parse_atts( $atts['course_accordion'] );
$css_class     = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
?>
<div class="accordion <?php echo esc_attr( $css_class ?? null ); ?>">
	<?php
	if ( ! empty( $course_module ) ) {
		foreach ( $course_module as $module ) {
			$color        = $module['color_step'] ?? '#000';
			$module_title = sprintf(
				'<h4>%s <span>%s</span></h4>',
				esc_html( $module['name_step_course'] ?? '' ),
				esc_html( $module['title_step'] ?? '' )
			);
			$module_times = vc_param_group_parse_atts( $module['time_steps'] );
			$module_topic = ! empty( $module['module_topics'] ) ? vc_param_group_parse_atts( $module['module_topics'] ) : null;
			?>
			<div
					class="accordion-item <?php echo empty( $module_topic ) || ! isset( $module_topic[0]['topic_name'] ) ? ' empty' : ''; ?>"
					style="color:<?php echo esc_attr( $color ); ?>;">
				<div class="dfr">
					<?php echo wp_kses_post( $module_title ); ?>
					<?php
					if ( ! empty( $module_times ) ) {
						?>
						<ul>
							<?php
							foreach ( $module_times as $item ) {
								if ( ! empty( $item ) ) {
									?>
									<li><?php echo wp_kses_post( $item['steps_time'] ); ?></li>
									<?php
								}
							}
							?>
						</ul>
						<?php
					}
					?>
				</div>
				<div class="hide">
					<?php if ( ! empty( $module_topic ) ) { ?>
						<?php foreach ( $module_topic as $topic ) { ?>
							<h3><?php echo esc_html( $topic['topic_name'] ?? '' ); ?></h3>
							<?php $module_plan = vc_param_group_parse_atts( $topic['listing_description_step'] ); ?>
							<ul>
								<?php
								if ( ! empty( $module_plan ) ) {
									foreach ( $module_plan as $plan ) {
										if ( ! empty( $plan ) ) {
											?>
											<li><?php echo esc_html( $plan['step_item'] ?? '' ); ?></li>
											<?php
										}
									}
								}
								?>
							</ul>
						<?php } ?>
					<?php } ?>
				</div>
			</div>
			<?php
		}
	}
	?>
</div>
