<?php
/**
 * Employment at school template.
 *
 * @package iwpdev/alevel
 */

$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$items     = vc_param_group_parse_atts( $atts['employment'] );

if ( ! empty( $items ) ) {
	?>
	<div class="accordion <?php echo esc_attr( $css_class ?? '' ); ?>">
		<?php
		foreach ( $items as $item ) {
			$icon_url = wp_get_attachment_image_url( $item['icon'], 'full' );
			?>
			<div class="accordion-item">
				<?php if ( ! empty( $icon_url ) ) { ?>
					<img
							src="<?php echo esc_url( $icon_url ); ?>"
							alt="<?php echo esc_attr( get_the_title( $item['icon'] ) ); ?>">
				<?php } ?>
				<h5><?php echo esc_html( $item['title'] ); ?></h5>
				<div class="employment-description">
					<?php echo wp_kses_post( wpautop( $item['text'] ) ); ?>
				</div>
			</div>
		<?php } ?>
	</div>
	<?php
}
