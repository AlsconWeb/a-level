<?php
/**
 * Salary Calculator template.
 *
 * @package iwpdev/alevel
 */

$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$calculate = vc_param_group_parse_atts( $atts['calculate'] );

if ( ! empty( $calculate ) ) {
	?>
	<div class="dfr <?php echo esc_attr( $css_class ?? '' ); ?>">
		<div class="tabs">
			<?php
			foreach ( $calculate as $key => $item ) {
				$active_class = 0 === $key ? 'active' : '';
				$tab_anchor   = mb_strtolower( str_replace( ' ', '', $item['tab_position'] ) );
				?>
				<div
						class="tab-pane <?php echo esc_attr( $active_class ); ?>"
						id="<?php echo esc_attr( $tab_anchor ); ?>">
					<?php echo wp_kses_post( wpautop( $item['tab_description'] ) ); ?>
					<h3><?php echo esc_html( $item['tab_experience'] ); ?></h3>
				</div>
			<?php } ?>
		</div>
		<div class="dfc">
			<p class="salaries"><?php echo wp_kses_post( $atts['title'] ?? '' ); ?></p>
			<ul class="dfr range">
				<?php
				foreach ( $calculate as $key => $value ) {
					$active_class = 0 === $key ? 'active' : '';
					$tab_anchor   = mb_strtolower( str_replace( ' ', '', $value['tab_position'] ) );
					?>
					<li
							class="<?php echo esc_attr( $active_class ); ?>"
							data-price="<?php echo esc_html( $value['tab_salary'] ); ?>">
						<a
								href="#<?php echo esc_attr( $tab_anchor ); ?>" data-toggle="tab"
								data-class="<?php echo esc_attr( $value['tab_steps'] ); ?>">
							<?php echo esc_html( $value['tab_position'] ); ?>
							<span><?php echo esc_html( $value['tab_amount_experience'] ); ?></span>
						</a>
					</li>
				<?php } ?>
				<li class="icon-smile"></li>
			</ul>
		</div>
	</div>
	<?php
}
