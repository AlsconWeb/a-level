<?php
/**
 * Events Slider tempalte.
 *
 * @package iwpdev/alevel
 */

$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );

$current_blog_id = get_current_blog_id();
if ( 1 !== $current_blog_id && is_multisite() ) {
	switch_to_blog( get_main_site_id() );
}

$arg = [
	'post_type'      => 'events',
	'posts_per_page' => $atts['count_output'] ?? - 1,
	'post_status'    => 'publish',
];

$events_query = new WP_Query( $arg );

if ( $events_query->have_posts() ) {
	?>
	<div class="events-block <?php echo esc_attr( $css_class ?? '' ); ?>">
		<?php
		while ( $events_query->have_posts() ) {
			$events_query->the_post();
			$event_id = get_the_ID();

			get_template_part( 'template-parts/events', 'item', [ 'event_id' => $event_id ] );
		}
		wp_reset_postdata();
		?>
	</div>
	<?php
}

if ( is_multisite() ) {
	switch_to_blog( $current_blog_id );
}
