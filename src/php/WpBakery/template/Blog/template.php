<?php
/**
 * Blog template.
 *
 * @package iwpdev/alevel
 */

$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );

$current_blog_id = get_current_blog_id();
if ( 1 !== $current_blog_id && is_multisite() ) {
	switch_to_blog( get_main_site_id() );
}

$arg = [
	'post_type'           => 'post',
	'post_status'         => 'publish',
	'posts_per_page'      => $atts['count_output'] ?? 4,
	'ignore_sticky_posts' => true,
];

$post_query = new WP_Query( $arg );
?>

<div class="medea-items dfr <?php echo esc_attr( $css_class ?? null ); ?>">
	<?php
	if ( $post_query->have_posts() ) {
		while ( $post_query->have_posts() ) {
			$post_query->the_post();
			$article_id = get_the_ID();
			get_template_part(
				'template-parts/blog',
				'item',
				[
					'article_id' => $article_id,
					'class_name' => 'medea-item',
				]
			);
		}
		wp_reset_postdata();
	}

	if ( is_multisite() ) {
		switch_to_blog( $current_blog_id );
	}
	?>
</div>
