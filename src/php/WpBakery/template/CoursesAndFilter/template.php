<?php
/**
 * Courses And Filter template.
 *
 * @package iwpdev/alevel
 */

$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$filter    = vc_param_group_parse_atts( $atts['filter_menu'] );

$arg = [
	'post_type'      => 'courses',
	'post_status'    => 'publish',
	'posts_per_page' => - 1,
	'meta_key'       => '_alv_course_start',
	'orderby'        => 'meta_value_num',
	'order'          => 'ASC',
];

$course_obg = new WP_Query( $arg );
if ( $course_obg->have_posts() ) {
	?>
	<div class="dfr <?php echo esc_attr( $css_class ); ?>">
		<div class="sidebar">
			<ul class="nav-tabs">
				<?php
				if ( ! empty( $filter ) ) {
					foreach ( $filter as $item ) {

						$category = get_term_by( 'slug', $item['course_category_filter'], 'category-courses' );
						?>
						<li>
							<button
									class="icon-<?php echo esc_attr( $item['icon_name'] ); ?>"
									data-filter="<?php echo esc_attr( $item['course_category_filter'] ); ?>">
								<?php echo esc_html( false !== $category ? $category->name : __( 'Всі', 'alevel' ) ); ?>
							</button>
						</li>
						<?php
					}
				}
				?>
			</ul>
		</div>

		<div class="courses dfr">
			<?php
			while ( $course_obg->have_posts() ) {
				$course_obg->the_post();
				$course_id = get_the_ID();

				get_template_part( 'template-parts/course-filter', 'item', [ 'course_id' => $course_id ] );
			}
			wp_reset_postdata();
			?>
		</div>
	</div>
	<?php
}
