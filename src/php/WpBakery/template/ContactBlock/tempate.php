<?php
/**
 * ContactBlock template.
 *
 * @package iwpdev/alevel
 */

$css_class    = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$work_daly    = vc_param_group_parse_atts( $atts['work_time'] );
$contact_fons = vc_param_group_parse_atts( $atts['contact_phone'] );
?>
<div class="contacts">
	<h3><?php echo esc_html( $atts['title'] ?? '' ); ?></h3>
	<a href="<?php echo esc_url( $atts['address_link'] ?? '#' ); ?>">
		<?php echo wp_kses_post( wpautop( $atts['address'] ?? '' ) ); ?>
	</a>
	<?php if ( ! empty( $work_daly ) ) { ?>
		<ul>
			<?php foreach ( $work_daly as $item ) { ?>
				<li>
					<span><?php echo esc_html( $item['days_text'] ); ?> </span>
					<?php echo esc_html( $item['days_work_time'] ); ?>
				</li>
			<?php } ?>
		</ul>
	<?php } ?>
	<hr class="sline">
	<ul class="dfr">
		<?php if ( ! empty( $contact_fons ) ) { ?>
			<li><?php esc_html_e( 'Контактнi телефони', 'alevel' ); ?>
				<?php
				foreach ( $contact_fons as $phone ) {
					$phone_link = filter_var( $phone['phone'], FILTER_SANITIZE_NUMBER_INT );
					?>
					<a href="tel:<?php echo esc_attr( $phone_link ); ?>">
						<?php echo esc_html( $phone['phone'] ); ?>
					</a>
				<?php } ?>
			</li>
		<?php } ?>
		<li>
			<?php esc_html_e( 'Пошта', 'alevel' ); ?>
			<a href="mailto:<?php echo esc_attr( $atts['email'] ?? '' ); ?>">
				<?php echo esc_html( $atts['email'] ?? '' ); ?>
			</a>
		</li>
	</ul>
	<a class="button" href="#">
		<?php esc_html_e( 'Передзвонiть менi', 'alevel' ); ?>
	</a>
</div>
