<?php
/**
 * Event Post template.
 *
 * @package iwpdev/alevel
 */

$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );

$current_blog_id = get_current_blog_id();
if ( 1 !== $current_blog_id ) {
	switch_to_blog( get_main_site_id() );
}

$arg = [
	'post_type'      => 'events',
	'post_status'    => 'publish',
	'posts_per_page' => $atts['count_output'] ?? 3,
	'order'          => 'DESC',
	'orderby'        => 'date',
];

$event_query = new WP_Query( $arg );
if ( $event_query->have_posts() ) {
	?>
	<div class="events <?php echo esc_attr( $css_class ?? null ); ?>">
		<h5 class="title"><?php echo esc_html( $atts['title'] ); ?></h5>
		<?php
		while ( $event_query->have_posts() ) {
			$event_query->the_post();
			$event_id    = get_the_ID();
			$event_start = carbon_get_post_meta( $event_id, 'alv_event_start' );
			$date        = date_create( $event_start );
			$speakers    = carbon_get_post_meta( $event_id, 'alv_mentors' );
			?>
			<div class="event-item <?php echo empty( $speakers ) ? 'no-speakers' : ''; ?>">
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="link"></a>
				<?php
				if ( has_post_thumbnail( $event_id ) ) {
					the_post_thumbnail( 'alv-thmb_mini' );
				} else {
					?>
					<img
							src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/thumbnail-mini.png' ); ?>"
							alt="No image">
				<?php } ?>
				<div class="desc">
					<h3 class="title"><?php the_title(); ?></h3>
					<p class="date"><?php echo esc_html( date_format( $date, 'd.m.Y' ) ); ?></p>
					<?php get_template_part( 'template-parts/speakers', 'info', [ 'speakers' => $speakers ] ); ?>
				</div>
			</div>
			<?php
		}
		wp_reset_postdata();
		?>
	</div>
	<?php
}
if ( is_multisite() ) {
	switch_to_blog( $current_blog_id );
}
