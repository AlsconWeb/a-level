<?php
/**
 * Courses and filter.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\WpBakery\Components\CoursesAndFilter;

use Alevel\Main;

/**
 * CoursesAndFilter class file.
 */
class CoursesAndFilter {
	/**
	 * CoursesAndFilter construct.
	 */
	public function __construct() {
		add_shortcode( 'alv_courses_and_filter', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'alv_courses_and_filter', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Курси та фільтр', 'alevel' ),
			'description'             => esc_html__( 'Курси та фільтр', 'alevel' ),
			'base'                    => 'alv_courses_and_filter',
			'category'                => __( 'Alevel', 'alevel' ),
			'show_settings_on_create' => false,
			'icon'                    => ALV_URL_PATH . '/assets/icons/filter-solid.svg',
			'params'                  => [
				[
					'type'       => 'param_group',
					'value'      => '',
					'heading'    => __( 'Меню фільтра', 'alevel' ),
					'param_name' => 'filter_menu',
					'params'     => [
						[
							'type'       => 'dropdown',
							'value'      => self::get_course_category(),
							'heading'    => __( 'Виберіть категорію для фільтра', 'alevel' ),
							'param_name' => 'course_category_filter',
						],
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Назва іконки', 'alevel' ),
							'param_name' => 'icon_name',
						],
					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Кастомний css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Варіанти дизайну', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::ALV_DIR_PATH . '/WpBakery/template/CoursesAndFilter/template.php';

		return ob_get_clean();
	}

	/**
	 * Get dropdown category array
	 *
	 * @return array
	 */
	private static function get_course_category(): array {
		$courses_category = get_categories(
			[
				'taxonomy'   => 'category-courses',
				'type'       => 'courses',
				'hide_empty' => 0,
			]
		);

		$category_array = [];

		if ( ! empty( $courses_category ) ) {
			$category_array[ __( 'Всі', 'alevel' ) ] = 'all';

			foreach ( $courses_category as $cat ) {
				$category_array[ $cat->name ] = '.' . $cat->slug;
			}
		}

		return $category_array ?? [];
	}
}
