<?php
/**
 * ToolsBlock by course.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\WpBakery\Components\Tools;

use Alevel\Main;

/**
 * ToolsBlock class file.
 */
class ToolsBlock {
	/**
	 * YourCVBlock construct.
	 */
	public function __construct() {
		add_shortcode( 'alv_tools_master', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'alv_tools_master', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Інструменти', 'alevel' ),
			'description'             => esc_html__( 'Інструменти, які ви освоїте', 'alevel' ),
			'base'                    => 'alv_tools_master',
			'category'                => __( 'Alevel', 'alevel' ),
			'show_settings_on_create' => false,
			'icon'                    => ALV_URL_PATH . '/assets/icons/screwdriver-wrench-solid.svg',
			'params'                  => [
				[
					'type'       => 'param_group',
					'value'      => '',
					'heading'    => __( 'Інструменти', 'alevel' ),
					'param_name' => 'tabs',
					'params'     => [
						[
							'type'       => 'attach_image',
							'value'      => '',
							'heading'    => __( 'Значок', 'alevel' ),
							'param_name' => 'icon',
						],
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Назва інструменту', 'alevel' ),
							'param_name' => 'tab_name',
						],
						[
							'type'       => 'textarea',
							'value'      => '',
							'heading'    => __( 'Опис інструментів', 'alevel' ),
							'param_name' => 'tab_description',
						],
					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Кастомний css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Варіанти дизайну', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::ALV_DIR_PATH . '/WpBakery/template/ToolsBlock/template.php';

		return ob_get_clean();
	}
}
