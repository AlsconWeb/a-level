<?php
/**
 * Salary Calculator.
 *
 * @package iwpdev/alevel.
 */

namespace Alevel\WpBakery\Components\SalaryCalculator;

use Alevel\Main;

/**
 * SalaryCalculator class file.
 */
class SalaryCalculator {
	/**
	 * SalaryCalculator construct.
	 */
	public function __construct() {
		add_shortcode( 'alv_salary_calculator', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'alv_salary_calculator', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Калькулятор зарплати', 'alevel' ),
			'description'             => esc_html__( 'Зарплатнi очiкування', 'alevel' ),
			'base'                    => 'alv_salary_calculator',
			'category'                => __( 'Alevel', 'alevel' ),
			'show_settings_on_create' => false,
			'icon'                    => ALV_URL_PATH . '/assets/icons/calculator-solid.svg',
			'params'                  => [
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Заголовок', 'alevel' ),
					'param_name' => 'title',
				],
				[
					'type'       => 'param_group',
					'value'      => '',
					'heading'    => __( 'Розрахунки', 'alevel' ),
					'param_name' => 'calculate',
					'params'     => [
						[
							'type'       => 'textarea',
							'value'      => '',
							'heading'    => __( 'Опис', 'alevel' ),
							'param_name' => 'tab_description',
						],
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Позиція', 'alevel' ),
							'param_name' => 'tab_position',
						],
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Досвід у місяцях', 'alevel' ),
							'param_name' => 'tab_experience',
						],
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Вилка зарплати на місяць', 'alevel' ),
							'param_name' => 'tab_salary',
						],
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Количество опыта на позицию', 'alevel' ),
							'param_name' => 'tab_amount_experience',
						],
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Кроки', 'alevel' ),
							'param_name' => 'tab_steps',
						],
					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Кастомний css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Варіанти дизайну', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::ALV_DIR_PATH . '/WpBakery/template/SalaryCalculator/template.php';

		return ob_get_clean();
	}
}
