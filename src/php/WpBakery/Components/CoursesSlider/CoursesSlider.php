<?php
/**
 * Courses Slider
 *
 * @package iwpdev/alevel
 */

namespace Alevel\WpBakery\Components\CoursesSlider;

use Alevel\Main;

/**
 * CoursesSlider class file.
 */
class CoursesSlider {
	/**
	 * CoursesSlider construct.
	 */
	public function __construct() {
		add_shortcode( 'alv_courses_slider', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'alv_courses_slider', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Курси слайдер', 'alevel' ),
			'description'             => esc_html__( 'Курси слайдер', 'alevel' ),
			'base'                    => 'alv_courses_slider',
			'category'                => __( 'Alevel', 'alevel' ),
			'show_settings_on_create' => false,
			'icon'                    => ALV_URL_PATH . '/assets/icons/scroll-solid.svg',
			'params'                  => [
				[
					'type'       => 'textfield',
					'value'      => 4,
					'heading'    => __( 'Кількість виведених курсів', 'alevel' ),
					'param_name' => 'count_output',
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Кастомний css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Варіанти дизайну', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::ALV_DIR_PATH . '/WpBakery/template/CoursesSlider/template.php';

		return ob_get_clean();
	}
}
