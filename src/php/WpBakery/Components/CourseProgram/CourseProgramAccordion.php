<?php
/**
 * Course Program Accordion
 *
 * @package iwpdev/alevel
 */

namespace Alevel\WpBakery\Components\CourseProgram;

use Alevel\Main;

/**
 * CourseProgramAccordion class file.
 */
class CourseProgramAccordion {
	/**
	 * CourseProgramAccordion construct.
	 */
	public function __construct() {
		add_shortcode( 'alv_course_program_accordion', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'alv_course_program_accordion', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Етапи курсу', 'alevel' ),
			'description'             => esc_html__( 'Етапи курсу за програмою', 'alevel' ),
			'base'                    => 'alv_course_program_accordion',
			'category'                => __( 'Alevel', 'alevel' ),
			'show_settings_on_create' => false,
			'icon'                    => ALV_URL_PATH . '/assets/icons/bars-staggered-solid.svg',
			'params'                  => [
				[
					'type'       => 'param_group',
					'value'      => '',
					'heading'    => __( 'Етапи курсу', 'alevel' ),
					'param_name' => 'course_accordion',
					'params'     => [
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Назва етапу курсу', 'alevel' ),
							'param_name' => 'name_step_course',
						],
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Заголовок етапу', 'alevel' ),
							'param_name' => 'title_step',
						],
						[
							'type'       => 'colorpicker',
							'value'      => '',
							'heading'    => __( 'Колір модуля', 'alevel' ),
							'param_name' => 'color_step',
						],
						[
							'type'       => 'param_group',
							'value'      => '',
							'heading'    => __( 'Назва етапу', 'alevel' ),
							'param_name' => 'time_steps',
							'params'     => [
								[
									'type'       => 'textfield',
									'value'      => '',
									'heading'    => __( 'Кількість годин', 'alevel' ),
									'param_name' => 'steps_time',
								],
							],
						],
						[

							'type'       => 'param_group',
							'value'      => '',
							'heading'    => __( 'Список тем', 'alevel' ),
							'param_name' => 'module_topics',
							'params'     => [
								[
									'type'       => 'textfield',
									'value'      => '',
									'heading'    => __( 'Назва пункту програми', 'alevel' ),
									'param_name' => 'topic_name',
								],
								[
									'type'       => 'param_group',
									'value'      => '',
									'heading'    => __( 'Список програми', 'alevel' ),
									'param_name' => 'listing_description_step',
									'params'     => [
										[
											'type'       => 'textfield',
											'value'      => '',
											'heading'    => __( 'Назва пункту програми', 'alevel' ),
											'param_name' => 'step_item',
										],
									],
								],
							],
						],
					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Кастомний css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Варіанти дизайну', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::ALV_DIR_PATH . '/WpBakery/template/CourseProgramAccordion/template.php';

		return ob_get_clean();
	}

}
