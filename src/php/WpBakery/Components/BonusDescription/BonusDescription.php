<?php
/**
 * Bonus description block.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\WpBakery\Components\BonusDescription;

use Alevel\Main;

/**
 * BonusDescription class file.
 */
class BonusDescription {
	/**
	 * BonusDescription construct.
	 */
	public function __construct() {
		add_shortcode( 'alv_bonus_description', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'alv_bonus_description', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Опис бонусу', 'alevel' ),
			'description'             => esc_html__( 'Опис бонусу', 'alevel' ),
			'base'                    => 'alv_bonus_description',
			'category'                => __( 'Alevel', 'alevel' ),
			'show_settings_on_create' => false,
			'icon'                    => ALV_URL_PATH . '/assets/icons/money-bill-trend-up-solid.svg',
			'params'                  => [
				[
					'type'       => 'attach_image',
					'value'      => '',
					'heading'    => __( 'Картинка', 'alevel' ),
					'param_name' => 'image',
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Заголовок', 'alevel' ),
					'param_name' => 'title',
				],
				[
					'type'       => 'textarea',
					'value'      => '',
					'heading'    => __( 'Під заголовок', 'alevel' ),
					'param_name' => 'sub_title',
				],
				[
					'type'       => 'textarea',
					'value'      => '',
					'heading'    => __( 'Текст', 'alevel' ),
					'param_name' => 'description',
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Кастомний css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Варіанти дизайну', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::ALV_DIR_PATH . '/WpBakery/template/BonusDescription/template.php';

		return ob_get_clean();
	}
}
