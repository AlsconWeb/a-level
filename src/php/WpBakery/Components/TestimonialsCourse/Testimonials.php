<?php
/**
 * Testimonials by course.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\WpBakery\Components\TestimonialsCourse;

use Alevel\Main;
use WP_Query;

/**
 * Testimonials class file.
 */
class Testimonials {
	/**
	 * Testimonials construct.
	 */
	public function __construct() {
		add_shortcode( 'alv_testimonials_course', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'alv_testimonials_course', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Відгуки про крус', 'alevel' ),
			'description'             => esc_html__( 'Відгуки про курс студентів', 'alevel' ),
			'base'                    => 'alv_testimonials_course',
			'category'                => __( 'Alevel', 'alevel' ),
			'show_settings_on_create' => false,
			'icon'                    => ALV_URL_PATH . '/assets/icons/comments-solid.svg',
			'params'                  => [
				[
					'type'       => 'dropdown',
					'value'      => [
						__( 'Використовується на сторінці курсу', 'alevel' )   => 'true',
						__( 'Використовується не в сторінки курсу', 'alevel' ) => 'false',
					],
					'heading'    => __( 'Де використовується блок відгуків', 'alevel' ),
					'param_name' => 'where_use',
				],
				[
					'type'       => 'dropdown',
					'value'      => [
						__( 'Звичайний відгук', 'alevel' ) => 'regular',
						__( 'Відгук від фірми', 'alevel' ) => 'firm',
						__( 'Історія успіху', 'alevel' )   => 'history',
					],
					'heading'    => __( 'Тип відгуку', 'alevel' ),
					'param_name' => 'type_review',
					'dependency' => [
						'element' => 'where_use',
						'value'   => [ 'false' ],
					],
				],
				[
					'type'       => 'textfield',
					'value'      => 5,
					'heading'    => __( 'Кількість виведених відгуків', 'alevel' ),
					'param_name' => 'count_output',
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Кнопка бiльше вiдгукiв', 'alevel' ),
					'param_name' => 'more_button',
					'dependency' => [
						'element' => 'where_use',
						'value'   => [ 'true' ],
					],
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Кнопка бiльше вiдгукiв посилання', 'alevel' ),
					'param_name' => 'more_button_link',
					'dependency' => [
						'element' => 'where_use',
						'value'   => [ 'true' ],
					],
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Кнопка бiльше вiдгукiв', 'alevel' ),
					'param_name' => 'add_text_button',
					'dependency' => [
						'element' => 'where_use',
						'value'   => [ 'true' ],
					],
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Кнопка бiльше вiдгукiв посилання', 'alevel' ),
					'param_name' => 'add_link_button',
					'dependency' => [
						'element' => 'where_use',
						'value'   => [ 'true' ],
					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Кастомний css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Варіанти дизайну', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::ALV_DIR_PATH . '/WpBakery/template/Testimonials/template.php';

		return ob_get_clean();
	}

	private function get_all_courses() {
		$arg = [
			'post_type'      => 'courses',
			'posts_per_page' => - 1,
			'post_status'    => 'publish',
		];

		$courses_select_array = [];
		$courses              = new WP_Query();
		if ( ! $courses->the_post() ) {
			return [];
		}

		foreach ( $courses->posts as $post ) {
			$courses_select_array[ $post->post_title ] = $post->ID;
		}
	}
}
