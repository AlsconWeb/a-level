<?php
/**
 * FAQ block.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\WpBakery\Components\FAQ;

use Alevel\Main;

/**
 * FAQ Block class file.
 */
class FAQBlock {
	/**
	 * FAQBlock construct
	 */
	public function __construct() {
		add_shortcode( 'alv_faq', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'alv_faq', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'FAQ', 'alevel' ),
			'description'             => esc_html__( 'FAQ', 'alevel' ),
			'base'                    => 'alv_faq',
			'category'                => __( 'Alevel', 'alevel' ),
			'show_settings_on_create' => false,
			'icon'                    => ALV_URL_PATH . '/assets/icons/rectangle-list-solid.svg',
			'params'                  => [
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Заголовок', 'alevel' ),
					'param_name' => 'title',
				],
				[
					'type'       => 'param_group',
					'value'      => '',
					'heading'    => __( 'Таби', 'alevel' ),
					'param_name' => 'tabs',
					'params'     => [
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Назва', 'alevel' ),
							'param_name' => 'tab_name',
						],
						[
							'type'       => 'param_group',
							'value'      => '',
							'heading'    => __( 'Відповіді', 'alevel' ),
							'param_name' => 'answers',
							'params'     => [
								[
									'type'       => 'textfield',
									'value'      => '',
									'heading'    => __( 'Заголовок', 'alevel' ),
									'param_name' => 'title',
								],
								[
									'type'       => 'textarea',
									'value'      => '',
									'heading'    => __( 'Опис', 'alevel' ),
									'param_name' => 'answers_text',
								],
							],
						],
					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Кастомний css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Варіанти дизайну', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::ALV_DIR_PATH . '/WpBakery/template/FAQBlock/template.php';

		return ob_get_clean();
	}
}
