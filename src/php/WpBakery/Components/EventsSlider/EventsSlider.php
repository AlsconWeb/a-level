<?php
/**
 * Events Slider.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\WpBakery\Components\EventsSlider;

use Alevel\Main;

/**
 * EventsSlider class file.
 */
class EventsSlider {
	/**
	 * EventPost construct.
	 */
	public function __construct() {
		add_shortcode( 'alv_event_slider', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'alv_event_slider', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Події слайдер', 'alevel' ),
			'description'             => esc_html__( 'Події слайдер', 'alevel' ),
			'base'                    => 'alv_event_slider',
			'category'                => __( 'Alevel', 'alevel' ),
			'show_settings_on_create' => false,
			'icon'                    => ALV_URL_PATH . '/assets/icons/window-restore-solid.svg',
			'params'                  => [
				[
					'type'       => 'textfield',
					'value'      => 4,
					'heading'    => __( 'Кількість виведених відгуків', 'alevel' ),
					'param_name' => 'count_output',
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Кастомний css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Варіанти дизайну', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::ALV_DIR_PATH . '/WpBakery/template/EventsSlider/template.php';

		return ob_get_clean();
	}
}
