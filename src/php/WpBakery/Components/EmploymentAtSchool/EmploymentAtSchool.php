<?php
/**
 * Employment at our school.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\WpBakery\Components\EmploymentAtSchool;

use Alevel\Main;

/**
 * EmploymentAtSchool class file.
 */
class EmploymentAtSchool {
	/**
	 * EmploymentAtSchool construct.
	 */
	public function __construct() {
		add_shortcode( 'alv_employment_at_school', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'alv_employment_at_school', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Працевлаштування в нашій школі', 'alevel' ),
			'description'             => esc_html__( 'Працевлаштування в нашій школі', 'alevel' ),
			'base'                    => 'alv_employment_at_school',
			'category'                => __( 'Alevel', 'alevel' ),
			'show_settings_on_create' => false,
			'icon'                    => ALV_URL_PATH . '/assets/icons/person-walking-luggage-solid.svg',
			'params'                  => [
				[
					'type'       => 'param_group',
					'value'      => '',
					'heading'    => __( 'Кроки працевлаштування ', 'alevel' ),
					'param_name' => 'employment',
					'params'     => [
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Заголовок', 'alevel' ),
							'param_name' => 'title',
						],
						[
							'type'       => 'attach_image',
							'value'      => '',
							'heading'    => __( 'Значок', 'alevel' ),
							'param_name' => 'icon',
						],
						[
							'type'       => 'textarea',
							'value'      => '',
							'heading'    => __( 'Текст', 'alevel' ),
							'param_name' => 'text',
						],
					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Кастомний css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Варіанти дизайну', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::ALV_DIR_PATH . '/WpBakery/template/EmploymentAtSchool/template.php';

		return ob_get_clean();
	}
}
