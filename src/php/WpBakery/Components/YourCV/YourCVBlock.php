<?php
/**
 * YourCVBlock by course.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\WpBakery\Components\YourCV;

use Alevel\Main;

/**
 * YourCVBlock class file.
 */
class YourCVBlock {
	/**
	 * YourCVBlock construct.
	 */
	public function __construct() {
		add_shortcode( 'alv_your_cv', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'alv_your_cv', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Ваше резюме', 'alevel' ),
			'description'             => esc_html__( 'Ваше резюме після проходження курсу', 'alevel' ),
			'base'                    => 'alv_your_cv',
			'category'                => __( 'Alevel', 'alevel' ),
			'show_settings_on_create' => false,
			'icon'                    => ALV_URL_PATH . '/assets/icons/file-invoice-solid.svg',
			'params'                  => [
				[
					'type'       => 'attach_image',
					'value'      => '',
					'heading'    => __( 'Значок', 'alevel' ),
					'param_name' => 'icon',
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Посада', 'alevel' ),
					'param_name' => 'position',
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Зарплата вiд', 'alevel' ),
					'param_name' => 'salary_from',
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Графік работы', 'alevel' ),
					'param_name' => 'work_schedule',
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Освіта', 'alevel' ),
					'param_name' => 'education',
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Кастомний css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Варіанти дизайну', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::ALV_DIR_PATH . '/WpBakery/template/YourCVBlock/template.php';

		return ob_get_clean();
	}
}
