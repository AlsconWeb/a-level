<?php
/**
 * Rating School block.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\WpBakery\Components\RatingsSchool;

use Alevel\Main;

/**
 * RatingsSchool class file.
 */
class RatingsSchool {
	/**
	 * RatingsSchool construct.
	 */
	public function __construct() {
		add_shortcode( 'alv_ratings_school', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'alv_ratings_school', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Блок рейтинг школи', 'alevel' ),
			'description'             => esc_html__( 'Блок рейтинг школи', 'alevel' ),
			'base'                    => 'alv_ratings_school',
			'category'                => __( 'Alevel', 'alevel' ),
			'show_settings_on_create' => false,
			'icon'                    => ALV_URL_PATH . '/assets/icons/star-solid.svg',
			'params'                  => [
				[
					'type'       => 'param_group',
					'value'      => '',
					'heading'    => __( 'Інформація про школу', 'alevel' ),
					'param_name' => 'items',
					'params'     => [
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Заголовок', 'alevel' ),
							'param_name' => 'title',
						],
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Опис', 'alevel' ),
							'param_name' => 'description',
						],
					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Кастомний css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Варіанти дизайну', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::ALV_DIR_PATH . '/WpBakery/template/RatingsSchool/template.php';

		return ob_get_clean();
	}
}
