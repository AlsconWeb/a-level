<?php
/**
 * History Stores Steps block.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\WpBakery\Components\HistoryStoresSteps;

use Alevel\Main;

/**
 * HistoryStoresSteps class file.
 */
class HistoryStoresSteps {
	/**
	 * HistoryStoresSteps construct.
	 */
	public function __construct() {
		add_shortcode( 'alv_history_stores_steps', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'alv_history_stores_steps', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Історія по кроками', 'alevel' ),
			'description'             => esc_html__( 'Історія по кроками', 'alevel' ),
			'base'                    => 'alv_history_stores_steps',
			'category'                => __( 'Alevel', 'alevel' ),
			'show_settings_on_create' => false,
			'icon'                    => ALV_URL_PATH . '/assets/icons/shoe-prints-solid.svg',
			'params'                  => [
				[
					'type'       => 'param_group',
					'value'      => '',
					'heading'    => __( 'Картинки', 'alevel' ),
					'param_name' => 'steps',
					'params'     => [
						[
							'type'       => 'attach_image',
							'value'      => '',
							'heading'    => __( 'Картинка', 'alevel' ),
							'param_name' => 'icon',
						],
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Заголовок', 'alevel' ),
							'param_name' => 'title',
						],
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Опис', 'alevel' ),
							'param_name' => 'description',
						],
					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Кастомний css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Варіанти дизайну', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::ALV_DIR_PATH . '/WpBakery/template/HistoryStoresSteps/template.php';

		return ob_get_clean();
	}
}
