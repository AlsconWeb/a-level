<?php
/**
 * FAQ Course block.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\WpBakery\Components\FAQCourse;

use Alevel\Main;

/**
 * FAQCourse class file.
 */
class FAQCourse {
	/**
	 * FAQCourse construct.
	 */
	public function __construct() {
		add_shortcode( 'alv_faq_course', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'alv_faq_course', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'FAQ обучение', 'alevel' ),
			'description'             => esc_html__( 'FAQ обучение', 'alevel' ),
			'base'                    => 'alv_faq_course',
			'category'                => __( 'Alevel', 'alevel' ),
			'show_settings_on_create' => false,
			'icon'                    => ALV_URL_PATH . '/assets/icons/list-check-solid.svg',
			'params'                  => [
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Заголовок', 'alevel' ),
					'param_name' => 'title',
				],
				[
					'type'       => 'textarea',
					'value'      => '',
					'heading'    => __( 'Під заголовок', 'alevel' ),
					'param_name' => 'sub_title',
				],
				[
					'type'       => 'param_group',
					'value'      => '',
					'heading'    => __( 'Таби', 'alevel' ),
					'param_name' => 'tabs',
					'params'     => [
						[
							'type'       => 'attach_image',
							'value'      => '',
							'heading'    => __( 'Картинка', 'alevel' ),
							'param_name' => 'image',
						],
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Назва етапу', 'alevel' ),
							'param_name' => 'stage_name',
						],
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Заголовок', 'alevel' ),
							'param_name' => 'title',
						],
						[
							'type'       => 'textarea',
							'value'      => '',
							'heading'    => __( 'Опис', 'alevel' ),
							'param_name' => 'description',
						],
					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Кастомний css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Варіанти дизайну', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::ALV_DIR_PATH . '/WpBakery/template/FAQCourse/template.php';

		return ob_get_clean();
	}
}
