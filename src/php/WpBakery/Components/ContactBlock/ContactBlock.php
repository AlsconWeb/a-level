<?php
/**
 * Contact Block.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\WpBakery\Components\ContactBlock;

use Alevel\Main;

/**
 * ContactBlock class file.
 */
class ContactBlock {
	/**
	 * ContactBlock construct.
	 */
	public function __construct() {
		add_shortcode( 'alv_contact_block', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'alv_contact_block', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Контакт блок', 'alevel' ),
			'description'             => esc_html__( 'Контакт блок', 'alevel' ),
			'base'                    => 'alv_contact_block',
			'category'                => __( 'Alevel', 'alevel' ),
			'show_settings_on_create' => false,
			'icon'                    => ALV_URL_PATH . '/assets/icons/address-card-solid.svg',
			'params'                  => [
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Заголовок', 'alevel' ),
					'param_name' => 'title',
				],
				[
					'type'       => 'textarea',
					'value'      => carbon_get_theme_option( 'alv_address' ) ?? '',
					'heading'    => __( 'Адреса', 'alevel' ),
					'param_name' => 'address',
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Адреса посилання', 'alevel' ),
					'param_name' => 'address_link',
				],
				[
					'type'       => 'param_group',
					'value'      => '',
					'heading'    => __( 'Час роботи', 'alevel' ),
					'param_name' => 'work_time',
					'params'     => [
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Дні тижня текст', 'alevel' ),
							'param_name' => 'days_text',
						],
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Час роботи', 'alevel' ),
							'param_name' => 'days_work_time',
						],
					],
				],
				[
					'type'       => 'param_group',
					'value'      => '',
					'heading'    => __( 'Контактні телефони', 'alevel' ),
					'param_name' => 'contact_phone',
					'params'     => [
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Телефон', 'alevel' ),
							'param_name' => 'phone',
						],
					],
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Електронна пошта', 'alevel' ),
					'param_name' => 'email',
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Кастомний css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Варіанти дизайну', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::ALV_DIR_PATH . '/WpBakery/template/ContactBlock/template.php';

		return ob_get_clean();
	}
}
