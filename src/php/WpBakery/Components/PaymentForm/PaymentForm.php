<?php
/**
 * Payment Form from course.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\WpBakery\Components\PaymentForm;

use Alevel\Main;

/**
 * PaymentForm class file.
 */
class PaymentForm {
	/**
	 * PaymentForm construct.
	 */
	public function __construct() {
		add_shortcode( 'alv_payment_form', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'alv_payment_form', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Форма оплати', 'alevel' ),
			'description'             => esc_html__( 'Форма оплати', 'alevel' ),
			'base'                    => 'alv_payment_form',
			'category'                => __( 'Alevel', 'alevel' ),
			'show_settings_on_create' => false,
			'icon'                    => ALV_URL_PATH . '/assets/icons/money-bill-solid.svg',
			'params'                  => [
				[
					'type'       => 'dropdown',
					'value'      =>
						[
							__( 'Оплата за весь курс', 'alevel' ) => 'single_payment',
							__( 'Оплата частинами', 'alevel' )    => 'part_payment',
						],
					'heading'    => __( 'Тип форми', 'alevel' ),
					'param_name' => 'form_type',
					'group'      => esc_html__( 'Основні налаштування', 'alevel' ),
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Заголовок акции', 'alevel' ),
					'param_name' => 'sales_title',
					'group'      => esc_html__( 'Акція', 'alevel' ),
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Під заголовок акции', 'alevel' ),
					'param_name' => 'sales_sub_title',
					'group'      => esc_html__( 'Акція', 'alevel' ),
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Банер заголовок', 'alevel' ),
					'param_name' => 'course_sales_banner_title',
					'group'      => esc_html__( 'Акція', 'alevel' ),
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Банер текст', 'alevel' ),
					'param_name' => 'course_sales_banner_text',
					'group'      => esc_html__( 'Акція', 'alevel' ),
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Ціна курсу', 'alevel' ),
					'param_name' => 'course_prise',
					'group'      => esc_html__( 'Сплата за весь курс', 'alevel' ),
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Ціна курсу з акції', 'alevel' ),
					'param_name' => 'course_sales_prise',
					'group'      => esc_html__( 'Сплата за весь курс', 'alevel' ),
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Заголовок', 'alevel' ),
					'param_name' => 'course_one_payment_title',
					'group'      => esc_html__( 'Сплата за весь курс', 'alevel' ),
				],
				[
					'type'       => 'textarea',
					'value'      => '',
					'heading'    => __( 'Текст оплати', 'alevel' ),
					'param_name' => 'course_one_payment_text',
					'group'      => esc_html__( 'Сплата за весь курс', 'alevel' ),
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Текс перед сумою', 'alevel' ),
					'param_name' => 'course_one_payment_description',
					'group'      => esc_html__( 'Сплата за весь курс', 'alevel' ),
				],
				[
					'type'       => 'param_group',
					'value'      => '',
					'heading'    => __( 'Блок опису', 'alevel' ),
					'param_name' => 'course_ona_payment_description_list',
					'group'      => esc_html__( 'Сплата за весь курс', 'alevel' ),
					'params'     => [
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Текст', 'alevel' ),
							'param_name' => 'text',
						],
					],
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Ціна курсу', 'alevel' ),
					'param_name' => 'course_prise_school',
					'group'      => esc_html__( 'Розстрочка вiд школи', 'alevel' ),
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Ціна курсу з акції', 'alevel' ),
					'param_name' => 'course_sales_school',
					'group'      => esc_html__( 'Розстрочка вiд школи', 'alevel' ),
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Заголовок', 'alevel' ),
					'param_name' => 'course_school_installment_title',
					'group'      => esc_html__( 'Розстрочка вiд школи', 'alevel' ),
				],
				[
					'type'       => 'textarea',
					'value'      => '',
					'heading'    => __( 'Текст оплати', 'alevel' ),
					'param_name' => 'course_school_installment_text',
					'group'      => esc_html__( 'Розстрочка вiд школи', 'alevel' ),
				],
				[
					'type'       => 'textarea',
					'value'      => '',
					'heading'    => __( 'Текст опис оплати', 'alevel' ),
					'param_name' => 'course_school_installment_description',
					'group'      => esc_html__( 'Розстрочка вiд школи', 'alevel' ),
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Текст першого платежу', 'alevel' ),
					'param_name' => 'course_school_installment_one_payment',
					'group'      => esc_html__( 'Розстрочка вiд школи', 'alevel' ),
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Текст другого платежу', 'alevel' ),
					'param_name' => 'course_school_installment_second_payment',
					'group'      => esc_html__( 'Розстрочка вiд школи', 'alevel' ),
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Ціна курсу', 'alevel' ),
					'param_name' => 'course_prise_mono',
					'group'      => esc_html__( 'Розстрочка від моно банку', 'alevel' ),
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Ціна курсу з акції', 'alevel' ),
					'param_name' => 'course_sales_mono',
					'group'      => esc_html__( 'Розстрочка від моно банку', 'alevel' ),
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Заголовок', 'alevel' ),
					'param_name' => 'course_mono_title',
					'group'      => esc_html__( 'Розстрочка від моно банку', 'alevel' ),
				],
				[
					'type'       => 'textarea',
					'value'      => '',
					'heading'    => __( 'Текст оплати', 'alevel' ),
					'param_name' => 'course_mono_text',
					'group'      => esc_html__( 'Розстрочка від моно банку', 'alevel' ),
				],
				[
					'type'       => 'textarea',
					'value'      => '',
					'heading'    => __( 'Текст опис оплати', 'alevel' ),
					'param_name' => 'course_mono_description',
					'group'      => esc_html__( 'Розстрочка від моно банку', 'alevel' ),
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Кастомний css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Варіанти дизайну', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::ALV_DIR_PATH . '/WpBakery/template/PaymentForm/template.php';

		return ob_get_clean();
	}
}
