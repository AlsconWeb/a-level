<?php
/**
 * Anchorage Menu short code.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\WpBakery\Components\AnchorageMenu;

use Alevel\Main;

/**
 * AnchorageMenu class file.
 */
class AnchorageMenu {

	/**
	 * AnchorageMenu construct.
	 */
	public function __construct() {
		add_shortcode( 'alv_anchorage_menu', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'alv_anchorage_menu', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Якорне меню', 'alevel' ),
			'description'             => esc_html__( 'Якорне меню для секцій і блоків', 'alevel' ),
			'base'                    => 'alv_anchorage_menu',
			'category'                => __( 'Alevel', 'alevel' ),
			'show_settings_on_create' => false,
			'icon'                    => ALV_URL_PATH . '/assets/icons/anchor-solid.svg',
			'params'                  => [
				[
					'type'       => 'param_group',
					'value'      => '',
					'heading'    => __( 'Якоря', 'alevel' ),
					'param_name' => 'anchorage',
					'params'     => [
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Текст якірного посилання', 'alevel' ),
							'param_name' => 'anchorage_text',
						],
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Якоря', 'alevel' ),
							'param_name' => 'anchorage_link',
						],
					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Кастомний css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Варіанти дизайну', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::ALV_DIR_PATH . '/WpBakery/template/AnchorageMenu/template.php';

		return ob_get_clean();
	}

}
