<?php
/**
 * Teachers Course short code.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\WpBakery\Components\TeachersCourse;

use Alevel\Main;

/**
 * TeachersCourse class file.
 */
class TeachersCourse {
	/**
	 * TeachersCourse construct.
	 */
	public function __construct() {
		add_shortcode( 'alv_teachers_course', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'alv_teachers_course', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Педагоги на курсі', 'alevel' ),
			'description'             => esc_html__( 'Педагогічний склад курсу', 'alevel' ),
			'base'                    => 'alv_teachers_course',
			'category'                => __( 'Alevel', 'alevel' ),
			'show_settings_on_create' => false,
			'icon'                    => ALV_URL_PATH . '/assets/icons/chalkboard-user-solid.svg',
			'params'                  => [
				[
					'type'       => 'dropdown',
					'value'      => [
						__( 'Використовується на сторінці курсу', 'alevel' )   => 'true',
						__( 'Використовується не в сторінки курсу', 'alevel' ) => 'false',
					],
					'heading'    => __( 'Де використовується блок вчителів', 'alevel' ),
					'param_name' => 'where_use',
				],
				[
					'type'       => 'textfield',
					'value'      => 5,
					'heading'    => __( 'Кількість виведених педагогів', 'alevel' ),
					'param_name' => 'count_output',
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Кнопка стати ментором', 'alevel' ),
					'param_name' => 'text_button',
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Кнопка стати ментором посилання', 'alevel' ),
					'param_name' => 'link_button',
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Кастомний css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Варіанти дизайну', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::ALV_DIR_PATH . '/WpBakery/template/TeachersCourse/template.php';

		return ob_get_clean();
	}
}
