<?php
/**
 * History Block.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\WpBakery\Components\HistoryBlock;

use Alevel\Main;

/**
 * HistoryBlock class file.
 */
class HistoryBlock {
	/**
	 * HistoryBlock construct
	 */
	public function __construct() {
		add_shortcode( 'alv_history_block', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'alv_history_block', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Блок історія успіху', 'alevel' ),
			'description'             => esc_html__( 'Блок історія успіху', 'alevel' ),
			'base'                    => 'alv_history_block',
			'category'                => __( 'Alevel', 'alevel' ),
			'show_settings_on_create' => false,
			'icon'                    => ALV_URL_PATH . '/assets/icons/timeline-solid.svg',
			'params'                  => [
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Заголовок', 'alevel' ),
					'param_name' => 'title',
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Підзаголовок', 'alevel' ),
					'param_name' => 'sub_title',
				],
				[
					'type'       => 'attach_image',
					'value'      => '',
					'heading'    => __( 'Картинка', 'alevel' ),
					'param_name' => 'image',
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Текст', 'alevel' ),
					'param_name' => 'text',
				],
				[
					'type'       => 'vc_link',
					'value'      => '',
					'heading'    => __( 'Посилання', 'alevel' ),
					'param_name' => 'link',
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Кастомний css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Варіанти дизайну', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::ALV_DIR_PATH . '/WpBakery/template/HistoryBlock/template.php';

		return ob_get_clean();
	}
}
