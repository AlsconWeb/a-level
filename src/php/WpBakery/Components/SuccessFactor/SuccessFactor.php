<?php
/**
 * Factors of your success.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\WpBakery\Components\SuccessFactor;

use Alevel\Main;

/**
 * SuccessFactor class file.
 */
class SuccessFactor {
	/**
	 * SuccessFactor construct.
	 */
	public function __construct() {
		add_shortcode( 'alv_success_factor', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'alv_success_factor', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Фактор успiху', 'alevel' ),
			'description'             => esc_html__( 'Фактор успiху', 'alevel' ),
			'base'                    => 'alv_success_factor',
			'category'                => __( 'Alevel', 'alevel' ),
			'show_settings_on_create' => false,
			'icon'                    => ALV_URL_PATH . '/assets/icons/square-check-solid.svg',
			'params'                  => [
				[
					'type'       => 'param_group',
					'value'      => '',
					'heading'    => __( 'Таби', 'alevel' ),
					'param_name' => 'items',
					'params'     => [
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Назва іконки', 'alevel' ),
							'param_name' => 'icon_name',
						],
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Заголовок', 'alevel' ),
							'param_name' => 'title',
						],
						[
							'type'       => 'textarea',
							'value'      => '',
							'heading'    => __( 'Опис', 'alevel' ),
							'param_name' => 'description',
						],
					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Кастомний css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Варіанти дизайну', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::ALV_DIR_PATH . '/WpBakery/template/SuccessFactor/template.php';

		return ob_get_clean();
	}
}