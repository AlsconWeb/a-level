<?php
/**
 * Event Post.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\WpBakery\Components\EventPost;

use Alevel\Main;

/**
 * EventPost class file.
 */
class EventPost {
	/**
	 * EventPost construct.
	 */
	public function __construct() {
		add_shortcode( 'alv_event_post', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'alv_event_post', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Події', 'alevel' ),
			'description'             => esc_html__( 'Події', 'alevel' ),
			'base'                    => 'alv_event_post',
			'category'                => __( 'Alevel', 'alevel' ),
			'show_settings_on_create' => false,
			'icon'                    => ALV_URL_PATH . '/assets/icons/newspaper-solid.svg',
			'params'                  => [
				[
					'type'       => 'textfield',
					'value'      => 4,
					'heading'    => __( 'Кількість виведених відгуків', 'alevel' ),
					'param_name' => 'count_output',
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Заголовок', 'alevel' ),
					'param_name' => 'title',
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Кастомний css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Варіанти дизайну', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::ALV_DIR_PATH . '/WpBakery/template/EventPost/template.php';

		return ob_get_clean();
	}
}
