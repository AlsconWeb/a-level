<?php
/**
 * History Images block.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\WpBakery\Components\HistoryImages;

use Alevel\Main;

/**
 * HistoryImages class file.
 */
class HistoryImages {
	/**
	 * HistoryImages construct.
	 */
	public function __construct() {
		add_shortcode( 'alv_history_images', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'alv_history_images', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Історія школи в картинках', 'alevel' ),
			'description'             => esc_html__( 'Історія школи в картинках', 'alevel' ),
			'base'                    => 'alv_history_images',
			'category'                => __( 'Alevel', 'alevel' ),
			'show_settings_on_create' => false,
			'icon'                    => ALV_URL_PATH . '/assets/icons/images-regular.svg',
			'params'                  => [
				[
					'type'       => 'param_group',
					'value'      => '',
					'heading'    => __( 'Картинки', 'alevel' ),
					'param_name' => 'images',
					'params'     => [
						[
							'type'       => 'attach_image',
							'value'      => '',
							'heading'    => __( 'Картинка', 'alevel' ),
							'param_name' => 'image',
						],
					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Кастомний css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Варіанти дизайну', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::ALV_DIR_PATH . '/WpBakery/template/HistoryImages/template.php';

		return ob_get_clean();
	}
}
