<?php
/**
 * Logo Slider.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\WpBakery\Components\LogoSlider;

use Alevel\Main;

/**
 * LogoSlider class file.
 */
class LogoSlider {
	/**
	 * LogoSlider construct.
	 */
	public function __construct() {
		add_shortcode( 'alv_logo_slider', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'alv_logo_slider', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Слайдер логотипи', 'alevel' ),
			'description'             => esc_html__( 'Слайдер логотипи', 'alevel' ),
			'base'                    => 'alv_logo_slider',
			'category'                => __( 'Alevel', 'alevel' ),
			'show_settings_on_create' => false,
			'icon'                    => ALV_URL_PATH . '/assets/icons/photo-film-solid.svg',
			'params'                  => [
				[
					'type'       => 'param_group',
					'value'      => '',
					'heading'    => __( 'Логотипи', 'alevel' ),
					'param_name' => 'logos',
					'params'     => [
						[
							'type'       => 'attach_image',
							'value'      => '',
							'heading'    => __( 'Логотип', 'alevel' ),
							'param_name' => 'slide_image',
						],
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Посилання на компанію', 'alevel' ),
							'param_name' => 'company_link',
						],
					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Кастомний css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Варіанти дизайну', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::ALV_DIR_PATH . '/WpBakery/template/LogoSlider/template.php';

		return ob_get_clean();
	}
}
