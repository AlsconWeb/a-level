<?php
/**
 * Key crm api.
 *
 * @package iwpdev/alevel.
 */

namespace Alevel\KeyCrm\API;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * KeyCrmApi class file.
 */
class KeyCrmApi {

	/**
	 * CRM Api key.
	 */
	public const CRM_API_KEY = 'ODE3MTRkOGM4OGVlMWQ2Y2Q2NzQ4NTlkMDgyZjcxY2NiNDgyMjc1Zg';

	/**
	 * Operation name.
	 *
	 * @var string
	 */
	private string $operation_name;

	/**
	 * CRM routes.
	 */
	private const CRM_OPERATION_ROUT = [
		'lead'           => '/v1/leads',
		'order'          => '/v1/order',
		'offers'         => '/v1/offers',
		'storage'        => '/v1/storage',
		'storage_upload' => '/v1/storage/upload',
	];

	/**
	 * Operation rout url.
	 *
	 * @var string
	 */
	private string $operation_url;


	/**
	 * KeyCrmApi construct.
	 */
	public function __construct() {

	}

	/**
	 * Get client.
	 *
	 * @return Client
	 */
	private function get_client(): Client {
		$base_url = 'https://openapi.keycrm.app/';

		return new Client(
			[
				'base_uri' => $base_url,
				'timeout'  => 2.0,
				'headers'  => [
					'accept'        => 'application/json',
					'Authorization' => 'Bearer ' . self::CRM_API_KEY,
					'Content-Type'  => 'application/json',
				],
			]
		);
	}

	/**
	 * Set operation name.
	 *
	 * Valid values:  lead|order|offers|storage|storage_upload
	 *
	 * @param string $operation_name Operation name.
	 */
	public function set_operation_name( string $operation_name ): void {
		$this->operation_name = $operation_name;
		$this->set_operation_url();
	}


	/**
	 * Set url rout.
	 *
	 * @return void
	 */
	private function set_operation_url(): void {
		$this->operation_url = self::CRM_OPERATION_ROUT[ $this->operation_name ];
	}

	/**
	 * Create lead.
	 *
	 * @param array $data data array.
	 *
	 * @return array|string
	 */
	public function create_lead( array $data ) {

		$description_candidate = $this->generate_body_message( $data );
		$lead_contact          = $this->generate_contact_liad( $data );
		$lead_data             = [
			'title'           => $data['title'] . ': ' . isset( $data['course_name'] ) ?: '',
			'source_id'       => 3,
			'manager_id'      => 3,
			'pipeline_id'     => $data['pipeline_id'],
			'contact'         => $lead_contact,
			'utm_source'      => 'a-level site',
			'manager_comment' => $description_candidate,
		];
		$data                  = wp_json_encode( $lead_data, JSON_PRETTY_PRINT );

		$client = $this->get_client();
		try {
			$response = $client->request( 'POST', $this->operation_url, [ 'body' => $data ] );

			return $response->getBody()->getContents();
		} catch ( GuzzleException $e ) {
			return [ 'error' => $e->getMessage() ];
		}
	}

	/**
	 * Generate contact array from crm.
	 *
	 * @param array $data Form data.
	 *
	 * @return array
	 */
	private function generate_contact_liad( array $data ) {

		$lead_contact = [];
		$full_name    = $data['first_name'] . ' ' . isset( $data['last_name'] ) ?: '';
		switch ( $data['form_name'] ) {
			case 'course_payment_from':
				$lead_contact = [
					'full_name' => $full_name,
					'email'     => $data['email'],
					'phone'     => '+38' . $data['phone'],
					'LD_1001'   => isset( $data['method_payment'] ) ? $data['method_payment'] : null,
					'LD_1002'   => isset( $data['from_count_payment'] ) ? $data['from_count_payment'] : null,
				];
				break;
			case 'new_teacher':
			case 'review_form':
			case 'register_to_event':
			case 'register_on_partner':
			case 'call_back_modal':
			case 'call_back_form':
				$lead_contact = [
					'full_name' => $full_name,
					'email'     => $data['email'],
					'phone'     => '+38' . $data['phone'],
				];
				break;
			case 'consultation_on_course':
				$lead_contact = [
					'full_name' => $full_name,
					'email'     => $data['email'],
					'phone'     => '+38' . $data['phone'],
					'name'      => $data['course_name'],
				];
				break;
		}

		return $lead_contact;
	}

	/**
	 * Generate body message.
	 *
	 * @param array $data form data.
	 *
	 * @return string
	 */
	public function generate_body_message( array $data ): string {
		$site_name = is_multisite() ? get_blog_details()->blogname : '';

		switch ( $data['form_name'] ) {
			case 'new_teacher':
				$body = __( 'Назва компанії:', 'alevel' ) . ' ' . $data['company'] . "\n";

				$body .= __( 'Посада в компанії:', 'alevel' ) . ' ' . $data['position'] . "\n";
				$body .= __( 'Посилання на linkedin профіль:', 'alevel' ) . ' ' . $data['linkedin'] . "\n";
				$body .= __( 'Супровідне повідомлення:', 'alevel' ) . ' ' . $data['description'];
				break;
			case 'consultation_on_course':
				$body = __( 'Курс:', 'alevel' ) . ' ' . $data['course_name'] . "\n";

				$body .= __( 'Допис від користувача:', 'alevel' ) . ' ' . $data['description'] . "\n";
				break;
			case 'register_on_partner':
				$body = __( 'Назва компанії:', 'alevel' ) . ' ' . $data['company'] . "\n";

				$body .= __( 'Прiзвище представника:', 'alevel' ) . $data['partner_last_mane'] . "\n";
				$body .= __( 'Email:', 'alevel' ) . $data['email'] . "\n";
				$body .= __( 'Телефон:', 'alevel' ) . $data['phone'] . "\n";
				$body .= __( 'Веб сайт компанії:', 'alevel' ) . $data['partner_web'] . "\n";
				$body .= __( 'Посилання на linkedin профіль:', 'alevel' ) . $data['linkedin'] . "\n";
				$body .= __( 'Супровідне повідомлення:', 'alevel' ) . $data['description'] . "\n";
				break;
			case 'tg_body_theaters':
				$full_name = $data['first_name'] . ' ' . $data['last_name'] ?? '';

				$body = '*' . $data['title'] . ' ' . $data['course_name'] . '*' . "\n";

				$body .= '*' . __( 'Заявка із сайту:', 'alevel' ) . '* ' . $site_name . "\n";
				$body .= '*' . __( 'Назва компанії:', 'alevel' ) . '* ' . $data['company'] . "\n";
				$body .= '*' . __( 'Ім\'я та прізвище:', 'alevel' ) . '* ' . $full_name . "\n";
				$body .= '*' . __( 'Email:', 'alevel' ) . '* ' . $data['email'] . "\n";
				$body .= '*' . __( 'Телефон:', 'alevel' ) . '* ' . $data['phone'] . "\n";
				$body .= '*' . __( 'Посада в компанії:', 'alevel' ) . '* ' . $data['position'] . "\n";
				$body .= '*' . __( 'Посилання на linkedin профіль:', 'alevel' ) . '* ' . $data['linkedin'] . "\n";
				$body .= '*' . __( 'Супровідне повідомлення:', 'alevel' ) . '* ' . $data['description'];
				break;
			case 'tg_body_course':
				$full_name = $data['first_name'] . ' ' . ! empty( $data['last_name'] ) ? $data['last_name'] : '';

				$body = '*' . $data['title'] . ' ' . $data['course_name'] . '*' . "\n";

				$body .= '*' . __( 'Заявка із сайту:', 'alevel' ) . '* ' . $site_name . "\n";
				$body .= '*' . __( 'Ім\'я та прізвище:', 'alevel' ) . '* ' . $full_name . "\n";
				$body .= '*' . __( 'Email:', 'alevel' ) . '* ' . $data['email'] . "\n";
				$body .= '*' . __( 'Телефон:', 'alevel' ) . '* ' . $data['phone'] . "\n";
				$body .= '*' . __( 'Супровідне повідомлення:', 'alevel' ) . '* ' . $data['description'];
				break;
			case 'tg_body_on_partner':
				$name = $data['first_name'];

				$body = '*' . __( 'Назва компанії:', 'alevel' ) . '* ' . $data['company'] . "\n";

				$body .= '*' . __( 'Заявка із сайту:', 'alevel' ) . '* ' . $site_name . "\n";
				$body .= '*' . __( 'Ім\'я та прізвище:', 'alevel' ) . '* ' . $name . "\n";
				$body .= '*' . __( 'Прiзвище представника:', 'alevel' ) . '* ' . $data['partner_last_mane'] . "\n";
				$body .= '*' . __( 'Email:', 'alevel' ) . '* ' . $data['email'] . "\n";
				$body .= '*' . __( 'Телефон:', 'alevel' ) . '* ' . $data['phone'] . "\n";
				$body .= '*' . __( 'Веб сайт компанії:', 'alevel' ) . '* ' . $data['partner_web'] . "\n";
				$body .= '*' . __( 'Посилання на linkedin профіль:', 'alevel' ) . '* ' . $data['linkedin'] . "\n";
				$body .= '*' . __( 'Супровідне повідомлення:', 'alevel' ) . '* ' . $data['description'] . "\n";
				break;
			case 'course_payment_from':

				$body = $data['title'] . "\n";

				$body .= __( 'Email:', 'alevel' ) . $data['email'] . "\n";
				$body .= __( 'Телефон:', 'alevel' ) . $data['phone'] . "\n";
				$body .= __( 'Тип оплати за курс:', 'alevel' ) . $data['method_payment'] . "\n";
				if ( ! empty( $data['from_count_payment'] ) ) {
					$body .= __( 'Кількість місяців на розстрочку моно банку:', 'alevel' ) . $data['from_count_payment'] . "\n";
				}

				break;
			case 'tg_course_payment_from':
				$name = $data['first_name'] . ' ' . $data['last_name'];

				$body = '*' . $data['title'] . '*' . "\n";

				$body .= '*' . __( 'Заявка із сайту:', 'alevel' ) . '* ' . $site_name . "\n";
				$body .= '*' . __( 'Ім\'я та прізвище:', 'alevel' ) . '*' . $name . "\n";
				$body .= '*' . __( 'Email: ', 'alevel' ) . '*' . $data['email'] . "\n";
				$body .= '*' . __( 'Телефон:', 'alevel' ) . '*' . $data['phone'] . "\n";
				$body .= '*' . __( 'Тип оплати за курс:', 'alevel' ) . '*' . $data['method_payment'] . "\n";
				if ( ! empty( $data['from_count_payment'] ) ) {
					$body .= '*' . __( 'Кількість місяців на розстрочку моно банку:', 'alevel' ) . '*' . $data['from_count_payment'] . "\n";
				}
				break;

			case 'call_back_modal':
				$body = $data['title'] . "\n";

				$body .= __( 'Телефон:', 'alevel' ) . $data['phone'] . "\n";
				$body .= __( 'Дата для дзвінка:', 'alevel' ) . $data['date'] . "\n";
				$body .= __( 'Час:', 'alevel' ) . $data['time'] . "\n";
				break;

			case 'tg_call_back_modal':
				$body = $data['title'] . "\n";

				$body .= '*' . __( 'Заявка із сайту:', 'alevel' ) . '* ' . $site_name . "\n";
				$body .= '*' . __( 'Ім\'я та прізвище:', 'alevel' ) . '* ' . $data['first_name'] . "\n";
				$body .= '*' . __( 'Телефон:', 'alevel' ) . '* ' . $data['phone'] . "\n";
				$body .= '*' . __( 'Дата для дзвінка:', 'alevel' ) . '* ' . $data['date'] . "\n";
				$body .= '*' . __( 'Час:', 'alevel' ) . '* ' . $data['time'] . "\n";
				break;

			case 'call_back_form':
				$body = $data['title'] . "\n";

				$body .= __( 'Телефон:', 'alevel' ) . $data['phone'] . "\n";
				$body .= __( 'Email:', 'alevel' ) . $data['email'] . "\n";
				$body .= __( 'Супровідне повідомлення:', 'alevel' ) . $data['description'] . "\n";
				break;
			case 'tg_call_back_form':
				$body = $data['title'] . "\n";

				$body .= '*' . __( 'Заявка із сайту:', 'alevel' ) . '* ' . $site_name . "\n";
				$body .= '*' . __( 'Ім\'я та прізвище:', 'alevel' ) . '* ' . $data['first_name'] . "\n";
				$body .= '*' . __( 'Телефон:', 'alevel' ) . '* ' . $data['phone'] . "\n";
				$body .= '*' . __( 'Email:', 'alevel' ) . '* ' . $data['email'] . "\n";
				$body .= '*' . __( 'Супровідне повідомлення:', 'alevel' ) . '* ' . $data['description'] . "\n";
				break;
			case 'review_form':
				$body = $data['title'] . "\n";

				$body .= __( 'Профіль у соцмережах:', 'alevel' ) . $data['social'] . "\n";
				$body .= __( 'Фото:', 'alevel' ) . get_the_permalink( $data['avatar_id'] ) . "\n";
				foreach ( $data['teachers'] as $teacher ) {
					$body .= __( 'Викладач:', 'alevel' ) . get_the_title( $teacher ) . "\n";
				}
				$body .= __( 'Відгук:', 'alevel' ) . $data['content'] . "\n";
				$body .= __( 'Оцініть курс:', 'alevel' ) . $data['rating'] . "\n";
				break;
			default:
				$body = '';

		}

		return $body;
	}


}
