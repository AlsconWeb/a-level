<?php
/**
 * City by ip pop-up.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\CityByIP;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * CityByIPPopUp class file.
 */
class CityByIPPopUp {
	/**
	 * Set cookie to ip
	 *
	 * @return string
	 */
	public static function get_user_ip(): string {

		$cookie_ip = isset( $_COOKIE['alv_user_ip'] ) ? filter_var( wp_unslash( $_COOKIE['alv_user_ip'] ), FILTER_VALIDATE_IP ) : null;

		if ( $cookie_ip ) {
			return $cookie_ip;
		}

		if ( defined( 'ALV_IP_DEBUG' ) ) {
			return '31.202.89.212';
		}

		if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
			$ip = filter_var( wp_unslash( $_SERVER['HTTP_CLIENT_IP'] ), FILTER_VALIDATE_IP );
		} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
			$ip = filter_var( wp_unslash( $_SERVER['HTTP_X_FORWARDED_FOR'] ), FILTER_VALIDATE_IP );
		} else {
			$ip = isset( $_SERVER['REMOTE_ADDR'] ) ? filter_var( wp_unslash( $_SERVER['REMOTE_ADDR'] ), FILTER_VALIDATE_IP ) : '';
		}

		setcookie( 'alv_user_ip', $ip, time() + ( 60 * 60 * 24 ), COOKIEPATH, COOKIE_DOMAIN );

		return $ip;
	}

	/**
	 * Get location user by IP.
	 *
	 * @return array|mixed
	 */
	public static function get_local_by_ip() {
		$cookie_location = isset( $_COOKIE['alv_user_location'] ) ? sanitize_text_field( wp_unslash( $_COOKIE['alv_user_location'] ) ) : null;
		if ( $cookie_location ) {
			return self::get( 'alv_user_location' );
		}

		$cookie_ip = isset( $_COOKIE['alv_user_ip'] ) ? filter_var( wp_unslash( $_COOKIE['alv_user_ip'] ), FILTER_VALIDATE_IP ) : null;

		if ( empty( $cookie_ip ) ) {
			$cookie_ip = self::get_user_ip();
		}

		$http_client = new Client(
			[
				'base_uri' => 'http://ip-api.com/json/',
			]
		);

		try {
			$response = $http_client->request( 'GET', $cookie_ip . '?fields=status,city,countryCode' );
			$body     = ( string ) $response->getBody();

			self::set( 'alv_user_location', $body );

			return self::get( 'alv_user_location' );
		} catch ( GuzzleException $e ) {
			return json_decode( [ 'error' => $e->getMessage() ] );
		}
	}

	/**
	 * Get blogs name.
	 *
	 * @return array
	 */
	public static function get_blogs_name(): array {
		return [
			1 => [
				'ua' => __( 'Київ', 'alevel' ),
				'en' => 'Kyiv',
			],
			2 => [
				'ua' => __( 'Харків', 'alevel' ),
				'en' => 'Kharkiv',
			],
			3 => [
				'ua' => __( 'Полтава', 'alevel' ),
				'en' => 'Poltava',
			],
			4 => [
				'ua' => __( 'Львів', 'alevel' ),
				'en' => 'Lviv',
			],
			5 => [
				'ua' => __( 'Вінниця', 'alevel' ),
				'en' => 'Vinnytsia',
			],
			6 => [
				'ua' => __( 'Дніпро', 'alevel' ),
				'en' => 'Dnipro',
			],
			7 => [
				'ua' => __( 'Черкаси', 'alevel' ),
				'en' => 'Cherkasy',
			],
			8 => [
				'ua' => __( 'Івано-Франківськ', 'alevel' ),
				'en' => 'Ivano-Frankivsk',
			],
			9 => [
				'ua' => __( 'Онлайн (Якщо немає вашого міста)', 'alevel' ),
				'en' => 'Online',
			],
		];
	}

	/**
	 * Show City pop-up.
	 *
	 * @return void
	 */
	public static function show_pop_up() {
		$blog_id       = get_current_blog_id();
		$user_location = self::get_local_by_ip();
		$modal_show    = self::get( 'alv_modal_show' );
		if ( 'success' === $user_location->status && empty( $modal_show ) ) {
			$found_key = 0;
			foreach ( self::get_blogs_name() as $key => $city ) {
				if ( $user_location->city === $city['en'] ) {
					$found_key = $key;
				}
			}

			if ( $found_key && $blog_id !== self::get_blogs_name()[ $found_key ] ) {
				get_template_part(
					'template-parts/city-pop-up',
					null,
					[
						'blog_id'   => $blog_id,
						'city_name' => $found_key,
					]
				);
			} else {
				get_template_part(
					'template-parts/city-pop-up',
					null,
					[
						'blog_id'   => $blog_id,
						'city_name' => $found_key,
					]
				);
			}
		}

	}

	/**
	 * Get cookie.
	 *
	 * @param string $name Cookie name.
	 *
	 * @return object|null
	 */
	private static function get( $name ) {
		$cookie = isset( $_COOKIE[ $name ] ) ?
			sanitize_text_field( wp_unslash( $_COOKIE[ $name ] ) ) :
			'';

		return json_decode( $cookie );
	}

	/**
	 * Set cookie.
	 *
	 * @param string $name  Cookie name.
	 * @param mixed  $value Cookie value.
	 *
	 * @return void
	 */
	public static function set( $name, $value ) {

		if ( is_array( $value ) ) {
			$value = wp_json_encode( $value, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE );
		}

		setcookie( $name, $value, strtotime( '+30 days' ), COOKIEPATH, COOKIE_DOMAIN );

		$_COOKIE[ $name ] = $value;
	}

}
