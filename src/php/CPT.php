<?php
/**
 * Create custom post type.
 *
 * @package iwpdev/alevel
 */

namespace Alevel;

/**
 * CPT class file.
 */
class CPT {

	/**
	 * CPT construct.
	 */
	public function __construct() {
		add_action( 'init', [ $this, 'register_courses' ] );
		add_action( 'init', [ $this, 'register_teachers' ] );
		add_action( 'init', [ $this, 'register_testimonials' ] );
		add_action( 'init', [ $this, 'register_events' ] );
		add_action( 'init', [ $this, 'courses_type_tax' ] );
		add_action( 'init', [ $this, 'courses_category_tax' ] );
		add_action( 'init', [ $this, 'courses_teacher_specialization_tax' ] );
		add_action( 'init', [ $this, 'event_category_tax' ] );
		add_action( 'init', [ $this, 'event_tag_tax' ] );

		add_filter( 'pre_get_posts', [ $this, 'change_teachers_posts_per_page' ] );
	}

	/**
	 * Register custom post type courses.
	 *
	 * @return void
	 */
	public function register_courses(): void {
		register_post_type(
			'courses',
			[
				'label'         => null,
				'labels'        => [
					'name'               => __( 'Курси', 'alevel' ),
					'singular_name'      => __( 'Курс', 'alevel' ),
					'add_new'            => __( 'Додати курс', 'alevel' ),
					'add_new_item'       => __( 'Додавання курсу', 'alevel' ),
					'edit_item'          => __( 'Редагування курсу', 'alevel' ),
					'new_item'           => __( 'Новий курс', 'alevel' ),
					'view_item'          => __( 'Дивитись курс', 'alevel' ),
					'search_items'       => __( 'Шукати курс', 'alevel' ),
					'not_found'          => __( 'Не знайдено', 'alevel' ),
					'not_found_in_trash' => __( 'Не знайдено в кошику', 'alevel' ),
					'parent_item_colon'  => '',
					'menu_name'          => __( 'Курси', 'alevel' ),
				],
				'description'   => '',
				'public'        => true,
				'show_in_menu'  => null,
				'show_in_rest'  => null,
				'rest_base'     => null,
				'menu_position' => 5,
				'menu_icon'     => 'dashicons-open-folder',
				'hierarchical'  => true,
				'supports'      => [
					'title',
					'editor',
					'author',
					'thumbnail',
					'excerpt',
					'custom-fields',
					'comments',
					'revisions',
				],
				'taxonomies'    => [],
				'has_archive'   => true,
				'rewrite'       => true,
				'query_var'     => true,
			]
		);
	}

	/**
	 * Register custom post type teachers.
	 *
	 * @return void
	 */
	public function register_teachers(): void {
		register_post_type(
			'teachers',
			[
				'label'         => null,
				'labels'        => [
					'name'               => __( 'Педагоги', 'alevel' ),
					'singular_name'      => __( 'Педагоги', 'alevel' ),
					'add_new'            => __( 'Додати викладача', 'alevel' ),
					'add_new_item'       => __( 'Додавання викладача', 'alevel' ),
					'edit_item'          => __( 'Редагування викладача', 'alevel' ),
					'new_item'           => __( 'Новий викладач', 'alevel' ),
					'view_item'          => __( 'Дивитись викладача', 'alevel' ),
					'search_items'       => __( 'Шукати викладача', 'alevel' ),
					'not_found'          => __( 'Не знайдено викладача', 'alevel' ),
					'not_found_in_trash' => __( 'Не знайдено викладача в кошику', 'alevel' ),
					'parent_item_colon'  => '',
					'menu_name'          => __( 'Педагоги', 'alevel' ),
				],
				'description'   => '',
				'public'        => true,
				'show_in_menu'  => null,
				'show_in_rest'  => null,
				'rest_base'     => null,
				'menu_position' => 6,
				'menu_icon'     => 'dashicons-nametag',
				'hierarchical'  => true,
				'supports'      => [
					'title',
					'editor',
					'author',
					'thumbnail',
					'excerpt',
					'custom-fields',
					'comments',
					'revisions',
				],
				'taxonomies'    => [ 'teacher-specialization' ],
				'has_archive'   => true,
				'rewrite'       => true,
				'query_var'     => true,
			]
		);
	}

	/**
	 * Register events post type.
	 *
	 * @return void
	 */
	public function register_events(): void {
		register_post_type(
			'events',
			[
				'label'         => null,
				'labels'        => [
					'name'               => __( 'Події', 'alevel' ),
					'singular_name'      => __( 'Подія', 'alevel' ),
					'add_new'            => __( 'Додати подія', 'alevel' ),
					'add_new_item'       => __( 'Додавання подію', 'alevel' ),
					'edit_item'          => __( 'Редагування подію', 'alevel' ),
					'new_item'           => __( 'Нова подія', 'alevel' ),
					'view_item'          => __( 'Дивитись подію', 'alevel' ),
					'search_items'       => __( 'Шукати подію', 'alevel' ),
					'not_found'          => __( 'Не знайдено подію', 'alevel' ),
					'not_found_in_trash' => __( 'Не знайдено подію в кошику', 'alevel' ),
					'parent_item_colon'  => '',
					'menu_name'          => __( 'Події', 'alevel' ),
				],
				'description'   => '',
				'public'        => true,
				'show_in_menu'  => null,
				'show_in_rest'  => null,
				'rest_base'     => null,
				'menu_position' => 6,
				'menu_icon'     => 'dashicons-welcome-view-site',
				'hierarchical'  => true,
				'supports'      => [
					'title',
					'editor',
					'author',
					'thumbnail',
					'excerpt',
					'custom-fields',
					'comments',
					'revisions',
				],
				'taxonomies'    => [ 'event-category', 'event-tag' ],
				'has_archive'   => true,
				'rewrite'       => true,
				'query_var'     => true,
			]
		);
	}

	/**
	 * Register custom post type testimonials.
	 *
	 * @return void
	 */
	public function register_testimonials(): void {
		register_post_type(
			'testimonials',
			[
				'label'         => null,
				'labels'        => [
					'name'               => __( 'Відгуки', 'alevel' ),
					'singular_name'      => __( 'Відгуки', 'alevel' ),
					'add_new'            => __( 'Додати відгук', 'alevel' ),
					'add_new_item'       => __( 'Додавання відгука', 'alevel' ),
					'edit_item'          => __( 'Редагування відгук', 'alevel' ),
					'new_item'           => __( 'Новий відгук', 'alevel' ),
					'view_item'          => __( 'Дивитись відгук', 'alevel' ),
					'search_items'       => __( 'Шукати відгук', 'alevel' ),
					'not_found'          => __( 'Не знайдено відгук', 'alevel' ),
					'not_found_in_trash' => __( 'Не знайдено відгук в кошику', 'alevel' ),
					'parent_item_colon'  => '',
					'menu_name'          => __( 'Відгуки', 'alevel' ),
				],
				'description'   => '',
				'public'        => true,
				'show_in_menu'  => null,
				'show_in_rest'  => null,
				'rest_base'     => null,
				'menu_position' => 6,
				'menu_icon'     => 'dashicons-format-status',
				'hierarchical'  => true,
				'supports'      => [
					'title',
					'editor',
					'author',
					'thumbnail',
					'excerpt',
					'custom-fields',
					'revisions',
				],
				'taxonomies'    => [],
				'has_archive'   => true,
				'rewrite'       => true,
				'query_var'     => true,
			]
		);
	}

	/**
	 * Register taxonomy type.
	 *
	 * @return void
	 */
	public function courses_type_tax(): void {
		register_taxonomy(
			'type-courses',
			[ 'courses' ],
			[
				'label'             => __( 'Тип зайнятості', 'alevel' ),
				'labels'            => [
					'name'          => __( 'Тип зайнятості', 'alevel' ),
					'singular_name' => __( 'Тип зайнятості', 'alevel' ),
					'search_items'  => __( 'Шукати тип зайнятості', 'alevel' ),
					'all_items'     => __( 'Усі тип зайнятості', 'alevel' ),
					'view_item '    => __( 'Дивитись тип зайнятості', 'alevel' ),
					'edit_item'     => __( 'Редагувати тип зайнятості', 'alevel' ),
					'update_item'   => __( 'Оновити тип зайнятості', 'alevel' ),
					'add_new_item'  => __( 'Додати тип зайнятості', 'alevel' ),
					'new_item_name' => __( 'Новий тип зайнятості', 'alevel' ),
					'menu_name'     => __( 'Тип зайнятості', 'alevel' ),
					'back_to_items' => __( '← Повернутися до типу зайнятості', 'alevel' ),
				],
				'description'       => __( 'Тип зайнятості відповідає за те чи є курс з працевлаштуванням або іншими додатковими умовами', 'alevel' ),
				'public'            => true,
				'hierarchical'      => false,
				'rewrite'           => true,
				'capabilities'      => [],
				'meta_box_cb'       => null,
				'show_admin_column' => false,
				'show_in_rest'      => null,
				'rest_base'         => null,
			]
		);
	}

	/**
	 * Register taxonomy category.
	 *
	 * @return void
	 */
	public function courses_category_tax(): void {
		register_taxonomy(
			'category-courses',
			[ 'courses' ],
			[
				'label'             => __( 'Категорія курсу', 'alevel' ),
				'labels'            => [
					'name'          => __( 'Категорія курсу', 'alevel' ),
					'singular_name' => __( 'Категорія курсу', 'alevel' ),
					'search_items'  => __( 'Шукати категорію курсу', 'alevel' ),
					'all_items'     => __( 'Усі категорії курсу', 'alevel' ),
					'view_item '    => __( 'Дивитись категорію курсу', 'alevel' ),
					'edit_item'     => __( 'Редагувати категорію курсу', 'alevel' ),
					'update_item'   => __( 'Оновити категорію курсу', 'alevel' ),
					'add_new_item'  => __( 'Додати категорію курсу', 'alevel' ),
					'new_item_name' => __( 'Новий категорія курсу', 'alevel' ),
					'menu_name'     => __( 'Категорія курсу', 'alevel' ),
					'back_to_items' => __( '← Повернутися до категорії курсу', 'alevel' ),
				],
				'description'       => '',
				'public'            => true,
				'hierarchical'      => true,
				'rewrite'           => true,
				'capabilities'      => [],
				'meta_box_cb'       => null,
				'show_admin_column' => false,
				'show_in_rest'      => null,
				'rest_base'         => null,
			]
		);
	}

	/**
	 * Register taxonomy teacher specialization.
	 *
	 * @return void
	 */
	public function courses_teacher_specialization_tax(): void {
		register_taxonomy(
			'teacher-specialization',
			[ 'teachers' ],
			[
				'label'             => __( 'Спеціалізація вчителя', 'alevel' ),
				'labels'            => [
					'name'          => __( 'Спеціалізація вчителя', 'alevel' ),
					'singular_name' => __( 'Спеціалізація вчителя', 'alevel' ),
					'search_items'  => __( 'Шукати спеціалізацію', 'alevel' ),
					'all_items'     => __( 'Усі спеціалізації', 'alevel' ),
					'view_item '    => __( 'Дивитись спеціалізацію', 'alevel' ),
					'edit_item'     => __( 'Редагувати спеціалізацію', 'alevel' ),
					'update_item'   => __( 'Оновити спеціалізацію', 'alevel' ),
					'add_new_item'  => __( 'Додати спеціалізацію', 'alevel' ),
					'new_item_name' => __( 'Нова спеціалізацію', 'alevel' ),
					'menu_name'     => __( 'Спеціалізація вчителя', 'alevel' ),
					'back_to_items' => __( '← Повернутися до Спеціалізація вчителя', 'alevel' ),
				],
				'description'       => '',
				'public'            => true,
				'hierarchical'      => true,
				'rewrite'           => true,
				'capabilities'      => [],
				'meta_box_cb'       => null,
				'show_admin_column' => false,
				'show_in_rest'      => null,
				'rest_base'         => null,
			]
		);
	}

	/**
	 * Register taxonomy event category.
	 *
	 * @return void
	 */
	public function event_category_tax(): void {
		register_taxonomy(
			'event-category',
			[ 'events' ],
			[
				'label'             => __( 'Категорія події', 'alevel' ),
				'labels'            => [
					'name'          => __( 'Категорія події', 'alevel' ),
					'singular_name' => __( 'Категорія події', 'alevel' ),
					'search_items'  => __( 'Шукати категорію події', 'alevel' ),
					'all_items'     => __( 'Усі категорії події', 'alevel' ),
					'view_item '    => __( 'Дивитись категорію події', 'alevel' ),
					'edit_item'     => __( 'Редагувати категорію події', 'alevel' ),
					'update_item'   => __( 'Оновити категорію події', 'alevel' ),
					'add_new_item'  => __( 'Додати категорію події', 'alevel' ),
					'new_item_name' => __( 'Нова категорія події', 'alevel' ),
					'menu_name'     => __( 'Категорія події', 'alevel' ),
					'back_to_items' => __( '← Повернутися до категорію події', 'alevel' ),
				],
				'description'       => '',
				'public'            => true,
				'hierarchical'      => true,
				'rewrite'           => true,
				'capabilities'      => [],
				'meta_box_cb'       => null,
				'show_admin_column' => false,
				'show_in_rest'      => null,
				'rest_base'         => null,
			]
		);
	}

	/**
	 * Register taxonomy event tag.
	 *
	 * @return void
	 */
	public function event_tag_tax(): void {
		register_taxonomy(
			'event-tag',
			[ 'events' ],
			[
				'label'             => __( 'Позначки події', 'alevel' ),
				'labels'            => [
					'name'          => __( 'Позначки події', 'alevel' ),
					'singular_name' => __( 'Позначки події', 'alevel' ),
					'search_items'  => __( 'Шукати позначки події', 'alevel' ),
					'all_items'     => __( 'Усі позначки події', 'alevel' ),
					'view_item '    => __( 'Дивитись позначку події', 'alevel' ),
					'edit_item'     => __( 'Редагувати позначку події', 'alevel' ),
					'update_item'   => __( 'Оновити позначку події', 'alevel' ),
					'add_new_item'  => __( 'Додати позначку події', 'alevel' ),
					'new_item_name' => __( 'Нова позначка події', 'alevel' ),
					'menu_name'     => __( 'Позначки події', 'alevel' ),
					'back_to_items' => __( '← Повернутися до позначки події', 'alevel' ),
				],
				'description'       => '',
				'public'            => true,
				'hierarchical'      => false,
				'rewrite'           => true,
				'capabilities'      => [],
				'meta_box_cb'       => null,
				'show_admin_column' => false,
				'show_in_rest'      => null,
				'rest_base'         => null,
			]
		);
	}

	/**
	 * Change default outputs.
	 *
	 * @param object $query data.
	 *
	 * @return object
	 */
	public function change_teachers_posts_per_page( object $query ): object {
		if ( $query->is_post_type_archive( 'teachers' ) && ! is_admin() && $query->is_main_query() ) {
			$query->set( 'posts_per_page', '11' );
			$query->set( 'orderby', 'rand' );
		}

		if ( $query->is_post_type_archive( 'testimonials' ) && ! is_admin() && $query->is_main_query() ) {
			$query->set( 'orderby', 'rand' );
			$arg = [
				'relation' => 'OR',
				[
					'key'     => '_alv_company_no_review',
					'value'   => 'true',
					'compare' => '!=',
				],
				[
					'key'         => '_alv_company_no_review',
					'compare_key' => 'NOT EXISTS',
				],
			];
			$query->set( 'meta_query', $arg );

		}

		if ( $query->is_post_type_archive( 'courses' ) && ! is_admin() && $query->is_main_query() ) {
			$query->set( 'posts_per_page', - 1 );
		}

		return $query;
	}
}
