<?php
/**
 * Helpers class.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\Helpers;

use WP_Query;

/**
 * Helper class file
 */
class Helper {

	/**
	 * Basic programming course ID.
	 */
	private const ALV_BASIC_PROG = [ 663 ];

	/**
	 * Has Short code in page on array.
	 *
	 * @param array $tags Short codes array.
	 *
	 * @return bool
	 */
	public static function alv_has_shortcode_array( array $tags ): bool {
		global $post;
		$content = $post->post_content;

		foreach ( $tags as $tag ) {
			if ( has_shortcode( $content, $tag ) ) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Set teachers by review.
	 *
	 * @param array $teachers_id Teachers ids.
	 * @param int   $review_id   Review id.
	 *
	 * @return void
	 */
	public static function alv_set_teachers_by_review( array $teachers_id, int $review_id ): void {
		$teachers_data = [];
		foreach ( $teachers_id as $teacher ) {
			$teachers_data[] = [
				'value'   => 'post:teachers:' . $teacher,
				'id'      => $teacher,
				'type'    => 'post',
				'subtype' => 'teachers',
			];
		}

		carbon_set_post_meta(
			$review_id,
			'alv_reviewer_teachers',
			$teachers_data,
		);
	}

	/**
	 * Set Socials media complex field.
	 *
	 * @param array $socials Socials meida.
	 * @param int   $post_id Reviwe id.
	 *
	 * @return void
	 */
	public static function alv_set_social_media( array $socials, int $post_id ): void {
		$social_data = [];

		foreach ( $socials as $key => $social ) {
			if ( $social ) {
				$social_data[] = [
					'social_name' => $key,
					'social_link' => $social,
				];
			}
		}

		carbon_set_post_meta( $post_id, 'alv_social_media_reviewer', $social_data );
	}

	/**
	 * Set review rating.
	 *
	 * @param float $rainting Rating course.
	 * @param int   $post_id  Review ID.
	 *
	 * @return void
	 */
	public static function alv_set_rating( float $rainting, int $post_id ): void {
		carbon_set_post_meta( $post_id, 'alev_reviewer_rating', $rainting );
	}

	/**
	 * Set reviewer phone.
	 *
	 * @param string $phone   Phone number.
	 * @param int    $post_id Review ID.
	 *
	 * @return void
	 */
	public static function alv_set_phone( string $phone, int $post_id ): void {
		carbon_set_post_meta( $post_id, 'alev_reviewer_phone', $phone );
	}

	/**
	 * Set reviewer email.
	 *
	 * @param string $email   Email reviewer.
	 * @param int    $post_id Review ID.
	 *
	 * @return void
	 */
	public static function alv_set_email( string $email, int $post_id ): void {
		carbon_set_post_meta( $post_id, 'alev_reviewer_email', $email );
	}

	/**
	 * Show Read minutes in article.
	 *
	 * @param string $content Content.
	 */
	public static function alv_show_read_min( string $content ) {
		$count_words = count( explode( ' ', $content ) );

		$minutes_read = round( $count_words / 150, 0, PHP_ROUND_HALF_UP );

		return esc_attr( $minutes_read . __( ' хвилин', 'alevel' ) );
	}

	/**
	 * Get count post teacher.
	 *
	 * @param int $teacher_id Teacher id.
	 *
	 * @return int
	 */
	public static function alv_get_count_post_teacher( int $teacher_id ): int {

		$arg = [
			'post_type'     => 'post',
			'post_status'   => 'publish',
			'post_per_page' => - 1,
			'meta_query'    => [
				[
					'key'   => '_alv_post_author_teacher|||0|id',
					'value' => $teacher_id,
				],
			],
		];

		return ( new WP_Query( $arg ) )->post_count;

	}

	/**
	 * Show course footer menu.
	 *
	 * @return void
	 */
	public static function show_course_footer_menu(): void {
		$categories = get_categories(
			[
				'taxonomy'   => 'category-courses',
				'type'       => 'courses',
				'orderby'    => 'name',
				'order'      => 'ASC',
				'hide_empty' => 1,
			]
		);

		if ( ! empty( $categories ) ) {
			foreach ( $categories as $category ) {
				$course_posts = get_posts(
					[
						'posts_per_page' => - 1,
						'post_type'      => 'courses',
						'tax_query'      => [
							[
								'taxonomy' => 'category-courses',
								'field'    => 'id',
								'terms'    => $category->term_id,
							],
						],
					]
				);
				?>
				<div class="menu-block">
					<h4 class="title"><?php echo esc_html( $category->name ); ?></h4>
					<ul class="menu">
						<?php
						foreach ( $course_posts as $course ) {
							?>
							<li>
								<a href="<?php echo esc_url( get_the_permalink( $course->ID ) ); ?>">
									<?php echo esc_html( $course->post_title ); ?>
								</a>
							</li>
							<?php
						}
						?>
					</ul>
				</div>
				<?php
			}
		}
	}

	/**
	 * Get event from thanks modal.
	 *
	 * @param int|null $exclude_event post id from exclude.
	 *
	 * @return false|string
	 */
	public static function get_event_to_madal_thnas( int $exclude_event = null ) {
		$current_date = gmdate( 'Y-m-d' );

		$current_blog_id = get_current_blog_id();
		if ( 1 !== $current_blog_id && is_multisite() ) {
			switch_to_blog( get_main_site_id() );
		}

		$arg = [
			'post_type'      => 'events',
			'post_status'    => 'publish',
			'post__not_in'   => [ $exclude_event ],
			'posts_per_page' => 1,
			'order'          => 'DESC',
			'orderby'        => 'rand',
			'meta_query'     => [
				[
					'key'     => '_alv_event_start',
					'value'   => $current_date,
					'compare' => '>=',
					'type'    => 'DATE',
				],
			],
		];

		$event_obj = new WP_Query( $arg );

		if ( $event_obj->have_posts() ) {
			while ( $event_obj->have_posts() ) {
				$event_obj->the_post();
				$event_id    = get_the_ID();
				$event_start = carbon_get_post_meta( $event_id, 'alv_event_start' );
				$date        = date_create( $event_start );
				$speakers    = carbon_get_post_meta( $event_id, 'alv_mentors' );
				ob_start()
				?>
				<div class="event-item">
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="link"></a>
					<?php
					if ( has_post_thumbnail( $event_id ) ) {
						the_post_thumbnail( 'alv-thmb_mini' );
					} else {
						?>
						<img
								src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/thumbnail-mini.png' ); ?>"
								alt="No image">
					<?php } ?>
					<div class="desc">
						<h3 class="title"><?php the_title(); ?></h3>
						<p class="date"><?php echo esc_html( date_format( $date, 'd.m.Y' ) ); ?></p>
						<?php get_template_part( 'template-parts/speakers', 'info', [ 'speakers' => $speakers ] ); ?>
					</div>
				</div>
				<?php
			}
			wp_reset_postdata();

			if ( is_multisite() ) {
				switch_to_blog( $current_blog_id );
			}

			return ob_get_clean();
		} else {

			if ( is_multisite() ) {
				switch_to_blog( $current_blog_id );
			}

			return '<h3>' . __( 'На даний момент немає подій', 'alevel' ) . '</h3>';
		}

	}

	/**
	 * Show course select.
	 *
	 * @param string $id ID selector.
	 *
	 * @return void
	 */
	public static function show_course_select( string $id ): void {

		$arg = [
			'post_type'      => 'courses',
			'post_status'    => 'publish',
			'posts_per_page' => - 1,
		];

		$courses_obj = new WP_Query( $arg );

		if ( $courses_obj->have_posts() ) {
			?>
			<select name="consultation_course" id="<?php echo esc_attr( $id ); ?>" required>
				<option value="0"><?php esc_html_e( 'Курси', 'alevel' ); ?></option>
				<?php
				while ( $courses_obj->have_posts() ) {
					$courses_obj->the_post();
					?>
					<option value="<?php echo esc_attr( get_the_ID() ); ?>"><?php the_title(); ?></option>
				<?php } ?>
			</select>
			<?php
		}
	}

	/**
	 * Get blog post to thank page become partner.
	 *
	 * @return false|string
	 */
	public static function get_blog_post() {
		$arg = [
			'post_type'      => 'post',
			'posts_per_page' => 2,
			'orderby'        => 'rand',
		];

		$post_query = new WP_Query( $arg );

		ob_start();
		if ( $post_query->have_posts() ) {
			while ( $post_query->have_posts() ) {
				$post_query->the_post();
				get_template_part( 'template-parts/medea', 'item', [ 'post_id' => get_the_ID() ] );
			}

			wp_reset_postdata();
			?>
			<a class="button" href="<?php echo esc_url( get_permalink( 40 ) ); ?>">
				<?php esc_html_e( 'Усi статті', 'alevel' ); ?>
			</a>
			<?php
			return ob_get_clean();
		} else {
			return '';
		}
	}

	/**
	 * Get review firm to thank page become partner.
	 *
	 * @return false|string
	 */
	public static function get_partners() {
		$arg = [
			'post_type'      => 'testimonials',
			'posts_per_page' => 2,
			'orderby'        => 'rand',
			'meta_query'     => [
				'relation' => 'AND',
				[
					'key'   => '_alv_type_review',
					'value' => 'firm',
				],
				[
					'key'     => '_alv_company_no_review',
					'value'   => 'true',
					'compare' => '!=',
				],
			],
		];

		$post_query = new WP_Query( $arg );

		ob_start();
		if ( $post_query->have_posts() ) {
			while ( $post_query->have_posts() ) {
				$post_query->the_post();
				$testimonial_id = get_the_ID();
				$course_id      = carbon_get_post_meta( $testimonial_id, 'alv_course_event' )[0]['id'] ?? 0;
				get_template_part(
					'template-parts/reviews',
					'item',
					[
						'testimonials_id' => $testimonial_id,
						'course_id'       => $course_id,
					]
				);
				get_template_part( 'template-parts/reviews', 'firm', [ 'post_id' => get_the_ID() ] );
			}

			wp_reset_postdata();
			?>
			<a class="button" href="<?php echo esc_url( get_permalink( 1613 ) ); ?>">
				<?php esc_html_e( 'Усi партнери', 'alevel' ); ?>
			</a>
			<?php
			return ob_get_clean();
		} else {
			return '';
		}
	}

	/**
	 * End date check.
	 *
	 * @param int $data Date in time stamp.
	 *
	 * @return bool
	 */
	public static function is_discount_endet( int $data ): bool {
		$current_date = gmdate( 'Y-m-d h:i:sa' );
		$now          = strtotime( $current_date );

		$distance = $data - $now;

		return $distance < 0;
	}

	/**
	 * Checking whether the course is the basics of programming.
	 *
	 * @param int $course_id Course ID.
	 *
	 * @return bool
	 */
	public static function is_basic_programiong( int $course_id ) {
		if ( in_array( $course_id, self::ALV_BASIC_PROG ) ) {
			return true;
		}

		return false;
	}

	/**
	 * Get Post id in main_site.
	 *
	 * @param string $slug      Post slug.
	 * @param string $post_type Post type.
	 *
	 * @return int
	 */
	public static function get_post_id_in_main_site( string $slug, string $post_type ): int {
		$main_course_post = get_page_by_path( $slug, OBJECT, [ $post_type ] );

		if ( ! empty( $main_course_post ) ) {

			return $main_course_post->ID;
		}

		return 0;

	}

}
