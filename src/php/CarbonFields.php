<?php
/**
 * Create carbon fields from theme.
 *
 * @package iwpdev/alevel
 */

namespace Alevel;

use Carbon_Fields\Carbon_Fields;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * CarbonFields class file.
 */
class CarbonFields {

	private const LINE_GRADIENT = [
		'default'            => 'Стандартный градиент',
		'dont-net'           => '.NET/C# ',
		'basics-programming' => 'Основи програмування',
		'java'               => 'Java',
		'front-end'          => 'Front-end',
		'python'             => 'Python',
		'project-management' => 'Project management',
		'it-recruiting'      => 'IT Recruiting',
		'web-design'         => 'Web Design',
		'qa-manual'          => 'QA Manual',
		'yellow-green'       => 'Жовто-зелений',
		'orange'             => 'Помаранчевий',
		'blue'               => 'Блакитний',
		'lilac'              => 'Бузковий',
		'pink-blue'          => 'Рожево-синій',
		'green'              => 'Зелений',
		'tridoustroistvo'    => 'Працевлаштування',
		'yellow-blue'        => 'Жовто-синій',
		'hr_online'          => 'HR',
		'business_analyst'   => 'Business analyst',
		'sales_manager'      => 'Sales manager',
		'php'                => 'PHP',
		'graphic_design'     => 'Graphic Design',
		'qa-automation'      => 'QA Automation',
		'ruby'               => 'Ruby',
		'golang'             => 'Golang',
	];

	/**
	 * CarbonFields construct.
	 */
	public function __construct() {
		add_action( 'after_setup_theme', [ $this, 'carbon_fields_load' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_carbon_fields_courses' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_carbon_fields_teacher' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_carbon_fields_testimonials' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_carbon_fields_event' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_carbon_fields_post' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_carbon_fields_blog_page' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_carbon_fields_partners_page' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_theme_options_page' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_networks_settings' ] );
		add_filter( 'carbon_fields_should_save_field_value', [ $this, 'save_alv_teacher_course' ], 10, 3 );
		add_filter( 'carbon_fields_should_save_field_value', [ $this, 'network_save_courses' ], 10, 3 );
	}

	/**
	 * Load carbon field.
	 *
	 * @return void
	 */
	public function carbon_fields_load(): void {
		Carbon_Fields::boot();
	}

	/**
	 * Add carbon fields to courses CPT.
	 *
	 * @return void
	 */
	public function add_carbon_fields_courses(): void {
		Container::make( 'post_meta', __( 'Дані курсу', 'alevel' ) )
			->where( 'post_type', '=', 'courses' )
			->add_fields(
				[
					Field::make( 'date', 'alv_course_start', __( 'Старт курсу', 'alevel' ) )
						->set_storage_format( 'd.m.Y' )
						->set_width( 33 )
						->set_attribute( 'placeholder', __( 'Дата початок курсу', 'alevel' ) ),
					Field::make( 'text', 'alv_course_duration', __( 'Тривалість курсу', 'alevel' ) )->set_width( 33 ),
					Field::make( 'text', 'alv_course_available_seats', __( 'Кількість вільних місць', 'alevel' ) )
						->set_width( 33 ),
					Field::make( 'text', 'alv_course_discount_text', __( 'Текст знижки на курс', 'alevel' ) )
						->set_width( 50 ),
					Field::make( 'date', 'alv_course_end_date_discount', __( 'Дата завершення акції', 'alevel' ) )
						->set_storage_format( 'd.m.Y' )
						->set_width( 50 ),
					Field::make( 'file', 'alv_course_program', __( 'Програма курсу', 'alevel' ) )->set_width( 50 ),
					Field::make( 'image', 'alv_course_icon', __( 'Значок курсу', 'alevel' ) )
						->set_value_type( 'url' )
						->set_width( 50 ),
					Field::make( 'separator', 'alv_style_options', 'Style' ),
					Field::make( 'select', 'alv_line_gradient_class', __( 'Вибери градієнт для курсу', 'alevel' ) )
						->add_options( self::LINE_GRADIENT ),
				]
			);
	}

	/**
	 * Add carbon fields to teacher CPT.
	 *
	 * @return void
	 */
	public function add_carbon_fields_teacher(): void {
		Container::make( 'post_meta', __( 'Дані Вчителі', 'alevel' ) )
			->where( 'post_type', '=', 'teachers' )
			->add_fields(
				[
					Field::make( 'association', 'alv_teacher_course', __( 'Виберіть курси, які веде викладач', 'alevel' ) )
						->set_max( 5 )
						->set_types(
							[
								[
									'type'      => 'post',
									'post_type' => 'courses',
								],
							]
						),
					Field::make( 'text', 'alv_teacher_job_title', __( 'Посада вчителя', 'alevel' ) ),
					Field::make( 'complex', 'alv_social_media_teacher', __( 'Соціальні мережі', 'alevel' ) )
						->add_fields(
							[
								Field::make( 'text', 'social_name', __( 'Назва соціальної мережі', 'alevel' ) ),
								Field::make( 'text', 'social_link', __( 'Посилання на соціальну мережу', 'alevel' ) ),
							]
						),

				]
			);
	}

	/**
	 * Add carbon fields to testimonials CPT.
	 *
	 * @return void
	 */
	public function add_carbon_fields_testimonials(): void {
		Container::make( 'post_meta', __( 'Дані відгуку', 'alevel' ) )
			->where( 'post_type', '=', 'testimonials' )
			->add_fields(
				[
					Field::make( 'select', 'alv_type_review', __( 'Тип відгуку: фірма чи учень' ) )
						->add_options(
							[
								'regular' => __( 'Звичайний відгук', 'alevel' ),
								'firm'    => __( 'Відгук від фірми', 'alevel' ),
								'history' => __( 'Історія успіху', 'alevel' ),
							]
						),
					Field::make( 'association', 'alv_course_event', __( 'Виберіть пов\'язаний курс', 'alevel' ) )
						->set_max( 1 )
						->set_conditional_logic(
							[
								'relation' => 'OR',
								[
									'field'   => 'alv_type_review',
									'value'   => 'history',
									'compare' => '=',
								],
								[
									'field'   => 'alv_type_review',
									'value'   => 'regular',
									'compare' => '=',
								],
							]
						)
						->set_types(
							[
								[
									'type'      => 'post',
									'post_type' => 'courses',
								],
							]
						),
					Field::make( 'text', 'alev_review_title', __( 'Заголовок', 'alevel' ) )
						->set_conditional_logic(
							[
								'relation' => 'OR',
								[
									'field'   => 'alv_type_review',
									'value'   => 'history',
									'compare' => '=',
								],
								[
									'field'   => 'alv_type_review',
									'value'   => 'regular',
									'compare' => '=',
								],
							]
						),
					Field::make( 'text', 'alv_course_count_students', __( 'Кількість працюючих студентів', 'alevel' ) )
						->set_conditional_logic(
							[
								'relation' => 'AND',
								[
									'field'   => 'alv_type_review',
									'value'   => 'firm',
									'compare' => '=',
								],
							]
						),
					Field::make( 'radio', 'alv_company_no_review', __( 'Компанія без відгуків', 'alevel' ) )
						->set_conditional_logic(
							[
								'relation' => 'AND',
								[
									'field'   => 'alv_type_review',
									'value'   => 'firm',
									'compare' => '=',
								],
							]
						)
						->add_options(
							[
								'false' => __( 'Компанія з відгуком', 'alevel' ),
								'true'  => __( 'Компанія з без відгуку', 'alevel' ),
							]
						),
					Field::make( 'text', 'alv_reviewer_position', __( 'Посада', 'alevel' ) )
						->set_conditional_logic(
							[
								'relation' => 'OR',
								[
									'field'   => 'alv_type_review',
									'value'   => 'history',
									'compare' => '=',
								],
								[
									'field'   => 'alv_type_review',
									'value'   => 'regular',
									'compare' => '=',
								],
							]
						),
					Field::make( 'complex', 'alv_social_media_reviewer', __( 'Соціальні мережі', 'alevel' ) )
						->add_fields(
							[
								Field::make( 'text', 'social_name', __( 'Назва соціальної мережі', 'alevel' ) ),
								Field::make( 'text', 'social_link', __( 'Посилання на соціальну мережу', 'alevel' ) ),
							]
						),
					Field::make( 'association', 'alv_reviewer_teachers' )
						->set_conditional_logic(
							[
								'relation' => 'OR',
								[
									'field'   => 'alv_type_review',
									'value'   => 'history',
									'compare' => '=',
								],
								[
									'field'   => 'alv_type_review',
									'value'   => 'regular',
									'compare' => '=',
								],
							]
						)
						->set_types(
							[
								[
									'type'      => 'post',
									'post_type' => 'teachers',
								],

							]
						),
					Field::make( 'text', 'alev_reviewer_rating', __( 'Рейтинг', 'alevel' ) )
						->set_conditional_logic(
							[
								'relation' => 'OR',
								[
									'field'   => 'alv_type_review',
									'value'   => 'history',
									'compare' => '=',
								],
								[
									'field'   => 'alv_type_review',
									'value'   => 'regular',
									'compare' => '=',
								],
							]
						)
						->set_width( 50 ),
					Field::make( 'text', 'alev_reviewer_phone', __( 'Номер телефону', 'alevel' ) )
						->set_conditional_logic(
							[
								'relation' => 'OR',
								[
									'field'   => 'alv_type_review',
									'value'   => 'history',
									'compare' => '=',
								],
								[
									'field'   => 'alv_type_review',
									'value'   => 'regular',
									'compare' => '=',
								],
							]
						)
						->set_width( 50 ),
					Field::make( 'text', 'alev_reviewer_email', __( 'Email', 'alevel' ) )
						->set_conditional_logic(
							[
								'relation' => 'OR',
								[
									'field'   => 'alv_type_review',
									'value'   => 'history',
									'compare' => '=',
								],
								[
									'field'   => 'alv_type_review',
									'value'   => 'regular',
									'compare' => '=',
								],
							]
						)
						->set_width( 50 ),
					Field::make( 'text', 'alv_firm_text_link', __( 'Текст посилання на сайт фірми', 'alevel' ) )
						->set_conditional_logic(
							[
								'relation' => 'AND',
								[
									'field'   => 'alv_type_review',
									'value'   => 'firm',
									'compare' => '=',
								],
							]
						),
					Field::make( 'text', 'alv_firm_link', __( 'Посилання на сайт фірми', 'alevel' ) )
						->set_conditional_logic(
							[
								'relation' => 'AND',
								[
									'field'   => 'alv_type_review',
									'value'   => 'firm',
									'compare' => '=',
								],
							]
						),
					Field::make( 'image', 'alv_history_banner', __( 'Банер для блогу', 'alevel' ) )
						->set_value_type( 'url' )
						->set_conditional_logic(
							[
								'relation' => 'AND',
								[
									'field'   => 'alv_type_review',
									'value'   => 'history',
									'compare' => '=',
								],
							]
						),
				]
			);
	}

	/**
	 * Add carbon fields to event CPT.
	 *
	 * @return void
	 */
	public function add_carbon_fields_event(): void {
		Container::make( 'post_meta', __( 'Дані події', 'alevel' ) )
			->where( 'post_type', '=', 'events' )
			->add_fields(
				[
					Field::make( 'select', 'alv_event_type', __( 'Тип події', 'alevel' ) )
						->add_options(
							[
								'school' => __( 'Події від школи', 'alevel' ),
								'firm'   => __( 'Подія від фірми', 'alevel' ),
							]
						),
					Field::make( 'text', 'alv_event_firm_link', __( 'Посилання на сторінку реєстрації', 'alevel' ) )
						->set_conditional_logic(
							[
								'relation' => 'AND',
								[
									'field'   => 'alv_event_type',
									'value'   => 'firm',
									'compare' => '=',
								],
							]
						),
					Field::make( 'date', 'alv_event_start', __( 'Початок події', 'alevel' ) )
						->set_input_format( 'd.m.Y', 'd.m.Y' )
						->set_picker_options( [ 'dateFormat' => 'd.m.Y' ] )
						->set_attribute( 'placeholder', __( 'Дата початку події', 'alevel' ) ),
					Field::make( 'complex', 'alv_mentors', __( 'Ментори', 'alevel' ) )
						->add_fields(
							[
								Field::make( 'text', 'name', __( 'П.І.Б', 'alevel' ) ),
								Field::make( 'text', 'position', __( 'Посада', 'alevel' ) ),
								Field::make( 'image', 'photo', __( 'Фотографія', 'alevel' ) )
									->set_value_type( 'id' ),
							]
						),
					Field::make( 'complex', 'alv_event_plan', __( 'Зміст заходу', 'alevel' ) )
						->add_fields(
							[
								Field::make( 'text', 'item_plan', __( 'Пункт плану', 'alevel' ) ),
							]
						),
					Field::make( 'association', 'alv_course_event', __( 'Виберіть пов\'язаний курс', 'alevel' ) )
						->set_max( 1 )
						->set_types(
							[
								[
									'type'      => 'post',
									'post_type' => 'courses',
								],
							]
						),
					Field::make( 'separator', 'alv_style_options', __( 'За підтримки', 'alevel' ) ),
					Field::make( 'image', 'alv_company_logo', __( 'Логотип компанії', 'alevel' ) )
						->set_value_type( 'id' ),
					Field::make( 'text', 'alv_company_name', __( 'Назва компанії', 'alevel' ) ),
					Field::make( 'text', 'alv_company_web_link', __( 'Посилання на сайт компанії', 'alevel' ) ),
				]
			);
	}

	/**
	 *  Add carbon fields to post.
	 *
	 * @return void
	 */
	public function add_carbon_fields_post(): void {
		Container::make( 'post_meta', __( 'Дані статті', 'alevel' ) )
			->where( 'post_type', '=', 'post' )
			->add_fields(
				[
					Field::make( 'radio', 'alv_author_teachers', __( 'Автор вчитель чи ні', 'alevel' ) )
						->add_options(
							[
								'yes' => __( 'Так', 'alevel' ),
								'no'  => __( 'Ні', 'alevel' ),
							]
						),
					Field::make( 'association', 'alv_post_author_teacher', __( 'Виберіть автора з учителя як актора статті', 'alevel' ) )
						->set_max( 1 )
						->set_conditional_logic(
							[
								'relation' => 'AND',
								[
									'field'   => 'alv_author_teachers',
									'value'   => 'yes',
									'compare' => '=',
								],
							]
						)
						->set_types(
							[
								[
									'type'      => 'post',
									'post_type' => 'teachers',
								],
							]
						),
					Field::make( 'complex', 'alv_post_author_no_teacher', __( 'Автор', 'alevel' ) )
						->set_conditional_logic(
							[
								'relation' => 'AND',
								[
									'field'   => 'alv_author_teachers',
									'value'   => 'no',
									'compare' => '=',
								],
							]
						)
						->add_fields(
							[
								Field::make( 'text', 'name', __( 'П.І.Б', 'alevel' ) ),
								Field::make( 'text', 'position', __( 'Посада', 'alevel' ) ),
								Field::make( 'text', 'link', __( 'Посилання на сайт', 'alevel' ) ),
								Field::make( 'image', 'photo', __( 'Фотографія', 'alevel' ) )
									->set_value_type( 'id' ),
							]
						),
					Field::make( 'association', 'alv_course_post', __( 'Виберіть пов\'язаний курс', 'alevel' ) )
						->set_max( 1 )
						->set_types(
							[
								[
									'type'      => 'post',
									'post_type' => 'courses',
								],
							]
						),
				]
			);
	}

	/**
	 * Add carbon fields to blog page template.
	 *
	 * @return void
	 */
	public function add_carbon_fields_blog_page(): void {
		Container::make( 'post_meta', __( 'Налаштування блогу', 'alevel' ) )
			->where( 'post_template', '=', 'blog.php' )
			->add_fields(
				[
					Field::make( 'textarea', 'alv_blog_custom_title', __( 'Костомний заголовок', 'alevel' ) )
						->set_rows( 4 ),

				]
			);
	}

	/**
	 * Add carbon fields to partners page template.
	 *
	 * @return void
	 */
	public function add_carbon_fields_partners_page(): void {
		Container::make( 'post_meta', __( 'Налаштування блогу', 'alevel' ) )
			->where( 'post_template', '=', 'partners.php' )
			->or_where( 'post_type', '=', 'page' )
			->add_fields(
				[
					Field::make( 'select', 'alv_line_gradient_class', __( 'Вибери градієнт для курсу', 'alevel' ) )
						->add_options( self::LINE_GRADIENT ),
					Field::make( 'textarea', 'alv_partners_custom_title', __( 'Костомний заголовок', 'alevel' ) )
						->set_rows( 4 ),

				]
			);
	}

	/**
	 * Add theme options page.
	 *
	 * @return void
	 */
	public function add_theme_options_page(): void {

		$basic_options_container = Container::make( 'theme_options', __( 'Налаштування теми', 'alevel' ) )
			->set_page_file( 'theme-options' )
			->add_fields(
				[
					Field::make( 'header_scripts', 'alv_header_script', __( 'Script у шапці', 'alevel' ) )
						->set_hook( 'wp_print_scripts', 20 ),
					Field::make( 'footer_scripts', 'alv_footer_script', __( 'Script у підвалі', 'alevel' ) ),
					Field::make( 'textarea', 'alv_footer_text', __( 'Текст під логотипом', 'alevel' ) )
						->set_rows( 4 ),
					Field::make( 'separator', 'alv_separator_black_friday', __( 'Налаштування блоку чорної п\'ятниці', 'alevel' ) ),
					Field::make( 'textarea', 'alv_header_sales_text', __( 'Текст у хедері для чорної п\'ятниці', 'alevel' ) )
						->set_rows( 4 ),
					Field::make( 'text', 'alv_height', __( 'Висота Блоку', 'alevel' ) )
						->set_width( 30 )
						->set_attribute( 'type', 'number' ),
					Field::make( 'color', 'alv_bg_color', __( 'Колір бекграунду', 'alevel' ) )
						->set_width( 30 ),
					Field::make( 'color', 'alv_text_color', __( 'Колір Тексту', 'alevel' ) )
						->set_width( 30 ),
					Field::make( 'text', 'alv_text_size', __( 'Розмір Тексту', 'alevel' ) )
						->set_width( 30 ),
					Field::make( 'select', 'alv_text_weight', __( 'Колір Тексту', 'alevel' ) )
						->add_options(
							[
								'bold'   => __( 'Жирний', 'alevel' ),
								'normal' => __( 'Нормальний', 'alevel' ),
							]
						)
						->set_width( 30 ),
				]
			);

		Container::make( 'theme_options', __( 'Соціальні посилання', 'alevel' ) )
			->set_page_parent( $basic_options_container )
			->add_fields(
				[
					Field::make( 'text', 'alv_facebook_link', __( 'Facebook посилання', 'alevel' ) ),
					Field::make( 'text', 'alv_instagram_link', __( 'Instagram посилання', 'alevel' ) ),
					Field::make( 'text', 'alv_linkedin_link', __( 'Linkedin посилання', 'alevel' ) ),
					Field::make( 'text', 'alv_telegram_link', __( 'Telegram посилання', 'alevel' ) ),
					Field::make( 'text', 'alv_dou_link', __( 'Dou посилання', 'alevel' ) ),
					Field::make( 'text', 'alv_youtube_link', __( 'YouTube посилання', 'alevel' ) ),
					Field::make( 'text', 'alv_twitter_link', __( 'Twitter посилання', 'alevel' ) ),
					Field::make( 'text', 'alv_behance_link', __( 'Behance посилання', 'alevel' ) ),
				]
			);

		Container::make( 'theme_options', __( 'Контакти', 'alevel' ) )
			->set_page_parent( $basic_options_container )
			->add_fields(
				[
					Field::make( 'textarea', 'alv_address', __( 'Адресс', 'alevel' ) ),
					Field::make( 'text', 'alv_phone_one', __( 'Контактний телефон', 'alevel' ) ),
					Field::make( 'text', 'alv_phone_two', __( 'Контактний телефон', 'alevel' ) ),
					Field::make( 'text', 'alv_email', __( 'Контактний email', 'alevel' ) ),
				]
			);

		Container::make( 'theme_options', __( 'Налаштування архівів', 'alevel' ) )
			->set_page_parent( $basic_options_container )
			->add_fields(
				[
					Field::make( 'textarea', 'alv_archive_custom_title_course', __( 'Костомний заголовок курсу', 'alevel' ) )
						->set_rows( 4 ),
					Field::make( 'textarea', 'alv_archive_custom_title_event', __( 'Костомний заголовок події', 'alevel' ) )
						->set_rows( 4 ),
					Field::make( 'textarea', 'alv_archive_custom_title_teacher', __( 'Костомний заголовок вчителів', 'alevel' ) )
						->set_rows( 4 ),
					Field::make( 'textarea', 'alv_archive_custom_title_testimonials', __( 'Костомний заголовок відгуків', 'alevel' ) )
						->set_rows( 4 ),
				]
			);

		Container::make( 'theme_options', __( 'Налаштування pop-up', 'alevel' ) )
			->set_page_parent( $basic_options_container )
			->add_tab(
				__( 'Налаштування подій', 'alevel' ),
				[
					Field::make( 'text', 'alv_modal_register_event_title', __( 'Заголовок вікна реєстрації', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_register_event_sub_title', __( 'Підзаголовок вікна реєстрації', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_register_event_send_to', __( 'Електронна пошта надіслати на', 'alevel' ) )
						->set_attribute( 'type', 'email' ),
					Field::make( 'separator', 'alv_modal_register_event_separator', __( 'Налаштування вікна подяка', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_thanks_event_title', __( 'Заголовок вікна подяки', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_thanks_event_sub_title', __( 'Підзаголовок вікна подяки', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_thanks_event_text_title', __( 'Заголовок перед виведенням події', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_thanks_event_text_sub_title', __( 'Підзаголовок перед виведенням події', 'alevel' ) ),
				]
			)
			->add_tab(
				__( 'Налаштування запит на консультацiю', 'alevel' ),
				[
					Field::make( 'text', 'alv_modal_consultation_title', __( 'Заголовок вікна консультація', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_consultation_sub_title', __( 'Підзаголовок вікна консультація', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_consultation_send_to', __( 'Електронна пошта надіслати на', 'alevel' ) )
						->set_attribute( 'type', 'email' ),
					Field::make( 'separator', 'alv_modal_register_consultation_separator', __( 'Налаштування вікна подяка', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_thanks_consultation_title', __( 'Заголовок вікна подяки', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_thanks_consultation_sub_title', __( 'Підзаголовок вікна подяки', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_thanks_consultation_text_title', __( 'Заголовок перед виведенням консультації', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_thanks_consultation_text_sub_title', __( 'Підзаголовок перед виведенням консультації', 'alevel' ) ),
				]
			)
			->add_tab(
				__( 'Налаштування стати партнером', 'alevel' ),
				[
					Field::make( 'text', 'alv_modal_partner_title', __( 'Заголовок вікна консультація', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_partner_sub_title', __( 'Підзаголовок вікна консультація', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_partner_send_to', __( 'Електронна пошта надіслати на', 'alevel' ) )
						->set_attribute( 'type', 'email' ),
					Field::make( 'separator', 'alv_modal_partner_separator', __( 'Налаштування вікна подяка', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_thanks_partner_title', __( 'Заголовок вікна подяки', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_thanks_partner_sub_title', __( 'Підзаголовок вікна подяки', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_thanks_partner_text_title', __( 'Заголовок перед виведенням консультації', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_thanks_partner_text_sub_title', __( 'Підзаголовок перед виведенням консультації', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_thanks_partner_text_center', __( 'Підзаголовок перед виведенням блогу', 'alevel' ) ),
				]
			)
			->add_tab(
				__( 'Налаштування передзвони мені', 'alevel' ),
				[
					Field::make( 'text', 'alv_modal_callback_title', __( 'Заголовок вікна консультація', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_callback_sub_title', __( 'Підзаголовок вікна консультація', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_callback_middle_text', __( 'Текс вибору часу', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_callback_send_to', __( 'Електронна пошта надіслати на', 'alevel' ) )
						->set_attribute( 'type', 'email' ),
					Field::make( 'separator', 'alv_modal_callback_separator', __( 'Налаштування вікна подяка', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_thanks_callback_title', __( 'Заголовок вікна подяки', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_thanks_callback_sub_title', __( 'Підзаголовок вікна подяки', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_thanks_callback_text_title', __( 'Заголовок перед виведенням консультації', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_thanks_callback_text_sub_title', __( 'Підзаголовок перед виведенням консультації', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_thanks_callback_text_center', __( 'Підзаголовок перед виведенням блогу', 'alevel' ) ),
				]
			)
			->add_tab(
				__( 'Налаштування залишити відгук', 'alevel' ),
				[
					Field::make( 'text', 'alv_modal_leave_review_title', __( 'Заголовок вікна відгук', 'alevel' ) ),
					Field::make( 'textarea', 'alv_modal_leave_review_sub_title', __( 'Підзаголовок вікна відгук', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_leave_review_social_text', __( 'Текст над соціальними іконками', 'alevel' ) ),
					Field::make( 'text', 'alv_modal_leave_review_send_to', __( 'Електронна пошта надіслати на', 'alevel' ) )
						->set_attribute( 'type', 'email' ),
				]
			);

		Container::make( 'theme_options', __( 'Налаштування 404 помилки', 'alevel' ) )
			->set_page_parent( $basic_options_container )
			->add_fields(
				[
					Field::make( 'textarea', 'alv_404_error_title', __( 'Заголовок 404 сторінки', 'alevel' ) )
						->set_rows( 4 ),
					Field::make( 'textarea', 'alv_404_error_sub_title', __( 'Підзаголовок 404 сторінки', 'alevel' ) )
						->set_rows( 4 ),
					Field::make( 'textarea', 'alv_404_error_text', __( 'Текст', 'alevel' ) )
						->set_rows( 4 ),
				]
			);
	}

	/**
	 * Add Network settings shortcode.
	 *
	 * @return void
	 */
	public function add_networks_settings(): void {
		$all_sites = '';
		if ( is_multisite() ) {
			$all_sites = get_sites(
				[
					'orderby' => 'id',
					'order'   => 'ASC',
				]
			);
		}

		if ( ! empty( $all_sites ) ) {
			$fields = [];
			foreach ( $all_sites as $site ) {
				$fields[] = Field::make( 'separator', 'alv_site_name_' . $site->blog_id . '_separator', __( 'Налаштування темплетери на сайті:', 'alevel' ) . $site->domain );
				$fields[] = Field::make( 'text', 'alv_short_code' . $site->blog_id . '_events', __( 'Шорт код для подій:', 'alevel' ) )
					->set_help_text( __( 'Введіть ID шорткоду', 'alevel' ) );
				$fields[] = Field::make( 'text', 'alv_short_code' . $site->blog_id . '_faq', __( 'Шорт код для FAQ:', 'alevel' ) )
					->set_help_text( __( 'Введіть ID шорткоду', 'alevel' ) );
				$fields[] = Field::make( 'text', 'alv_short_code' . $site->blog_id . '_logo_slider', __( 'Шорт код для слайдер логотипів:', 'alevel' ) )
					->set_help_text( __( 'Введіть ID шорткоду', 'alevel' ) );
				$fields[] = Field::make( 'text', 'alv_short_code' . $site->blog_id . '_review', __( 'Шорт код відгуків:', 'alevel' ) )
					->set_help_text( __( 'Введіть ID шорткоду', 'alevel' ) );
				$fields[] = Field::make( 'text', 'alv_short_code' . $site->blog_id . '_review_block', __( 'Шорт код відгуків блок:', 'alevel' ) )
					->set_help_text( __( 'Введіть ID шорткоду', 'alevel' ) );
				$fields[] = Field::make( 'text', 'alv_short_code' . $site->blog_id . '_teachers_title', __( 'Шорт код вчителі:', 'alevel' ) )
					->set_help_text( __( 'Введіть ID шорткоду', 'alevel' ) );
			}
			Container::make( 'network', 'crb_network_container', 'Network Settings' )
				->add_tab(
					__( 'Налаштування подій', 'alevel' ),
					$fields,
				)->add_tab(
					__( 'Налаштування поп-ап акції', 'alevel' ),
					[
						Field::make( 'radio', 'alv_pop_up_sales_start', __( 'Відображення вікна акції', 'alevel' ) )
							->add_options(
								[
									'off' => __( 'Вимкнути', 'alevel' ),
									'on'  => __( 'Увімкнути', 'alevel' ),
								]
							),
						Field::make( 'text', 'alv_pop_up_sales_title', __( 'Заголовок поп-ап', 'alevel' ) ),
						Field::make( 'text', 'alv_pop_up_sales_sub_title', __( 'Под заголовок поп-ап', 'alevel' ) ),
						Field::make( 'text', 'alv_pop_up_sales_text_sales', __( 'Текст знижки', 'alevel' ) ),
						Field::make( 'date', 'alv_pop_up_sales_end_date', __( 'Дата кінця акції', 'alevel' ) ),
					]
				)->add_tab(
					__( 'Налаштування кількості місць та дати старту курсів', 'alevel' ),
					$this->get_course_field_date()
				)->add_tab(
					__( 'Налаштування pipedrive API', 'alevel' ),
					[
						Field::make( 'text', 'alv_pipedrive_api_key', __( 'Pipedrive api key', 'alevel' ) ),
					]
				);
		}
	}

	/**
	 * Get all course field date and number of seats.
	 *
	 * @return array
	 */
	private function get_course_field_date(): array {

		$arg = [
			'post_type'      => 'courses',
			'posts_per_page' => - 1,
			'post_status'    => 'publish',
			'order'          => 'ASC',
			'orderby'        => 'ID',
		];

		$query = new \WP_Query( $arg );

		if ( $query->have_posts() ) {
			$fields = [];
			while ( $query->have_posts() ) {
				$query->the_post();

				$slug = get_post_field( 'post_name', get_the_ID() );

				$fields[] = Field::make( 'separator', 'alv_' . $slug . '_separator', __( 'Курс:', 'alevel' ) . get_the_title() );
				$fields[] = Field::make( 'date', 'alv__' . $slug . '__date', __( 'Дата старту:', 'alevel' ) )
					->set_storage_format( 'd.m.Y' )
					->set_width( 50 );
				$fields[] = Field::make( 'text', 'alv__' . $slug . '__seats', __( 'Кількість місць:', 'alevel' ) )
					->set_width( 50 );

			}
		} else {
			return [
				Field::make( 'separator', 'alv_no_found_separator', __( 'Немає курсів', 'alevel' ) . get_the_title() ),
			];
		}

		return $fields;
	}

	/**
	 * Update date and seats fields.
	 *
	 * @param bool $save
	 * @param      $value
	 * @param      $field
	 *
	 * @return mixed
	 */
	public function network_save_courses( $save, $value, $field ) {

		if ( empty( $value ) ) {
			return $value;
		}

		if ( preg_match( '/[seats]{5}$/', $field->get_base_name() ) ) {
			$slug = explode( '__', $field->get_base_name() )[1];
			if ( ! empty( $slug ) && is_multisite() ) {
				$all_sites = get_sites(
					[
						'orderby' => 'id',
						'order'   => 'ASC',
					]
				);

				foreach ( $all_sites as $site ) {
					switch_to_blog( $site->blog_id );

					$post_ids = get_posts(
						[
							'name'        => $slug,
							'post_type'   => 'courses',
							'numberposts' => 1,
							'fields'      => 'ids',
						]
					);
					$post_ids = array_shift( $post_ids );

					update_post_meta( $post_ids, '_alv_course_available_seats', $value );
				}

			}
		}

		if ( preg_match( '/[date]{4}$/', $field->get_base_name() ) ) {
			$slug = explode( '__', $field->get_base_name() )[1];
			if ( ! empty( $slug ) && is_multisite() ) {
				$all_sites = get_sites(
					[
						'orderby' => 'id',
						'order'   => 'ASC',
					]
				);

				foreach ( $all_sites as $site ) {
					switch_to_blog( $site->blog_id );

					$post_ids = get_posts(
						[
							'name'        => $slug,
							'post_type'   => 'courses',
							'numberposts' => 1,
							'fields'      => 'ids',
						]
					);
					$post_ids = array_shift( $post_ids );

					update_post_meta( $post_ids, '_alv_course_start', $value );
				}

			}
		}

		return $value;
	}

	/**
	 * Save teachers courser
	 *
	 * @param bool         $save  Save.
	 * @param string|array $value Values.
	 * @param mixed        $field Carbon fields classes.
	 *
	 * @return mixed
	 */
	public function save_alv_teacher_course( bool $save, $value, $field ) {
		if ( 'alv_teacher_course' === $field->get_base_name() ) {

			if ( 0 === count( $value ) ) {
				return $value;
			}

			global $post;

			foreach ( $value as $item ) {
				$mata_value_old = get_post_meta( $item['id'], 'alv_course_teachers', true );
				if ( ! empty( $mata_value_old ) ) {
					$mata_value = $mata_value_old;
				}
				$mata_value[] = $post->ID;
				$result       = array_unique( $mata_value );
				update_post_meta( $item['id'], 'alv_course_teachers', $result );
			}
		}

		return $value;
	}
}
