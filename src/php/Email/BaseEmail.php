<?php
/**
 * Base email interface.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\Email;

/**
 * BaseEmail class file.
 */
abstract class BaseEmail {

	/**
	 * Send abstract email to customer.
	 *
	 * @param string      $subject Email subject.
	 * @param string      $body    Email body.
	 * @param string|null $email   Email send to.
	 */
	protected static function send( string $subject, string $body, string $email ): void {
		ob_start();
		?>
		<div class="body-email">
			<?php echo wp_kses_post( $body ); ?>
		</div>
		<?php

		$email_content = ob_get_clean();

		$to = $email;

		$headers = 'MIME-Version: 1.0' . "\r\n";

		$headers .= 'Content-type:text/html; charset=UTF-8' . "\r\n";
		$headers .= 'From: GTS <admin@gts-translation.com>' . "\r\n";
		wp_mail( $to, $subject, $email_content, $headers );
	}

	/**
	 * Generate email content.
	 *
	 * @param array $arg Arguments.
	 *
	 * @return string HTML string.
	 */
	protected static function generate_content( array $arg ): string {
		$content = '';

		foreach ( $arg as $key => $item ) {
			switch ( $key ) {
				case 'email':
					$text = __( 'Електронна пошта:', 'alevel' );

					$content .= "<p>$text <b>$item</b></p>";
					break;
				case 'first_name':
				case 'name':
					$text = __( 'Ім\'я:', 'alevel' );

					$content .= "<p>$text <b>$item</b></p>";
					break;
				case 'last_name':
					$text = __( 'Прiзвище:', 'alevel' );

					$content .= "<p>$text <b>$item</b></p>";
					break;
				case 'partner_last_mane':
					$text = __( 'Прiзвище представника', 'alevel' );

					$content .= "<p>$text <b>$item</b></p>";
					break;
				case 'company':
					$text = __( 'Компанiя', 'alevel' );

					$content .= "<p>$text <b>$item</b></p>";
					break;
				case 'partner_web':
					$text  = __( 'Веб сайт', 'alevel' );
					$url   = esc_url( $item );
					$title = esc_html( $item );

					$content .= "<p>$text <a href='$url'>$title</a></p>";
					break;
				case 'linkedin':
					$text  = __( 'Linkedin', 'alevel' );
					$url   = esc_url( $item );
					$title = esc_html( $item );

					$content .= "<p>$text <a href='$url'>$title</a></p>";
					break;
				case 'phone':
					$text = __( 'Телефон:', 'alevel' );

					$content .= "<p>$text <b>$item</b></p>";
					break;
				case 'event_id':
					$text  = __( 'Реєстрація на подію:', 'alevel' );
					$url   = esc_url( get_the_permalink( $item ) );
					$title = esc_html( get_the_title( $item ) );

					$content .= "<h1>$text <a href='$url'>$title</a></h1>";
					break;
				case 'title':
					$content .= "<h1>$item</h1>";
					break;
				case 'description':
					$content .= wp_kses_post( wpautop( $text ) );
					break;
				case 'method_payment':
					$text = __( 'Спосіб оплати', 'alevel' );

					$content .= "<p>$text <b>$item</b></p>";
					break;
				case 'from_count_payment':
					if ( ! empty( $data['from_count_payment'] ) ) {
						$text = __( 'Кількість місяців на розстрочку моно банку:', 'alevel' );

						$content .= "<p>$text <b>$item</b></p>";
					}
					break;
				case 'date':
					$text = __( 'Дата для дзвінка:', 'alevel' );

					$content .= "<p>$text <b>$item</b></p>";
					break;
				case 'time':
					$text = __( 'Час дзвінка:', 'alevel' );

					$content .= "<p>$text <b>$item</b></p>";
					break;
			}
		}

		return $content;
	}
}
