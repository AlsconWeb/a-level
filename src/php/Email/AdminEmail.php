<?php
/**
 * Send Admin email.
 *
 * @package iwpdev/alevel
 */

namespace Alevel\Email;

/**
 * AdminEmail class file.
 */
class AdminEmail extends BaseEmail {
	/**
	 * Send admin register to event.
	 *
	 * @param array $arg Arguments.
	 *
	 * @return void
	 */
	public static function send_request_to_event_register( array $arg ): void {
		$content = self::generate_content( $arg );
		$subject = __( 'Заявка на участь у події', 'alevel' );
		$email   = carbon_get_theme_option( 'alv_modal_register_event_send_to' ) ?? get_option( 'admin_email' );

		self::send( $subject, $content, $email );
	}

	/**
	 * Send admin consultation to course.
	 *
	 * @param array $arg Arguments.
	 *
	 * @return void
	 */
	public static function send_request_to_consultation_course( array $arg ): void {
		$content = self::generate_content( $arg );
		$subject = __( 'Заявка на консультацію', 'alevel' );
		$email   = carbon_get_theme_option( 'alv_modal_consultation_send_to' ) ?? get_option( 'admin_email' );

		self::send( $subject, $content, $email );
	}

	/**
	 * Send admin become partner.
	 *
	 * @param array $arg Arguments.
	 *
	 * @return void
	 */
	public static function send_request_become_partners( array $arg ): void {
		$content = self::generate_content( $arg );
		$subject = __( 'Заявка на стати партнером', 'alevel' );
		$email   = carbon_get_theme_option( 'alv_modal_partner_send_to' ) ?? get_option( 'admin_email' );

		self::send( $subject, $content, $email );
	}

	/**
	 * Send admin payment from.
	 *
	 * @param array $arg Arguments.
	 *
	 * @return void
	 */
	public static function send_request_payment_course( array $arg ): void {
		$content = self::generate_content( $arg );
		$subject = $arg['title'];
		$email   = get_option( 'admin_email' );

		self::send( $subject, $content, $email );
	}

	/**
	 * Send admin call back from.
	 *
	 * @param array $arg Arguments.
	 *
	 * @return void
	 */
	public static function send_request_call_back( array $arg ): void {
		$content = self::generate_content( $arg );
		$subject = $arg['title'];
		$email   = carbon_get_theme_option( 'alv_modal_callback_send_to' ) ?? get_option( 'admin_email' );

		self::send( $subject, $content, $email );
	}

}
