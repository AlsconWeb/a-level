<?php
/**
 * Main class theme A-Level.
 *
 * @package iwpdev/alevel
 */

namespace Alevel;

use Alevel\Ajax\Blog;
use Alevel\Ajax\ModalHandler;
use Alevel\Ajax\Partner;
use Alevel\Ajax\Testimonials as TestimonialsAjax;
use Alevel\Helpers\Helper;
use Alevel\WpBakery\Components\AnchorageMenu\AnchorageMenu;
use Alevel\WpBakery\Components\Blog\Blog as BlogComponents;
use Alevel\WpBakery\Components\BonusDescription\BonusDescription;
use Alevel\WpBakery\Components\CompanyTestimonials\CompanyTestimonials;
use Alevel\WpBakery\Components\ContactBlock\ContactBlock;
use Alevel\WpBakery\Components\CourseProgram\CourseProgramAccordion;
use Alevel\WpBakery\Components\CoursesAndFilter\CoursesAndFilter;
use Alevel\WpBakery\Components\CoursesSlider\CoursesSlider;
use Alevel\WpBakery\Components\EmploymentAtSchool\EmploymentAtSchool;
use Alevel\WpBakery\Components\EventPost\EventPost;
use Alevel\WpBakery\Components\EventsSlider\EventsSlider;
use Alevel\WpBakery\Components\FAQ\FAQBlock;
use Alevel\WpBakery\Components\FAQCourse\FAQCourse;
use Alevel\WpBakery\Components\HistoryBlock\HistoryBlock;
use Alevel\WpBakery\Components\HistoryImages\HistoryImages;
use Alevel\WpBakery\Components\HistoryStoresSteps\HistoryStoresSteps;
use Alevel\WpBakery\Components\LogoSlider\LogoSlider;
use Alevel\WpBakery\Components\PaymentForm\PaymentForm;
use Alevel\WpBakery\Components\RatingsSchool\RatingsSchool;
use Alevel\WpBakery\Components\SalaryCalculator\SalaryCalculator;
use Alevel\WpBakery\Components\SuccessFactor\SuccessFactor;
use Alevel\WpBakery\Components\TeachersCourse\TeachersCourse;
use Alevel\WpBakery\Components\TestimonialsCourse\Testimonials;
use Alevel\WpBakery\Components\Tools\ToolsBlock;
use Alevel\WpBakery\Components\YourCV\YourCVBlock;
use DateTime;
use WP_Customize_Image_Control;
use WP_Query;

/**
 * Main class file.
 */
class Main {

	/**
	 * Dir path.
	 */
	public const ALV_DIR_PATH = __DIR__;

	/**
	 * Version
	 */
	public const ALV_VERSION = '1.11.3';

	/**
	 * Color pattern.
	 */
	public const ALV_COLOR_PATTERN = [
		'#4BD2FD',
		'#406BD9',
		'#1E3F37',
		'#9AEECF',
		'#9D82C9',
		'#F5C2E5',
	];

	/**
	 * Short code name.
	 */
	public const ALV_SHORT_CODE_MODAL_NAME = [
		'alv_teachers_course',
		'alv_payment_form',
		'alv_testimonials_course',
	];

	/**
	 * Main construct
	 */
	public function __construct() {
		$this->init();
		new CPT();
		new CarbonFields();
		new ModalHandler();
		new Blog();
		new TestimonialsAjax();
		new NewHandlerCF7();
		new Partner();

		if ( ! wp_next_scheduled( 'alv_update_event_status' ) ) {
			wp_schedule_event( time(), 'hourly', 'alv_update_event_status' );
		}
	}

	/**
	 * Init hooks.
	 *
	 * @return void
	 */
	public function init(): void {

		add_action( 'wp_enqueue_scripts', [ $this, 'add_script' ] );
		add_action( 'after_setup_theme', [ $this, 'add_theme_support' ] );
		add_action( 'vc_before_init', [ $this, 'add_bakery_components' ] );
		add_action( 'admin_enqueue_scripts', [ $this, 'add_admin_script' ] );
		add_action( 'customize_register', [ $this, 'add_white_logo' ] );
		add_action( 'init', [ $this, 'add_post_status_end_event' ] );
		add_action( 'admin_footer', [ $this, 'display_custom_post_status_option' ] );
		add_action( 'alv_update_event_status', [ $this, 'update_event_status' ] );

		add_filter( 'mime_types', [ $this, 'add_support_mimes' ] );
		add_filter( 'get_custom_logo', [ $this, 'output_logo' ] );
		add_filter( 'display_post_states', [ $this, 'display_events_state' ] );
		add_filter( 'excerpt_more', [ $this, 'delete_excerpt_more' ] );
	}

	/**
	 * Output select change city.
	 *
	 * @param string|null $position Position id.
	 *
	 * @return void
	 */
	public static function get_city_in_select( string $position = null ): void {
		$menu_name = 'top_bar_menu';
		$locations = get_nav_menu_locations();

		if ( $locations && isset( $locations[ $menu_name ] ) ) {

			$menu_items = wp_get_nav_menu_items( $locations[ $menu_name ] );
			?>
			<label for="city-<?php echo esc_attr( $position ?? 'header' ); ?>" class="hidden"></label>
			<select name="city" id="city-<?php echo esc_attr( $position ?? 'header' ); ?>">
				<?php
				foreach ( (array) $menu_items as $menu_item ) {
					?>
					<option value="<?php echo esc_url( $menu_item->url ); ?>" <?php selected( $menu_item->url, get_bloginfo( 'url' ) . '/' ); ?>>
						<?php echo esc_html( $menu_item->title ); ?>
					</option>
					<?php
				}
				?>
			</select>
			<?php
		}
	}

	/**
	 * Show switch language.
	 *
	 * @param string $location Location output from id.
	 *
	 * @return void
	 */
	public static function show_switch_language( string $location ): void {
		$languages = [];
		if ( function_exists( 'icl_get_languages' ) ) {
			$languages = icl_get_languages( 'skip_missing=0&orderby=code' );
		}

		if ( defined( 'ICL_LANGUAGE_CODE' ) ) {
			?>
			<div class="language dfr">
				<div class="radio-button">
					<input
							id="ua-<?php echo esc_attr( $location ); ?>"
							type="radio"
							name="language-<?php echo esc_attr( $location ); ?>"
							value="uk"
							data-url_lang="<?php echo esc_url( $languages['uk']['url'] ?? '#' ); ?>"
						<?php echo 'uk' === ICL_LANGUAGE_CODE ? 'checked' : ''; ?>>
					<label for="ua-<?php echo esc_attr( $location ); ?>">Ua</label>
				</div>
				<div class="radio-button">
					<input
							id="ru-<?php echo esc_attr( $location ); ?>"
							type="radio"
							name="language-<?php echo esc_attr( $location ); ?>"
						<?php echo 'ru' === ICL_LANGUAGE_CODE ? 'checked' : ''; ?>
							data-url_lang="<?php echo esc_url( $languages['ru']['url'] ?? '#' ); ?>" value="ru">
					<label for="ru-<?php echo esc_attr( $location ); ?>">Ru</label>
				</div>
			</div>
			<?php
		}
	}

	/**
	 * Add scripts and style.
	 *
	 * @return void
	 */
	public function add_script(): void {

		wp_enqueue_script(
			'alv_moment',
			get_template_directory_uri() . '/assets/js/moment-with-locales.js',
			[
				'build',
			],
			'2.9.0',
			true
		);

		wp_enqueue_script(
			'alv_datepiket',
			'//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/src/js/bootstrap-datetimepicker.js',
			[
				'jquery',
				'build',
			],
			'2.9.0',
			true
		);

		wp_enqueue_script(
			'build',
			get_template_directory_uri() . '/assets/js/build.js',
			[
				'jquery',
				'mixitup',
				'moment',
				'mosaic',
			],
			self::ALV_VERSION,
			true
		);

		wp_enqueue_script(
			'mosaic',
			get_template_directory_uri() . '/assets/js/jquery.mosaic.min.js',
			[
				'jquery',
			],
			self::ALV_VERSION,
			true
		);

		wp_enqueue_script(
			'alv-modal-ajax',
			get_template_directory_uri() . '/assets/js/modal-ajax.js',
			[
				'jquery',
				'build',
				'alv-modal',
				'sweetalert2',
			],
			self::ALV_VERSION,
			true
		);

		wp_localize_script(
			'alv-modal-ajax',
			'ModalObject',
			[
				'url'                   => admin_url( 'admin-ajax.php' ),
				'actionNamesNewTeacher' => ModalHandler::NEW_TEACHER,
				'actionNameNewReview'   => ModalHandler::NEW_REVIEW,
				'actionNameGetTeachers' => ModalHandler::GET_COURSE_TEACHERS,
				'nonceGetTeachers'      => wp_create_nonce( ModalHandler::GET_COURSE_TEACHERS ),
				'beforeSendNewTeacher'  => __( 'Ми обробляємо ваші дані зачекайте закінчення обробки', 'alevel' ),
				'validateFormText'      => [
					'first_name'            => __( 'Порожнє поле імені', 'alevel' ),
					'last_name'             => __( 'Порожнє поле прізвище', 'alevel' ),
					'email'                 => __( 'Email порожній чи не коректний', 'alevel' ),
					'phone'                 => __( 'Телефон порожній або неправильно введений: +380...', 'alevel' ),
					'avatar_file'           => __( 'Завантажте фото', 'alevel' ),
					'leave_review_teachers' => __( 'Ви повинні вибрати хоча б одного викладача', 'alevel' ),
					'message'               => __( 'Ви не залишили коментар до відгуку', 'alevel' ),
					'rating'                => __( 'Виберіть рейтинг цього курсу', 'alevel' ),
					'social'                => __( 'Неправильний URL соціальної мережі. Приклад: http://exemple.com', 'alevel' ),
				],
			]
		);

		wp_enqueue_script(
			'alv-payment-from-ajax',
			get_template_directory_uri() . '/assets/js/payment-from-ajax.js',
			[
				'jquery',
				'build',
				'sweetalert2',
			],
			self::ALV_VERSION,
			true
		);

		wp_localize_script(
			'alv-payment-from-ajax',
			'paymentFormObject',
			[
				'url'                    => admin_url( 'admin-ajax.php' ),
				'actionNamesPaymentForm' => ModalHandler::COURSE_PAYMENT_FORM,
				'ajaxBeforeSendTitle'    => __( 'Зачекайте до кінця обробки запиту', 'alevel' ),
			]
		);

		wp_enqueue_script(
			'alv-blog-ajax',
			get_template_directory_uri() . '/assets/js/blog-ajax.js',
			[
				'jquery',
				'build',
				'sweetalert2',
			],
			self::ALV_VERSION,
			true
		);

		wp_localize_script(
			'alv-blog-ajax',
			'blogObject',
			[
				'url'                 => admin_url( 'admin-ajax.php' ),
				'actionNamesCategory' => Blog::CATEGORY_ACTION,
				'alvBlogNonce'        => wp_create_nonce( Blog::CATEGORY_ACTION ),
				'ajaxBeforeSendTitle' => __( 'Зачекайте до кінця обробки запиту', 'alevel' ),
			]
		);

		if (
			is_post_type_archive( 'testimonials' ) ||
			is_singular( 'teachers' ) ||
			is_tax( 'teacher-specialization' ) ||
			Helper::alv_has_shortcode_array( self::ALV_SHORT_CODE_MODAL_NAME ) ||
			is_front_page() ||
			is_page(
				[
					553,
					1613,
				]
			)
		) {
			wp_enqueue_script(
				'alv-review-ajax',
				get_template_directory_uri() . '/assets/js/review-ajax.js',
				[
					'jquery',
					'build',
					'sweetalert2',
				],
				self::ALV_VERSION,
				true
			);

			wp_enqueue_script(
				'alv-review-regular-ajax',
				get_template_directory_uri() . '/assets/js/review-regular-ajax.js',
				[
					'jquery',
					'build',
					'sweetalert2',
				],
				self::ALV_VERSION,
				true
			);


			wp_localize_script(
				'alv-review-ajax',
				'reviewObject',
				[
					'url'                             => admin_url( 'admin-ajax.php' ),
					'actionNamesTestimonialsCategory' => TestimonialsAjax::TESTIMONIALS_CATEGORY_ACTION,
					'actionNamesTestimonialsRegular'  => ModalHandler::REVIEW_REGULAR_ACTION,
					'alvTestimonialsNonce'            => wp_create_nonce( TestimonialsAjax::TESTIMONIALS_CATEGORY_ACTION ),
					'alvTestimonialsRegularNonce'     => wp_create_nonce( ModalHandler::REVIEW_REGULAR_ACTION ),
					'ajaxBeforeSendTitle'             => __( 'Зачекайте до кінця обробки запиту', 'alevel' ),
				]
			);

		}

		if ( is_post_type_archive( 'events' ) ) {
			wp_enqueue_script(
				'alv-register-event-ajax',
				get_template_directory_uri() . '/assets/js/register-event-ajax.js',
				[
					'jquery',
					'build',
					'sweetalert2',
					'alv-modal',
				],
				self::ALV_VERSION,
				true
			);

			wp_localize_script(
				'alv-register-event-ajax',
				'eventObject',
				[
					'url'                      => admin_url( 'admin-ajax.php' ),
					'actionNamesEventRegister' => ModalHandler::REGISTER_ON_EVENT,
					'ajaxBeforeSendTitle'      => __( 'Зачекайте до кінця обробки запиту', 'alevel' ),
				]
			);
		}

		if ( is_singular( 'courses' ) ) {
			wp_enqueue_script(
				'alv-jquery-ui',
				'//ajax.aspnetcdn.com/ajax/jquery.ui/1.10.3/jquery-ui.min.js',
				[
					'jquery',
				],
				self::ALV_VERSION,
				true
			);
		}

		wp_enqueue_script(
			'alv-become-partner-ajax',
			get_template_directory_uri() . '/assets/js/become-partner-ajax.js',
			[
				'jquery',
				'build',
				'sweetalert2',
				'alv-modal',
			],
			self::ALV_VERSION,
			true
		);

		wp_localize_script(
			'alv-become-partner-ajax',
			'partnerObject',
			[
				'url'                      => admin_url( 'admin-ajax.php' ),
				'actionNamesEventRegister' => ModalHandler::REGISTER_PARTNERS,
				'ajaxBeforeSendTitle'      => __( 'Зачекайте до кінця обробки запиту', 'alevel' ),
			]
		);

		wp_enqueue_script(
			'alv-modal',
			get_template_directory_uri() . '/assets/js/modal.js',
			[
				'jquery',
				'build',
			],
			self::ALV_VERSION,
			true
		);

		wp_enqueue_script(
			'alv-register-consultation',
			get_template_directory_uri() . '/assets/js/register-consultation.js',
			[
				'jquery',
				'build',
				'sweetalert2',
				'alv-modal',
			],
			self::ALV_VERSION,
			true
		);

		wp_localize_script(
			'alv-register-consultation',
			'courseObject',
			[
				'url'                       => admin_url( 'admin-ajax.php' ),
				'actionNamesCourseRegister' => ModalHandler::REGISTER_ON_COURSE,
				'ajaxBeforeSendTitle'       => __( 'Зачекайте до кінця обробки запиту', 'alevel' ),
			]
		);
		wp_enqueue_script(
			'intlTelInput',
			'//cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.0/js/intlTelInput.min.js',
			[
				'jquery',
			],
			self::ALV_VERSION,
			true
		);
		wp_enqueue_script(
			'inputmask-bundle',
			'//cdnjs.cloudflare.com/ajax/libs/inputmask/4.0.8/jquery.inputmask.bundle.min.js',
			[
				'jquery',
			],
			self::ALV_VERSION,
			true
		);
		wp_enqueue_script(
			'alv-main',
			get_template_directory_uri() . '/assets/js/main.js',
			[
				'jquery',
				'build',
				'wp-i18n',
			],
			self::ALV_VERSION,
			true
		);

		wp_enqueue_script(
			'alv-call-back-modal-ajax',
			get_template_directory_uri() . '/assets/js/call-back-modal-ajax.js',
			[
				'jquery',
				'build',
				'sweetalert2',
				'alv-modal',
			],
			self::ALV_VERSION,
			true
		);

		wp_localize_script(
			'alv-call-back-modal-ajax',
			'callBackModalObject',
			[
				'url'                 => admin_url( 'admin-ajax.php' ),
				'actionNamesCallBack' => ModalHandler::CALL_BACK_MODAL_ACTION,
				'ajaxBeforeSendTitle' => __( 'Зачекайте до кінця обробки запиту', 'alevel' ),
			]
		);

		if ( is_page_template( 'partners.php' ) ) {
			wp_enqueue_script(
				'alv-partner-page',
				get_template_directory_uri() . '/assets/js/partner-page.js',
				[
					'jquery',
					'build',
				],
				self::ALV_VERSION,
				true
			);

			wp_localize_script(
				'alv-call-back-modal-ajax',
				'callPartnerObj',
				[
					'url'                 => admin_url( 'admin-ajax.php' ),
					'actionNamesPartner'  => Partner::PARTNER_TYPE_ACTION,
					'noncePartner'        => wp_create_nonce( Partner::PARTNER_TYPE_ACTION ),
					'ajaxBeforeSendTitle' => __( 'Зачекайте до кінця обробки запиту', 'alevel' ),
				]
			);
		}

		wp_enqueue_script( 'sweetalert2', '//cdn.jsdelivr.net/npm/sweetalert2@11', [ 'jquery' ], '2.11', 'true' );

		wp_enqueue_script( 'mixitup', get_template_directory_uri() . '/assets/js/mixitup.min.js', [ 'jquery' ], '3.3.1', true );

		wp_enqueue_script( 'html5shiv', '//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js', [], '3.7.0', false );
		wp_enqueue_script( 'respond', '//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js', [], '1.4.2', false );
		wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );
		wp_script_add_data( 'respond', 'conditional', 'lt IE 9' );

		wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css2?family=Inter:wght@400;700&family=Montserrat:wght@900&display=swap', '', self::ALV_VERSION );
		wp_enqueue_style( 'main', get_template_directory_uri() . '/assets/css/main.css', '', self::ALV_VERSION );
		wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css', '', self::ALV_VERSION );
		wp_enqueue_style( 'intl-tel-input', get_template_directory_uri() . '/assets/css/intlTelInput.css', '', self::ALV_VERSION );

		wp_deregister_style( 'js_composer_front' );
	}

	/**
	 * Add script and style to admin panel.
	 *
	 * @return void
	 */
	public function add_admin_script(): void {
		wp_enqueue_style( 'admin-css', get_template_directory_uri() . '/assets/css/admin.css', [], self::ALV_VERSION );
	}

	/**
	 * Add SVG and Webp formats to upload.
	 *
	 * @param array $mimes Mimes type.
	 *
	 * @return array
	 */
	public function add_support_mimes( array $mimes ): array {

		$mimes['webp'] = 'image/webp';
		$mimes['svg']  = 'image/svg+xml';

		return $mimes;
	}

	/**
	 * Theme support.
	 *
	 * @return void
	 */
	public function add_theme_support(): void {

		// add custom logo.
		add_theme_support( 'custom-logo', [ 'unlink-homepage-logo' => true ] );

		// add menu support.
		add_theme_support( 'menus' );

		register_nav_menus(
			[
				'top_bar_menu' => __( 'Меню верхньої панелі', 'alevel' ),
				'main_menu'    => __( 'Головне меню', 'alevel' ),
				'scroll_menu'  => __( 'Ковзне меню', 'alevel' ),
			]
		);

		// add images sizes.
		add_image_size( 'alv-courses-icon', 80, 80, [ 'top', 'center' ] );
		add_image_size( 'alv-teacher-avatar', 100, 100, [ 'top', 'center' ] );
		add_image_size( 'alv-thmb_mini', 165, 70, [ 'top', 'center' ] );
		add_image_size( 'alv-event', 780, 320, [ 'top', 'center' ] );
		add_image_size( 'alv-blog', 780, 440, [ 'top', 'center' ] );
		add_image_size( 'alv-thumbnail-big', 564, 317, [ 'top', 'center' ] );
		add_image_size( 'alv-blog-min', 248, 140, [ 'top', 'center' ] );
		add_image_size( 'alv-blog-sticky', 512, 288, [ 'top', 'center' ] );
	}

	/**
	 * Change output custom logo.
	 *
	 * @param string $html HTML custom logo.
	 *
	 * @return string
	 */
	public function output_logo( string $html ): string {

		$home  = esc_url( get_bloginfo( 'url' ) );
		$class = 'logo';
		if ( has_custom_logo() ) {
			$white_logo = get_theme_mod( 'alv_logo' );
			$logo       = wp_get_attachment_image(
				get_theme_mod( 'custom_logo' ),
				'full',
				false,
				[
					'class'    => 'black-logo',
					'itemprop' => 'logo',
				]
			);
			$content    = $logo;

			if ( ! empty( $white_logo ) ) {
				$content .= '<img src="' . esc_url( $white_logo ) . '" class="white-logo" alt="White logo">';
			}

			$content .= '<span class="sr-only">' . get_bloginfo( 'name' ) . ' | ' . get_bloginfo( 'description' ) . '</span>';

			$html = sprintf(
				'<a href="%s" class="%s" rel="home" itemprop="url">%s</a>',
				$home,
				$class,
				$content
			);

		}

		return $html;
	}

	/**
	 * Add White logo.
	 *
	 * @param $wp_customize
	 *
	 * @return void
	 */
	public function add_white_logo( $wp_customize ): void {
		$wp_customize->add_setting( 'alv_logo' );
		$wp_customize->add_setting( 'alv_logo_footer' );

		$wp_customize->add_control(
			new WP_Customize_Image_Control(
				$wp_customize,
				'alv_white_control',
				[
					'label'         => __( 'Завантажити логотип', 'alevel' ),
					'priority'      => 20,
					'section'       => 'title_tagline',
					'settings'      => 'alv_logo',
					'button_labels' => [
						'select' => __( 'Виберіть Логотип', 'alevel' ),
						'remove' => __( 'Видалити логотип', 'alevel' ),
						'change' => __( 'Змінити логотип', 'alevel' ),
					],
				]
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Image_Control(
				$wp_customize,
				'alv_white_footer_control',
				[
					'label'         => __( 'Завантажити логотип для підвалу', 'alevel' ),
					'priority'      => 22,
					'section'       => 'title_tagline',
					'settings'      => 'alv_logo_footer',
					'button_labels' => [
						'select' => __( 'Виберіть Логотип', 'alevel' ),
						'remove' => __( 'Видалити логотип', 'alevel' ),
						'change' => __( 'Змінити логотип', 'alevel' ),
					],
				]
			)
		);
	}

	/**
	 * Add WPBakery components.
	 *
	 * @return void
	 */
	public function add_bakery_components(): void {
		new AnchorageMenu();
		new CourseProgramAccordion();
		new BonusDescription();
		new FAQCourse();
		new TeachersCourse();
		new Testimonials();
		new YourCVBlock();
		new ToolsBlock();
		new SalaryCalculator();
		new LogoSlider();
		new EventPost();
		new FAQBlock();
		new BlogComponents();
		new EventsSlider();
		new HistoryBlock();
		new CoursesSlider();
		new CoursesAndFilter();
		new CompanyTestimonials();
		new EmploymentAtSchool();
		new SuccessFactor();
		new RatingsSchool();
		new HistoryImages();
		new HistoryStoresSteps();
		new ContactBlock();
		new PaymentForm();
	}

	/**
	 * Add custom post status.
	 *
	 * @return void
	 */
	public function add_post_status_end_event(): void {
		register_post_status(
			'end_event',
			[
				'label'                     => _x( 'Кінць подія', 'events', 'alevel' ),
				/* translators: %s: count, %s: counts */
				'label_count'               => _n_noop(
					'Кінць подія <span class="count">(%s)</span>',
					'Кінць події <span class="count">(%s)</span>'
				),
				'public'                    => true,
				'exclude_from_search'       => false,
				'show_in_admin_all_list'    => true,
				'show_in_admin_status_list' => true,
			]
		);
	}

	/**
	 * Add options custom post status in event post type.
	 *
	 * @return void
	 */
	public function display_custom_post_status_option(): void {
		global $post;

		if ( isset( $post->post_type ) && 'events' === $post->post_type ) {
			if ( 'end_event' === $post->post_status ) {
				$selected = 'selected';
			}
			?>

			<script>
				jQuery( document ).ready( function( $ ) {
					$( 'select#post_status, select[name=_status]' ).append( '<option value="end_event" <?php echo esc_attr( $selected ?? '' ); ?>> <?php echo esc_html( __( 'Кінць подія', 'alevel' ) ); ?> </option>' );
					$( '.misc-pub-section label' ).append( '<span id="post-status-display"> <?php echo esc_html( __( 'Кінець подія', 'alevel' ) ); ?> </span>' );
				} );
			</script>

			<?php
		}
	}

	/**
	 * Adding custom post status to post type index.
	 *
	 * @param array $states Statse.
	 *
	 * @return array|mixed
	 */
	public function display_events_state( array $states ) {
		global $post;
		$arg = get_query_var( 'post_status' );
		if ( 'events' !== $arg ) {
			if ( 'end_event' === $post->post_status ) {
				return [ __( 'Кінець подія', 'alevel' ) ];
			}
		}

		return $states;
	}

	/**
	 * Delete [...] from the_excerpt().
	 *
	 * @return string
	 */
	public function delete_excerpt_more(): string {
		return '';
	}

	/**
	 * Change status event by end.
	 *
	 * @return void
	 */
	public function update_event_status() {
		$arg = [
			'post_type'      => 'events',
			'post_status'    => 'publish',
			'posts_per_page' => - 1,
		];

		$event_query = new WP_Query( $arg );

		if ( $event_query->have_posts() ) {
			while ( $event_query->have_posts() ) {
				$event_query->the_post();

				$event_id = get_the_ID();

				$date_end           = get_post_meta( $event_id, '_alv_event_start', true );
				$date_end_timestamp = strtotime( $date_end . '+1 day' );
				$now                = new DateTime();

				if ( $now->getTimestamp() > $date_end_timestamp ) {
					wp_update_post(
						[
							'ID'          => $event_id,
							'post_status' => 'end_event',
						]
					);
				}
			}
		}
	}

}
