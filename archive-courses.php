<?php
/**
 * Archive courses template.
 *
 * @package iwpdev/alevel
 */

get_header();
?>
	<section style="background:#E9F1FD;">
		<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex banner-gradient"
			 style="--var-gradient: linear-gradient(97.71deg, #4BD2FD -16.21%, #406BD9 77.81%);">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<div class="vc_col-lg-8 vc_col-md-8 vc_col-sm-8 vc_col-xs-12">
							<h1 class="title"><?php esc_html_e( 'Усi курси', 'alevel' ); ?></h1>
							<p><?php echo wp_kses_post( wpautop( carbon_get_theme_option( 'alv_archive_custom_title_course' ) ) ); ?></p>
							<?php get_template_part( 'template-parts/courses', 'menu' ); ?>
						</div>
						<div class="vc_col-lg-4 vc_col-md-4 vc_col-sm-4 vc_col-xs-12">
							<a class="button" href="<?php bloginfo( 'url' ); ?>">
								<?php esc_attr_e( 'На головну', 'alevel' ); ?>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex courses-block">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<div class="vc_col-lg-12 vc_col-md-12 vc_col-sm-12 vc_col-xs-12">
							<div class="courses dfr">
								<?php
								if ( have_posts() ) {
									while ( have_posts() ) {
										the_post();
										$course_id = get_the_ID();

										get_template_part( 'template-parts/courses', 'item', [ 'course_id' => $course_id ] );
									}
								}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
		get_template_part( 'template-parts/short-code-reviews-block' );
		get_template_part( 'template-parts/short-code-faq' );
		?>
	</section>
<?php
get_footer();
