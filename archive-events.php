<?php
/**
 * Archive events template.
 *
 * @package iwpdev/alevel
 */

get_header();

$current_date        = gmdate( 'Y-m-d' );
$current_event_paged = get_query_var( 'paged' ) ?: 1;

$arg_events = [
	'post_type'     => 'events',
	'post_status'   => 'publish',
	'post_per_page' => - 1,
	'paged'         => $current_event_paged,
];

$current_event_obj = new WP_Query( $arg_events );
?>

	<section style="background:#F9F6F1;">
		<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex banner-gradient"
			 style="--var-gradient:  linear-gradient(97.71deg, #4BD2FD -16.21%, #406BD9 77.81%);">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<div class="vc_col-lg-12 vc_col-md-12 vc_col-sm-12 vc_col-xs-12">
							<h1 class="title"><?php esc_html_e( 'Усі заходи A-Level Ukraine', 'alevel' ); ?></h1>
							<p><?php echo wp_kses_post( wpautop( carbon_get_theme_option( 'alv_archive_custom_title_event' ) ) ); ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<div class="vc_col-lg-12 vc_col-md-12 vc_col-sm-12 vc_col-xs-12">
							<?php get_template_part( 'template-parts/menu', 'event' ); ?>
							<div class="tabs">
								<div class="tab-pane active" id="events">
									<div class="events-block no-slider">
										<?php
										if ( $current_event_obj->have_posts() ) {
											while ( $current_event_obj->have_posts() ) {
												$current_event_obj->the_post();
												$event_id = get_the_ID();

												get_template_part( 'template-parts/events', 'item', [ 'event_id' => $event_id ] );
											}
											wp_reset_postdata();
										} else {
											echo '<h3>' . esc_html( __( 'Немає активних подій', 'alevel' ) ) . '</h3>';
										}
										?>
									</div>
									<div class="page_navigation_wrapper">
										<?php
										if ( function_exists( 'wp_pagenavi' ) ) {
											wp_pagenavi( [ 'query' => $current_event_obj ] );
										}
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php get_template_part( 'template-parts/short-code-faq' ); ?>
	</section>
<?php
get_footer();
