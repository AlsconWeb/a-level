<?php
/**
 * Index template.
 *
 * @package iwpdev/alevel
 */

get_header();
?>
	<section style="background:#f1f5f9;">
		<?php
		the_content();
		?>
	</section>
<?php
get_footer();
