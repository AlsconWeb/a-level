<?php
/**
 * Single page teachers.
 *
 * @package iwpdev/alevel
 */

use Alevel\Helpers\Helper;

get_header();

$arg = [
	'post_type'      => 'testimonials',
	'posts_per_page' => - 1,
	'post_status'    => 'publish',
	'meta_query'     => [
		[
			'key'         => '(_alv_reviewer_teachers\|)',
			'value'       => get_the_ID(),
			'compare_key' => 'REGEXP',
			'compare'     => '=',
		],
	],
];


$testimonials_query = new WP_Query( $arg );

$article_author_arg = [
	'post_type'           => 'post',
	'post_status'         => 'publish',
	'ignore_sticky_posts' => true,
	'meta_query'          => [
		[
			'key'         => '(_alv_post_author_teacher\|)',
			'value'       => get_the_ID(),
			'compare_key' => 'REGEXP',
			'compare'     => '=',
		],
	],
];

$article_author_query = new WP_Query( $article_author_arg );

?>
	<section style="background:#F9F6F1;">
		<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex banner-gradient"
			 style="--var-gradient:linear-gradient(82.27deg, #9D82C9 0%, #F5C2E5 100%);">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<div class="vc_col-lg-12 vc_col-md-12 vc_col-sm-12 vc_col-xs-12">
							<a class="back" href="<?php echo esc_url( get_post_type_archive_link( 'teachers' ) ); ?>">
								<i class="icon-arrow-left"></i>
								<?php esc_html_e( 'Усi викладачі', 'alevel' ); ?>
							</a>
							<?php
							if ( have_posts() ) {
								while ( have_posts() ) {
									the_post();
									$teacher_id        = get_the_ID();
									$author_avatar_url = isset( $teacher_id ) && has_post_thumbnail( $teacher_id ) ? get_the_post_thumbnail_url( $teacher_id, 'alv-teacher-avatar' ) : 'https://via.placeholder.com/80x80';
									$author_name       = isset( $teacher_id ) ? get_the_title( $teacher_id ) : '';
									$author_position   = isset( $teacher_id ) ? carbon_get_post_meta( $teacher_id, 'alv_teacher_job_title' ) : '';
									$author_link       = isset( $teacher_id ) ? get_the_permalink( $teacher_id ) : '';
									$author_post_count = isset( $teacher_id ) ? Helper::alv_get_count_post_teacher( $teacher_id ) : '';
									$teacher_social    = carbon_get_post_meta( $teacher_id, 'alv_social_media_teacher' );
									$teacher_course    = carbon_get_post_meta( $teacher_id, 'alv_teacher_course' );
									?>
									<div class="teacher dfr">
										<div class="user dfr">
											<img
													class="user"
													src="<?php echo esc_url( $author_avatar_url ); ?>"
													alt="<?php echo esc_html( $author_name ); ?>">
											<div class="user-desc">
												<h3><?php echo esc_html( $author_name ); ?></h3>
												<p><?php echo esc_html( $author_position ); ?></p>
												<?php get_template_part( 'template-parts/social', '', [ 'social' => $teacher_social ] ); ?>
											</div>
										</div>
										<p class="desc"><?php echo esc_html( get_the_content( $teacher_id ) ); ?></p>
										<?php get_template_part( 'template-parts/teacher', 'course', [ 'courses' => $teacher_course ] ); ?>
									</div>
									<?php
								}
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex blog-block">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<div class="vc_col-lg-12 vc_col-md-12 vc_col-sm-12 vc_col-xs-12">
							<div class="nav-block">
								<ul class="nav dfr">
									<li class="active">
										<a href="#reviews" data-toggle="tab">
											<?php esc_html_e( 'Вiдгуки о викладачi', 'alevel' ); ?>
										</a>
									</li>
									<li>
										<a href="#articles" data-toggle="tab">
											<?php esc_html_e( 'Статтi викладача', 'alevel' ); ?>
										</a>
									</li>
								</ul>
							</div>
							<div class="tabs">
								<div class="tab-pane active" id="reviews">
									<?php if ( $testimonials_query->have_posts() ) { ?>
										<div class="reviews">
											<div class="reviews-items dfr">
												<?php
												while ( $testimonials_query->have_posts() ) {
													$testimonials_query->the_post();
													$testimonials_id = get_the_ID();
													$course_id       = carbon_get_post_meta( $testimonials_id, 'alv_course_event' );

													get_template_part(
														'template-parts/reviews',
														'item',
														[
															'testimonials_id' => $testimonials_id,
															'course_id'       => $course_id[0]['id'] ?? null,
														]
													);

												}
												wp_reset_postdata();
												?>

											</div>
										</div>
										<?php
									} else {
										echo '<h3>' . esc_html( __( 'Немає відгуків про цього викладача', 'alevel' ) ) . '</h3>';
									}
									?>
								</div>
								<div class="tab-pane" id="articles">
									<?php if ( $article_author_query->have_posts() ) { ?>
										<div class="tags-block">
											<?php get_template_part( 'template-parts/category', 'menu', [ 'teacher_post' => true ] ); ?>
										</div>
										<div class="blog-items dfr">
											<?php
											while ( $article_author_query->have_posts() ) {
												$article_author_query->the_post();

												$article_id = get_the_ID();
												get_template_part( 'template-parts/blog', 'item', [ 'article_id' => $article_id ] );
											}
											wp_reset_postdata();
											?>
										</div>
										<?php
									} else {
										echo '<h3>' . esc_html( __( 'Немає статей у викладача', 'alevel' ) ) . '</h3>';
									}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php get_template_part( 'template-parts/short-code-faq' ); ?>
	</section>
<?php
get_footer();
