<?php
/**
 * Courses single page template.
 *
 * @package iwpdev/alevel
 */

get_header();
?>
	<section style="background:#E9F1FD;">
		<?php
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();

				$course_id         = get_the_ID();
				$category_taxonomy = wp_get_post_terms( $course_id, 'category-courses' );
				if ( ! empty( $category_taxonomy[0] ) ) {
					$category_link = get_term_link( $category_taxonomy[0]->term_id );
				}
				$type_course          = wp_get_post_terms( $course_id, 'type-courses' );
				$course_icon          = carbon_get_post_meta( $course_id, 'alv_course_icon' );
				$date_start           = carbon_get_post_meta( $course_id, 'alv_course_start' ) ?? '';
				$course_duration      = carbon_get_post_meta( $course_id, 'alv_course_duration' ) ?? '';
				$course_available     = carbon_get_post_meta( $course_id, 'alv_course_available_seats' ) ?? '';
				$course_discount_text = carbon_get_post_meta( $course_id, 'alv_course_discount_text' );
				$gradient_class       = carbon_get_post_meta( $course_id, 'alv_line_gradient_class' );
				?>
				<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex banner-gradient <?php echo esc_attr( $gradient_class ) ?? ''; ?>">
					<div class="wpb_column vc_column_container vc_col-sm-12">
						<div class="vc_column-inner">
							<div class="wpb_wrapper">
								<div class="vc_col-lg-12 vc_col-md-12 vc_col-sm-12 vc_col-xs-12">
									<a class="back" href="<?php echo esc_url( $category_link ?? '#' ); ?>">
										<i class="icon-arrow-left"></i><?php esc_html_e( 'Всі курси', 'alevel' ); ?>
									</a>
									<div class="dfr">
										<?php if ( ! empty( $course_icon ) ) { ?>
											<img
													src="<?php echo esc_url( $course_icon ); ?>"
													alt="<?php the_title(); ?> icon">
										<?php } else { ?>
											<img
													src="https://via.placeholder.com/80x80"
													alt="<?php the_title(); ?> no image">
										<?php } ?>
										<div class="desc">
											<p><?php echo esc_html( $category_taxonomy[0]->name ?? '' ); ?></p>
											<h1 class="title"><?php the_title(); ?>
												<span><?php echo ! empty( $type_course ) ? esc_html( $type_course[0]->name ) : ''; ?></span>
											</h1>
										</div>
									</div>
									<ul class="meta dfr">
										<?php
										if ( ! empty( $course_discount_text ) ) {
											?>
											<li class="action">
												<span><?php echo esc_html( $course_discount_text ); ?></span>
											</li>
										<?php } ?>
										<li>
											<?php esc_html_e( 'Старт:', 'alevel' ); ?>
											<span><?php echo esc_html( $date_start ); ?></span>
										</li>
										<li>
											<?php esc_html_e( 'Тривалість:', 'alevel' ); ?>
											<span>
												<?php
												echo esc_html( $course_duration );
												?>
											</span>
										</li>
										<li>
											<?php esc_html_e( 'Залишилось місць:', 'alevel' ); ?>
											<span><?php echo esc_html( $course_available ); ?></span>
										</li>
									</ul>
									<a
											id="recording_course"
											class="button"
											href="#recording"
											data-course_id="<?php echo esc_attr( $course_id ); ?>">
										<?php esc_html_e( 'Записатися на курс', 'alevel' ); ?>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php

				the_content();
			}
		}
		?>
	</section>
<?php
get_footer();
