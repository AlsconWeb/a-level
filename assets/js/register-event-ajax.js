/* global eventObject, Swal */

/**
 * @param eventObject.url
 * @param eventObject.actionNamesEventRegister
 * @param eventObject.ajaxBeforeSendTitle
 */

jQuery( document ).ready( function( $ ) {
	$( '.event-register' ).click( function( e ) {
		$( '#event-id' ).val( $( this ).data( 'event_id' ) );
	} );

	$( '#register-to-event' ).submit( function( e ) {
		e.preventDefault()

		let data = {
			action: eventObject.actionNamesEventRegister,
			event_id: $( '#event-id' ).val(),
			name: $( '#event-name' ).val(),
			phone: $( '#event-phone' ).val(),
			email: $( '#event-email' ).val(),
			nonce: $( '#alv_register_on_event' ).val()
		}

		let error = $( '#register-to-event .error' );

		if ( ! error.length ) {
			$.ajax( {
				type: 'POST',
				url: eventObject.url,
				data: data,
				beforeSend: function() {
					loader_ajax( false );
				},
				success: function( res ) {
					if ( res.success ) {
						const form = document.getElementById( 'register-to-event' );
						form.reset();
						$( '.events-wrapper .event-item' ).remove()
						$( '.events-wrapper' ).append( res.data.event );
						$( '.registration-event' ).modal( 'hide' );
						$( '.registration-event-thanks' ).modal( 'show' );
						loader_ajax( true );
					} else {
						loader_ajax( true );
						Swal.fire( {
							icon: 'error',
							title: 'Oops...',
							text: res.data.message,
						} )
					}

				},
				error: function( xhr, ajaxOptions, thrownError ) {
					console.log( 'error...', xhr );
					//error logging
				},
			} );
		}

	} )


} );