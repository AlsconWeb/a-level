/* global reviewObject, Swal */

/**
 * @param reviewObject.url
 * @param reviewObject.actionNamesTestimonialsCategory
 * @param reviewObject.alvTestimonialsNonce
 * @param reviewObject.ajaxBeforeSendTitle
 */

jQuery( document ).ready( function( $ ) {
	let filter = $( '.filter-menu .active' );
	if ( filter.length ) {
		$( filter )[ 0 ].scrollIntoView( { block: 'nearest' } )
	}
	
	$( '.tags-menu .review-category' ).click( function( e ) {
		e.preventDefault();

		$( '.tags-menu li' ).removeClass( 'active' );
		$( this ).parent().addClass( 'active' );

		sendAjax( $( this ) );

	} );

	$( '.filter-menu li a' ).click( function( e ) {
		e.preventDefault();

		$( '.filter-menu li' ).removeClass( 'active' );
		$( this ).parent().addClass( 'active' );

		sendAjax( $( this ) );
	} );

	function sendAjax( el ) {
		let data = {
			action: reviewObject.actionNamesTestimonialsCategory,
			courses_id: $( '.tags-menu li.active a' ).data( 'courses_id' ),
			nonce: reviewObject.alvTestimonialsNonce,
			course_type: $( '.filter-menu li.active a' ).data( 'type' ),
			page: $( '.wp-pagenavi .current' ).text()
		}

		$.ajax( {
			type: 'POST',
			url: reviewObject.url,
			data: data,
			success: function( res ) {
				if ( res.success ) {
					let baseUrl = window.location.protocol + '//' + window.location.host + window.location.pathname;

					location.href = baseUrl + '?' + res.data.params;
					$( '.filter-menu .active' )[ 0 ].scrollIntoView( { block: 'nearest' } )
				} else {
					Swal.fire( {
						icon: 'error',
						title: 'Oops...',
						text: res.data.message,
					} )
				}
			},
			error: function( xhr, ajaxOptions, thrownError ) {
				console.log( 'error...', xhr );
				//error logging
			}
		} );
	}
} );