"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*! Select2 4.1.0-rc.0 | https://github.com/select2/select2/blob/master/LICENSE.md */
!function (n) {
  "function" == typeof define && define.amd ? define(["jquery"], n) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = function (e, t) {
    return void 0 === t && (t = "undefined" != typeof window ? require("jquery") : require("jquery")(e)), n(t), t;
  } : n(jQuery);
}(function (t) {
  var e,
      n,
      s,
      p,
      r,
      _o,
      h,
      f,
      g,
      m,
      y,
      v,
      i,
      a,
      _,
      s = ((u = t && t.fn && t.fn.select2 && t.fn.select2.amd ? t.fn.select2.amd : u) && u.requirejs || (u ? n = u : u = {}, g = {}, m = {}, y = {}, v = {}, i = Object.prototype.hasOwnProperty, a = [].slice, _ = /\.js$/, h = function h(e, t) {
    var n,
        s,
        i = c(e),
        r = i[0],
        t = t[1];return e = i[1], r && (n = x(r = l(r, t))), r ? e = n && n.normalize ? n.normalize(e, (s = t, function (e) {
      return l(e, s);
    })) : l(e, t) : (r = (i = c(e = l(e, t)))[0], e = i[1], r && (n = x(r))), { f: r ? r + "!" + e : e, n: e, pr: r, p: n };
  }, f = { require: function require(e) {
      return w(e);
    }, exports: function exports(e) {
      var t = g[e];return void 0 !== t ? t : g[e] = {};
    }, module: function module(e) {
      return { id: e, uri: "", exports: g[e], config: (t = e, function () {
          return y && y.config && y.config[t] || {};
        }) };var t;
    } }, r = function r(e, t, n, s) {
    var i,
        r,
        o,
        a,
        l,
        c = [],
        u = typeof n === "undefined" ? "undefined" : _typeof(n),
        d = A(s = s || e);if ("undefined" == u || "function" == u) {
      for (t = !t.length && n.length ? ["require", "exports", "module"] : t, a = 0; a < t.length; a += 1) {
        if ("require" === (r = (o = h(t[a], d)).f)) c[a] = f.require(e);else if ("exports" === r) c[a] = f.exports(e), l = !0;else if ("module" === r) i = c[a] = f.module(e);else if (b(g, r) || b(m, r) || b(v, r)) c[a] = x(r);else {
          if (!o.p) throw new Error(e + " missing " + r);o.p.load(o.n, w(s, !0), function (t) {
            return function (e) {
              g[t] = e;
            };
          }(r), {}), c[a] = g[r];
        }
      }u = n ? n.apply(g[e], c) : void 0, e && (i && i.exports !== p && i.exports !== g[e] ? g[e] = i.exports : u === p && l || (g[e] = u));
    } else e && (g[e] = n);
  }, e = n = _o = function o(e, t, n, s, i) {
    if ("string" == typeof e) return f[e] ? f[e](t) : x(h(e, A(t)).f);if (!e.splice) {
      if ((y = e).deps && _o(y.deps, y.callback), !t) return;t.splice ? (e = t, t = n, n = null) : e = p;
    }return t = t || function () {}, "function" == typeof n && (n = s, s = i), s ? r(p, e, t, n) : setTimeout(function () {
      r(p, e, t, n);
    }, 4), _o;
  }, _o.config = function (e) {
    return _o(e);
  }, e._defined = g, (s = function s(e, t, n) {
    if ("string" != typeof e) throw new Error("See almond README: incorrect module build, no module name");t.splice || (n = t, t = []), b(g, e) || b(m, e) || (m[e] = [e, t, n]);
  }).amd = { jQuery: !0 }, u.requirejs = e, u.require = n, u.define = s), u.define("almond", function () {}), u.define("jquery", [], function () {
    var e = t || $;return null == e && console && console.error && console.error("Select2: An instance of jQuery or a jQuery-compatible library was not found. Make sure that you are including jQuery before Select2 on your web page."), e;
  }), u.define("select2/utils", ["jquery"], function (r) {
    var s = {};function c(e) {
      var t,
          n = e.prototype,
          s = [];for (t in n) {
        "function" == typeof n[t] && "constructor" !== t && s.push(t);
      }return s;
    }s.Extend = function (e, t) {
      var n,
          s = {}.hasOwnProperty;function i() {
        this.constructor = e;
      }for (n in t) {
        s.call(t, n) && (e[n] = t[n]);
      }return i.prototype = t.prototype, e.prototype = new i(), e.__super__ = t.prototype, e;
    }, s.Decorate = function (s, i) {
      var e = c(i),
          t = c(s);function r() {
        var e = Array.prototype.unshift,
            t = i.prototype.constructor.length,
            n = s.prototype.constructor;0 < t && (e.call(arguments, s.prototype.constructor), n = i.prototype.constructor), n.apply(this, arguments);
      }i.displayName = s.displayName, r.prototype = new function () {
        this.constructor = r;
      }();for (var n = 0; n < t.length; n++) {
        var o = t[n];r.prototype[o] = s.prototype[o];
      }for (var a = 0; a < e.length; a++) {
        var l = e[a];r.prototype[l] = function (e) {
          var t = function t() {};e in r.prototype && (t = r.prototype[e]);var n = i.prototype[e];return function () {
            return Array.prototype.unshift.call(arguments, t), n.apply(this, arguments);
          };
        }(l);
      }return r;
    };function e() {
      this.listeners = {};
    }e.prototype.on = function (e, t) {
      this.listeners = this.listeners || {}, e in this.listeners ? this.listeners[e].push(t) : this.listeners[e] = [t];
    }, e.prototype.trigger = function (e) {
      var t = Array.prototype.slice,
          n = t.call(arguments, 1);this.listeners = this.listeners || {}, 0 === (n = null == n ? [] : n).length && n.push({}), (n[0]._type = e) in this.listeners && this.invoke(this.listeners[e], t.call(arguments, 1)), "*" in this.listeners && this.invoke(this.listeners["*"], arguments);
    }, e.prototype.invoke = function (e, t) {
      for (var n = 0, s = e.length; n < s; n++) {
        e[n].apply(this, t);
      }
    }, s.Observable = e, s.generateChars = function (e) {
      for (var t = "", n = 0; n < e; n++) {
        t += Math.floor(36 * Math.random()).toString(36);
      }return t;
    }, s.bind = function (e, t) {
      return function () {
        e.apply(t, arguments);
      };
    }, s._convertData = function (e) {
      for (var t in e) {
        var n = t.split("-"),
            s = e;if (1 !== n.length) {
          for (var i = 0; i < n.length; i++) {
            var r = n[i];(r = r.substring(0, 1).toLowerCase() + r.substring(1)) in s || (s[r] = {}), i == n.length - 1 && (s[r] = e[t]), s = s[r];
          }delete e[t];
        }
      }return e;
    }, s.hasScroll = function (e, t) {
      var n = r(t),
          s = t.style.overflowX,
          i = t.style.overflowY;return (s !== i || "hidden" !== i && "visible" !== i) && ("scroll" === s || "scroll" === i || n.innerHeight() < t.scrollHeight || n.innerWidth() < t.scrollWidth);
    }, s.escapeMarkup = function (e) {
      var t = { "\\": "&#92;", "&": "&amp;", "<": "&lt;", ">": "&gt;", '"': "&quot;", "'": "&#39;", "/": "&#47;" };return "string" != typeof e ? e : String(e).replace(/[&<>"'\/\\]/g, function (e) {
        return t[e];
      });
    }, s.__cache = {};var n = 0;return s.GetUniqueElementId = function (e) {
      var t = e.getAttribute("data-select2-id");return null != t || (t = e.id ? "select2-data-" + e.id : "select2-data-" + (++n).toString() + "-" + s.generateChars(4), e.setAttribute("data-select2-id", t)), t;
    }, s.StoreData = function (e, t, n) {
      e = s.GetUniqueElementId(e);s.__cache[e] || (s.__cache[e] = {}), s.__cache[e][t] = n;
    }, s.GetData = function (e, t) {
      var n = s.GetUniqueElementId(e);return t ? s.__cache[n] && null != s.__cache[n][t] ? s.__cache[n][t] : r(e).data(t) : s.__cache[n];
    }, s.RemoveData = function (e) {
      var t = s.GetUniqueElementId(e);null != s.__cache[t] && delete s.__cache[t], e.removeAttribute("data-select2-id");
    }, s.copyNonInternalCssClasses = function (e, t) {
      var n = (n = e.getAttribute("class").trim().split(/\s+/)).filter(function (e) {
        return 0 === e.indexOf("select2-");
      }),
          t = (t = t.getAttribute("class").trim().split(/\s+/)).filter(function (e) {
        return 0 !== e.indexOf("select2-");
      }),
          t = n.concat(t);e.setAttribute("class", t.join(" "));
    }, s;
  }), u.define("select2/results", ["jquery", "./utils"], function (d, p) {
    function s(e, t, n) {
      this.$element = e, this.data = n, this.options = t, s.__super__.constructor.call(this);
    }return p.Extend(s, p.Observable), s.prototype.render = function () {
      var e = d('<ul class="select2-results__options" role="listbox"></ul>');return this.options.get("multiple") && e.attr("aria-multiselectable", "true"), this.$results = e;
    }, s.prototype.clear = function () {
      this.$results.empty();
    }, s.prototype.displayMessage = function (e) {
      var t = this.options.get("escapeMarkup");this.clear(), this.hideLoading();var n = d('<li role="alert" aria-live="assertive" class="select2-results__option"></li>'),
          s = this.options.get("translations").get(e.message);n.append(t(s(e.args))), n[0].className += " select2-results__message", this.$results.append(n);
    }, s.prototype.hideMessages = function () {
      this.$results.find(".select2-results__message").remove();
    }, s.prototype.append = function (e) {
      this.hideLoading();var t = [];if (null != e.results && 0 !== e.results.length) {
        e.results = this.sort(e.results);for (var n = 0; n < e.results.length; n++) {
          var s = e.results[n],
              s = this.option(s);t.push(s);
        }this.$results.append(t);
      } else 0 === this.$results.children().length && this.trigger("results:message", { message: "noResults" });
    }, s.prototype.position = function (e, t) {
      t.find(".select2-results").append(e);
    }, s.prototype.sort = function (e) {
      return this.options.get("sorter")(e);
    }, s.prototype.highlightFirstItem = function () {
      var e = this.$results.find(".select2-results__option--selectable"),
          t = e.filter(".select2-results__option--selected");(0 < t.length ? t : e).first().trigger("mouseenter"), this.ensureHighlightVisible();
    }, s.prototype.setClasses = function () {
      var t = this;this.data.current(function (e) {
        var s = e.map(function (e) {
          return e.id.toString();
        });t.$results.find(".select2-results__option--selectable").each(function () {
          var e = d(this),
              t = p.GetData(this, "data"),
              n = "" + t.id;null != t.element && t.element.selected || null == t.element && -1 < s.indexOf(n) ? (this.classList.add("select2-results__option--selected"), e.attr("aria-selected", "true")) : (this.classList.remove("select2-results__option--selected"), e.attr("aria-selected", "false"));
        });
      });
    }, s.prototype.showLoading = function (e) {
      this.hideLoading();e = { disabled: !0, loading: !0, text: this.options.get("translations").get("searching")(e) }, e = this.option(e);e.className += " loading-results", this.$results.prepend(e);
    }, s.prototype.hideLoading = function () {
      this.$results.find(".loading-results").remove();
    }, s.prototype.option = function (e) {
      var t = document.createElement("li");t.classList.add("select2-results__option"), t.classList.add("select2-results__option--selectable");var n,
          s = { role: "option" },
          i = window.Element.prototype.matches || window.Element.prototype.msMatchesSelector || window.Element.prototype.webkitMatchesSelector;for (n in (null != e.element && i.call(e.element, ":disabled") || null == e.element && e.disabled) && (s["aria-disabled"] = "true", t.classList.remove("select2-results__option--selectable"), t.classList.add("select2-results__option--disabled")), null == e.id && t.classList.remove("select2-results__option--selectable"), null != e._resultId && (t.id = e._resultId), e.title && (t.title = e.title), e.children && (s.role = "group", s["aria-label"] = e.text, t.classList.remove("select2-results__option--selectable"), t.classList.add("select2-results__option--group")), s) {
        var r = s[n];t.setAttribute(n, r);
      }if (e.children) {
        var o = d(t),
            a = document.createElement("strong");a.className = "select2-results__group", this.template(e, a);for (var l = [], c = 0; c < e.children.length; c++) {
          var u = e.children[c],
              u = this.option(u);l.push(u);
        }i = d("<ul></ul>", { class: "select2-results__options select2-results__options--nested", role: "none" });i.append(l), o.append(a), o.append(i);
      } else this.template(e, t);return p.StoreData(t, "data", e), t;
    }, s.prototype.bind = function (t, e) {
      var i = this,
          n = t.id + "-results";this.$results.attr("id", n), t.on("results:all", function (e) {
        i.clear(), i.append(e.data), t.isOpen() && (i.setClasses(), i.highlightFirstItem());
      }), t.on("results:append", function (e) {
        i.append(e.data), t.isOpen() && i.setClasses();
      }), t.on("query", function (e) {
        i.hideMessages(), i.showLoading(e);
      }), t.on("select", function () {
        t.isOpen() && (i.setClasses(), i.options.get("scrollAfterSelect") && i.highlightFirstItem());
      }), t.on("unselect", function () {
        t.isOpen() && (i.setClasses(), i.options.get("scrollAfterSelect") && i.highlightFirstItem());
      }), t.on("open", function () {
        i.$results.attr("aria-expanded", "true"), i.$results.attr("aria-hidden", "false"), i.setClasses(), i.ensureHighlightVisible();
      }), t.on("close", function () {
        i.$results.attr("aria-expanded", "false"), i.$results.attr("aria-hidden", "true"), i.$results.removeAttr("aria-activedescendant");
      }), t.on("results:toggle", function () {
        var e = i.getHighlightedResults();0 !== e.length && e.trigger("mouseup");
      }), t.on("results:select", function () {
        var e,
            t = i.getHighlightedResults();0 !== t.length && (e = p.GetData(t[0], "data"), t.hasClass("select2-results__option--selected") ? i.trigger("close", {}) : i.trigger("select", { data: e }));
      }), t.on("results:previous", function () {
        var e,
            t = i.getHighlightedResults(),
            n = i.$results.find(".select2-results__option--selectable"),
            s = n.index(t);s <= 0 || (e = s - 1, 0 === t.length && (e = 0), (s = n.eq(e)).trigger("mouseenter"), t = i.$results.offset().top, n = s.offset().top, s = i.$results.scrollTop() + (n - t), 0 === e ? i.$results.scrollTop(0) : n - t < 0 && i.$results.scrollTop(s));
      }), t.on("results:next", function () {
        var e,
            t = i.getHighlightedResults(),
            n = i.$results.find(".select2-results__option--selectable"),
            s = n.index(t) + 1;s >= n.length || ((e = n.eq(s)).trigger("mouseenter"), t = i.$results.offset().top + i.$results.outerHeight(!1), n = e.offset().top + e.outerHeight(!1), e = i.$results.scrollTop() + n - t, 0 === s ? i.$results.scrollTop(0) : t < n && i.$results.scrollTop(e));
      }), t.on("results:focus", function (e) {
        e.element[0].classList.add("select2-results__option--highlighted"), e.element[0].setAttribute("aria-selected", "true");
      }), t.on("results:message", function (e) {
        i.displayMessage(e);
      }), d.fn.mousewheel && this.$results.on("mousewheel", function (e) {
        var t = i.$results.scrollTop(),
            n = i.$results.get(0).scrollHeight - t + e.deltaY,
            t = 0 < e.deltaY && t - e.deltaY <= 0,
            n = e.deltaY < 0 && n <= i.$results.height();t ? (i.$results.scrollTop(0), e.preventDefault(), e.stopPropagation()) : n && (i.$results.scrollTop(i.$results.get(0).scrollHeight - i.$results.height()), e.preventDefault(), e.stopPropagation());
      }), this.$results.on("mouseup", ".select2-results__option--selectable", function (e) {
        var t = d(this),
            n = p.GetData(this, "data");t.hasClass("select2-results__option--selected") ? i.options.get("multiple") ? i.trigger("unselect", { originalEvent: e, data: n }) : i.trigger("close", {}) : i.trigger("select", { originalEvent: e, data: n });
      }), this.$results.on("mouseenter", ".select2-results__option--selectable", function (e) {
        var t = p.GetData(this, "data");i.getHighlightedResults().removeClass("select2-results__option--highlighted").attr("aria-selected", "false"), i.trigger("results:focus", { data: t, element: d(this) });
      });
    }, s.prototype.getHighlightedResults = function () {
      return this.$results.find(".select2-results__option--highlighted");
    }, s.prototype.destroy = function () {
      this.$results.remove();
    }, s.prototype.ensureHighlightVisible = function () {
      var e,
          t,
          n,
          s,
          i = this.getHighlightedResults();0 !== i.length && (e = this.$results.find(".select2-results__option--selectable").index(i), s = this.$results.offset().top, t = i.offset().top, n = this.$results.scrollTop() + (t - s), s = t - s, n -= 2 * i.outerHeight(!1), e <= 2 ? this.$results.scrollTop(0) : (s > this.$results.outerHeight() || s < 0) && this.$results.scrollTop(n));
    }, s.prototype.template = function (e, t) {
      var n = this.options.get("templateResult"),
          s = this.options.get("escapeMarkup"),
          e = n(e, t);null == e ? t.style.display = "none" : "string" == typeof e ? t.innerHTML = s(e) : d(t).append(e);
    }, s;
  }), u.define("select2/keys", [], function () {
    return { BACKSPACE: 8, TAB: 9, ENTER: 13, SHIFT: 16, CTRL: 17, ALT: 18, ESC: 27, SPACE: 32, PAGE_UP: 33, PAGE_DOWN: 34, END: 35, HOME: 36, LEFT: 37, UP: 38, RIGHT: 39, DOWN: 40, DELETE: 46 };
  }), u.define("select2/selection/base", ["jquery", "../utils", "../keys"], function (n, s, i) {
    function r(e, t) {
      this.$element = e, this.options = t, r.__super__.constructor.call(this);
    }return s.Extend(r, s.Observable), r.prototype.render = function () {
      var e = n('<span class="select2-selection" role="combobox"  aria-haspopup="true" aria-expanded="false"></span>');return this._tabindex = 0, null != s.GetData(this.$element[0], "old-tabindex") ? this._tabindex = s.GetData(this.$element[0], "old-tabindex") : null != this.$element.attr("tabindex") && (this._tabindex = this.$element.attr("tabindex")), e.attr("title", this.$element.attr("title")), e.attr("tabindex", this._tabindex), e.attr("aria-disabled", "false"), this.$selection = e;
    }, r.prototype.bind = function (e, t) {
      var n = this,
          s = e.id + "-results";this.container = e, this.$selection.on("focus", function (e) {
        n.trigger("focus", e);
      }), this.$selection.on("blur", function (e) {
        n._handleBlur(e);
      }), this.$selection.on("keydown", function (e) {
        n.trigger("keypress", e), e.which === i.SPACE && e.preventDefault();
      }), e.on("results:focus", function (e) {
        n.$selection.attr("aria-activedescendant", e.data._resultId);
      }), e.on("selection:update", function (e) {
        n.update(e.data);
      }), e.on("open", function () {
        n.$selection.attr("aria-expanded", "true"), n.$selection.attr("aria-owns", s), n._attachCloseHandler(e);
      }), e.on("close", function () {
        n.$selection.attr("aria-expanded", "false"), n.$selection.removeAttr("aria-activedescendant"), n.$selection.removeAttr("aria-owns"), n.$selection.trigger("focus"), n._detachCloseHandler(e);
      }), e.on("enable", function () {
        n.$selection.attr("tabindex", n._tabindex), n.$selection.attr("aria-disabled", "false");
      }), e.on("disable", function () {
        n.$selection.attr("tabindex", "-1"), n.$selection.attr("aria-disabled", "true");
      });
    }, r.prototype._handleBlur = function (e) {
      var t = this;window.setTimeout(function () {
        document.activeElement == t.$selection[0] || n.contains(t.$selection[0], document.activeElement) || t.trigger("blur", e);
      }, 1);
    }, r.prototype._attachCloseHandler = function (e) {
      n(document.body).on("mousedown.select2." + e.id, function (e) {
        var t = n(e.target).closest(".select2");n(".select2.select2-container--open").each(function () {
          this != t[0] && s.GetData(this, "element").select2("close");
        });
      });
    }, r.prototype._detachCloseHandler = function (e) {
      n(document.body).off("mousedown.select2." + e.id);
    }, r.prototype.position = function (e, t) {
      t.find(".selection").append(e);
    }, r.prototype.destroy = function () {
      this._detachCloseHandler(this.container);
    }, r.prototype.update = function (e) {
      throw new Error("The `update` method must be defined in child classes.");
    }, r.prototype.isEnabled = function () {
      return !this.isDisabled();
    }, r.prototype.isDisabled = function () {
      return this.options.get("disabled");
    }, r;
  }), u.define("select2/selection/single", ["jquery", "./base", "../utils", "../keys"], function (e, t, n, s) {
    function i() {
      i.__super__.constructor.apply(this, arguments);
    }return n.Extend(i, t), i.prototype.render = function () {
      var e = i.__super__.render.call(this);return e[0].classList.add("select2-selection--single"), e.html('<span class="select2-selection__rendered"></span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span>'), e;
    }, i.prototype.bind = function (t, e) {
      var n = this;i.__super__.bind.apply(this, arguments);var s = t.id + "-container";this.$selection.find(".select2-selection__rendered").attr("id", s).attr("role", "textbox").attr("aria-readonly", "true"), this.$selection.attr("aria-labelledby", s), this.$selection.attr("aria-controls", s), this.$selection.on("mousedown", function (e) {
        1 === e.which && n.trigger("toggle", { originalEvent: e });
      }), this.$selection.on("focus", function (e) {}), this.$selection.on("blur", function (e) {}), t.on("focus", function (e) {
        t.isOpen() || n.$selection.trigger("focus");
      });
    }, i.prototype.clear = function () {
      var e = this.$selection.find(".select2-selection__rendered");e.empty(), e.removeAttr("title");
    }, i.prototype.display = function (e, t) {
      var n = this.options.get("templateSelection");return this.options.get("escapeMarkup")(n(e, t));
    }, i.prototype.selectionContainer = function () {
      return e("<span></span>");
    }, i.prototype.update = function (e) {
      var t, n;0 !== e.length ? (n = e[0], t = this.$selection.find(".select2-selection__rendered"), e = this.display(n, t), t.empty().append(e), (n = n.title || n.text) ? t.attr("title", n) : t.removeAttr("title")) : this.clear();
    }, i;
  }), u.define("select2/selection/multiple", ["jquery", "./base", "../utils"], function (i, e, c) {
    function r(e, t) {
      r.__super__.constructor.apply(this, arguments);
    }return c.Extend(r, e), r.prototype.render = function () {
      var e = r.__super__.render.call(this);return e[0].classList.add("select2-selection--multiple"), e.html('<ul class="select2-selection__rendered"></ul>'), e;
    }, r.prototype.bind = function (e, t) {
      var n = this;r.__super__.bind.apply(this, arguments);var s = e.id + "-container";this.$selection.find(".select2-selection__rendered").attr("id", s), this.$selection.on("click", function (e) {
        n.trigger("toggle", { originalEvent: e });
      }), this.$selection.on("click", ".select2-selection__choice__remove", function (e) {
        var t;n.isDisabled() || (t = i(this).parent(), t = c.GetData(t[0], "data"), n.trigger("unselect", { originalEvent: e, data: t }));
      }), this.$selection.on("keydown", ".select2-selection__choice__remove", function (e) {
        n.isDisabled() || e.stopPropagation();
      });
    }, r.prototype.clear = function () {
      var e = this.$selection.find(".select2-selection__rendered");e.empty(), e.removeAttr("title");
    }, r.prototype.display = function (e, t) {
      var n = this.options.get("templateSelection");return this.options.get("escapeMarkup")(n(e, t));
    }, r.prototype.selectionContainer = function () {
      return i('<li class="select2-selection__choice"><button type="button" class="select2-selection__choice__remove" tabindex="-1"><span aria-hidden="true">&times;</span></button><span class="select2-selection__choice__display"></span></li>');
    }, r.prototype.update = function (e) {
      if (this.clear(), 0 !== e.length) {
        for (var t = [], n = this.$selection.find(".select2-selection__rendered").attr("id") + "-choice-", s = 0; s < e.length; s++) {
          var i = e[s],
              r = this.selectionContainer(),
              o = this.display(i, r),
              a = n + c.generateChars(4) + "-";i.id ? a += i.id : a += c.generateChars(4), r.find(".select2-selection__choice__display").append(o).attr("id", a);var l = i.title || i.text;l && r.attr("title", l);o = this.options.get("translations").get("removeItem"), l = r.find(".select2-selection__choice__remove");l.attr("title", o()), l.attr("aria-label", o()), l.attr("aria-describedby", a), c.StoreData(r[0], "data", i), t.push(r);
        }this.$selection.find(".select2-selection__rendered").append(t);
      }
    }, r;
  }), u.define("select2/selection/placeholder", [], function () {
    function e(e, t, n) {
      this.placeholder = this.normalizePlaceholder(n.get("placeholder")), e.call(this, t, n);
    }return e.prototype.normalizePlaceholder = function (e, t) {
      return t = "string" == typeof t ? { id: "", text: t } : t;
    }, e.prototype.createPlaceholder = function (e, t) {
      var n = this.selectionContainer();n.html(this.display(t)), n[0].classList.add("select2-selection__placeholder"), n[0].classList.remove("select2-selection__choice");t = t.title || t.text || n.text();return this.$selection.find(".select2-selection__rendered").attr("title", t), n;
    }, e.prototype.update = function (e, t) {
      var n = 1 == t.length && t[0].id != this.placeholder.id;if (1 < t.length || n) return e.call(this, t);this.clear();t = this.createPlaceholder(this.placeholder);this.$selection.find(".select2-selection__rendered").append(t);
    }, e;
  }), u.define("select2/selection/allowClear", ["jquery", "../keys", "../utils"], function (i, s, a) {
    function e() {}return e.prototype.bind = function (e, t, n) {
      var s = this;e.call(this, t, n), null == this.placeholder && this.options.get("debug") && window.console && console.error && console.error("Select2: The `allowClear` option should be used in combination with the `placeholder` option."), this.$selection.on("mousedown", ".select2-selection__clear", function (e) {
        s._handleClear(e);
      }), t.on("keypress", function (e) {
        s._handleKeyboardClear(e, t);
      });
    }, e.prototype._handleClear = function (e, t) {
      if (!this.isDisabled()) {
        var n = this.$selection.find(".select2-selection__clear");if (0 !== n.length) {
          t.stopPropagation();var s = a.GetData(n[0], "data"),
              i = this.$element.val();this.$element.val(this.placeholder.id);var r = { data: s };if (this.trigger("clear", r), r.prevented) this.$element.val(i);else {
            for (var o = 0; o < s.length; o++) {
              if (r = { data: s[o] }, this.trigger("unselect", r), r.prevented) return void this.$element.val(i);
            }this.$element.trigger("input").trigger("change"), this.trigger("toggle", {});
          }
        }
      }
    }, e.prototype._handleKeyboardClear = function (e, t, n) {
      n.isOpen() || t.which != s.DELETE && t.which != s.BACKSPACE || this._handleClear(t);
    }, e.prototype.update = function (e, t) {
      var n, s;e.call(this, t), this.$selection.find(".select2-selection__clear").remove(), this.$selection[0].classList.remove("select2-selection--clearable"), 0 < this.$selection.find(".select2-selection__placeholder").length || 0 === t.length || (n = this.$selection.find(".select2-selection__rendered").attr("id"), s = this.options.get("translations").get("removeAllItems"), (e = i('<button type="button" class="select2-selection__clear" tabindex="-1"><span aria-hidden="true">&times;</span></button>')).attr("title", s()), e.attr("aria-label", s()), e.attr("aria-describedby", n), a.StoreData(e[0], "data", t), this.$selection.prepend(e), this.$selection[0].classList.add("select2-selection--clearable"));
    }, e;
  }), u.define("select2/selection/search", ["jquery", "../utils", "../keys"], function (s, a, l) {
    function e(e, t, n) {
      e.call(this, t, n);
    }return e.prototype.render = function (e) {
      var t = this.options.get("translations").get("search"),
          n = s('<span class="select2-search select2-search--inline"><textarea class="select2-search__field" type="search" tabindex="-1" autocorrect="off" autocapitalize="none" spellcheck="false" role="searchbox" aria-autocomplete="list" ></textarea></span>');this.$searchContainer = n, this.$search = n.find("textarea"), this.$search.prop("autocomplete", this.options.get("autocomplete")), this.$search.attr("aria-label", t());e = e.call(this);return this._transferTabIndex(), e.append(this.$searchContainer), e;
    }, e.prototype.bind = function (e, t, n) {
      var s = this,
          i = t.id + "-results",
          r = t.id + "-container";e.call(this, t, n), s.$search.attr("aria-describedby", r), t.on("open", function () {
        s.$search.attr("aria-controls", i), s.$search.trigger("focus");
      }), t.on("close", function () {
        s.$search.val(""), s.resizeSearch(), s.$search.removeAttr("aria-controls"), s.$search.removeAttr("aria-activedescendant"), s.$search.trigger("focus");
      }), t.on("enable", function () {
        s.$search.prop("disabled", !1), s._transferTabIndex();
      }), t.on("disable", function () {
        s.$search.prop("disabled", !0);
      }), t.on("focus", function (e) {
        s.$search.trigger("focus");
      }), t.on("results:focus", function (e) {
        e.data._resultId ? s.$search.attr("aria-activedescendant", e.data._resultId) : s.$search.removeAttr("aria-activedescendant");
      }), this.$selection.on("focusin", ".select2-search--inline", function (e) {
        s.trigger("focus", e);
      }), this.$selection.on("focusout", ".select2-search--inline", function (e) {
        s._handleBlur(e);
      }), this.$selection.on("keydown", ".select2-search--inline", function (e) {
        var t;e.stopPropagation(), s.trigger("keypress", e), s._keyUpPrevented = e.isDefaultPrevented(), e.which !== l.BACKSPACE || "" !== s.$search.val() || 0 < (t = s.$selection.find(".select2-selection__choice").last()).length && (t = a.GetData(t[0], "data"), s.searchRemoveChoice(t), e.preventDefault());
      }), this.$selection.on("click", ".select2-search--inline", function (e) {
        s.$search.val() && e.stopPropagation();
      });var t = document.documentMode,
          o = t && t <= 11;this.$selection.on("input.searchcheck", ".select2-search--inline", function (e) {
        o ? s.$selection.off("input.search input.searchcheck") : s.$selection.off("keyup.search");
      }), this.$selection.on("keyup.search input.search", ".select2-search--inline", function (e) {
        var t;o && "input" === e.type ? s.$selection.off("input.search input.searchcheck") : (t = e.which) != l.SHIFT && t != l.CTRL && t != l.ALT && t != l.TAB && s.handleSearch(e);
      });
    }, e.prototype._transferTabIndex = function (e) {
      this.$search.attr("tabindex", this.$selection.attr("tabindex")), this.$selection.attr("tabindex", "-1");
    }, e.prototype.createPlaceholder = function (e, t) {
      this.$search.attr("placeholder", t.text);
    }, e.prototype.update = function (e, t) {
      var n = this.$search[0] == document.activeElement;this.$search.attr("placeholder", ""), e.call(this, t), this.resizeSearch(), n && this.$search.trigger("focus");
    }, e.prototype.handleSearch = function () {
      var e;this.resizeSearch(), this._keyUpPrevented || (e = this.$search.val(), this.trigger("query", { term: e })), this._keyUpPrevented = !1;
    }, e.prototype.searchRemoveChoice = function (e, t) {
      this.trigger("unselect", { data: t }), this.$search.val(t.text), this.handleSearch();
    }, e.prototype.resizeSearch = function () {
      this.$search.css("width", "25px");var e = "100%";"" === this.$search.attr("placeholder") && (e = .75 * (this.$search.val().length + 1) + "em"), this.$search.css("width", e);
    }, e;
  }), u.define("select2/selection/selectionCss", ["../utils"], function (n) {
    function e() {}return e.prototype.render = function (e) {
      var t = e.call(this),
          e = this.options.get("selectionCssClass") || "";return -1 !== e.indexOf(":all:") && (e = e.replace(":all:", ""), n.copyNonInternalCssClasses(t[0], this.$element[0])), t.addClass(e), t;
    }, e;
  }), u.define("select2/selection/eventRelay", ["jquery"], function (o) {
    function e() {}return e.prototype.bind = function (e, t, n) {
      var s = this,
          i = ["open", "opening", "close", "closing", "select", "selecting", "unselect", "unselecting", "clear", "clearing"],
          r = ["opening", "closing", "selecting", "unselecting", "clearing"];e.call(this, t, n), t.on("*", function (e, t) {
        var n;-1 !== i.indexOf(e) && (t = t || {}, n = o.Event("select2:" + e, { params: t }), s.$element.trigger(n), -1 !== r.indexOf(e) && (t.prevented = n.isDefaultPrevented()));
      });
    }, e;
  }), u.define("select2/translation", ["jquery", "require"], function (t, n) {
    function s(e) {
      this.dict = e || {};
    }return s.prototype.all = function () {
      return this.dict;
    }, s.prototype.get = function (e) {
      return this.dict[e];
    }, s.prototype.extend = function (e) {
      this.dict = t.extend({}, e.all(), this.dict);
    }, s._cache = {}, s.loadPath = function (e) {
      var t;return e in s._cache || (t = n(e), s._cache[e] = t), new s(s._cache[e]);
    }, s;
  }), u.define("select2/diacritics", [], function () {
    return { "Ⓐ": "A", "Ａ": "A", "À": "A", "Á": "A", "Â": "A", "Ầ": "A", "Ấ": "A", "Ẫ": "A", "Ẩ": "A", "Ã": "A", "Ā": "A", "Ă": "A", "Ằ": "A", "Ắ": "A", "Ẵ": "A", "Ẳ": "A", "Ȧ": "A", "Ǡ": "A", "Ä": "A", "Ǟ": "A", "Ả": "A", "Å": "A", "Ǻ": "A", "Ǎ": "A", "Ȁ": "A", "Ȃ": "A", "Ạ": "A", "Ậ": "A", "Ặ": "A", "Ḁ": "A", "Ą": "A", "Ⱥ": "A", "Ɐ": "A", "Ꜳ": "AA", "Æ": "AE", "Ǽ": "AE", "Ǣ": "AE", "Ꜵ": "AO", "Ꜷ": "AU", "Ꜹ": "AV", "Ꜻ": "AV", "Ꜽ": "AY", "Ⓑ": "B", "Ｂ": "B", "Ḃ": "B", "Ḅ": "B", "Ḇ": "B", "Ƀ": "B", "Ƃ": "B", "Ɓ": "B", "Ⓒ": "C", "Ｃ": "C", "Ć": "C", "Ĉ": "C", "Ċ": "C", "Č": "C", "Ç": "C", "Ḉ": "C", "Ƈ": "C", "Ȼ": "C", "Ꜿ": "C", "Ⓓ": "D", "Ｄ": "D", "Ḋ": "D", "Ď": "D", "Ḍ": "D", "Ḑ": "D", "Ḓ": "D", "Ḏ": "D", "Đ": "D", "Ƌ": "D", "Ɗ": "D", "Ɖ": "D", "Ꝺ": "D", "Ǳ": "DZ", "Ǆ": "DZ", "ǲ": "Dz", "ǅ": "Dz", "Ⓔ": "E", "Ｅ": "E", "È": "E", "É": "E", "Ê": "E", "Ề": "E", "Ế": "E", "Ễ": "E", "Ể": "E", "Ẽ": "E", "Ē": "E", "Ḕ": "E", "Ḗ": "E", "Ĕ": "E", "Ė": "E", "Ë": "E", "Ẻ": "E", "Ě": "E", "Ȅ": "E", "Ȇ": "E", "Ẹ": "E", "Ệ": "E", "Ȩ": "E", "Ḝ": "E", "Ę": "E", "Ḙ": "E", "Ḛ": "E", "Ɛ": "E", "Ǝ": "E", "Ⓕ": "F", "Ｆ": "F", "Ḟ": "F", "Ƒ": "F", "Ꝼ": "F", "Ⓖ": "G", "Ｇ": "G", "Ǵ": "G", "Ĝ": "G", "Ḡ": "G", "Ğ": "G", "Ġ": "G", "Ǧ": "G", "Ģ": "G", "Ǥ": "G", "Ɠ": "G", "Ꞡ": "G", "Ᵹ": "G", "Ꝿ": "G", "Ⓗ": "H", "Ｈ": "H", "Ĥ": "H", "Ḣ": "H", "Ḧ": "H", "Ȟ": "H", "Ḥ": "H", "Ḩ": "H", "Ḫ": "H", "Ħ": "H", "Ⱨ": "H", "Ⱶ": "H", "Ɥ": "H", "Ⓘ": "I", "Ｉ": "I", "Ì": "I", "Í": "I", "Î": "I", "Ĩ": "I", "Ī": "I", "Ĭ": "I", "İ": "I", "Ï": "I", "Ḯ": "I", "Ỉ": "I", "Ǐ": "I", "Ȉ": "I", "Ȋ": "I", "Ị": "I", "Į": "I", "Ḭ": "I", "Ɨ": "I", "Ⓙ": "J", "Ｊ": "J", "Ĵ": "J", "Ɉ": "J", "Ⓚ": "K", "Ｋ": "K", "Ḱ": "K", "Ǩ": "K", "Ḳ": "K", "Ķ": "K", "Ḵ": "K", "Ƙ": "K", "Ⱪ": "K", "Ꝁ": "K", "Ꝃ": "K", "Ꝅ": "K", "Ꞣ": "K", "Ⓛ": "L", "Ｌ": "L", "Ŀ": "L", "Ĺ": "L", "Ľ": "L", "Ḷ": "L", "Ḹ": "L", "Ļ": "L", "Ḽ": "L", "Ḻ": "L", "Ł": "L", "Ƚ": "L", "Ɫ": "L", "Ⱡ": "L", "Ꝉ": "L", "Ꝇ": "L", "Ꞁ": "L", "Ǉ": "LJ", "ǈ": "Lj", "Ⓜ": "M", "Ｍ": "M", "Ḿ": "M", "Ṁ": "M", "Ṃ": "M", "Ɱ": "M", "Ɯ": "M", "Ⓝ": "N", "Ｎ": "N", "Ǹ": "N", "Ń": "N", "Ñ": "N", "Ṅ": "N", "Ň": "N", "Ṇ": "N", "Ņ": "N", "Ṋ": "N", "Ṉ": "N", "Ƞ": "N", "Ɲ": "N", "Ꞑ": "N", "Ꞥ": "N", "Ǌ": "NJ", "ǋ": "Nj", "Ⓞ": "O", "Ｏ": "O", "Ò": "O", "Ó": "O", "Ô": "O", "Ồ": "O", "Ố": "O", "Ỗ": "O", "Ổ": "O", "Õ": "O", "Ṍ": "O", "Ȭ": "O", "Ṏ": "O", "Ō": "O", "Ṑ": "O", "Ṓ": "O", "Ŏ": "O", "Ȯ": "O", "Ȱ": "O", "Ö": "O", "Ȫ": "O", "Ỏ": "O", "Ő": "O", "Ǒ": "O", "Ȍ": "O", "Ȏ": "O", "Ơ": "O", "Ờ": "O", "Ớ": "O", "Ỡ": "O", "Ở": "O", "Ợ": "O", "Ọ": "O", "Ộ": "O", "Ǫ": "O", "Ǭ": "O", "Ø": "O", "Ǿ": "O", "Ɔ": "O", "Ɵ": "O", "Ꝋ": "O", "Ꝍ": "O", "Œ": "OE", "Ƣ": "OI", "Ꝏ": "OO", "Ȣ": "OU", "Ⓟ": "P", "Ｐ": "P", "Ṕ": "P", "Ṗ": "P", "Ƥ": "P", "Ᵽ": "P", "Ꝑ": "P", "Ꝓ": "P", "Ꝕ": "P", "Ⓠ": "Q", "Ｑ": "Q", "Ꝗ": "Q", "Ꝙ": "Q", "Ɋ": "Q", "Ⓡ": "R", "Ｒ": "R", "Ŕ": "R", "Ṙ": "R", "Ř": "R", "Ȑ": "R", "Ȓ": "R", "Ṛ": "R", "Ṝ": "R", "Ŗ": "R", "Ṟ": "R", "Ɍ": "R", "Ɽ": "R", "Ꝛ": "R", "Ꞧ": "R", "Ꞃ": "R", "Ⓢ": "S", "Ｓ": "S", "ẞ": "S", "Ś": "S", "Ṥ": "S", "Ŝ": "S", "Ṡ": "S", "Š": "S", "Ṧ": "S", "Ṣ": "S", "Ṩ": "S", "Ș": "S", "Ş": "S", "Ȿ": "S", "Ꞩ": "S", "Ꞅ": "S", "Ⓣ": "T", "Ｔ": "T", "Ṫ": "T", "Ť": "T", "Ṭ": "T", "Ț": "T", "Ţ": "T", "Ṱ": "T", "Ṯ": "T", "Ŧ": "T", "Ƭ": "T", "Ʈ": "T", "Ⱦ": "T", "Ꞇ": "T", "Ꜩ": "TZ", "Ⓤ": "U", "Ｕ": "U", "Ù": "U", "Ú": "U", "Û": "U", "Ũ": "U", "Ṹ": "U", "Ū": "U", "Ṻ": "U", "Ŭ": "U", "Ü": "U", "Ǜ": "U", "Ǘ": "U", "Ǖ": "U", "Ǚ": "U", "Ủ": "U", "Ů": "U", "Ű": "U", "Ǔ": "U", "Ȕ": "U", "Ȗ": "U", "Ư": "U", "Ừ": "U", "Ứ": "U", "Ữ": "U", "Ử": "U", "Ự": "U", "Ụ": "U", "Ṳ": "U", "Ų": "U", "Ṷ": "U", "Ṵ": "U", "Ʉ": "U", "Ⓥ": "V", "Ｖ": "V", "Ṽ": "V", "Ṿ": "V", "Ʋ": "V", "Ꝟ": "V", "Ʌ": "V", "Ꝡ": "VY", "Ⓦ": "W", "Ｗ": "W", "Ẁ": "W", "Ẃ": "W", "Ŵ": "W", "Ẇ": "W", "Ẅ": "W", "Ẉ": "W", "Ⱳ": "W", "Ⓧ": "X", "Ｘ": "X", "Ẋ": "X", "Ẍ": "X", "Ⓨ": "Y", "Ｙ": "Y", "Ỳ": "Y", "Ý": "Y", "Ŷ": "Y", "Ỹ": "Y", "Ȳ": "Y", "Ẏ": "Y", "Ÿ": "Y", "Ỷ": "Y", "Ỵ": "Y", "Ƴ": "Y", "Ɏ": "Y", "Ỿ": "Y", "Ⓩ": "Z", "Ｚ": "Z", "Ź": "Z", "Ẑ": "Z", "Ż": "Z", "Ž": "Z", "Ẓ": "Z", "Ẕ": "Z", "Ƶ": "Z", "Ȥ": "Z", "Ɀ": "Z", "Ⱬ": "Z", "Ꝣ": "Z", "ⓐ": "a", "ａ": "a", "ẚ": "a", "à": "a", "á": "a", "â": "a", "ầ": "a", "ấ": "a", "ẫ": "a", "ẩ": "a", "ã": "a", "ā": "a", "ă": "a", "ằ": "a", "ắ": "a", "ẵ": "a", "ẳ": "a", "ȧ": "a", "ǡ": "a", "ä": "a", "ǟ": "a", "ả": "a", "å": "a", "ǻ": "a", "ǎ": "a", "ȁ": "a", "ȃ": "a", "ạ": "a", "ậ": "a", "ặ": "a", "ḁ": "a", "ą": "a", "ⱥ": "a", "ɐ": "a", "ꜳ": "aa", "æ": "ae", "ǽ": "ae", "ǣ": "ae", "ꜵ": "ao", "ꜷ": "au", "ꜹ": "av", "ꜻ": "av", "ꜽ": "ay", "ⓑ": "b", "ｂ": "b", "ḃ": "b", "ḅ": "b", "ḇ": "b", "ƀ": "b", "ƃ": "b", "ɓ": "b", "ⓒ": "c", "ｃ": "c", "ć": "c", "ĉ": "c", "ċ": "c", "č": "c", "ç": "c", "ḉ": "c", "ƈ": "c", "ȼ": "c", "ꜿ": "c", "ↄ": "c", "ⓓ": "d", "ｄ": "d", "ḋ": "d", "ď": "d", "ḍ": "d", "ḑ": "d", "ḓ": "d", "ḏ": "d", "đ": "d", "ƌ": "d", "ɖ": "d", "ɗ": "d", "ꝺ": "d", "ǳ": "dz", "ǆ": "dz", "ⓔ": "e", "ｅ": "e", "è": "e", "é": "e", "ê": "e", "ề": "e", "ế": "e", "ễ": "e", "ể": "e", "ẽ": "e", "ē": "e", "ḕ": "e", "ḗ": "e", "ĕ": "e", "ė": "e", "ë": "e", "ẻ": "e", "ě": "e", "ȅ": "e", "ȇ": "e", "ẹ": "e", "ệ": "e", "ȩ": "e", "ḝ": "e", "ę": "e", "ḙ": "e", "ḛ": "e", "ɇ": "e", "ɛ": "e", "ǝ": "e", "ⓕ": "f", "ｆ": "f", "ḟ": "f", "ƒ": "f", "ꝼ": "f", "ⓖ": "g", "ｇ": "g", "ǵ": "g", "ĝ": "g", "ḡ": "g", "ğ": "g", "ġ": "g", "ǧ": "g", "ģ": "g", "ǥ": "g", "ɠ": "g", "ꞡ": "g", "ᵹ": "g", "ꝿ": "g", "ⓗ": "h", "ｈ": "h", "ĥ": "h", "ḣ": "h", "ḧ": "h", "ȟ": "h", "ḥ": "h", "ḩ": "h", "ḫ": "h", "ẖ": "h", "ħ": "h", "ⱨ": "h", "ⱶ": "h", "ɥ": "h", "ƕ": "hv", "ⓘ": "i", "ｉ": "i", "ì": "i", "í": "i", "î": "i", "ĩ": "i", "ī": "i", "ĭ": "i", "ï": "i", "ḯ": "i", "ỉ": "i", "ǐ": "i", "ȉ": "i", "ȋ": "i", "ị": "i", "į": "i", "ḭ": "i", "ɨ": "i", "ı": "i", "ⓙ": "j", "ｊ": "j", "ĵ": "j", "ǰ": "j", "ɉ": "j", "ⓚ": "k", "ｋ": "k", "ḱ": "k", "ǩ": "k", "ḳ": "k", "ķ": "k", "ḵ": "k", "ƙ": "k", "ⱪ": "k", "ꝁ": "k", "ꝃ": "k", "ꝅ": "k", "ꞣ": "k", "ⓛ": "l", "ｌ": "l", "ŀ": "l", "ĺ": "l", "ľ": "l", "ḷ": "l", "ḹ": "l", "ļ": "l", "ḽ": "l", "ḻ": "l", "ſ": "l", "ł": "l", "ƚ": "l", "ɫ": "l", "ⱡ": "l", "ꝉ": "l", "ꞁ": "l", "ꝇ": "l", "ǉ": "lj", "ⓜ": "m", "ｍ": "m", "ḿ": "m", "ṁ": "m", "ṃ": "m", "ɱ": "m", "ɯ": "m", "ⓝ": "n", "ｎ": "n", "ǹ": "n", "ń": "n", "ñ": "n", "ṅ": "n", "ň": "n", "ṇ": "n", "ņ": "n", "ṋ": "n", "ṉ": "n", "ƞ": "n", "ɲ": "n", "ŉ": "n", "ꞑ": "n", "ꞥ": "n", "ǌ": "nj", "ⓞ": "o", "ｏ": "o", "ò": "o", "ó": "o", "ô": "o", "ồ": "o", "ố": "o", "ỗ": "o", "ổ": "o", "õ": "o", "ṍ": "o", "ȭ": "o", "ṏ": "o", "ō": "o", "ṑ": "o", "ṓ": "o", "ŏ": "o", "ȯ": "o", "ȱ": "o", "ö": "o", "ȫ": "o", "ỏ": "o", "ő": "o", "ǒ": "o", "ȍ": "o", "ȏ": "o", "ơ": "o", "ờ": "o", "ớ": "o", "ỡ": "o", "ở": "o", "ợ": "o", "ọ": "o", "ộ": "o", "ǫ": "o", "ǭ": "o", "ø": "o", "ǿ": "o", "ɔ": "o", "ꝋ": "o", "ꝍ": "o", "ɵ": "o", "œ": "oe", "ƣ": "oi", "ȣ": "ou", "ꝏ": "oo", "ⓟ": "p", "ｐ": "p", "ṕ": "p", "ṗ": "p", "ƥ": "p", "ᵽ": "p", "ꝑ": "p", "ꝓ": "p", "ꝕ": "p", "ⓠ": "q", "ｑ": "q", "ɋ": "q", "ꝗ": "q", "ꝙ": "q", "ⓡ": "r", "ｒ": "r", "ŕ": "r", "ṙ": "r", "ř": "r", "ȑ": "r", "ȓ": "r", "ṛ": "r", "ṝ": "r", "ŗ": "r", "ṟ": "r", "ɍ": "r", "ɽ": "r", "ꝛ": "r", "ꞧ": "r", "ꞃ": "r", "ⓢ": "s", "ｓ": "s", "ß": "s", "ś": "s", "ṥ": "s", "ŝ": "s", "ṡ": "s", "š": "s", "ṧ": "s", "ṣ": "s", "ṩ": "s", "ș": "s", "ş": "s", "ȿ": "s", "ꞩ": "s", "ꞅ": "s", "ẛ": "s", "ⓣ": "t", "ｔ": "t", "ṫ": "t", "ẗ": "t", "ť": "t", "ṭ": "t", "ț": "t", "ţ": "t", "ṱ": "t", "ṯ": "t", "ŧ": "t", "ƭ": "t", "ʈ": "t", "ⱦ": "t", "ꞇ": "t", "ꜩ": "tz", "ⓤ": "u", "ｕ": "u", "ù": "u", "ú": "u", "û": "u", "ũ": "u", "ṹ": "u", "ū": "u", "ṻ": "u", "ŭ": "u", "ü": "u", "ǜ": "u", "ǘ": "u", "ǖ": "u", "ǚ": "u", "ủ": "u", "ů": "u", "ű": "u", "ǔ": "u", "ȕ": "u", "ȗ": "u", "ư": "u", "ừ": "u", "ứ": "u", "ữ": "u", "ử": "u", "ự": "u", "ụ": "u", "ṳ": "u", "ų": "u", "ṷ": "u", "ṵ": "u", "ʉ": "u", "ⓥ": "v", "ｖ": "v", "ṽ": "v", "ṿ": "v", "ʋ": "v", "ꝟ": "v", "ʌ": "v", "ꝡ": "vy", "ⓦ": "w", "ｗ": "w", "ẁ": "w", "ẃ": "w", "ŵ": "w", "ẇ": "w", "ẅ": "w", "ẘ": "w", "ẉ": "w", "ⱳ": "w", "ⓧ": "x", "ｘ": "x", "ẋ": "x", "ẍ": "x", "ⓨ": "y", "ｙ": "y", "ỳ": "y", "ý": "y", "ŷ": "y", "ỹ": "y", "ȳ": "y", "ẏ": "y", "ÿ": "y", "ỷ": "y", "ẙ": "y", "ỵ": "y", "ƴ": "y", "ɏ": "y", "ỿ": "y", "ⓩ": "z", "ｚ": "z", "ź": "z", "ẑ": "z", "ż": "z", "ž": "z", "ẓ": "z", "ẕ": "z", "ƶ": "z", "ȥ": "z", "ɀ": "z", "ⱬ": "z", "ꝣ": "z", "Ά": "Α", "Έ": "Ε", "Ή": "Η", "Ί": "Ι", "Ϊ": "Ι", "Ό": "Ο", "Ύ": "Υ", "Ϋ": "Υ", "Ώ": "Ω", "ά": "α", "έ": "ε", "ή": "η", "ί": "ι", "ϊ": "ι", "ΐ": "ι", "ό": "ο", "ύ": "υ", "ϋ": "υ", "ΰ": "υ", "ώ": "ω", "ς": "σ", "’": "'" };
  }), u.define("select2/data/base", ["../utils"], function (n) {
    function s(e, t) {
      s.__super__.constructor.call(this);
    }return n.Extend(s, n.Observable), s.prototype.current = function (e) {
      throw new Error("The `current` method must be defined in child classes.");
    }, s.prototype.query = function (e, t) {
      throw new Error("The `query` method must be defined in child classes.");
    }, s.prototype.bind = function (e, t) {}, s.prototype.destroy = function () {}, s.prototype.generateResultId = function (e, t) {
      e = e.id + "-result-";return e += n.generateChars(4), null != t.id ? e += "-" + t.id.toString() : e += "-" + n.generateChars(4), e;
    }, s;
  }), u.define("select2/data/select", ["./base", "../utils", "jquery"], function (e, a, l) {
    function n(e, t) {
      this.$element = e, this.options = t, n.__super__.constructor.call(this);
    }return a.Extend(n, e), n.prototype.current = function (e) {
      var t = this;e(Array.prototype.map.call(this.$element[0].querySelectorAll(":checked"), function (e) {
        return t.item(l(e));
      }));
    }, n.prototype.select = function (i) {
      var e,
          r = this;if (i.selected = !0, null != i.element && "option" === i.element.tagName.toLowerCase()) return i.element.selected = !0, void this.$element.trigger("input").trigger("change");this.$element.prop("multiple") ? this.current(function (e) {
        var t = [];(i = [i]).push.apply(i, e);for (var n = 0; n < i.length; n++) {
          var s = i[n].id;-1 === t.indexOf(s) && t.push(s);
        }r.$element.val(t), r.$element.trigger("input").trigger("change");
      }) : (e = i.id, this.$element.val(e), this.$element.trigger("input").trigger("change"));
    }, n.prototype.unselect = function (i) {
      var r = this;if (this.$element.prop("multiple")) {
        if (i.selected = !1, null != i.element && "option" === i.element.tagName.toLowerCase()) return i.element.selected = !1, void this.$element.trigger("input").trigger("change");this.current(function (e) {
          for (var t = [], n = 0; n < e.length; n++) {
            var s = e[n].id;s !== i.id && -1 === t.indexOf(s) && t.push(s);
          }r.$element.val(t), r.$element.trigger("input").trigger("change");
        });
      }
    }, n.prototype.bind = function (e, t) {
      var n = this;(this.container = e).on("select", function (e) {
        n.select(e.data);
      }), e.on("unselect", function (e) {
        n.unselect(e.data);
      });
    }, n.prototype.destroy = function () {
      this.$element.find("*").each(function () {
        a.RemoveData(this);
      });
    }, n.prototype.query = function (t, e) {
      var n = [],
          s = this;this.$element.children().each(function () {
        var e;"option" !== this.tagName.toLowerCase() && "optgroup" !== this.tagName.toLowerCase() || (e = l(this), e = s.item(e), null !== (e = s.matches(t, e)) && n.push(e));
      }), e({ results: n });
    }, n.prototype.addOptions = function (e) {
      this.$element.append(e);
    }, n.prototype.option = function (e) {
      var t;e.children ? (t = document.createElement("optgroup")).label = e.text : void 0 !== (t = document.createElement("option")).textContent ? t.textContent = e.text : t.innerText = e.text, void 0 !== e.id && (t.value = e.id), e.disabled && (t.disabled = !0), e.selected && (t.selected = !0), e.title && (t.title = e.title);e = this._normalizeItem(e);return e.element = t, a.StoreData(t, "data", e), l(t);
    }, n.prototype.item = function (e) {
      var t = {};if (null != (t = a.GetData(e[0], "data"))) return t;var n = e[0];if ("option" === n.tagName.toLowerCase()) t = { id: e.val(), text: e.text(), disabled: e.prop("disabled"), selected: e.prop("selected"), title: e.prop("title") };else if ("optgroup" === n.tagName.toLowerCase()) {
        t = { text: e.prop("label"), children: [], title: e.prop("title") };for (var s = e.children("option"), i = [], r = 0; r < s.length; r++) {
          var o = l(s[r]),
              o = this.item(o);i.push(o);
        }t.children = i;
      }return (t = this._normalizeItem(t)).element = e[0], a.StoreData(e[0], "data", t), t;
    }, n.prototype._normalizeItem = function (e) {
      e !== Object(e) && (e = { id: e, text: e });return null != (e = l.extend({}, { text: "" }, e)).id && (e.id = e.id.toString()), null != e.text && (e.text = e.text.toString()), null == e._resultId && e.id && null != this.container && (e._resultId = this.generateResultId(this.container, e)), l.extend({}, { selected: !1, disabled: !1 }, e);
    }, n.prototype.matches = function (e, t) {
      return this.options.get("matcher")(e, t);
    }, n;
  }), u.define("select2/data/array", ["./select", "../utils", "jquery"], function (e, t, c) {
    function s(e, t) {
      this._dataToConvert = t.get("data") || [], s.__super__.constructor.call(this, e, t);
    }return t.Extend(s, e), s.prototype.bind = function (e, t) {
      s.__super__.bind.call(this, e, t), this.addOptions(this.convertToOptions(this._dataToConvert));
    }, s.prototype.select = function (n) {
      var e = this.$element.find("option").filter(function (e, t) {
        return t.value == n.id.toString();
      });0 === e.length && (e = this.option(n), this.addOptions(e)), s.__super__.select.call(this, n);
    }, s.prototype.convertToOptions = function (e) {
      var t = this,
          n = this.$element.find("option"),
          s = n.map(function () {
        return t.item(c(this)).id;
      }).get(),
          i = [];for (var r = 0; r < e.length; r++) {
        var o,
            a,
            l = this._normalizeItem(e[r]);0 <= s.indexOf(l.id) ? (o = n.filter(function (e) {
          return function () {
            return c(this).val() == e.id;
          };
        }(l)), a = this.item(o), a = c.extend(!0, {}, l, a), a = this.option(a), o.replaceWith(a)) : (a = this.option(l), l.children && (l = this.convertToOptions(l.children), a.append(l)), i.push(a));
      }return i;
    }, s;
  }), u.define("select2/data/ajax", ["./array", "../utils", "jquery"], function (e, t, r) {
    function n(e, t) {
      this.ajaxOptions = this._applyDefaults(t.get("ajax")), null != this.ajaxOptions.processResults && (this.processResults = this.ajaxOptions.processResults), n.__super__.constructor.call(this, e, t);
    }return t.Extend(n, e), n.prototype._applyDefaults = function (e) {
      var t = { data: function data(e) {
          return r.extend({}, e, { q: e.term });
        }, transport: function transport(e, t, n) {
          e = r.ajax(e);return e.then(t), e.fail(n), e;
        } };return r.extend({}, t, e, !0);
    }, n.prototype.processResults = function (e) {
      return e;
    }, n.prototype.query = function (t, n) {
      var s = this;null != this._request && ("function" == typeof this._request.abort && this._request.abort(), this._request = null);var i = r.extend({ type: "GET" }, this.ajaxOptions);function e() {
        var e = i.transport(i, function (e) {
          e = s.processResults(e, t);s.options.get("debug") && window.console && console.error && (e && e.results && Array.isArray(e.results) || console.error("Select2: The AJAX results did not return an array in the `results` key of the response.")), n(e);
        }, function () {
          "status" in e && (0 === e.status || "0" === e.status) || s.trigger("results:message", { message: "errorLoading" });
        });s._request = e;
      }"function" == typeof i.url && (i.url = i.url.call(this.$element, t)), "function" == typeof i.data && (i.data = i.data.call(this.$element, t)), this.ajaxOptions.delay && null != t.term ? (this._queryTimeout && window.clearTimeout(this._queryTimeout), this._queryTimeout = window.setTimeout(e, this.ajaxOptions.delay)) : e();
    }, n;
  }), u.define("select2/data/tags", ["jquery"], function (t) {
    function e(e, t, n) {
      var s = n.get("tags"),
          i = n.get("createTag");void 0 !== i && (this.createTag = i);i = n.get("insertTag");if (void 0 !== i && (this.insertTag = i), e.call(this, t, n), Array.isArray(s)) for (var r = 0; r < s.length; r++) {
        var o = s[r],
            o = this._normalizeItem(o),
            o = this.option(o);this.$element.append(o);
      }
    }return e.prototype.query = function (e, c, u) {
      var d = this;this._removeOldTags(), null != c.term && null == c.page ? e.call(this, c, function e(t, n) {
        for (var s = t.results, i = 0; i < s.length; i++) {
          var r = s[i],
              o = null != r.children && !e({ results: r.children }, !0);if ((r.text || "").toUpperCase() === (c.term || "").toUpperCase() || o) return !n && (t.data = s, void u(t));
        }if (n) return !0;var a,
            l = d.createTag(c);null != l && ((a = d.option(l)).attr("data-select2-tag", "true"), d.addOptions([a]), d.insertTag(s, l)), t.results = s, u(t);
      }) : e.call(this, c, u);
    }, e.prototype.createTag = function (e, t) {
      if (null == t.term) return null;t = t.term.trim();return "" === t ? null : { id: t, text: t };
    }, e.prototype.insertTag = function (e, t, n) {
      t.unshift(n);
    }, e.prototype._removeOldTags = function (e) {
      this.$element.find("option[data-select2-tag]").each(function () {
        this.selected || t(this).remove();
      });
    }, e;
  }), u.define("select2/data/tokenizer", ["jquery"], function (c) {
    function e(e, t, n) {
      var s = n.get("tokenizer");void 0 !== s && (this.tokenizer = s), e.call(this, t, n);
    }return e.prototype.bind = function (e, t, n) {
      e.call(this, t, n), this.$search = t.dropdown.$search || t.selection.$search || n.find(".select2-search__field");
    }, e.prototype.query = function (e, t, n) {
      var s = this;t.term = t.term || "";var i = this.tokenizer(t, this.options, function (e) {
        var t,
            n = s._normalizeItem(e);s.$element.find("option").filter(function () {
          return c(this).val() === n.id;
        }).length || ((t = s.option(n)).attr("data-select2-tag", !0), s._removeOldTags(), s.addOptions([t])), t = n, s.trigger("select", { data: t });
      });i.term !== t.term && (this.$search.length && (this.$search.val(i.term), this.$search.trigger("focus")), t.term = i.term), e.call(this, t, n);
    }, e.prototype.tokenizer = function (e, t, n, s) {
      for (var i = n.get("tokenSeparators") || [], r = t.term, o = 0, a = this.createTag || function (e) {
        return { id: e.term, text: e.term };
      }; o < r.length;) {
        var l = r[o];-1 !== i.indexOf(l) ? (l = r.substr(0, o), null != (l = a(c.extend({}, t, { term: l }))) ? (s(l), r = r.substr(o + 1) || "", o = 0) : o++) : o++;
      }return { term: r };
    }, e;
  }), u.define("select2/data/minimumInputLength", [], function () {
    function e(e, t, n) {
      this.minimumInputLength = n.get("minimumInputLength"), e.call(this, t, n);
    }return e.prototype.query = function (e, t, n) {
      t.term = t.term || "", t.term.length < this.minimumInputLength ? this.trigger("results:message", { message: "inputTooShort", args: { minimum: this.minimumInputLength, input: t.term, params: t } }) : e.call(this, t, n);
    }, e;
  }), u.define("select2/data/maximumInputLength", [], function () {
    function e(e, t, n) {
      this.maximumInputLength = n.get("maximumInputLength"), e.call(this, t, n);
    }return e.prototype.query = function (e, t, n) {
      t.term = t.term || "", 0 < this.maximumInputLength && t.term.length > this.maximumInputLength ? this.trigger("results:message", { message: "inputTooLong", args: { maximum: this.maximumInputLength, input: t.term, params: t } }) : e.call(this, t, n);
    }, e;
  }), u.define("select2/data/maximumSelectionLength", [], function () {
    function e(e, t, n) {
      this.maximumSelectionLength = n.get("maximumSelectionLength"), e.call(this, t, n);
    }return e.prototype.bind = function (e, t, n) {
      var s = this;e.call(this, t, n), t.on("select", function () {
        s._checkIfMaximumSelected();
      });
    }, e.prototype.query = function (e, t, n) {
      var s = this;this._checkIfMaximumSelected(function () {
        e.call(s, t, n);
      });
    }, e.prototype._checkIfMaximumSelected = function (e, t) {
      var n = this;this.current(function (e) {
        e = null != e ? e.length : 0;0 < n.maximumSelectionLength && e >= n.maximumSelectionLength ? n.trigger("results:message", { message: "maximumSelected", args: { maximum: n.maximumSelectionLength } }) : t && t();
      });
    }, e;
  }), u.define("select2/dropdown", ["jquery", "./utils"], function (t, e) {
    function n(e, t) {
      this.$element = e, this.options = t, n.__super__.constructor.call(this);
    }return e.Extend(n, e.Observable), n.prototype.render = function () {
      var e = t('<span class="select2-dropdown"><span class="select2-results"></span></span>');return e.attr("dir", this.options.get("dir")), this.$dropdown = e;
    }, n.prototype.bind = function () {}, n.prototype.position = function (e, t) {}, n.prototype.destroy = function () {
      this.$dropdown.remove();
    }, n;
  }), u.define("select2/dropdown/search", ["jquery"], function (r) {
    function e() {}return e.prototype.render = function (e) {
      var t = e.call(this),
          n = this.options.get("translations").get("search"),
          e = r('<span class="select2-search select2-search--dropdown"><input class="select2-search__field" type="search" tabindex="-1" autocorrect="off" autocapitalize="none" spellcheck="false" role="searchbox" aria-autocomplete="list" /></span>');return this.$searchContainer = e, this.$search = e.find("input"), this.$search.prop("autocomplete", this.options.get("autocomplete")), this.$search.attr("aria-label", n()), t.prepend(e), t;
    }, e.prototype.bind = function (e, t, n) {
      var s = this,
          i = t.id + "-results";e.call(this, t, n), this.$search.on("keydown", function (e) {
        s.trigger("keypress", e), s._keyUpPrevented = e.isDefaultPrevented();
      }), this.$search.on("input", function (e) {
        r(this).off("keyup");
      }), this.$search.on("keyup input", function (e) {
        s.handleSearch(e);
      }), t.on("open", function () {
        s.$search.attr("tabindex", 0), s.$search.attr("aria-controls", i), s.$search.trigger("focus"), window.setTimeout(function () {
          s.$search.trigger("focus");
        }, 0);
      }), t.on("close", function () {
        s.$search.attr("tabindex", -1), s.$search.removeAttr("aria-controls"), s.$search.removeAttr("aria-activedescendant"), s.$search.val(""), s.$search.trigger("blur");
      }), t.on("focus", function () {
        t.isOpen() || s.$search.trigger("focus");
      }), t.on("results:all", function (e) {
        null != e.query.term && "" !== e.query.term || (s.showSearch(e) ? s.$searchContainer[0].classList.remove("select2-search--hide") : s.$searchContainer[0].classList.add("select2-search--hide"));
      }), t.on("results:focus", function (e) {
        e.data._resultId ? s.$search.attr("aria-activedescendant", e.data._resultId) : s.$search.removeAttr("aria-activedescendant");
      });
    }, e.prototype.handleSearch = function (e) {
      var t;this._keyUpPrevented || (t = this.$search.val(), this.trigger("query", { term: t })), this._keyUpPrevented = !1;
    }, e.prototype.showSearch = function (e, t) {
      return !0;
    }, e;
  }), u.define("select2/dropdown/hidePlaceholder", [], function () {
    function e(e, t, n, s) {
      this.placeholder = this.normalizePlaceholder(n.get("placeholder")), e.call(this, t, n, s);
    }return e.prototype.append = function (e, t) {
      t.results = this.removePlaceholder(t.results), e.call(this, t);
    }, e.prototype.normalizePlaceholder = function (e, t) {
      return t = "string" == typeof t ? { id: "", text: t } : t;
    }, e.prototype.removePlaceholder = function (e, t) {
      for (var n = t.slice(0), s = t.length - 1; 0 <= s; s--) {
        var i = t[s];this.placeholder.id === i.id && n.splice(s, 1);
      }return n;
    }, e;
  }), u.define("select2/dropdown/infiniteScroll", ["jquery"], function (n) {
    function e(e, t, n, s) {
      this.lastParams = {}, e.call(this, t, n, s), this.$loadingMore = this.createLoadingMore(), this.loading = !1;
    }return e.prototype.append = function (e, t) {
      this.$loadingMore.remove(), this.loading = !1, e.call(this, t), this.showLoadingMore(t) && (this.$results.append(this.$loadingMore), this.loadMoreIfNeeded());
    }, e.prototype.bind = function (e, t, n) {
      var s = this;e.call(this, t, n), t.on("query", function (e) {
        s.lastParams = e, s.loading = !0;
      }), t.on("query:append", function (e) {
        s.lastParams = e, s.loading = !0;
      }), this.$results.on("scroll", this.loadMoreIfNeeded.bind(this));
    }, e.prototype.loadMoreIfNeeded = function () {
      var e = n.contains(document.documentElement, this.$loadingMore[0]);!this.loading && e && (e = this.$results.offset().top + this.$results.outerHeight(!1), this.$loadingMore.offset().top + this.$loadingMore.outerHeight(!1) <= e + 50 && this.loadMore());
    }, e.prototype.loadMore = function () {
      this.loading = !0;var e = n.extend({}, { page: 1 }, this.lastParams);e.page++, this.trigger("query:append", e);
    }, e.prototype.showLoadingMore = function (e, t) {
      return t.pagination && t.pagination.more;
    }, e.prototype.createLoadingMore = function () {
      var e = n('<li class="select2-results__option select2-results__option--load-more"role="option" aria-disabled="true"></li>'),
          t = this.options.get("translations").get("loadingMore");return e.html(t(this.lastParams)), e;
    }, e;
  }), u.define("select2/dropdown/attachBody", ["jquery", "../utils"], function (u, o) {
    function e(e, t, n) {
      this.$dropdownParent = u(n.get("dropdownParent") || document.body), e.call(this, t, n);
    }return e.prototype.bind = function (e, t, n) {
      var s = this;e.call(this, t, n), t.on("open", function () {
        s._showDropdown(), s._attachPositioningHandler(t), s._bindContainerResultHandlers(t);
      }), t.on("close", function () {
        s._hideDropdown(), s._detachPositioningHandler(t);
      }), this.$dropdownContainer.on("mousedown", function (e) {
        e.stopPropagation();
      });
    }, e.prototype.destroy = function (e) {
      e.call(this), this.$dropdownContainer.remove();
    }, e.prototype.position = function (e, t, n) {
      t.attr("class", n.attr("class")), t[0].classList.remove("select2"), t[0].classList.add("select2-container--open"), t.css({ position: "absolute", top: -999999 }), this.$container = n;
    }, e.prototype.render = function (e) {
      var t = u("<span></span>"),
          e = e.call(this);return t.append(e), this.$dropdownContainer = t;
    }, e.prototype._hideDropdown = function (e) {
      this.$dropdownContainer.detach();
    }, e.prototype._bindContainerResultHandlers = function (e, t) {
      var n;this._containerResultsHandlersBound || (n = this, t.on("results:all", function () {
        n._positionDropdown(), n._resizeDropdown();
      }), t.on("results:append", function () {
        n._positionDropdown(), n._resizeDropdown();
      }), t.on("results:message", function () {
        n._positionDropdown(), n._resizeDropdown();
      }), t.on("select", function () {
        n._positionDropdown(), n._resizeDropdown();
      }), t.on("unselect", function () {
        n._positionDropdown(), n._resizeDropdown();
      }), this._containerResultsHandlersBound = !0);
    }, e.prototype._attachPositioningHandler = function (e, t) {
      var n = this,
          s = "scroll.select2." + t.id,
          i = "resize.select2." + t.id,
          r = "orientationchange.select2." + t.id,
          t = this.$container.parents().filter(o.hasScroll);t.each(function () {
        o.StoreData(this, "select2-scroll-position", { x: u(this).scrollLeft(), y: u(this).scrollTop() });
      }), t.on(s, function (e) {
        var t = o.GetData(this, "select2-scroll-position");u(this).scrollTop(t.y);
      }), u(window).on(s + " " + i + " " + r, function (e) {
        n._positionDropdown(), n._resizeDropdown();
      });
    }, e.prototype._detachPositioningHandler = function (e, t) {
      var n = "scroll.select2." + t.id,
          s = "resize.select2." + t.id,
          t = "orientationchange.select2." + t.id;this.$container.parents().filter(o.hasScroll).off(n), u(window).off(n + " " + s + " " + t);
    }, e.prototype._positionDropdown = function () {
      var e = u(window),
          t = this.$dropdown[0].classList.contains("select2-dropdown--above"),
          n = this.$dropdown[0].classList.contains("select2-dropdown--below"),
          s = null,
          i = this.$container.offset();i.bottom = i.top + this.$container.outerHeight(!1);var r = { height: this.$container.outerHeight(!1) };r.top = i.top, r.bottom = i.top + r.height;var o = this.$dropdown.outerHeight(!1),
          a = e.scrollTop(),
          l = e.scrollTop() + e.height(),
          c = a < i.top - o,
          e = l > i.bottom + o,
          a = { left: i.left, top: r.bottom },
          l = this.$dropdownParent;"static" === l.css("position") && (l = l.offsetParent());i = { top: 0, left: 0 };(u.contains(document.body, l[0]) || l[0].isConnected) && (i = l.offset()), a.top -= i.top, a.left -= i.left, t || n || (s = "below"), e || !c || t ? !c && e && t && (s = "below") : s = "above", ("above" == s || t && "below" !== s) && (a.top = r.top - i.top - o), null != s && (this.$dropdown[0].classList.remove("select2-dropdown--below"), this.$dropdown[0].classList.remove("select2-dropdown--above"), this.$dropdown[0].classList.add("select2-dropdown--" + s), this.$container[0].classList.remove("select2-container--below"), this.$container[0].classList.remove("select2-container--above"), this.$container[0].classList.add("select2-container--" + s)), this.$dropdownContainer.css(a);
    }, e.prototype._resizeDropdown = function () {
      var e = { width: this.$container.outerWidth(!1) + "px" };this.options.get("dropdownAutoWidth") && (e.minWidth = e.width, e.position = "relative", e.width = "auto"), this.$dropdown.css(e);
    }, e.prototype._showDropdown = function (e) {
      this.$dropdownContainer.appendTo(this.$dropdownParent), this._positionDropdown(), this._resizeDropdown();
    }, e;
  }), u.define("select2/dropdown/minimumResultsForSearch", [], function () {
    function e(e, t, n, s) {
      this.minimumResultsForSearch = n.get("minimumResultsForSearch"), this.minimumResultsForSearch < 0 && (this.minimumResultsForSearch = 1 / 0), e.call(this, t, n, s);
    }return e.prototype.showSearch = function (e, t) {
      return !(function e(t) {
        for (var n = 0, s = 0; s < t.length; s++) {
          var i = t[s];i.children ? n += e(i.children) : n++;
        }return n;
      }(t.data.results) < this.minimumResultsForSearch) && e.call(this, t);
    }, e;
  }), u.define("select2/dropdown/selectOnClose", ["../utils"], function (s) {
    function e() {}return e.prototype.bind = function (e, t, n) {
      var s = this;e.call(this, t, n), t.on("close", function (e) {
        s._handleSelectOnClose(e);
      });
    }, e.prototype._handleSelectOnClose = function (e, t) {
      if (t && null != t.originalSelect2Event) {
        var n = t.originalSelect2Event;if ("select" === n._type || "unselect" === n._type) return;
      }n = this.getHighlightedResults();n.length < 1 || null != (n = s.GetData(n[0], "data")).element && n.element.selected || null == n.element && n.selected || this.trigger("select", { data: n });
    }, e;
  }), u.define("select2/dropdown/closeOnSelect", [], function () {
    function e() {}return e.prototype.bind = function (e, t, n) {
      var s = this;e.call(this, t, n), t.on("select", function (e) {
        s._selectTriggered(e);
      }), t.on("unselect", function (e) {
        s._selectTriggered(e);
      });
    }, e.prototype._selectTriggered = function (e, t) {
      var n = t.originalEvent;n && (n.ctrlKey || n.metaKey) || this.trigger("close", { originalEvent: n, originalSelect2Event: t });
    }, e;
  }), u.define("select2/dropdown/dropdownCss", ["../utils"], function (n) {
    function e() {}return e.prototype.render = function (e) {
      var t = e.call(this),
          e = this.options.get("dropdownCssClass") || "";return -1 !== e.indexOf(":all:") && (e = e.replace(":all:", ""), n.copyNonInternalCssClasses(t[0], this.$element[0])), t.addClass(e), t;
    }, e;
  }), u.define("select2/dropdown/tagsSearchHighlight", ["../utils"], function (s) {
    function e() {}return e.prototype.highlightFirstItem = function (e) {
      var t = this.$results.find(".select2-results__option--selectable:not(.select2-results__option--selected)");if (0 < t.length) {
        var n = t.first(),
            t = s.GetData(n[0], "data").element;if (t && t.getAttribute && "true" === t.getAttribute("data-select2-tag")) return void n.trigger("mouseenter");
      }e.call(this);
    }, e;
  }), u.define("select2/i18n/en", [], function () {
    return { errorLoading: function errorLoading() {
        return "The results could not be loaded.";
      }, inputTooLong: function inputTooLong(e) {
        var t = e.input.length - e.maximum,
            e = "Please delete " + t + " character";return 1 != t && (e += "s"), e;
      }, inputTooShort: function inputTooShort(e) {
        return "Please enter " + (e.minimum - e.input.length) + " or more characters";
      }, loadingMore: function loadingMore() {
        return "Loading more results…";
      }, maximumSelected: function maximumSelected(e) {
        var t = "You can only select " + e.maximum + " item";return 1 != e.maximum && (t += "s"), t;
      }, noResults: function noResults() {
        return "No results found";
      }, searching: function searching() {
        return "Searching…";
      }, removeAllItems: function removeAllItems() {
        return "Remove all items";
      }, removeItem: function removeItem() {
        return "Remove item";
      }, search: function search() {
        return "Search";
      } };
  }), u.define("select2/defaults", ["jquery", "./results", "./selection/single", "./selection/multiple", "./selection/placeholder", "./selection/allowClear", "./selection/search", "./selection/selectionCss", "./selection/eventRelay", "./utils", "./translation", "./diacritics", "./data/select", "./data/array", "./data/ajax", "./data/tags", "./data/tokenizer", "./data/minimumInputLength", "./data/maximumInputLength", "./data/maximumSelectionLength", "./dropdown", "./dropdown/search", "./dropdown/hidePlaceholder", "./dropdown/infiniteScroll", "./dropdown/attachBody", "./dropdown/minimumResultsForSearch", "./dropdown/selectOnClose", "./dropdown/closeOnSelect", "./dropdown/dropdownCss", "./dropdown/tagsSearchHighlight", "./i18n/en"], function (l, r, o, a, c, u, d, p, h, f, g, t, m, y, v, _, b, $, w, x, A, D, S, E, O, C, L, T, q, I, e) {
    function n() {
      this.reset();
    }return n.prototype.apply = function (e) {
      var t;null == (e = l.extend(!0, {}, this.defaults, e)).dataAdapter && (null != e.ajax ? e.dataAdapter = v : null != e.data ? e.dataAdapter = y : e.dataAdapter = m, 0 < e.minimumInputLength && (e.dataAdapter = f.Decorate(e.dataAdapter, $)), 0 < e.maximumInputLength && (e.dataAdapter = f.Decorate(e.dataAdapter, w)), 0 < e.maximumSelectionLength && (e.dataAdapter = f.Decorate(e.dataAdapter, x)), e.tags && (e.dataAdapter = f.Decorate(e.dataAdapter, _)), null == e.tokenSeparators && null == e.tokenizer || (e.dataAdapter = f.Decorate(e.dataAdapter, b))), null == e.resultsAdapter && (e.resultsAdapter = r, null != e.ajax && (e.resultsAdapter = f.Decorate(e.resultsAdapter, E)), null != e.placeholder && (e.resultsAdapter = f.Decorate(e.resultsAdapter, S)), e.selectOnClose && (e.resultsAdapter = f.Decorate(e.resultsAdapter, L)), e.tags && (e.resultsAdapter = f.Decorate(e.resultsAdapter, I))), null == e.dropdownAdapter && (e.multiple ? e.dropdownAdapter = A : (t = f.Decorate(A, D), e.dropdownAdapter = t), 0 !== e.minimumResultsForSearch && (e.dropdownAdapter = f.Decorate(e.dropdownAdapter, C)), e.closeOnSelect && (e.dropdownAdapter = f.Decorate(e.dropdownAdapter, T)), null != e.dropdownCssClass && (e.dropdownAdapter = f.Decorate(e.dropdownAdapter, q)), e.dropdownAdapter = f.Decorate(e.dropdownAdapter, O)), null == e.selectionAdapter && (e.multiple ? e.selectionAdapter = a : e.selectionAdapter = o, null != e.placeholder && (e.selectionAdapter = f.Decorate(e.selectionAdapter, c)), e.allowClear && (e.selectionAdapter = f.Decorate(e.selectionAdapter, u)), e.multiple && (e.selectionAdapter = f.Decorate(e.selectionAdapter, d)), null != e.selectionCssClass && (e.selectionAdapter = f.Decorate(e.selectionAdapter, p)), e.selectionAdapter = f.Decorate(e.selectionAdapter, h)), e.language = this._resolveLanguage(e.language), e.language.push("en");for (var n = [], s = 0; s < e.language.length; s++) {
        var i = e.language[s];-1 === n.indexOf(i) && n.push(i);
      }return e.language = n, e.translations = this._processTranslations(e.language, e.debug), e;
    }, n.prototype.reset = function () {
      function a(e) {
        return e.replace(/[^\u0000-\u007E]/g, function (e) {
          return t[e] || e;
        });
      }this.defaults = { amdLanguageBase: "./i18n/", autocomplete: "off", closeOnSelect: !0, debug: !1, dropdownAutoWidth: !1, escapeMarkup: f.escapeMarkup, language: {}, matcher: function e(t, n) {
          if (null == t.term || "" === t.term.trim()) return n;if (n.children && 0 < n.children.length) {
            for (var s = l.extend(!0, {}, n), i = n.children.length - 1; 0 <= i; i--) {
              null == e(t, n.children[i]) && s.children.splice(i, 1);
            }return 0 < s.children.length ? s : e(t, s);
          }var r = a(n.text).toUpperCase(),
              o = a(t.term).toUpperCase();return -1 < r.indexOf(o) ? n : null;
        }, minimumInputLength: 0, maximumInputLength: 0, maximumSelectionLength: 0, minimumResultsForSearch: 0, selectOnClose: !1, scrollAfterSelect: !1, sorter: function sorter(e) {
          return e;
        }, templateResult: function templateResult(e) {
          return e.text;
        }, templateSelection: function templateSelection(e) {
          return e.text;
        }, theme: "default", width: "resolve" };
    }, n.prototype.applyFromElement = function (e, t) {
      var n = e.language,
          s = this.defaults.language,
          i = t.prop("lang"),
          t = t.closest("[lang]").prop("lang"),
          t = Array.prototype.concat.call(this._resolveLanguage(i), this._resolveLanguage(n), this._resolveLanguage(s), this._resolveLanguage(t));return e.language = t, e;
    }, n.prototype._resolveLanguage = function (e) {
      if (!e) return [];if (l.isEmptyObject(e)) return [];if (l.isPlainObject(e)) return [e];for (var t, n = Array.isArray(e) ? e : [e], s = [], i = 0; i < n.length; i++) {
        s.push(n[i]), "string" == typeof n[i] && 0 < n[i].indexOf("-") && (t = n[i].split("-")[0], s.push(t));
      }return s;
    }, n.prototype._processTranslations = function (e, t) {
      for (var n = new g(), s = 0; s < e.length; s++) {
        var i = new g(),
            r = e[s];if ("string" == typeof r) try {
          i = g.loadPath(r);
        } catch (e) {
          try {
            r = this.defaults.amdLanguageBase + r, i = g.loadPath(r);
          } catch (e) {
            t && window.console && console.warn && console.warn('Select2: The language file for "' + r + '" could not be automatically loaded. A fallback will be used instead.');
          }
        } else i = l.isPlainObject(r) ? new g(r) : r;n.extend(i);
      }return n;
    }, n.prototype.set = function (e, t) {
      var n = {};n[l.camelCase(e)] = t;n = f._convertData(n);l.extend(!0, this.defaults, n);
    }, new n();
  }), u.define("select2/options", ["jquery", "./defaults", "./utils"], function (c, n, u) {
    function e(e, t) {
      this.options = e, null != t && this.fromElement(t), null != t && (this.options = n.applyFromElement(this.options, t)), this.options = n.apply(this.options);
    }return e.prototype.fromElement = function (e) {
      var t = ["select2"];null == this.options.multiple && (this.options.multiple = e.prop("multiple")), null == this.options.disabled && (this.options.disabled = e.prop("disabled")), null == this.options.autocomplete && e.prop("autocomplete") && (this.options.autocomplete = e.prop("autocomplete")), null == this.options.dir && (e.prop("dir") ? this.options.dir = e.prop("dir") : e.closest("[dir]").prop("dir") ? this.options.dir = e.closest("[dir]").prop("dir") : this.options.dir = "ltr"), e.prop("disabled", this.options.disabled), e.prop("multiple", this.options.multiple), u.GetData(e[0], "select2Tags") && (this.options.debug && window.console && console.warn && console.warn('Select2: The `data-select2-tags` attribute has been changed to use the `data-data` and `data-tags="true"` attributes and will be removed in future versions of Select2.'), u.StoreData(e[0], "data", u.GetData(e[0], "select2Tags")), u.StoreData(e[0], "tags", !0)), u.GetData(e[0], "ajaxUrl") && (this.options.debug && window.console && console.warn && console.warn("Select2: The `data-ajax-url` attribute has been changed to `data-ajax--url` and support for the old attribute will be removed in future versions of Select2."), e.attr("ajax--url", u.GetData(e[0], "ajaxUrl")), u.StoreData(e[0], "ajax-Url", u.GetData(e[0], "ajaxUrl")));var n = {};function s(e, t) {
        return t.toUpperCase();
      }for (var i = 0; i < e[0].attributes.length; i++) {
        var r = e[0].attributes[i].name,
            o = "data-";r.substr(0, o.length) == o && (r = r.substring(o.length), o = u.GetData(e[0], r), n[r.replace(/-([a-z])/g, s)] = o);
      }c.fn.jquery && "1." == c.fn.jquery.substr(0, 2) && e[0].dataset && (n = c.extend(!0, {}, e[0].dataset, n));var a,
          l = c.extend(!0, {}, u.GetData(e[0]), n);for (a in l = u._convertData(l)) {
        -1 < t.indexOf(a) || (c.isPlainObject(this.options[a]) ? c.extend(this.options[a], l[a]) : this.options[a] = l[a]);
      }return this;
    }, e.prototype.get = function (e) {
      return this.options[e];
    }, e.prototype.set = function (e, t) {
      this.options[e] = t;
    }, e;
  }), u.define("select2/core", ["jquery", "./options", "./utils", "./keys"], function (t, i, r, s) {
    var o = function o(e, t) {
      null != r.GetData(e[0], "select2") && r.GetData(e[0], "select2").destroy(), this.$element = e, this.id = this._generateId(e), t = t || {}, this.options = new i(t, e), o.__super__.constructor.call(this);var n = e.attr("tabindex") || 0;r.StoreData(e[0], "old-tabindex", n), e.attr("tabindex", "-1");t = this.options.get("dataAdapter");this.dataAdapter = new t(e, this.options);n = this.render();this._placeContainer(n);t = this.options.get("selectionAdapter");this.selection = new t(e, this.options), this.$selection = this.selection.render(), this.selection.position(this.$selection, n);t = this.options.get("dropdownAdapter");this.dropdown = new t(e, this.options), this.$dropdown = this.dropdown.render(), this.dropdown.position(this.$dropdown, n);n = this.options.get("resultsAdapter");this.results = new n(e, this.options, this.dataAdapter), this.$results = this.results.render(), this.results.position(this.$results, this.$dropdown);var s = this;this._bindAdapters(), this._registerDomEvents(), this._registerDataEvents(), this._registerSelectionEvents(), this._registerDropdownEvents(), this._registerResultsEvents(), this._registerEvents(), this.dataAdapter.current(function (e) {
        s.trigger("selection:update", { data: e });
      }), e[0].classList.add("select2-hidden-accessible"), e.attr("aria-hidden", "true"), this._syncAttributes(), r.StoreData(e[0], "select2", this), e.data("select2", this);
    };return r.Extend(o, r.Observable), o.prototype._generateId = function (e) {
      return "select2-" + (null != e.attr("id") ? e.attr("id") : null != e.attr("name") ? e.attr("name") + "-" + r.generateChars(2) : r.generateChars(4)).replace(/(:|\.|\[|\]|,)/g, "");
    }, o.prototype._placeContainer = function (e) {
      e.insertAfter(this.$element);var t = this._resolveWidth(this.$element, this.options.get("width"));null != t && e.css("width", t);
    }, o.prototype._resolveWidth = function (e, t) {
      var n = /^width:(([-+]?([0-9]*\.)?[0-9]+)(px|em|ex|%|in|cm|mm|pt|pc))/i;if ("resolve" == t) {
        var s = this._resolveWidth(e, "style");return null != s ? s : this._resolveWidth(e, "element");
      }if ("element" == t) {
        s = e.outerWidth(!1);return s <= 0 ? "auto" : s + "px";
      }if ("style" != t) return "computedstyle" != t ? t : window.getComputedStyle(e[0]).width;e = e.attr("style");if ("string" != typeof e) return null;for (var i = e.split(";"), r = 0, o = i.length; r < o; r += 1) {
        var a = i[r].replace(/\s/g, "").match(n);if (null !== a && 1 <= a.length) return a[1];
      }return null;
    }, o.prototype._bindAdapters = function () {
      this.dataAdapter.bind(this, this.$container), this.selection.bind(this, this.$container), this.dropdown.bind(this, this.$container), this.results.bind(this, this.$container);
    }, o.prototype._registerDomEvents = function () {
      var t = this;this.$element.on("change.select2", function () {
        t.dataAdapter.current(function (e) {
          t.trigger("selection:update", { data: e });
        });
      }), this.$element.on("focus.select2", function (e) {
        t.trigger("focus", e);
      }), this._syncA = r.bind(this._syncAttributes, this), this._syncS = r.bind(this._syncSubtree, this), this._observer = new window.MutationObserver(function (e) {
        t._syncA(), t._syncS(e);
      }), this._observer.observe(this.$element[0], { attributes: !0, childList: !0, subtree: !1 });
    }, o.prototype._registerDataEvents = function () {
      var n = this;this.dataAdapter.on("*", function (e, t) {
        n.trigger(e, t);
      });
    }, o.prototype._registerSelectionEvents = function () {
      var n = this,
          s = ["toggle", "focus"];this.selection.on("toggle", function () {
        n.toggleDropdown();
      }), this.selection.on("focus", function (e) {
        n.focus(e);
      }), this.selection.on("*", function (e, t) {
        -1 === s.indexOf(e) && n.trigger(e, t);
      });
    }, o.prototype._registerDropdownEvents = function () {
      var n = this;this.dropdown.on("*", function (e, t) {
        n.trigger(e, t);
      });
    }, o.prototype._registerResultsEvents = function () {
      var n = this;this.results.on("*", function (e, t) {
        n.trigger(e, t);
      });
    }, o.prototype._registerEvents = function () {
      var n = this;this.on("open", function () {
        n.$container[0].classList.add("select2-container--open");
      }), this.on("close", function () {
        n.$container[0].classList.remove("select2-container--open");
      }), this.on("enable", function () {
        n.$container[0].classList.remove("select2-container--disabled");
      }), this.on("disable", function () {
        n.$container[0].classList.add("select2-container--disabled");
      }), this.on("blur", function () {
        n.$container[0].classList.remove("select2-container--focus");
      }), this.on("query", function (t) {
        n.isOpen() || n.trigger("open", {}), this.dataAdapter.query(t, function (e) {
          n.trigger("results:all", { data: e, query: t });
        });
      }), this.on("query:append", function (t) {
        this.dataAdapter.query(t, function (e) {
          n.trigger("results:append", { data: e, query: t });
        });
      }), this.on("keypress", function (e) {
        var t = e.which;n.isOpen() ? t === s.ESC || t === s.UP && e.altKey ? (n.close(e), e.preventDefault()) : t === s.ENTER || t === s.TAB ? (n.trigger("results:select", {}), e.preventDefault()) : t === s.SPACE && e.ctrlKey ? (n.trigger("results:toggle", {}), e.preventDefault()) : t === s.UP ? (n.trigger("results:previous", {}), e.preventDefault()) : t === s.DOWN && (n.trigger("results:next", {}), e.preventDefault()) : (t === s.ENTER || t === s.SPACE || t === s.DOWN && e.altKey) && (n.open(), e.preventDefault());
      });
    }, o.prototype._syncAttributes = function () {
      this.options.set("disabled", this.$element.prop("disabled")), this.isDisabled() ? (this.isOpen() && this.close(), this.trigger("disable", {})) : this.trigger("enable", {});
    }, o.prototype._isChangeMutation = function (e) {
      var t = this;if (e.addedNodes && 0 < e.addedNodes.length) {
        for (var n = 0; n < e.addedNodes.length; n++) {
          if (e.addedNodes[n].selected) return !0;
        }
      } else {
        if (e.removedNodes && 0 < e.removedNodes.length) return !0;if (Array.isArray(e)) return e.some(function (e) {
          return t._isChangeMutation(e);
        });
      }return !1;
    }, o.prototype._syncSubtree = function (e) {
      var e = this._isChangeMutation(e),
          t = this;e && this.dataAdapter.current(function (e) {
        t.trigger("selection:update", { data: e });
      });
    }, o.prototype.trigger = function (e, t) {
      var n = o.__super__.trigger,
          s = { open: "opening", close: "closing", select: "selecting", unselect: "unselecting", clear: "clearing" };if (void 0 === t && (t = {}), e in s) {
        var i = s[e],
            s = { prevented: !1, name: e, args: t };if (n.call(this, i, s), s.prevented) return void (t.prevented = !0);
      }n.call(this, e, t);
    }, o.prototype.toggleDropdown = function () {
      this.isDisabled() || (this.isOpen() ? this.close() : this.open());
    }, o.prototype.open = function () {
      this.isOpen() || this.isDisabled() || this.trigger("query", {});
    }, o.prototype.close = function (e) {
      this.isOpen() && this.trigger("close", { originalEvent: e });
    }, o.prototype.isEnabled = function () {
      return !this.isDisabled();
    }, o.prototype.isDisabled = function () {
      return this.options.get("disabled");
    }, o.prototype.isOpen = function () {
      return this.$container[0].classList.contains("select2-container--open");
    }, o.prototype.hasFocus = function () {
      return this.$container[0].classList.contains("select2-container--focus");
    }, o.prototype.focus = function (e) {
      this.hasFocus() || (this.$container[0].classList.add("select2-container--focus"), this.trigger("focus", {}));
    }, o.prototype.enable = function (e) {
      this.options.get("debug") && window.console && console.warn && console.warn('Select2: The `select2("enable")` method has been deprecated and will be removed in later Select2 versions. Use $element.prop("disabled") instead.');e = !(e = null == e || 0 === e.length ? [!0] : e)[0];this.$element.prop("disabled", e);
    }, o.prototype.data = function () {
      this.options.get("debug") && 0 < arguments.length && window.console && console.warn && console.warn('Select2: Data can no longer be set using `select2("data")`. You should consider setting the value instead using `$element.val()`.');var t = [];return this.dataAdapter.current(function (e) {
        t = e;
      }), t;
    }, o.prototype.val = function (e) {
      if (this.options.get("debug") && window.console && console.warn && console.warn('Select2: The `select2("val")` method has been deprecated and will be removed in later Select2 versions. Use $element.val() instead.'), null == e || 0 === e.length) return this.$element.val();e = e[0];Array.isArray(e) && (e = e.map(function (e) {
        return e.toString();
      })), this.$element.val(e).trigger("input").trigger("change");
    }, o.prototype.destroy = function () {
      r.RemoveData(this.$container[0]), this.$container.remove(), this._observer.disconnect(), this._observer = null, this._syncA = null, this._syncS = null, this.$element.off(".select2"), this.$element.attr("tabindex", r.GetData(this.$element[0], "old-tabindex")), this.$element[0].classList.remove("select2-hidden-accessible"), this.$element.attr("aria-hidden", "false"), r.RemoveData(this.$element[0]), this.$element.removeData("select2"), this.dataAdapter.destroy(), this.selection.destroy(), this.dropdown.destroy(), this.results.destroy(), this.dataAdapter = null, this.selection = null, this.dropdown = null, this.results = null;
    }, o.prototype.render = function () {
      var e = t('<span class="select2 select2-container"><span class="selection"></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>');return e.attr("dir", this.options.get("dir")), this.$container = e, this.$container[0].classList.add("select2-container--" + this.options.get("theme")), r.StoreData(e[0], "element", this.$element), e;
    }, o;
  }), u.define("jquery-mousewheel", ["jquery"], function (e) {
    return e;
  }), u.define("jquery.select2", ["jquery", "jquery-mousewheel", "./select2/core", "./select2/defaults", "./select2/utils"], function (i, e, r, t, o) {
    var a;return null == i.fn.select2 && (a = ["open", "close", "destroy"], i.fn.select2 = function (t) {
      if ("object" == _typeof(t = t || {})) return this.each(function () {
        var e = i.extend(!0, {}, t);new r(i(this), e);
      }), this;if ("string" != typeof t) throw new Error("Invalid arguments for Select2: " + t);var n,
          s = Array.prototype.slice.call(arguments, 1);return this.each(function () {
        var e = o.GetData(this, "select2");null == e && window.console && console.error && console.error("The select2('" + t + "') method was called on an element that is not using Select2."), n = e[t].apply(e, s);
      }), -1 < a.indexOf(t) ? this : n;
    }), null == i.fn.select2.defaults && (i.fn.select2.defaults = t), r;
  }), { define: u.define, require: u.require });function b(e, t) {
    return i.call(e, t);
  }function l(e, t) {
    var n,
        s,
        i,
        r,
        o,
        a,
        l,
        c,
        u,
        d,
        p = t && t.split("/"),
        h = y.map,
        f = h && h["*"] || {};if (e) {
      for (t = (e = e.split("/")).length - 1, y.nodeIdCompat && _.test(e[t]) && (e[t] = e[t].replace(_, "")), "." === e[0].charAt(0) && p && (e = p.slice(0, p.length - 1).concat(e)), c = 0; c < e.length; c++) {
        "." === (d = e[c]) ? (e.splice(c, 1), --c) : ".." === d && (0 === c || 1 === c && ".." === e[2] || ".." === e[c - 1] || 0 < c && (e.splice(c - 1, 2), c -= 2));
      }e = e.join("/");
    }if ((p || f) && h) {
      for (c = (n = e.split("/")).length; 0 < c; --c) {
        if (s = n.slice(0, c).join("/"), p) for (u = p.length; 0 < u; --u) {
          if (i = h[p.slice(0, u).join("/")], i = i && i[s]) {
            r = i, o = c;break;
          }
        }if (r) break;!a && f && f[s] && (a = f[s], l = c);
      }!r && a && (r = a, o = l), r && (n.splice(0, o, r), e = n.join("/"));
    }return e;
  }function w(t, n) {
    return function () {
      var e = a.call(arguments, 0);return "string" != typeof e[0] && 1 === e.length && e.push(null), _o.apply(p, e.concat([t, n]));
    };
  }function x(e) {
    var t;if (b(m, e) && (t = m[e], delete m[e], v[e] = !0, r.apply(p, t)), !b(g, e) && !b(v, e)) throw new Error("No " + e);return g[e];
  }function c(e) {
    var t,
        n = e ? e.indexOf("!") : -1;return -1 < n && (t = e.substring(0, n), e = e.substring(n + 1, e.length)), [t, e];
  }function A(e) {
    return e ? c(e) : [];
  }var u = s.require("jquery.select2");return t.fn.select2.amd = s, u;
});
"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

!function (i) {
  "use strict";
  "function" == typeof define && define.amd ? define(["jquery"], i) : "undefined" != typeof exports ? module.exports = i(require("jquery")) : i(jQuery);
}(function (i) {
  "use strict";
  var e = window.Slick || {};(e = function () {
    var e = 0;return function (t, o) {
      var s,
          n = this;n.defaults = { accessibility: !0, adaptiveHeight: !1, appendArrows: i(t), appendDots: i(t), arrows: !0, asNavFor: null, prevArrow: '<button class="slick-prev" aria-label="Previous" type="button">Previous</button>', nextArrow: '<button class="slick-next" aria-label="Next" type="button">Next</button>', autoplay: !1, autoplaySpeed: 3e3, centerMode: !1, centerPadding: "50px", cssEase: "ease", customPaging: function customPaging(e, t) {
          return i('<button type="button" />').text(t + 1);
        }, dots: !1, dotsClass: "slick-dots", draggable: !0, easing: "linear", edgeFriction: .35, fade: !1, focusOnSelect: !1, focusOnChange: !1, infinite: !0, initialSlide: 0, lazyLoad: "ondemand", mobileFirst: !1, pauseOnHover: !0, pauseOnFocus: !0, pauseOnDotsHover: !1, respondTo: "window", responsive: null, rows: 1, rtl: !1, slide: "", slidesPerRow: 1, slidesToShow: 1, slidesToScroll: 1, speed: 500, swipe: !0, swipeToSlide: !1, touchMove: !0, touchThreshold: 5, useCSS: !0, useTransform: !0, variableWidth: !1, vertical: !1, verticalSwiping: !1, waitForAnimate: !0, zIndex: 1e3 }, n.initials = { animating: !1, dragging: !1, autoPlayTimer: null, currentDirection: 0, currentLeft: null, currentSlide: 0, direction: 1, $dots: null, listWidth: null, listHeight: null, loadIndex: 0, $nextArrow: null, $prevArrow: null, scrolling: !1, slideCount: null, slideWidth: null, $slideTrack: null, $slides: null, sliding: !1, slideOffset: 0, swipeLeft: null, swiping: !1, $list: null, touchObject: {}, transformsEnabled: !1, unslicked: !1 }, i.extend(n, n.initials), n.activeBreakpoint = null, n.animType = null, n.animProp = null, n.breakpoints = [], n.breakpointSettings = [], n.cssTransitions = !1, n.focussed = !1, n.interrupted = !1, n.hidden = "hidden", n.paused = !0, n.positionProp = null, n.respondTo = null, n.rowCount = 1, n.shouldClick = !0, n.$slider = i(t), n.$slidesCache = null, n.transformType = null, n.transitionType = null, n.visibilityChange = "visibilitychange", n.windowWidth = 0, n.windowTimer = null, s = i(t).data("slick") || {}, n.options = i.extend({}, n.defaults, o, s), n.currentSlide = n.options.initialSlide, n.originalSettings = n.options, void 0 !== document.mozHidden ? (n.hidden = "mozHidden", n.visibilityChange = "mozvisibilitychange") : void 0 !== document.webkitHidden && (n.hidden = "webkitHidden", n.visibilityChange = "webkitvisibilitychange"), n.autoPlay = i.proxy(n.autoPlay, n), n.autoPlayClear = i.proxy(n.autoPlayClear, n), n.autoPlayIterator = i.proxy(n.autoPlayIterator, n), n.changeSlide = i.proxy(n.changeSlide, n), n.clickHandler = i.proxy(n.clickHandler, n), n.selectHandler = i.proxy(n.selectHandler, n), n.setPosition = i.proxy(n.setPosition, n), n.swipeHandler = i.proxy(n.swipeHandler, n), n.dragHandler = i.proxy(n.dragHandler, n), n.keyHandler = i.proxy(n.keyHandler, n), n.instanceUid = e++, n.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/, n.registerBreakpoints(), n.init(!0);
    };
  }()).prototype.activateADA = function () {
    this.$slideTrack.find(".slick-active").attr({ "aria-hidden": "false" }).find("a, input, button, select").attr({ tabindex: "0" });
  }, e.prototype.addSlide = e.prototype.slickAdd = function (e, t, o) {
    var s = this;if ("boolean" == typeof t) o = t, t = null;else if (t < 0 || t >= s.slideCount) return !1;s.unload(), "number" == typeof t ? 0 === t && 0 === s.$slides.length ? i(e).appendTo(s.$slideTrack) : o ? i(e).insertBefore(s.$slides.eq(t)) : i(e).insertAfter(s.$slides.eq(t)) : !0 === o ? i(e).prependTo(s.$slideTrack) : i(e).appendTo(s.$slideTrack), s.$slides = s.$slideTrack.children(this.options.slide), s.$slideTrack.children(this.options.slide).detach(), s.$slideTrack.append(s.$slides), s.$slides.each(function (e, t) {
      i(t).attr("data-slick-index", e);
    }), s.$slidesCache = s.$slides, s.reinit();
  }, e.prototype.animateHeight = function () {
    var i = this;if (1 === i.options.slidesToShow && !0 === i.options.adaptiveHeight && !1 === i.options.vertical) {
      var e = i.$slides.eq(i.currentSlide).outerHeight(!0);i.$list.animate({ height: e }, i.options.speed);
    }
  }, e.prototype.animateSlide = function (e, t) {
    var o = {},
        s = this;s.animateHeight(), !0 === s.options.rtl && !1 === s.options.vertical && (e = -e), !1 === s.transformsEnabled ? !1 === s.options.vertical ? s.$slideTrack.animate({ left: e }, s.options.speed, s.options.easing, t) : s.$slideTrack.animate({ top: e }, s.options.speed, s.options.easing, t) : !1 === s.cssTransitions ? (!0 === s.options.rtl && (s.currentLeft = -s.currentLeft), i({ animStart: s.currentLeft }).animate({ animStart: e }, { duration: s.options.speed, easing: s.options.easing, step: function step(i) {
        i = Math.ceil(i), !1 === s.options.vertical ? (o[s.animType] = "translate(" + i + "px, 0px)", s.$slideTrack.css(o)) : (o[s.animType] = "translate(0px," + i + "px)", s.$slideTrack.css(o));
      }, complete: function complete() {
        t && t.call();
      } })) : (s.applyTransition(), e = Math.ceil(e), !1 === s.options.vertical ? o[s.animType] = "translate3d(" + e + "px, 0px, 0px)" : o[s.animType] = "translate3d(0px," + e + "px, 0px)", s.$slideTrack.css(o), t && setTimeout(function () {
      s.disableTransition(), t.call();
    }, s.options.speed));
  }, e.prototype.getNavTarget = function () {
    var e = this,
        t = e.options.asNavFor;return t && null !== t && (t = i(t).not(e.$slider)), t;
  }, e.prototype.asNavFor = function (e) {
    var t = this.getNavTarget();null !== t && "object" == (typeof t === "undefined" ? "undefined" : _typeof(t)) && t.each(function () {
      var t = i(this).slick("getSlick");t.unslicked || t.slideHandler(e, !0);
    });
  }, e.prototype.applyTransition = function (i) {
    var e = this,
        t = {};!1 === e.options.fade ? t[e.transitionType] = e.transformType + " " + e.options.speed + "ms " + e.options.cssEase : t[e.transitionType] = "opacity " + e.options.speed + "ms " + e.options.cssEase, !1 === e.options.fade ? e.$slideTrack.css(t) : e.$slides.eq(i).css(t);
  }, e.prototype.autoPlay = function () {
    var i = this;i.autoPlayClear(), i.slideCount > i.options.slidesToShow && (i.autoPlayTimer = setInterval(i.autoPlayIterator, i.options.autoplaySpeed));
  }, e.prototype.autoPlayClear = function () {
    var i = this;i.autoPlayTimer && clearInterval(i.autoPlayTimer);
  }, e.prototype.autoPlayIterator = function () {
    var i = this,
        e = i.currentSlide + i.options.slidesToScroll;i.paused || i.interrupted || i.focussed || (!1 === i.options.infinite && (1 === i.direction && i.currentSlide + 1 === i.slideCount - 1 ? i.direction = 0 : 0 === i.direction && (e = i.currentSlide - i.options.slidesToScroll, i.currentSlide - 1 == 0 && (i.direction = 1))), i.slideHandler(e));
  }, e.prototype.buildArrows = function () {
    var e = this;!0 === e.options.arrows && (e.$prevArrow = i(e.options.prevArrow).addClass("slick-arrow"), e.$nextArrow = i(e.options.nextArrow).addClass("slick-arrow"), e.slideCount > e.options.slidesToShow ? (e.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), e.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.prependTo(e.options.appendArrows), e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.appendTo(e.options.appendArrows), !0 !== e.options.infinite && e.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true")) : e.$prevArrow.add(e.$nextArrow).addClass("slick-hidden").attr({ "aria-disabled": "true", tabindex: "-1" }));
  }, e.prototype.buildDots = function () {
    var e,
        t,
        o = this;if (!0 === o.options.dots) {
      for (o.$slider.addClass("slick-dotted"), t = i("<ul />").addClass(o.options.dotsClass), e = 0; e <= o.getDotCount(); e += 1) {
        t.append(i("<li />").append(o.options.customPaging.call(this, o, e)));
      }o.$dots = t.appendTo(o.options.appendDots), o.$dots.find("li").first().addClass("slick-active");
    }
  }, e.prototype.buildOut = function () {
    var e = this;e.$slides = e.$slider.children(e.options.slide + ":not(.slick-cloned)").addClass("slick-slide"), e.slideCount = e.$slides.length, e.$slides.each(function (e, t) {
      i(t).attr("data-slick-index", e).data("originalStyling", i(t).attr("style") || "");
    }), e.$slider.addClass("slick-slider"), e.$slideTrack = 0 === e.slideCount ? i('<div class="slick-track"/>').appendTo(e.$slider) : e.$slides.wrapAll('<div class="slick-track"/>').parent(), e.$list = e.$slideTrack.wrap('<div class="slick-list"/>').parent(), e.$slideTrack.css("opacity", 0), !0 !== e.options.centerMode && !0 !== e.options.swipeToSlide || (e.options.slidesToScroll = 1), i("img[data-lazy]", e.$slider).not("[src]").addClass("slick-loading"), e.setupInfinite(), e.buildArrows(), e.buildDots(), e.updateDots(), e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0), !0 === e.options.draggable && e.$list.addClass("draggable");
  }, e.prototype.buildRows = function () {
    var i,
        e,
        t,
        o,
        s,
        n,
        r,
        l = this;if (o = document.createDocumentFragment(), n = l.$slider.children(), l.options.rows > 1) {
      for (r = l.options.slidesPerRow * l.options.rows, s = Math.ceil(n.length / r), i = 0; i < s; i++) {
        var d = document.createElement("div");for (e = 0; e < l.options.rows; e++) {
          var a = document.createElement("div");for (t = 0; t < l.options.slidesPerRow; t++) {
            var c = i * r + (e * l.options.slidesPerRow + t);n.get(c) && a.appendChild(n.get(c));
          }d.appendChild(a);
        }o.appendChild(d);
      }l.$slider.empty().append(o), l.$slider.children().children().children().css({ width: 100 / l.options.slidesPerRow + "%", display: "inline-block" });
    }
  }, e.prototype.checkResponsive = function (e, t) {
    var o,
        s,
        n,
        r = this,
        l = !1,
        d = r.$slider.width(),
        a = window.innerWidth || i(window).width();if ("window" === r.respondTo ? n = a : "slider" === r.respondTo ? n = d : "min" === r.respondTo && (n = Math.min(a, d)), r.options.responsive && r.options.responsive.length && null !== r.options.responsive) {
      s = null;for (o in r.breakpoints) {
        r.breakpoints.hasOwnProperty(o) && (!1 === r.originalSettings.mobileFirst ? n < r.breakpoints[o] && (s = r.breakpoints[o]) : n > r.breakpoints[o] && (s = r.breakpoints[o]));
      }null !== s ? null !== r.activeBreakpoint ? (s !== r.activeBreakpoint || t) && (r.activeBreakpoint = s, "unslick" === r.breakpointSettings[s] ? r.unslick(s) : (r.options = i.extend({}, r.originalSettings, r.breakpointSettings[s]), !0 === e && (r.currentSlide = r.options.initialSlide), r.refresh(e)), l = s) : (r.activeBreakpoint = s, "unslick" === r.breakpointSettings[s] ? r.unslick(s) : (r.options = i.extend({}, r.originalSettings, r.breakpointSettings[s]), !0 === e && (r.currentSlide = r.options.initialSlide), r.refresh(e)), l = s) : null !== r.activeBreakpoint && (r.activeBreakpoint = null, r.options = r.originalSettings, !0 === e && (r.currentSlide = r.options.initialSlide), r.refresh(e), l = s), e || !1 === l || r.$slider.trigger("breakpoint", [r, l]);
    }
  }, e.prototype.changeSlide = function (e, t) {
    var o,
        s,
        n,
        r = this,
        l = i(e.currentTarget);switch (l.is("a") && e.preventDefault(), l.is("li") || (l = l.closest("li")), n = r.slideCount % r.options.slidesToScroll != 0, o = n ? 0 : (r.slideCount - r.currentSlide) % r.options.slidesToScroll, e.data.message) {case "previous":
        s = 0 === o ? r.options.slidesToScroll : r.options.slidesToShow - o, r.slideCount > r.options.slidesToShow && r.slideHandler(r.currentSlide - s, !1, t);break;case "next":
        s = 0 === o ? r.options.slidesToScroll : o, r.slideCount > r.options.slidesToShow && r.slideHandler(r.currentSlide + s, !1, t);break;case "index":
        var d = 0 === e.data.index ? 0 : e.data.index || l.index() * r.options.slidesToScroll;r.slideHandler(r.checkNavigable(d), !1, t), l.children().trigger("focus");break;default:
        return;}
  }, e.prototype.checkNavigable = function (i) {
    var e, t;if (e = this.getNavigableIndexes(), t = 0, i > e[e.length - 1]) i = e[e.length - 1];else for (var o in e) {
      if (i < e[o]) {
        i = t;break;
      }t = e[o];
    }return i;
  }, e.prototype.cleanUpEvents = function () {
    var e = this;e.options.dots && null !== e.$dots && (i("li", e.$dots).off("click.slick", e.changeSlide).off("mouseenter.slick", i.proxy(e.interrupt, e, !0)).off("mouseleave.slick", i.proxy(e.interrupt, e, !1)), !0 === e.options.accessibility && e.$dots.off("keydown.slick", e.keyHandler)), e.$slider.off("focus.slick blur.slick"), !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow && e.$prevArrow.off("click.slick", e.changeSlide), e.$nextArrow && e.$nextArrow.off("click.slick", e.changeSlide), !0 === e.options.accessibility && (e.$prevArrow && e.$prevArrow.off("keydown.slick", e.keyHandler), e.$nextArrow && e.$nextArrow.off("keydown.slick", e.keyHandler))), e.$list.off("touchstart.slick mousedown.slick", e.swipeHandler), e.$list.off("touchmove.slick mousemove.slick", e.swipeHandler), e.$list.off("touchend.slick mouseup.slick", e.swipeHandler), e.$list.off("touchcancel.slick mouseleave.slick", e.swipeHandler), e.$list.off("click.slick", e.clickHandler), i(document).off(e.visibilityChange, e.visibility), e.cleanUpSlideEvents(), !0 === e.options.accessibility && e.$list.off("keydown.slick", e.keyHandler), !0 === e.options.focusOnSelect && i(e.$slideTrack).children().off("click.slick", e.selectHandler), i(window).off("orientationchange.slick.slick-" + e.instanceUid, e.orientationChange), i(window).off("resize.slick.slick-" + e.instanceUid, e.resize), i("[draggable!=true]", e.$slideTrack).off("dragstart", e.preventDefault), i(window).off("load.slick.slick-" + e.instanceUid, e.setPosition);
  }, e.prototype.cleanUpSlideEvents = function () {
    var e = this;e.$list.off("mouseenter.slick", i.proxy(e.interrupt, e, !0)), e.$list.off("mouseleave.slick", i.proxy(e.interrupt, e, !1));
  }, e.prototype.cleanUpRows = function () {
    var i,
        e = this;e.options.rows > 1 && ((i = e.$slides.children().children()).removeAttr("style"), e.$slider.empty().append(i));
  }, e.prototype.clickHandler = function (i) {
    !1 === this.shouldClick && (i.stopImmediatePropagation(), i.stopPropagation(), i.preventDefault());
  }, e.prototype.destroy = function (e) {
    var t = this;t.autoPlayClear(), t.touchObject = {}, t.cleanUpEvents(), i(".slick-cloned", t.$slider).detach(), t.$dots && t.$dots.remove(), t.$prevArrow && t.$prevArrow.length && (t.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), t.htmlExpr.test(t.options.prevArrow) && t.$prevArrow.remove()), t.$nextArrow && t.$nextArrow.length && (t.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), t.htmlExpr.test(t.options.nextArrow) && t.$nextArrow.remove()), t.$slides && (t.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function () {
      i(this).attr("style", i(this).data("originalStyling"));
    }), t.$slideTrack.children(this.options.slide).detach(), t.$slideTrack.detach(), t.$list.detach(), t.$slider.append(t.$slides)), t.cleanUpRows(), t.$slider.removeClass("slick-slider"), t.$slider.removeClass("slick-initialized"), t.$slider.removeClass("slick-dotted"), t.unslicked = !0, e || t.$slider.trigger("destroy", [t]);
  }, e.prototype.disableTransition = function (i) {
    var e = this,
        t = {};t[e.transitionType] = "", !1 === e.options.fade ? e.$slideTrack.css(t) : e.$slides.eq(i).css(t);
  }, e.prototype.fadeSlide = function (i, e) {
    var t = this;!1 === t.cssTransitions ? (t.$slides.eq(i).css({ zIndex: t.options.zIndex }), t.$slides.eq(i).animate({ opacity: 1 }, t.options.speed, t.options.easing, e)) : (t.applyTransition(i), t.$slides.eq(i).css({ opacity: 1, zIndex: t.options.zIndex }), e && setTimeout(function () {
      t.disableTransition(i), e.call();
    }, t.options.speed));
  }, e.prototype.fadeSlideOut = function (i) {
    var e = this;!1 === e.cssTransitions ? e.$slides.eq(i).animate({ opacity: 0, zIndex: e.options.zIndex - 2 }, e.options.speed, e.options.easing) : (e.applyTransition(i), e.$slides.eq(i).css({ opacity: 0, zIndex: e.options.zIndex - 2 }));
  }, e.prototype.filterSlides = e.prototype.slickFilter = function (i) {
    var e = this;null !== i && (e.$slidesCache = e.$slides, e.unload(), e.$slideTrack.children(this.options.slide).detach(), e.$slidesCache.filter(i).appendTo(e.$slideTrack), e.reinit());
  }, e.prototype.focusHandler = function () {
    var e = this;e.$slider.off("focus.slick blur.slick").on("focus.slick blur.slick", "*", function (t) {
      t.stopImmediatePropagation();var o = i(this);setTimeout(function () {
        e.options.pauseOnFocus && (e.focussed = o.is(":focus"), e.autoPlay());
      }, 0);
    });
  }, e.prototype.getCurrent = e.prototype.slickCurrentSlide = function () {
    return this.currentSlide;
  }, e.prototype.getDotCount = function () {
    var i = this,
        e = 0,
        t = 0,
        o = 0;if (!0 === i.options.infinite) {
      if (i.slideCount <= i.options.slidesToShow) ++o;else for (; e < i.slideCount;) {
        ++o, e = t + i.options.slidesToScroll, t += i.options.slidesToScroll <= i.options.slidesToShow ? i.options.slidesToScroll : i.options.slidesToShow;
      }
    } else if (!0 === i.options.centerMode) o = i.slideCount;else if (i.options.asNavFor) for (; e < i.slideCount;) {
      ++o, e = t + i.options.slidesToScroll, t += i.options.slidesToScroll <= i.options.slidesToShow ? i.options.slidesToScroll : i.options.slidesToShow;
    } else o = 1 + Math.ceil((i.slideCount - i.options.slidesToShow) / i.options.slidesToScroll);return o - 1;
  }, e.prototype.getLeft = function (i) {
    var e,
        t,
        o,
        s,
        n = this,
        r = 0;return n.slideOffset = 0, t = n.$slides.first().outerHeight(!0), !0 === n.options.infinite ? (n.slideCount > n.options.slidesToShow && (n.slideOffset = n.slideWidth * n.options.slidesToShow * -1, s = -1, !0 === n.options.vertical && !0 === n.options.centerMode && (2 === n.options.slidesToShow ? s = -1.5 : 1 === n.options.slidesToShow && (s = -2)), r = t * n.options.slidesToShow * s), n.slideCount % n.options.slidesToScroll != 0 && i + n.options.slidesToScroll > n.slideCount && n.slideCount > n.options.slidesToShow && (i > n.slideCount ? (n.slideOffset = (n.options.slidesToShow - (i - n.slideCount)) * n.slideWidth * -1, r = (n.options.slidesToShow - (i - n.slideCount)) * t * -1) : (n.slideOffset = n.slideCount % n.options.slidesToScroll * n.slideWidth * -1, r = n.slideCount % n.options.slidesToScroll * t * -1))) : i + n.options.slidesToShow > n.slideCount && (n.slideOffset = (i + n.options.slidesToShow - n.slideCount) * n.slideWidth, r = (i + n.options.slidesToShow - n.slideCount) * t), n.slideCount <= n.options.slidesToShow && (n.slideOffset = 0, r = 0), !0 === n.options.centerMode && n.slideCount <= n.options.slidesToShow ? n.slideOffset = n.slideWidth * Math.floor(n.options.slidesToShow) / 2 - n.slideWidth * n.slideCount / 2 : !0 === n.options.centerMode && !0 === n.options.infinite ? n.slideOffset += n.slideWidth * Math.floor(n.options.slidesToShow / 2) - n.slideWidth : !0 === n.options.centerMode && (n.slideOffset = 0, n.slideOffset += n.slideWidth * Math.floor(n.options.slidesToShow / 2)), e = !1 === n.options.vertical ? i * n.slideWidth * -1 + n.slideOffset : i * t * -1 + r, !0 === n.options.variableWidth && (o = n.slideCount <= n.options.slidesToShow || !1 === n.options.infinite ? n.$slideTrack.children(".slick-slide").eq(i) : n.$slideTrack.children(".slick-slide").eq(i + n.options.slidesToShow), e = !0 === n.options.rtl ? o[0] ? -1 * (n.$slideTrack.width() - o[0].offsetLeft - o.width()) : 0 : o[0] ? -1 * o[0].offsetLeft : 0, !0 === n.options.centerMode && (o = n.slideCount <= n.options.slidesToShow || !1 === n.options.infinite ? n.$slideTrack.children(".slick-slide").eq(i) : n.$slideTrack.children(".slick-slide").eq(i + n.options.slidesToShow + 1), e = !0 === n.options.rtl ? o[0] ? -1 * (n.$slideTrack.width() - o[0].offsetLeft - o.width()) : 0 : o[0] ? -1 * o[0].offsetLeft : 0, e += (n.$list.width() - o.outerWidth()) / 2)), e;
  }, e.prototype.getOption = e.prototype.slickGetOption = function (i) {
    return this.options[i];
  }, e.prototype.getNavigableIndexes = function () {
    var i,
        e = this,
        t = 0,
        o = 0,
        s = [];for (!1 === e.options.infinite ? i = e.slideCount : (t = -1 * e.options.slidesToScroll, o = -1 * e.options.slidesToScroll, i = 2 * e.slideCount); t < i;) {
      s.push(t), t = o + e.options.slidesToScroll, o += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow;
    }return s;
  }, e.prototype.getSlick = function () {
    return this;
  }, e.prototype.getSlideCount = function () {
    var e,
        t,
        o = this;return t = !0 === o.options.centerMode ? o.slideWidth * Math.floor(o.options.slidesToShow / 2) : 0, !0 === o.options.swipeToSlide ? (o.$slideTrack.find(".slick-slide").each(function (s, n) {
      if (n.offsetLeft - t + i(n).outerWidth() / 2 > -1 * o.swipeLeft) return e = n, !1;
    }), Math.abs(i(e).attr("data-slick-index") - o.currentSlide) || 1) : o.options.slidesToScroll;
  }, e.prototype.goTo = e.prototype.slickGoTo = function (i, e) {
    this.changeSlide({ data: { message: "index", index: parseInt(i) } }, e);
  }, e.prototype.init = function (e) {
    var t = this;i(t.$slider).hasClass("slick-initialized") || (i(t.$slider).addClass("slick-initialized"), t.buildRows(), t.buildOut(), t.setProps(), t.startLoad(), t.loadSlider(), t.initializeEvents(), t.updateArrows(), t.updateDots(), t.checkResponsive(!0), t.focusHandler()), e && t.$slider.trigger("init", [t]), !0 === t.options.accessibility && t.initADA(), t.options.autoplay && (t.paused = !1, t.autoPlay());
  }, e.prototype.initADA = function () {
    var e = this,
        t = Math.ceil(e.slideCount / e.options.slidesToShow),
        o = e.getNavigableIndexes().filter(function (i) {
      return i >= 0 && i < e.slideCount;
    });e.$slides.add(e.$slideTrack.find(".slick-cloned")).attr({ "aria-hidden": "true", tabindex: "-1" }).find("a, input, button, select").attr({ tabindex: "-1" }), null !== e.$dots && (e.$slides.not(e.$slideTrack.find(".slick-cloned")).each(function (t) {
      var s = o.indexOf(t);i(this).attr({ role: "tabpanel", id: "slick-slide" + e.instanceUid + t, tabindex: -1 }), -1 !== s && i(this).attr({ "aria-describedby": "slick-slide-control" + e.instanceUid + s });
    }), e.$dots.attr("role", "tablist").find("li").each(function (s) {
      var n = o[s];i(this).attr({ role: "presentation" }), i(this).find("button").first().attr({ role: "tab", id: "slick-slide-control" + e.instanceUid + s, "aria-controls": "slick-slide" + e.instanceUid + n, "aria-label": s + 1 + " of " + t, "aria-selected": null, tabindex: "-1" });
    }).eq(e.currentSlide).find("button").attr({ "aria-selected": "true", tabindex: "0" }).end());for (var s = e.currentSlide, n = s + e.options.slidesToShow; s < n; s++) {
      e.$slides.eq(s).attr("tabindex", 0);
    }e.activateADA();
  }, e.prototype.initArrowEvents = function () {
    var i = this;!0 === i.options.arrows && i.slideCount > i.options.slidesToShow && (i.$prevArrow.off("click.slick").on("click.slick", { message: "previous" }, i.changeSlide), i.$nextArrow.off("click.slick").on("click.slick", { message: "next" }, i.changeSlide), !0 === i.options.accessibility && (i.$prevArrow.on("keydown.slick", i.keyHandler), i.$nextArrow.on("keydown.slick", i.keyHandler)));
  }, e.prototype.initDotEvents = function () {
    var e = this;!0 === e.options.dots && (i("li", e.$dots).on("click.slick", { message: "index" }, e.changeSlide), !0 === e.options.accessibility && e.$dots.on("keydown.slick", e.keyHandler)), !0 === e.options.dots && !0 === e.options.pauseOnDotsHover && i("li", e.$dots).on("mouseenter.slick", i.proxy(e.interrupt, e, !0)).on("mouseleave.slick", i.proxy(e.interrupt, e, !1));
  }, e.prototype.initSlideEvents = function () {
    var e = this;e.options.pauseOnHover && (e.$list.on("mouseenter.slick", i.proxy(e.interrupt, e, !0)), e.$list.on("mouseleave.slick", i.proxy(e.interrupt, e, !1)));
  }, e.prototype.initializeEvents = function () {
    var e = this;e.initArrowEvents(), e.initDotEvents(), e.initSlideEvents(), e.$list.on("touchstart.slick mousedown.slick", { action: "start" }, e.swipeHandler), e.$list.on("touchmove.slick mousemove.slick", { action: "move" }, e.swipeHandler), e.$list.on("touchend.slick mouseup.slick", { action: "end" }, e.swipeHandler), e.$list.on("touchcancel.slick mouseleave.slick", { action: "end" }, e.swipeHandler), e.$list.on("click.slick", e.clickHandler), i(document).on(e.visibilityChange, i.proxy(e.visibility, e)), !0 === e.options.accessibility && e.$list.on("keydown.slick", e.keyHandler), !0 === e.options.focusOnSelect && i(e.$slideTrack).children().on("click.slick", e.selectHandler), i(window).on("orientationchange.slick.slick-" + e.instanceUid, i.proxy(e.orientationChange, e)), i(window).on("resize.slick.slick-" + e.instanceUid, i.proxy(e.resize, e)), i("[draggable!=true]", e.$slideTrack).on("dragstart", e.preventDefault), i(window).on("load.slick.slick-" + e.instanceUid, e.setPosition), i(e.setPosition);
  }, e.prototype.initUI = function () {
    var i = this;!0 === i.options.arrows && i.slideCount > i.options.slidesToShow && (i.$prevArrow.show(), i.$nextArrow.show()), !0 === i.options.dots && i.slideCount > i.options.slidesToShow && i.$dots.show();
  }, e.prototype.keyHandler = function (i) {
    var e = this;i.target.tagName.match("TEXTAREA|INPUT|SELECT") || (37 === i.keyCode && !0 === e.options.accessibility ? e.changeSlide({ data: { message: !0 === e.options.rtl ? "next" : "previous" } }) : 39 === i.keyCode && !0 === e.options.accessibility && e.changeSlide({ data: { message: !0 === e.options.rtl ? "previous" : "next" } }));
  }, e.prototype.lazyLoad = function () {
    function e(e) {
      i("img[data-lazy]", e).each(function () {
        var e = i(this),
            t = i(this).attr("data-lazy"),
            o = i(this).attr("data-srcset"),
            s = i(this).attr("data-sizes") || n.$slider.attr("data-sizes"),
            r = document.createElement("img");r.onload = function () {
          e.animate({ opacity: 0 }, 100, function () {
            o && (e.attr("srcset", o), s && e.attr("sizes", s)), e.attr("src", t).animate({ opacity: 1 }, 200, function () {
              e.removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading");
            }), n.$slider.trigger("lazyLoaded", [n, e, t]);
          });
        }, r.onerror = function () {
          e.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), n.$slider.trigger("lazyLoadError", [n, e, t]);
        }, r.src = t;
      });
    }var t,
        o,
        s,
        n = this;if (!0 === n.options.centerMode ? !0 === n.options.infinite ? s = (o = n.currentSlide + (n.options.slidesToShow / 2 + 1)) + n.options.slidesToShow + 2 : (o = Math.max(0, n.currentSlide - (n.options.slidesToShow / 2 + 1)), s = n.options.slidesToShow / 2 + 1 + 2 + n.currentSlide) : (o = n.options.infinite ? n.options.slidesToShow + n.currentSlide : n.currentSlide, s = Math.ceil(o + n.options.slidesToShow), !0 === n.options.fade && (o > 0 && o--, s <= n.slideCount && s++)), t = n.$slider.find(".slick-slide").slice(o, s), "anticipated" === n.options.lazyLoad) for (var r = o - 1, l = s, d = n.$slider.find(".slick-slide"), a = 0; a < n.options.slidesToScroll; a++) {
      r < 0 && (r = n.slideCount - 1), t = (t = t.add(d.eq(r))).add(d.eq(l)), r--, l++;
    }e(t), n.slideCount <= n.options.slidesToShow ? e(n.$slider.find(".slick-slide")) : n.currentSlide >= n.slideCount - n.options.slidesToShow ? e(n.$slider.find(".slick-cloned").slice(0, n.options.slidesToShow)) : 0 === n.currentSlide && e(n.$slider.find(".slick-cloned").slice(-1 * n.options.slidesToShow));
  }, e.prototype.loadSlider = function () {
    var i = this;i.setPosition(), i.$slideTrack.css({ opacity: 1 }), i.$slider.removeClass("slick-loading"), i.initUI(), "progressive" === i.options.lazyLoad && i.progressiveLazyLoad();
  }, e.prototype.next = e.prototype.slickNext = function () {
    this.changeSlide({ data: { message: "next" } });
  }, e.prototype.orientationChange = function () {
    var i = this;i.checkResponsive(), i.setPosition();
  }, e.prototype.pause = e.prototype.slickPause = function () {
    var i = this;i.autoPlayClear(), i.paused = !0;
  }, e.prototype.play = e.prototype.slickPlay = function () {
    var i = this;i.autoPlay(), i.options.autoplay = !0, i.paused = !1, i.focussed = !1, i.interrupted = !1;
  }, e.prototype.postSlide = function (e) {
    var t = this;t.unslicked || (t.$slider.trigger("afterChange", [t, e]), t.animating = !1, t.slideCount > t.options.slidesToShow && t.setPosition(), t.swipeLeft = null, t.options.autoplay && t.autoPlay(), !0 === t.options.accessibility && (t.initADA(), t.options.focusOnChange && i(t.$slides.get(t.currentSlide)).attr("tabindex", 0).focus()));
  }, e.prototype.prev = e.prototype.slickPrev = function () {
    this.changeSlide({ data: { message: "previous" } });
  }, e.prototype.preventDefault = function (i) {
    i.preventDefault();
  }, e.prototype.progressiveLazyLoad = function (e) {
    e = e || 1;var t,
        o,
        s,
        n,
        r,
        l = this,
        d = i("img[data-lazy]", l.$slider);d.length ? (t = d.first(), o = t.attr("data-lazy"), s = t.attr("data-srcset"), n = t.attr("data-sizes") || l.$slider.attr("data-sizes"), (r = document.createElement("img")).onload = function () {
      s && (t.attr("srcset", s), n && t.attr("sizes", n)), t.attr("src", o).removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading"), !0 === l.options.adaptiveHeight && l.setPosition(), l.$slider.trigger("lazyLoaded", [l, t, o]), l.progressiveLazyLoad();
    }, r.onerror = function () {
      e < 3 ? setTimeout(function () {
        l.progressiveLazyLoad(e + 1);
      }, 500) : (t.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), l.$slider.trigger("lazyLoadError", [l, t, o]), l.progressiveLazyLoad());
    }, r.src = o) : l.$slider.trigger("allImagesLoaded", [l]);
  }, e.prototype.refresh = function (e) {
    var t,
        o,
        s = this;o = s.slideCount - s.options.slidesToShow, !s.options.infinite && s.currentSlide > o && (s.currentSlide = o), s.slideCount <= s.options.slidesToShow && (s.currentSlide = 0), t = s.currentSlide, s.destroy(!0), i.extend(s, s.initials, { currentSlide: t }), s.init(), e || s.changeSlide({ data: { message: "index", index: t } }, !1);
  }, e.prototype.registerBreakpoints = function () {
    var e,
        t,
        o,
        s = this,
        n = s.options.responsive || null;if ("array" === i.type(n) && n.length) {
      s.respondTo = s.options.respondTo || "window";for (e in n) {
        if (o = s.breakpoints.length - 1, n.hasOwnProperty(e)) {
          for (t = n[e].breakpoint; o >= 0;) {
            s.breakpoints[o] && s.breakpoints[o] === t && s.breakpoints.splice(o, 1), o--;
          }s.breakpoints.push(t), s.breakpointSettings[t] = n[e].settings;
        }
      }s.breakpoints.sort(function (i, e) {
        return s.options.mobileFirst ? i - e : e - i;
      });
    }
  }, e.prototype.reinit = function () {
    var e = this;e.$slides = e.$slideTrack.children(e.options.slide).addClass("slick-slide"), e.slideCount = e.$slides.length, e.currentSlide >= e.slideCount && 0 !== e.currentSlide && (e.currentSlide = e.currentSlide - e.options.slidesToScroll), e.slideCount <= e.options.slidesToShow && (e.currentSlide = 0), e.registerBreakpoints(), e.setProps(), e.setupInfinite(), e.buildArrows(), e.updateArrows(), e.initArrowEvents(), e.buildDots(), e.updateDots(), e.initDotEvents(), e.cleanUpSlideEvents(), e.initSlideEvents(), e.checkResponsive(!1, !0), !0 === e.options.focusOnSelect && i(e.$slideTrack).children().on("click.slick", e.selectHandler), e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0), e.setPosition(), e.focusHandler(), e.paused = !e.options.autoplay, e.autoPlay(), e.$slider.trigger("reInit", [e]);
  }, e.prototype.resize = function () {
    var e = this;i(window).width() !== e.windowWidth && (clearTimeout(e.windowDelay), e.windowDelay = window.setTimeout(function () {
      e.windowWidth = i(window).width(), e.checkResponsive(), e.unslicked || e.setPosition();
    }, 50));
  }, e.prototype.removeSlide = e.prototype.slickRemove = function (i, e, t) {
    var o = this;if (i = "boolean" == typeof i ? !0 === (e = i) ? 0 : o.slideCount - 1 : !0 === e ? --i : i, o.slideCount < 1 || i < 0 || i > o.slideCount - 1) return !1;o.unload(), !0 === t ? o.$slideTrack.children().remove() : o.$slideTrack.children(this.options.slide).eq(i).remove(), o.$slides = o.$slideTrack.children(this.options.slide), o.$slideTrack.children(this.options.slide).detach(), o.$slideTrack.append(o.$slides), o.$slidesCache = o.$slides, o.reinit();
  }, e.prototype.setCSS = function (i) {
    var e,
        t,
        o = this,
        s = {};!0 === o.options.rtl && (i = -i), e = "left" == o.positionProp ? Math.ceil(i) + "px" : "0px", t = "top" == o.positionProp ? Math.ceil(i) + "px" : "0px", s[o.positionProp] = i, !1 === o.transformsEnabled ? o.$slideTrack.css(s) : (s = {}, !1 === o.cssTransitions ? (s[o.animType] = "translate(" + e + ", " + t + ")", o.$slideTrack.css(s)) : (s[o.animType] = "translate3d(" + e + ", " + t + ", 0px)", o.$slideTrack.css(s)));
  }, e.prototype.setDimensions = function () {
    var i = this;!1 === i.options.vertical ? !0 === i.options.centerMode && i.$list.css({ padding: "0px " + i.options.centerPadding }) : (i.$list.height(i.$slides.first().outerHeight(!0) * i.options.slidesToShow), !0 === i.options.centerMode && i.$list.css({ padding: i.options.centerPadding + " 0px" })), i.listWidth = i.$list.width(), i.listHeight = i.$list.height(), !1 === i.options.vertical && !1 === i.options.variableWidth ? (i.slideWidth = Math.ceil(i.listWidth / i.options.slidesToShow), i.$slideTrack.width(Math.ceil(i.slideWidth * i.$slideTrack.children(".slick-slide").length))) : !0 === i.options.variableWidth ? i.$slideTrack.width(5e3 * i.slideCount) : (i.slideWidth = Math.ceil(i.listWidth), i.$slideTrack.height(Math.ceil(i.$slides.first().outerHeight(!0) * i.$slideTrack.children(".slick-slide").length)));var e = i.$slides.first().outerWidth(!0) - i.$slides.first().width();!1 === i.options.variableWidth && i.$slideTrack.children(".slick-slide").width(i.slideWidth - e);
  }, e.prototype.setFade = function () {
    var e,
        t = this;t.$slides.each(function (o, s) {
      e = t.slideWidth * o * -1, !0 === t.options.rtl ? i(s).css({ position: "relative", right: e, top: 0, zIndex: t.options.zIndex - 2, opacity: 0 }) : i(s).css({ position: "relative", left: e, top: 0, zIndex: t.options.zIndex - 2, opacity: 0 });
    }), t.$slides.eq(t.currentSlide).css({ zIndex: t.options.zIndex - 1, opacity: 1 });
  }, e.prototype.setHeight = function () {
    var i = this;if (1 === i.options.slidesToShow && !0 === i.options.adaptiveHeight && !1 === i.options.vertical) {
      var e = i.$slides.eq(i.currentSlide).outerHeight(!0);i.$list.css("height", e);
    }
  }, e.prototype.setOption = e.prototype.slickSetOption = function () {
    var e,
        t,
        o,
        s,
        n,
        r = this,
        l = !1;if ("object" === i.type(arguments[0]) ? (o = arguments[0], l = arguments[1], n = "multiple") : "string" === i.type(arguments[0]) && (o = arguments[0], s = arguments[1], l = arguments[2], "responsive" === arguments[0] && "array" === i.type(arguments[1]) ? n = "responsive" : void 0 !== arguments[1] && (n = "single")), "single" === n) r.options[o] = s;else if ("multiple" === n) i.each(o, function (i, e) {
      r.options[i] = e;
    });else if ("responsive" === n) for (t in s) {
      if ("array" !== i.type(r.options.responsive)) r.options.responsive = [s[t]];else {
        for (e = r.options.responsive.length - 1; e >= 0;) {
          r.options.responsive[e].breakpoint === s[t].breakpoint && r.options.responsive.splice(e, 1), e--;
        }r.options.responsive.push(s[t]);
      }
    }l && (r.unload(), r.reinit());
  }, e.prototype.setPosition = function () {
    var i = this;i.setDimensions(), i.setHeight(), !1 === i.options.fade ? i.setCSS(i.getLeft(i.currentSlide)) : i.setFade(), i.$slider.trigger("setPosition", [i]);
  }, e.prototype.setProps = function () {
    var i = this,
        e = document.body.style;i.positionProp = !0 === i.options.vertical ? "top" : "left", "top" === i.positionProp ? i.$slider.addClass("slick-vertical") : i.$slider.removeClass("slick-vertical"), void 0 === e.WebkitTransition && void 0 === e.MozTransition && void 0 === e.msTransition || !0 === i.options.useCSS && (i.cssTransitions = !0), i.options.fade && ("number" == typeof i.options.zIndex ? i.options.zIndex < 3 && (i.options.zIndex = 3) : i.options.zIndex = i.defaults.zIndex), void 0 !== e.OTransform && (i.animType = "OTransform", i.transformType = "-o-transform", i.transitionType = "OTransition", void 0 === e.perspectiveProperty && void 0 === e.webkitPerspective && (i.animType = !1)), void 0 !== e.MozTransform && (i.animType = "MozTransform", i.transformType = "-moz-transform", i.transitionType = "MozTransition", void 0 === e.perspectiveProperty && void 0 === e.MozPerspective && (i.animType = !1)), void 0 !== e.webkitTransform && (i.animType = "webkitTransform", i.transformType = "-webkit-transform", i.transitionType = "webkitTransition", void 0 === e.perspectiveProperty && void 0 === e.webkitPerspective && (i.animType = !1)), void 0 !== e.msTransform && (i.animType = "msTransform", i.transformType = "-ms-transform", i.transitionType = "msTransition", void 0 === e.msTransform && (i.animType = !1)), void 0 !== e.transform && !1 !== i.animType && (i.animType = "transform", i.transformType = "transform", i.transitionType = "transition"), i.transformsEnabled = i.options.useTransform && null !== i.animType && !1 !== i.animType;
  }, e.prototype.setSlideClasses = function (i) {
    var e,
        t,
        o,
        s,
        n = this;if (t = n.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden", "true"), n.$slides.eq(i).addClass("slick-current"), !0 === n.options.centerMode) {
      var r = n.options.slidesToShow % 2 == 0 ? 1 : 0;e = Math.floor(n.options.slidesToShow / 2), !0 === n.options.infinite && (i >= e && i <= n.slideCount - 1 - e ? n.$slides.slice(i - e + r, i + e + 1).addClass("slick-active").attr("aria-hidden", "false") : (o = n.options.slidesToShow + i, t.slice(o - e + 1 + r, o + e + 2).addClass("slick-active").attr("aria-hidden", "false")), 0 === i ? t.eq(t.length - 1 - n.options.slidesToShow).addClass("slick-center") : i === n.slideCount - 1 && t.eq(n.options.slidesToShow).addClass("slick-center")), n.$slides.eq(i).addClass("slick-center");
    } else i >= 0 && i <= n.slideCount - n.options.slidesToShow ? n.$slides.slice(i, i + n.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false") : t.length <= n.options.slidesToShow ? t.addClass("slick-active").attr("aria-hidden", "false") : (s = n.slideCount % n.options.slidesToShow, o = !0 === n.options.infinite ? n.options.slidesToShow + i : i, n.options.slidesToShow == n.options.slidesToScroll && n.slideCount - i < n.options.slidesToShow ? t.slice(o - (n.options.slidesToShow - s), o + s).addClass("slick-active").attr("aria-hidden", "false") : t.slice(o, o + n.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false"));"ondemand" !== n.options.lazyLoad && "anticipated" !== n.options.lazyLoad || n.lazyLoad();
  }, e.prototype.setupInfinite = function () {
    var e,
        t,
        o,
        s = this;if (!0 === s.options.fade && (s.options.centerMode = !1), !0 === s.options.infinite && !1 === s.options.fade && (t = null, s.slideCount > s.options.slidesToShow)) {
      for (o = !0 === s.options.centerMode ? s.options.slidesToShow + 1 : s.options.slidesToShow, e = s.slideCount; e > s.slideCount - o; e -= 1) {
        t = e - 1, i(s.$slides[t]).clone(!0).attr("id", "").attr("data-slick-index", t - s.slideCount).prependTo(s.$slideTrack).addClass("slick-cloned");
      }for (e = 0; e < o + s.slideCount; e += 1) {
        t = e, i(s.$slides[t]).clone(!0).attr("id", "").attr("data-slick-index", t + s.slideCount).appendTo(s.$slideTrack).addClass("slick-cloned");
      }s.$slideTrack.find(".slick-cloned").find("[id]").each(function () {
        i(this).attr("id", "");
      });
    }
  }, e.prototype.interrupt = function (i) {
    var e = this;i || e.autoPlay(), e.interrupted = i;
  }, e.prototype.selectHandler = function (e) {
    var t = this,
        o = i(e.target).is(".slick-slide") ? i(e.target) : i(e.target).parents(".slick-slide"),
        s = parseInt(o.attr("data-slick-index"));s || (s = 0), t.slideCount <= t.options.slidesToShow ? t.slideHandler(s, !1, !0) : t.slideHandler(s);
  }, e.prototype.slideHandler = function (i, e, t) {
    var o,
        s,
        n,
        r,
        l,
        d = null,
        a = this;if (e = e || !1, !(!0 === a.animating && !0 === a.options.waitForAnimate || !0 === a.options.fade && a.currentSlide === i)) if (!1 === e && a.asNavFor(i), o = i, d = a.getLeft(o), r = a.getLeft(a.currentSlide), a.currentLeft = null === a.swipeLeft ? r : a.swipeLeft, !1 === a.options.infinite && !1 === a.options.centerMode && (i < 0 || i > a.getDotCount() * a.options.slidesToScroll)) !1 === a.options.fade && (o = a.currentSlide, !0 !== t ? a.animateSlide(r, function () {
      a.postSlide(o);
    }) : a.postSlide(o));else if (!1 === a.options.infinite && !0 === a.options.centerMode && (i < 0 || i > a.slideCount - a.options.slidesToScroll)) !1 === a.options.fade && (o = a.currentSlide, !0 !== t ? a.animateSlide(r, function () {
      a.postSlide(o);
    }) : a.postSlide(o));else {
      if (a.options.autoplay && clearInterval(a.autoPlayTimer), s = o < 0 ? a.slideCount % a.options.slidesToScroll != 0 ? a.slideCount - a.slideCount % a.options.slidesToScroll : a.slideCount + o : o >= a.slideCount ? a.slideCount % a.options.slidesToScroll != 0 ? 0 : o - a.slideCount : o, a.animating = !0, a.$slider.trigger("beforeChange", [a, a.currentSlide, s]), n = a.currentSlide, a.currentSlide = s, a.setSlideClasses(a.currentSlide), a.options.asNavFor && (l = (l = a.getNavTarget()).slick("getSlick")).slideCount <= l.options.slidesToShow && l.setSlideClasses(a.currentSlide), a.updateDots(), a.updateArrows(), !0 === a.options.fade) return !0 !== t ? (a.fadeSlideOut(n), a.fadeSlide(s, function () {
        a.postSlide(s);
      })) : a.postSlide(s), void a.animateHeight();!0 !== t ? a.animateSlide(d, function () {
        a.postSlide(s);
      }) : a.postSlide(s);
    }
  }, e.prototype.startLoad = function () {
    var i = this;!0 === i.options.arrows && i.slideCount > i.options.slidesToShow && (i.$prevArrow.hide(), i.$nextArrow.hide()), !0 === i.options.dots && i.slideCount > i.options.slidesToShow && i.$dots.hide(), i.$slider.addClass("slick-loading");
  }, e.prototype.swipeDirection = function () {
    var i,
        e,
        t,
        o,
        s = this;return i = s.touchObject.startX - s.touchObject.curX, e = s.touchObject.startY - s.touchObject.curY, t = Math.atan2(e, i), (o = Math.round(180 * t / Math.PI)) < 0 && (o = 360 - Math.abs(o)), o <= 45 && o >= 0 ? !1 === s.options.rtl ? "left" : "right" : o <= 360 && o >= 315 ? !1 === s.options.rtl ? "left" : "right" : o >= 135 && o <= 225 ? !1 === s.options.rtl ? "right" : "left" : !0 === s.options.verticalSwiping ? o >= 35 && o <= 135 ? "down" : "up" : "vertical";
  }, e.prototype.swipeEnd = function (i) {
    var e,
        t,
        o = this;if (o.dragging = !1, o.swiping = !1, o.scrolling) return o.scrolling = !1, !1;if (o.interrupted = !1, o.shouldClick = !(o.touchObject.swipeLength > 10), void 0 === o.touchObject.curX) return !1;if (!0 === o.touchObject.edgeHit && o.$slider.trigger("edge", [o, o.swipeDirection()]), o.touchObject.swipeLength >= o.touchObject.minSwipe) {
      switch (t = o.swipeDirection()) {case "left":case "down":
          e = o.options.swipeToSlide ? o.checkNavigable(o.currentSlide + o.getSlideCount()) : o.currentSlide + o.getSlideCount(), o.currentDirection = 0;break;case "right":case "up":
          e = o.options.swipeToSlide ? o.checkNavigable(o.currentSlide - o.getSlideCount()) : o.currentSlide - o.getSlideCount(), o.currentDirection = 1;}"vertical" != t && (o.slideHandler(e), o.touchObject = {}, o.$slider.trigger("swipe", [o, t]));
    } else o.touchObject.startX !== o.touchObject.curX && (o.slideHandler(o.currentSlide), o.touchObject = {});
  }, e.prototype.swipeHandler = function (i) {
    var e = this;if (!(!1 === e.options.swipe || "ontouchend" in document && !1 === e.options.swipe || !1 === e.options.draggable && -1 !== i.type.indexOf("mouse"))) switch (e.touchObject.fingerCount = i.originalEvent && void 0 !== i.originalEvent.touches ? i.originalEvent.touches.length : 1, e.touchObject.minSwipe = e.listWidth / e.options.touchThreshold, !0 === e.options.verticalSwiping && (e.touchObject.minSwipe = e.listHeight / e.options.touchThreshold), i.data.action) {case "start":
        e.swipeStart(i);break;case "move":
        e.swipeMove(i);break;case "end":
        e.swipeEnd(i);}
  }, e.prototype.swipeMove = function (i) {
    var e,
        t,
        o,
        s,
        n,
        r,
        l = this;return n = void 0 !== i.originalEvent ? i.originalEvent.touches : null, !(!l.dragging || l.scrolling || n && 1 !== n.length) && (e = l.getLeft(l.currentSlide), l.touchObject.curX = void 0 !== n ? n[0].pageX : i.clientX, l.touchObject.curY = void 0 !== n ? n[0].pageY : i.clientY, l.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(l.touchObject.curX - l.touchObject.startX, 2))), r = Math.round(Math.sqrt(Math.pow(l.touchObject.curY - l.touchObject.startY, 2))), !l.options.verticalSwiping && !l.swiping && r > 4 ? (l.scrolling = !0, !1) : (!0 === l.options.verticalSwiping && (l.touchObject.swipeLength = r), t = l.swipeDirection(), void 0 !== i.originalEvent && l.touchObject.swipeLength > 4 && (l.swiping = !0, i.preventDefault()), s = (!1 === l.options.rtl ? 1 : -1) * (l.touchObject.curX > l.touchObject.startX ? 1 : -1), !0 === l.options.verticalSwiping && (s = l.touchObject.curY > l.touchObject.startY ? 1 : -1), o = l.touchObject.swipeLength, l.touchObject.edgeHit = !1, !1 === l.options.infinite && (0 === l.currentSlide && "right" === t || l.currentSlide >= l.getDotCount() && "left" === t) && (o = l.touchObject.swipeLength * l.options.edgeFriction, l.touchObject.edgeHit = !0), !1 === l.options.vertical ? l.swipeLeft = e + o * s : l.swipeLeft = e + o * (l.$list.height() / l.listWidth) * s, !0 === l.options.verticalSwiping && (l.swipeLeft = e + o * s), !0 !== l.options.fade && !1 !== l.options.touchMove && (!0 === l.animating ? (l.swipeLeft = null, !1) : void l.setCSS(l.swipeLeft))));
  }, e.prototype.swipeStart = function (i) {
    var e,
        t = this;if (t.interrupted = !0, 1 !== t.touchObject.fingerCount || t.slideCount <= t.options.slidesToShow) return t.touchObject = {}, !1;void 0 !== i.originalEvent && void 0 !== i.originalEvent.touches && (e = i.originalEvent.touches[0]), t.touchObject.startX = t.touchObject.curX = void 0 !== e ? e.pageX : i.clientX, t.touchObject.startY = t.touchObject.curY = void 0 !== e ? e.pageY : i.clientY, t.dragging = !0;
  }, e.prototype.unfilterSlides = e.prototype.slickUnfilter = function () {
    var i = this;null !== i.$slidesCache && (i.unload(), i.$slideTrack.children(this.options.slide).detach(), i.$slidesCache.appendTo(i.$slideTrack), i.reinit());
  }, e.prototype.unload = function () {
    var e = this;i(".slick-cloned", e.$slider).remove(), e.$dots && e.$dots.remove(), e.$prevArrow && e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.remove(), e.$nextArrow && e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.remove(), e.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "");
  }, e.prototype.unslick = function (i) {
    var e = this;e.$slider.trigger("unslick", [e, i]), e.destroy();
  }, e.prototype.updateArrows = function () {
    var i = this;Math.floor(i.options.slidesToShow / 2), !0 === i.options.arrows && i.slideCount > i.options.slidesToShow && !i.options.infinite && (i.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), i.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), 0 === i.currentSlide ? (i.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"), i.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : i.currentSlide >= i.slideCount - i.options.slidesToShow && !1 === i.options.centerMode ? (i.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), i.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : i.currentSlide >= i.slideCount - 1 && !0 === i.options.centerMode && (i.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), i.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")));
  }, e.prototype.updateDots = function () {
    var i = this;null !== i.$dots && (i.$dots.find("li").removeClass("slick-active").end(), i.$dots.find("li").eq(Math.floor(i.currentSlide / i.options.slidesToScroll)).addClass("slick-active"));
  }, e.prototype.visibility = function () {
    var i = this;i.options.autoplay && (document[i.hidden] ? i.interrupted = !0 : i.interrupted = !1);
  }, i.fn.slick = function () {
    var i,
        t,
        o = this,
        s = arguments[0],
        n = Array.prototype.slice.call(arguments, 1),
        r = o.length;for (i = 0; i < r; i++) {
      if ("object" == (typeof s === "undefined" ? "undefined" : _typeof(s)) || void 0 === s ? o[i].slick = new e(o[i], s) : t = o[i].slick[s].apply(o[i].slick, n), void 0 !== t) return t;
    }return o;
  };
});
"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

!function (e, t) {
  "function" == typeof define && define.amd ? define(t) : "object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) && "string" != typeof exports.nodeName ? module.exports = t() : e.Croppie = t();
}("undefined" != typeof self ? self : undefined, function () {
  "function" != typeof Promise && function (e) {
    function n(e, t) {
      return function () {
        e.apply(t, arguments);
      };
    }function r(e) {
      if ("object" != _typeof(this)) throw new TypeError("Promises must be constructed via new");if ("function" != typeof e) throw new TypeError("not a function");this._state = null, this._value = null, this._deferreds = [], u(e, n(i, this), n(o, this));
    }function a(n) {
      var i = this;return null === this._state ? void this._deferreds.push(n) : void c(function () {
        var e = i._state ? n.onFulfilled : n.onRejected;if (null !== e) {
          var t;try {
            t = e(i._value);
          } catch (e) {
            return void n.reject(e);
          }n.resolve(t);
        } else (i._state ? n.resolve : n.reject)(i._value);
      });
    }function i(e) {
      try {
        if (e === this) throw new TypeError("A promise cannot be resolved with itself.");if (e && ("object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) || "function" == typeof e)) {
          var t = e.then;if ("function" == typeof t) return void u(n(t, e), n(i, this), n(o, this));
        }this._state = !0, this._value = e, s.call(this);
      } catch (e) {
        o.call(this, e);
      }
    }function o(e) {
      this._state = !1, this._value = e, s.call(this);
    }function s() {
      for (var e = 0, t = this._deferreds.length; e < t; e++) {
        a.call(this, this._deferreds[e]);
      }this._deferreds = null;
    }function l(e, t, n, i) {
      this.onFulfilled = "function" == typeof e ? e : null, this.onRejected = "function" == typeof t ? t : null, this.resolve = n, this.reject = i;
    }function u(e, t, n) {
      var i = !1;try {
        e(function (e) {
          i || (i = !0, t(e));
        }, function (e) {
          i || (i = !0, n(e));
        });
      } catch (e) {
        if (i) return;i = !0, n(e);
      }
    }var t = setTimeout,
        c = "function" == typeof setImmediate && setImmediate || function (e) {
      t(e, 1);
    },
        h = Array.isArray || function (e) {
      return "[object Array]" === Object.prototype.toString.call(e);
    };r.prototype.catch = function (e) {
      return this.then(null, e);
    }, r.prototype.then = function (n, i) {
      var o = this;return new r(function (e, t) {
        a.call(o, new l(n, i, e, t));
      });
    }, r.all = function () {
      var s = Array.prototype.slice.call(1 === arguments.length && h(arguments[0]) ? arguments[0] : arguments);return new r(function (i, o) {
        function r(t, e) {
          try {
            if (e && ("object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) || "function" == typeof e)) {
              var n = e.then;if ("function" == typeof n) return void n.call(e, function (e) {
                r(t, e);
              }, o);
            }s[t] = e, 0 == --a && i(s);
          } catch (e) {
            o(e);
          }
        }if (0 === s.length) return i([]);for (var a = s.length, e = 0; e < s.length; e++) {
          r(e, s[e]);
        }
      });
    }, r.resolve = function (t) {
      return t && "object" == (typeof t === "undefined" ? "undefined" : _typeof(t)) && t.constructor === r ? t : new r(function (e) {
        e(t);
      });
    }, r.reject = function (n) {
      return new r(function (e, t) {
        t(n);
      });
    }, r.race = function (o) {
      return new r(function (e, t) {
        for (var n = 0, i = o.length; n < i; n++) {
          o[n].then(e, t);
        }
      });
    }, r._setImmediateFn = function (e) {
      c = e;
    }, "undefined" != typeof module && module.exports ? module.exports = r : e.Promise || (e.Promise = r);
  }(this), "function" != typeof window.CustomEvent && function () {
    function e(e, t) {
      t = t || { bubbles: !1, cancelable: !1, detail: void 0 };var n = document.createEvent("CustomEvent");return n.initCustomEvent(e, t.bubbles, t.cancelable, t.detail), n;
    }e.prototype = window.Event.prototype, window.CustomEvent = e;
  }(), HTMLCanvasElement.prototype.toBlob || Object.defineProperty(HTMLCanvasElement.prototype, "toBlob", { value: function value(e, t, n) {
      for (var i = atob(this.toDataURL(t, n).split(",")[1]), o = i.length, r = new Uint8Array(o), a = 0; a < o; a++) {
        r[a] = i.charCodeAt(a);
      }e(new Blob([r], { type: t || "image/png" }));
    } });var v,
      g,
      w,
      i = ["Webkit", "Moz", "ms"],
      o = document.createElement("div").style,
      l = [1, 8, 3, 6],
      u = [2, 7, 4, 5];function e(e) {
    if (e in o) return e;for (var t = e[0].toUpperCase() + e.slice(1), n = i.length; n--;) {
      if ((e = i[n] + t) in o) return e;
    }
  }function p(e, t) {
    for (var n in e = e || {}, t) {
      t[n] && t[n].constructor && t[n].constructor === Object ? (e[n] = e[n] || {}, p(e[n], t[n])) : e[n] = t[n];
    }return e;
  }function d(e) {
    return p({}, e);
  }function y(e) {
    if ("createEvent" in document) {
      var t = document.createEvent("HTMLEvents");t.initEvent("change", !1, !0), e.dispatchEvent(t);
    } else e.fireEvent("onchange");
  }function b(e, t, n) {
    if ("string" == typeof t) {
      var i = t;(t = {})[i] = n;
    }for (var o in t) {
      e.style[o] = t[o];
    }
  }function x(e, t) {
    e.classList ? e.classList.add(t) : e.className += " " + t;
  }function c(e, t) {
    for (var n in t) {
      e.setAttribute(n, t[n]);
    }
  }function C(e) {
    return parseInt(e, 10);
  }function m(e, t) {
    var n = e.naturalWidth,
        i = e.naturalHeight,
        o = t || f(e);if (o && 5 <= o) {
      var r = n;n = i, i = r;
    }return { width: n, height: i };
  }g = e("transform"), v = e("transformOrigin"), w = e("userSelect");var t = { translate3d: { suffix: ", 0px" }, translate: { suffix: "" } },
      E = function E(e, t, n) {
    this.x = parseFloat(e), this.y = parseFloat(t), this.scale = parseFloat(n);
  };E.parse = function (e) {
    return e.style ? E.parse(e.style[g]) : -1 < e.indexOf("matrix") || -1 < e.indexOf("none") ? E.fromMatrix(e) : E.fromString(e);
  }, E.fromMatrix = function (e) {
    var t = e.substring(7).split(",");return t.length && "none" !== e || (t = [1, 0, 0, 1, 0, 0]), new E(C(t[4]), C(t[5]), parseFloat(t[0]));
  }, E.fromString = function (e) {
    var t = e.split(") "),
        n = t[0].substring(T.globals.translate.length + 1).split(","),
        i = 1 < t.length ? t[1].substring(6) : 1,
        o = 1 < n.length ? n[0] : 0,
        r = 1 < n.length ? n[1] : 0;return new E(o, r, i);
  }, E.prototype.toString = function () {
    var e = t[T.globals.translate].suffix || "";return T.globals.translate + "(" + this.x + "px, " + this.y + "px" + e + ") scale(" + this.scale + ")";
  };var L = function L(e) {
    if (!e || !e.style[v]) return this.x = 0, void (this.y = 0);var t = e.style[v].split(" ");this.x = parseFloat(t[0]), this.y = parseFloat(t[1]);
  };function f(e) {
    return e.exifdata && e.exifdata.Orientation ? C(e.exifdata.Orientation) : 1;
  }function _(e, t, n) {
    var i = t.width,
        o = t.height,
        r = e.getContext("2d");switch (e.width = t.width, e.height = t.height, r.save(), n) {case 2:
        r.translate(i, 0), r.scale(-1, 1);break;case 3:
        r.translate(i, o), r.rotate(180 * Math.PI / 180);break;case 4:
        r.translate(0, o), r.scale(1, -1);break;case 5:
        e.width = o, e.height = i, r.rotate(90 * Math.PI / 180), r.scale(1, -1);break;case 6:
        e.width = o, e.height = i, r.rotate(90 * Math.PI / 180), r.translate(0, -o);break;case 7:
        e.width = o, e.height = i, r.rotate(-90 * Math.PI / 180), r.translate(-i, o), r.scale(1, -1);break;case 8:
        e.width = o, e.height = i, r.translate(0, i), r.rotate(-90 * Math.PI / 180);}r.drawImage(t, 0, 0, i, o), r.restore();
  }function r() {
    var e,
        t,
        n,
        i,
        o,
        r,
        a = this,
        s = a.options.viewport.type ? "cr-vp-" + a.options.viewport.type : null;a.options.useCanvas = a.options.enableOrientation || R.call(a), a.data = {}, a.elements = {}, e = a.elements.boundary = document.createElement("div"), n = a.elements.viewport = document.createElement("div"), t = a.elements.img = document.createElement("img"), i = a.elements.overlay = document.createElement("div"), a.options.useCanvas ? (a.elements.canvas = document.createElement("canvas"), a.elements.preview = a.elements.canvas) : a.elements.preview = t, x(e, "cr-boundary"), e.setAttribute("aria-dropeffect", "none"), o = a.options.boundary.width, r = a.options.boundary.height, b(e, { width: o + (isNaN(o) ? "" : "px"), height: r + (isNaN(r) ? "" : "px") }), x(n, "cr-viewport"), s && x(n, s), b(n, { width: a.options.viewport.width + "px", height: a.options.viewport.height + "px" }), n.setAttribute("tabindex", 0), x(a.elements.preview, "cr-image"), c(a.elements.preview, { alt: "preview", "aria-grabbed": "false" }), x(i, "cr-overlay"), a.element.appendChild(e), e.appendChild(a.elements.preview), e.appendChild(n), e.appendChild(i), x(a.element, "croppie-container"), a.options.customClass && x(a.element, a.options.customClass), function () {
      var h,
          p,
          d,
          l,
          m,
          f = this,
          n = !1;function v(e, t) {
        var n = f.elements.preview.getBoundingClientRect(),
            i = m.y + t,
            o = m.x + e;f.options.enforceBoundary ? (l.top > n.top + t && l.bottom < n.bottom + t && (m.y = i), l.left > n.left + e && l.right < n.right + e && (m.x = o)) : (m.y = i, m.x = o);
      }function i(e) {
        f.elements.preview.setAttribute("aria-grabbed", e), f.elements.boundary.setAttribute("aria-dropeffect", e ? "move" : "none");
      }function e(e) {
        if ((void 0 === e.button || 0 === e.button) && (e.preventDefault(), !n)) {
          if (n = !0, h = e.pageX, p = e.pageY, e.touches) {
            var t = e.touches[0];h = t.pageX, p = t.pageY;
          }i(n), m = E.parse(f.elements.preview), window.addEventListener("mousemove", o), window.addEventListener("touchmove", o), window.addEventListener("mouseup", r), window.addEventListener("touchend", r), document.body.style[w] = "none", l = f.elements.viewport.getBoundingClientRect();
        }
      }function o(e) {
        e.preventDefault();var t = e.pageX,
            n = e.pageY;if (e.touches) {
          var i = e.touches[0];t = i.pageX, n = i.pageY;
        }var o = t - h,
            r = n - p,
            a = {};if ("touchmove" === e.type && 1 < e.touches.length) {
          var s = e.touches[0],
              l = e.touches[1],
              u = Math.sqrt((s.pageX - l.pageX) * (s.pageX - l.pageX) + (s.pageY - l.pageY) * (s.pageY - l.pageY));d || (d = u / f._currentZoom);var c = u / d;return B.call(f, c), void y(f.elements.zoomer);
        }v(o, r), a[g] = m.toString(), b(f.elements.preview, a), z.call(f), p = n, h = t;
      }function r() {
        i(n = !1), window.removeEventListener("mousemove", o), window.removeEventListener("touchmove", o), window.removeEventListener("mouseup", r), window.removeEventListener("touchend", r), document.body.style[w] = "", Z.call(f), F.call(f), d = 0;
      }f.elements.overlay.addEventListener("mousedown", e), f.elements.viewport.addEventListener("keydown", function (e) {
        if (!e.shiftKey || 38 !== e.keyCode && 40 !== e.keyCode) {
          if (f.options.enableKeyMovement && 37 <= e.keyCode && e.keyCode <= 40) {
            e.preventDefault();var t = s(e.keyCode);m = E.parse(f.elements.preview), document.body.style[w] = "none", l = f.elements.viewport.getBoundingClientRect(), o = (i = t)[0], r = i[1], a = {}, v(o, r), a[g] = m.toString(), b(f.elements.preview, a), z.call(f), document.body.style[w] = "", Z.call(f), F.call(f), d = 0;
          }
        } else {
          var n;n = 38 === e.keyCode ? parseFloat(f.elements.zoomer.value) + parseFloat(f.elements.zoomer.step) : parseFloat(f.elements.zoomer.value) - parseFloat(f.elements.zoomer.step), f.setZoom(n);
        }var i, o, r, a;function s(e) {
          switch (e) {case 37:
              return [1, 0];case 38:
              return [0, 1];case 39:
              return [-1, 0];case 40:
              return [0, -1];}
        }
      }), f.elements.overlay.addEventListener("touchstart", e);
    }.call(this), a.options.enableZoom && function () {
      var i = this,
          e = i.elements.zoomerWrap = document.createElement("div"),
          t = i.elements.zoomer = document.createElement("input");function o() {
        (function (e) {
          var t = this,
              n = e ? e.transform : E.parse(t.elements.preview),
              i = e ? e.viewportRect : t.elements.viewport.getBoundingClientRect(),
              o = e ? e.origin : new L(t.elements.preview);function r() {
            var e = {};e[g] = n.toString(), e[v] = o.toString(), b(t.elements.preview, e);
          }if (t._currentZoom = e ? e.value : t._currentZoom, n.scale = t._currentZoom, t.elements.zoomer.setAttribute("aria-valuenow", t._currentZoom), r(), t.options.enforceBoundary) {
            var a = function (e) {
              var t = this._currentZoom,
                  n = e.width,
                  i = e.height,
                  o = this.elements.boundary.clientWidth / 2,
                  r = this.elements.boundary.clientHeight / 2,
                  a = this.elements.preview.getBoundingClientRect(),
                  s = a.width,
                  l = a.height,
                  u = n / 2,
                  c = i / 2,
                  h = -1 * (u / t - o),
                  p = -1 * (c / t - r),
                  d = 1 / t * u,
                  m = 1 / t * c;return { translate: { maxX: h, minX: h - (s * (1 / t) - n * (1 / t)), maxY: p, minY: p - (l * (1 / t) - i * (1 / t)) }, origin: { maxX: s * (1 / t) - d, minX: d, maxY: l * (1 / t) - m, minY: m } };
            }.call(t, i),
                s = a.translate,
                l = a.origin;n.x >= s.maxX && (o.x = l.minX, n.x = s.maxX), n.x <= s.minX && (o.x = l.maxX, n.x = s.minX), n.y >= s.maxY && (o.y = l.minY, n.y = s.maxY), n.y <= s.minY && (o.y = l.maxY, n.y = s.minY);
          }r(), I.call(t), F.call(t);
        }).call(i, { value: parseFloat(t.value), origin: new L(i.elements.preview), viewportRect: i.elements.viewport.getBoundingClientRect(), transform: E.parse(i.elements.preview) });
      }function n(e) {
        var t, n;if ("ctrl" === i.options.mouseWheelZoom && !0 !== e.ctrlKey) return 0;t = e.wheelDelta ? e.wheelDelta / 1200 : e.deltaY ? e.deltaY / 1060 : e.detail ? e.detail / -60 : 0, n = i._currentZoom + t * i._currentZoom, e.preventDefault(), B.call(i, n), o.call(i);
      }x(e, "cr-slider-wrap"), x(t, "cr-slider"), t.type = "range", t.step = "0.0001", t.value = "1", t.style.display = i.options.showZoomer ? "" : "none", t.setAttribute("aria-label", "zoom"), i.element.appendChild(e), e.appendChild(t), i._currentZoom = 1, i.elements.zoomer.addEventListener("input", o), i.elements.zoomer.addEventListener("change", o), i.options.mouseWheelZoom && (i.elements.boundary.addEventListener("mousewheel", n), i.elements.boundary.addEventListener("DOMMouseScroll", n));
    }.call(a), a.options.enableResize && function () {
      var l,
          u,
          c,
          h,
          p,
          e,
          t,
          d = this,
          m = document.createElement("div"),
          i = !1,
          f = 50;x(m, "cr-resizer"), b(m, { width: this.options.viewport.width + "px", height: this.options.viewport.height + "px" }), this.options.resizeControls.height && (x(e = document.createElement("div"), "cr-resizer-vertical"), m.appendChild(e));this.options.resizeControls.width && (x(t = document.createElement("div"), "cr-resizer-horisontal"), m.appendChild(t));function n(e) {
        if ((void 0 === e.button || 0 === e.button) && (e.preventDefault(), !i)) {
          var t = d.elements.overlay.getBoundingClientRect();if (i = !0, u = e.pageX, c = e.pageY, l = -1 !== e.currentTarget.className.indexOf("vertical") ? "v" : "h", h = t.width, p = t.height, e.touches) {
            var n = e.touches[0];u = n.pageX, c = n.pageY;
          }window.addEventListener("mousemove", o), window.addEventListener("touchmove", o), window.addEventListener("mouseup", r), window.addEventListener("touchend", r), document.body.style[w] = "none";
        }
      }function o(e) {
        var t = e.pageX,
            n = e.pageY;if (e.preventDefault(), e.touches) {
          var i = e.touches[0];t = i.pageX, n = i.pageY;
        }var o = t - u,
            r = n - c,
            a = d.options.viewport.height + r,
            s = d.options.viewport.width + o;"v" === l && f <= a && a <= p ? (b(m, { height: a + "px" }), d.options.boundary.height += r, b(d.elements.boundary, { height: d.options.boundary.height + "px" }), d.options.viewport.height += r, b(d.elements.viewport, { height: d.options.viewport.height + "px" })) : "h" === l && f <= s && s <= h && (b(m, { width: s + "px" }), d.options.boundary.width += o, b(d.elements.boundary, { width: d.options.boundary.width + "px" }), d.options.viewport.width += o, b(d.elements.viewport, { width: d.options.viewport.width + "px" })), z.call(d), W.call(d), Z.call(d), F.call(d), c = n, u = t;
      }function r() {
        i = !1, window.removeEventListener("mousemove", o), window.removeEventListener("touchmove", o), window.removeEventListener("mouseup", r), window.removeEventListener("touchend", r), document.body.style[w] = "";
      }e && (e.addEventListener("mousedown", n), e.addEventListener("touchstart", n));t && (t.addEventListener("mousedown", n), t.addEventListener("touchstart", n));this.elements.boundary.appendChild(m);
    }.call(a);
  }function R() {
    return this.options.enableExif && window.EXIF;
  }function B(e) {
    if (this.options.enableZoom) {
      var t = this.elements.zoomer,
          n = A(e, 4);t.value = Math.max(parseFloat(t.min), Math.min(parseFloat(t.max), n)).toString();
    }
  }function Z(e) {
    var t = this,
        n = t._currentZoom,
        i = t.elements.preview.getBoundingClientRect(),
        o = t.elements.viewport.getBoundingClientRect(),
        r = E.parse(t.elements.preview.style[g]),
        a = new L(t.elements.preview),
        s = o.top - i.top + o.height / 2,
        l = o.left - i.left + o.width / 2,
        u = {},
        c = {};if (e) {
      var h = a.x,
          p = a.y,
          d = r.x,
          m = r.y;u.y = h, u.x = p, r.y = d, r.x = m;
    } else u.y = s / n, u.x = l / n, c.y = (u.y - a.y) * (1 - n), c.x = (u.x - a.x) * (1 - n), r.x -= c.x, r.y -= c.y;var f = {};f[v] = u.x + "px " + u.y + "px", f[g] = r.toString(), b(t.elements.preview, f);
  }function z() {
    if (this.elements) {
      var e = this.elements.boundary.getBoundingClientRect(),
          t = this.elements.preview.getBoundingClientRect();b(this.elements.overlay, { width: t.width + "px", height: t.height + "px", top: t.top - e.top + "px", left: t.left - e.left + "px" });
    }
  }L.prototype.toString = function () {
    return this.x + "px " + this.y + "px";
  };var a,
      s,
      h,
      M,
      I = (a = z, s = 500, function () {
    var e = this,
        t = arguments,
        n = h && !M;clearTimeout(M), M = setTimeout(function () {
      M = null, h || a.apply(e, t);
    }, s), n && a.apply(e, t);
  });function F() {
    var e,
        t = this,
        n = t.get();X.call(t) && (t.options.update.call(t, n), t.$ && "undefined" == typeof Prototype ? t.$(t.element).trigger("update.croppie", n) : (window.CustomEvent ? e = new CustomEvent("update", { detail: n }) : (e = document.createEvent("CustomEvent")).initCustomEvent("update", !0, !0, n), t.element.dispatchEvent(e)));
  }function X() {
    return 0 < this.elements.preview.offsetHeight && 0 < this.elements.preview.offsetWidth;
  }function Y() {
    var e,
        t = this,
        n = {},
        i = t.elements.preview,
        o = new E(0, 0, 1),
        r = new L();X.call(t) && !t.data.bound && (t.data.bound = !0, n[g] = o.toString(), n[v] = r.toString(), n.opacity = 1, b(i, n), e = t.elements.preview.getBoundingClientRect(), t._originalImageWidth = e.width, t._originalImageHeight = e.height, t.data.orientation = f(t.elements.img), t.options.enableZoom ? W.call(t, !0) : t._currentZoom = 1, o.scale = t._currentZoom, n[g] = o.toString(), b(i, n), t.data.points.length ? function (e) {
      if (4 !== e.length) throw "Croppie - Invalid number of points supplied: " + e;var t = this,
          n = e[2] - e[0],
          i = t.elements.viewport.getBoundingClientRect(),
          o = t.elements.boundary.getBoundingClientRect(),
          r = { left: i.left - o.left, top: i.top - o.top },
          a = i.width / n,
          s = e[1],
          l = e[0],
          u = -1 * e[1] + r.top,
          c = -1 * e[0] + r.left,
          h = {};h[v] = l + "px " + s + "px", h[g] = new E(c, u, a).toString(), b(t.elements.preview, h), B.call(t, a), t._currentZoom = a;
    }.call(t, t.data.points) : function () {
      var e = this,
          t = e.elements.preview.getBoundingClientRect(),
          n = e.elements.viewport.getBoundingClientRect(),
          i = e.elements.boundary.getBoundingClientRect(),
          o = n.left - i.left,
          r = n.top - i.top,
          a = o - (t.width - n.width) / 2,
          s = r - (t.height - n.height) / 2,
          l = new E(a, s, e._currentZoom);b(e.elements.preview, g, l.toString());
    }.call(t), Z.call(t), z.call(t));
  }function W(e) {
    var t,
        n,
        i,
        o,
        r = this,
        a = Math.max(r.options.minZoom, 0) || 0,
        s = r.options.maxZoom || 1.5,
        l = r.elements.zoomer,
        u = parseFloat(l.value),
        c = r.elements.boundary.getBoundingClientRect(),
        h = m(r.elements.img, r.data.orientation),
        p = r.elements.viewport.getBoundingClientRect();r.options.enforceBoundary && (i = p.width / h.width, o = p.height / h.height, a = Math.max(i, o)), s <= a && (s = a + 1), l.min = A(a, 4), l.max = A(s, 4), !e && (u < l.min || u > l.max) ? B.call(r, u < l.min ? l.min : l.max) : e && (n = Math.max(c.width / h.width, c.height / h.height), t = null !== r.data.boundZoom ? r.data.boundZoom : n, B.call(r, t)), y(l);
  }function O(e) {
    var t = e.points,
        n = C(t[0]),
        i = C(t[1]),
        o = C(t[2]) - n,
        r = C(t[3]) - i,
        a = e.circle,
        s = document.createElement("canvas"),
        l = s.getContext("2d"),
        u = e.outputWidth || o,
        c = e.outputHeight || r;s.width = u, s.height = c, e.backgroundColor && (l.fillStyle = e.backgroundColor, l.fillRect(0, 0, u, c));var h = n,
        p = i,
        d = o,
        m = r,
        f = 0,
        v = 0,
        g = u,
        w = c;return n < 0 && (h = 0, f = Math.abs(n) / o * u), d + h > this._originalImageWidth && (g = (d = this._originalImageWidth - h) / o * u), i < 0 && (p = 0, v = Math.abs(i) / r * c), m + p > this._originalImageHeight && (w = (m = this._originalImageHeight - p) / r * c), l.drawImage(this.elements.preview, h, p, d, m, f, v, g, w), a && (l.fillStyle = "#fff", l.globalCompositeOperation = "destination-in", l.beginPath(), l.arc(s.width / 2, s.height / 2, s.width / 2, 0, 2 * Math.PI, !0), l.closePath(), l.fill()), s;
  }function k(c, h) {
    var e,
        i,
        o,
        r,
        p = this,
        d = [],
        t = null,
        n = R.call(p);if ("string" == typeof c) e = c, c = {};else if (Array.isArray(c)) d = c.slice();else {
      if (void 0 === c && p.data.url) return Y.call(p), F.call(p), null;e = c.url, d = c.points || [], t = void 0 === c.zoom ? null : c.zoom;
    }return p.data.bound = !1, p.data.url = e || p.data.url, p.data.boundZoom = t, (i = e, o = n, r = new Image(), r.style.opacity = "0", new Promise(function (e, t) {
      function n() {
        r.style.opacity = "1", setTimeout(function () {
          e(r);
        }, 1);
      }r.removeAttribute("crossOrigin"), i.match(/^https?:\/\/|^\/\//) && r.setAttribute("crossOrigin", "anonymous"), r.onload = function () {
        o ? EXIF.getData(r, function () {
          n();
        }) : n();
      }, r.onerror = function (e) {
        r.style.opacity = 1, setTimeout(function () {
          t(e);
        }, 1);
      }, r.src = i;
    })).then(function (e) {
      if (function (t) {
        this.elements.img.parentNode && (Array.prototype.forEach.call(this.elements.img.classList, function (e) {
          t.classList.add(e);
        }), this.elements.img.parentNode.replaceChild(t, this.elements.img), this.elements.preview = t), this.elements.img = t;
      }.call(p, e), d.length) p.options.relative && (d = [d[0] * e.naturalWidth / 100, d[1] * e.naturalHeight / 100, d[2] * e.naturalWidth / 100, d[3] * e.naturalHeight / 100]);else {
        var t,
            n,
            i = m(e),
            o = p.elements.viewport.getBoundingClientRect(),
            r = o.width / o.height;r < i.width / i.height ? t = (n = i.height) * r : (t = i.width, n = i.height / r);var a = (i.width - t) / 2,
            s = (i.height - n) / 2,
            l = a + t,
            u = s + n;p.data.points = [a, s, l, u];
      }p.data.points = d.map(function (e) {
        return parseFloat(e);
      }), p.options.useCanvas && function (e) {
        var t = this.elements.canvas,
            n = this.elements.img;t.getContext("2d").clearRect(0, 0, t.width, t.height), t.width = n.width, t.height = n.height, _(t, n, this.options.enableOrientation && e || f(n));
      }.call(p, c.orientation), Y.call(p), F.call(p), h && h();
    });
  }function A(e, t) {
    return parseFloat(e).toFixed(t || 0);
  }function j() {
    var e = this,
        t = e.elements.preview.getBoundingClientRect(),
        n = e.elements.viewport.getBoundingClientRect(),
        i = n.left - t.left,
        o = n.top - t.top,
        r = (n.width - e.elements.viewport.offsetWidth) / 2,
        a = (n.height - e.elements.viewport.offsetHeight) / 2,
        s = i + e.elements.viewport.offsetWidth + r,
        l = o + e.elements.viewport.offsetHeight + a,
        u = e._currentZoom;(u === 1 / 0 || isNaN(u)) && (u = 1);var c = e.options.enforceBoundary ? 0 : Number.NEGATIVE_INFINITY;return i = Math.max(c, i / u), o = Math.max(c, o / u), s = Math.max(c, s / u), l = Math.max(c, l / u), { points: [A(i), A(o), A(s), A(l)], zoom: u, orientation: e.data.orientation };
  }var H = { type: "canvas", format: "png", quality: 1 },
      N = ["jpeg", "webp", "png"];function n(e) {
    var t = this,
        n = j.call(t),
        i = p(d(H), d(e)),
        o = "string" == typeof e ? e : i.type || "base64",
        r = i.size || "viewport",
        a = i.format,
        s = i.quality,
        l = i.backgroundColor,
        u = "boolean" == typeof i.circle ? i.circle : "circle" === t.options.viewport.type,
        c = t.elements.viewport.getBoundingClientRect(),
        h = c.width / c.height;return "viewport" === r ? (n.outputWidth = c.width, n.outputHeight = c.height) : "object" == (typeof r === "undefined" ? "undefined" : _typeof(r)) && (r.width && r.height ? (n.outputWidth = r.width, n.outputHeight = r.height) : r.width ? (n.outputWidth = r.width, n.outputHeight = r.width / h) : r.height && (n.outputWidth = r.height * h, n.outputHeight = r.height)), -1 < N.indexOf(a) && (n.format = "image/" + a, n.quality = s), n.circle = u, n.url = t.data.url, n.backgroundColor = l, new Promise(function (e) {
      switch (o.toLowerCase()) {case "rawcanvas":
          e(O.call(t, n));break;case "canvas":case "base64":
          e(function (e) {
            return O.call(this, e).toDataURL(e.format, e.quality);
          }.call(t, n));break;case "blob":
          (function (e) {
            var n = this;return new Promise(function (t) {
              O.call(n, e).toBlob(function (e) {
                t(e);
              }, e.format, e.quality);
            });
          }).call(t, n).then(e);break;default:
          e(function (e) {
            var t = e.points,
                n = document.createElement("div"),
                i = document.createElement("img"),
                o = t[2] - t[0],
                r = t[3] - t[1];return x(n, "croppie-result"), n.appendChild(i), b(i, { left: -1 * t[0] + "px", top: -1 * t[1] + "px" }), i.src = e.url, b(n, { width: o + "px", height: r + "px" }), n;
          }.call(t, n));}
    });
  }function S(e) {
    if (!this.options.useCanvas || !this.options.enableOrientation) throw "Croppie: Cannot rotate without enableOrientation && EXIF.js included";var t,
        n,
        i,
        o,
        r,
        a = this,
        s = a.elements.canvas;a.data.orientation = (t = a.data.orientation, n = e, i = -1 < l.indexOf(t) ? l : u, o = i.indexOf(t), r = n / 90 % i.length, i[(i.length + o + r % i.length) % i.length]), _(s, a.elements.img, a.data.orientation), Z.call(a, !0), W.call(a);
  }if (window.jQuery) {
    var P = window.jQuery;P.fn.croppie = function (n) {
      if ("string" !== typeof n) return this.each(function () {
        var e = new T(this, n);(e.$ = P)(this).data("croppie", e);
      });var i = Array.prototype.slice.call(arguments, 1),
          e = P(this).data("croppie");return "get" === n ? e.get() : "result" === n ? e.result.apply(e, i) : "bind" === n ? e.bind.apply(e, i) : this.each(function () {
        var e = P(this).data("croppie");if (e) {
          var t = e[n];if (!P.isFunction(t)) throw "Croppie " + n + " method not found";t.apply(e, i), "destroy" === n && P(this).removeData("croppie");
        }
      });
    };
  }function T(e, t) {
    if (-1 < e.className.indexOf("croppie-container")) throw new Error("Croppie: Can't initialize croppie more than once");if (this.element = e, this.options = p(d(T.defaults), t), "img" === this.element.tagName.toLowerCase()) {
      var n = this.element;x(n, "cr-original-image"), c(n, { "aria-hidden": "true", alt: "" });var i = document.createElement("div");this.element.parentNode.appendChild(i), i.appendChild(n), this.element = i, this.options.url = this.options.url || n.src;
    }if (r.call(this), this.options.url) {
      var o = { url: this.options.url, points: this.options.points };delete this.options.url, delete this.options.points, k.call(this, o);
    }
  }return T.defaults = { viewport: { width: 100, height: 100, type: "square" }, boundary: {}, orientationControls: { enabled: !0, leftClass: "", rightClass: "" }, resizeControls: { width: !0, height: !0 }, customClass: "", showZoomer: !0, enableZoom: !0, enableResize: !1, mouseWheelZoom: !0, enableExif: !1, enforceBoundary: !0, enableOrientation: !1, enableKeyMovement: !0, update: function update() {} }, T.globals = { translate: "translate3d" }, p(T.prototype, { bind: function bind(e, t) {
      return k.call(this, e, t);
    }, get: function get() {
      var e = j.call(this),
          t = e.points;return this.options.relative && (t[0] /= this.elements.img.naturalWidth / 100, t[1] /= this.elements.img.naturalHeight / 100, t[2] /= this.elements.img.naturalWidth / 100, t[3] /= this.elements.img.naturalHeight / 100), e;
    }, result: function result(e) {
      return n.call(this, e);
    }, refresh: function refresh() {
      return function () {
        Y.call(this);
      }.call(this);
    }, setZoom: function setZoom(e) {
      B.call(this, e), y(this.elements.zoomer);
    }, rotate: function rotate(e) {
      S.call(this, e);
    }, destroy: function destroy() {
      return function () {
        var e,
            t,
            n = this;n.element.removeChild(n.elements.boundary), e = n.element, t = "croppie-container", e.classList ? e.classList.remove(t) : e.className = e.className.replace(t, ""), n.options.enableZoom && n.element.removeChild(n.elements.zoomerWrap), delete n.elements;
      }.call(this);
    } }), T;
});
"use strict";

var _typeof2 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _typeof(t) {
    return (_typeof = "function" == typeof Symbol && "symbol" == _typeof2(Symbol.iterator) ? function (t) {
        return typeof t === "undefined" ? "undefined" : _typeof2(t);
    } : function (t) {
        return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t === "undefined" ? "undefined" : _typeof2(t);
    })(t);
}
/*!
 * HC-Sticky
 * =========
 * Version: 2.2.3
 * Author: Some Web Media
 * Author URL: http://somewebmedia.com
 * Plugin URL: https://github.com/somewebmedia/hc-sticky
 * Description: Cross-browser plugin that makes any element on your page visible while you scroll
 * License: MIT
 */
/*!
 * HC-Sticky
 * =========
 * Version: 2.2.3
 * Author: Some Web Media
 * Author URL: http://somewebmedia.com
 * Plugin URL: https://github.com/somewebmedia/hc-sticky
 * Description: Cross-browser plugin that makes any element on your page visible while you scroll
 * License: MIT
 */
!function (t, e) {
    "use strict";
    if ("object" === ("undefined" == typeof module ? "undefined" : _typeof(module)) && "object" === _typeof(module.exports)) {
        if (!t.document) throw new Error("HC-Sticky requires a browser to run.");module.exports = e(t);
    } else "function" == typeof define && define.amd ? define("hcSticky", [], e(t)) : e(t);
}("undefined" != typeof window ? window : undefined, function (_) {
    "use strict";
    var U = { top: 0, bottom: 0, bottomEnd: 0, innerTop: 0, innerSticker: null, stickyClass: "sticky", stickTo: null, followScroll: !0, responsive: null, mobileFirst: !1, onStart: null, onStop: null, onBeforeResize: null, onResize: null, resizeDebounce: 100, disable: !1, queries: null, queryFlow: "down" },
        Y = function Y(t, e, o) {
        console.warn("%cHC Sticky:%c " + o + "%c '" + t + "'%c is now deprecated and will be removed. Use%c '" + e + "'%c instead.", "color: #fa253b", "color: default", "color: #5595c6", "color: default", "color: #5595c6", "color: default");
    },
        $ = _.document,
        Q = function Q(i) {
        var o = this,
            f = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : {};if ("string" == typeof i && (i = $.querySelector(i)), !i) return !1;f.queries && Y("queries", "responsive", "option"), f.queryFlow && Y("queryFlow", "mobileFirst", "option");var p = {},
            u = Q.Helpers,
            s = i.parentNode;"static" === u.getStyle(s, "position") && (s.style.position = "relative");var r,
            l,
            a,
            c,
            d,
            y,
            m,
            g,
            h,
            b,
            v,
            S,
            w,
            k,
            E,
            x,
            L,
            T,
            j,
            O = function O() {
            var t = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : {};u.isEmptyObject(t) && !u.isEmptyObject(p) || (p = Object.assign({}, U, p, t));
        },
            t = function t() {
            return p.disable;
        },
            e = function e() {
            var t,
                e = p.responsive || p.queries;if (e) {
                var o = _.innerWidth;if (t = f, (p = Object.assign({}, U, t || {})).mobileFirst) for (var n in e) {
                    n <= o && !u.isEmptyObject(e[n]) && O(e[n]);
                } else {
                    var i = [];for (var s in e) {
                        var r = {};r[s] = e[s], i.push(r);
                    }for (var l = i.length - 1; 0 <= l; l--) {
                        var a = i[l],
                            c = Object.keys(a)[0];o <= c && !u.isEmptyObject(a[c]) && O(a[c]);
                    }
                }
            }
        },
            C = { css: {}, position: null, stick: function stick() {
                var t = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : {};u.hasClass(i, p.stickyClass) || (!1 === z.isAttached && z.attach(), C.position = "fixed", i.style.position = "fixed", i.style.left = z.offsetLeft + "px", i.style.width = z.width, void 0 === t.bottom ? i.style.bottom = "auto" : i.style.bottom = t.bottom + "px", void 0 === t.top ? i.style.top = "auto" : i.style.top = t.top + "px", i.classList ? i.classList.add(p.stickyClass) : i.className += " " + p.stickyClass, p.onStart && p.onStart.call(i, Object.assign({}, p)));
            }, release: function release() {
                var t = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : {};if (t.stop = t.stop || !1, !0 === t.stop || "fixed" === C.position || null === C.position || !(void 0 === t.top && void 0 === t.bottom || void 0 !== t.top && (parseInt(u.getStyle(i, "top")) || 0) === t.top || void 0 !== t.bottom && (parseInt(u.getStyle(i, "bottom")) || 0) === t.bottom)) {
                    !0 === t.stop ? !0 === z.isAttached && z.detach() : !1 === z.isAttached && z.attach();var e = t.position || C.css.position;C.position = e, i.style.position = e, i.style.left = !0 === t.stop ? C.css.left : z.positionLeft + "px", i.style.width = "absolute" !== e ? C.css.width : z.width, void 0 === t.bottom ? i.style.bottom = !0 === t.stop ? "" : "auto" : i.style.bottom = t.bottom + "px", void 0 === t.top ? i.style.top = !0 === t.stop ? "" : "auto" : i.style.top = t.top + "px", i.classList ? i.classList.remove(p.stickyClass) : i.className = i.className.replace(new RegExp("(^|\\b)" + p.stickyClass.split(" ").join("|") + "(\\b|$)", "gi"), " "), p.onStop && p.onStop.call(i, Object.assign({}, p));
                }
            } },
            z = { el: $.createElement("div"), offsetLeft: null, positionLeft: null, width: null, isAttached: !1, init: function init() {
                for (var t in z.el.className = "sticky-spacer", C.css) {
                    z.el.style[t] = C.css[t];
                }z.el.style["z-index"] = "-1";var e = u.getStyle(i);z.offsetLeft = u.offset(i).left - (parseInt(e.marginLeft) || 0), z.positionLeft = u.position(i).left, z.width = u.getStyle(i, "width");
            }, attach: function attach() {
                s.insertBefore(z.el, i), z.isAttached = !0;
            }, detach: function detach() {
                z.el = s.removeChild(z.el), z.isAttached = !1;
            } },
            n = function n() {
            var t, e, o, n;C.css = (t = i, e = u.getCascadedStyle(t), o = u.getStyle(t), n = { height: t.offsetHeight + "px", left: e.left, right: e.right, top: e.top, bottom: e.bottom, position: o.position, display: o.display, verticalAlign: o.verticalAlign, boxSizing: o.boxSizing, marginLeft: e.marginLeft, marginRight: e.marginRight, marginTop: e.marginTop, marginBottom: e.marginBottom, paddingLeft: e.paddingLeft, paddingRight: e.paddingRight }, e.float && (n.float = e.float || "none"), e.cssFloat && (n.cssFloat = e.cssFloat || "none"), o.MozBoxSizing && (n.MozBoxSizing = o.MozBoxSizing), n.width = "auto" !== e.width ? e.width : "border-box" === n.boxSizing || "border-box" === n.MozBoxSizing ? t.offsetWidth + "px" : o.width, n), z.init(), r = !(!p.stickTo || !("document" === p.stickTo || p.stickTo.nodeType && 9 === p.stickTo.nodeType || "object" === _typeof(p.stickTo) && p.stickTo instanceof ("undefined" != typeof HTMLDocument ? HTMLDocument : Document))), l = p.stickTo ? r ? $ : "string" == typeof p.stickTo ? $.querySelector(p.stickTo) : p.stickTo : s, E = (T = function T() {
                var t = i.offsetHeight + (parseInt(C.css.marginTop) || 0) + (parseInt(C.css.marginBottom) || 0),
                    e = (E || 0) - t;return -1 <= e && e <= 1 ? E : t;
            })(), c = (L = function L() {
                return r ? Math.max($.documentElement.clientHeight, $.body.scrollHeight, $.documentElement.scrollHeight, $.body.offsetHeight, $.documentElement.offsetHeight) : l.offsetHeight;
            })(), d = r ? 0 : u.offset(l).top, y = p.stickTo ? r ? 0 : u.offset(s).top : d, m = _.innerHeight, x = i.offsetTop - (parseInt(C.css.marginTop) || 0), a = p.innerSticker ? "string" == typeof p.innerSticker ? $.querySelector(p.innerSticker) : p.innerSticker : null, g = isNaN(p.top) && -1 < p.top.indexOf("%") ? parseFloat(p.top) / 100 * m : p.top, h = isNaN(p.bottom) && -1 < p.bottom.indexOf("%") ? parseFloat(p.bottom) / 100 * m : p.bottom, b = a ? a.offsetTop : p.innerTop ? p.innerTop : 0, v = isNaN(p.bottomEnd) && -1 < p.bottomEnd.indexOf("%") ? parseFloat(p.bottomEnd) / 100 * m : p.bottomEnd, S = d - g + b + x;
        },
            N = _.pageYOffset || $.documentElement.scrollTop,
            H = 0,
            R = function R() {
            E = T(), c = L(), w = d + c - g - v, k = m < E;var t,
                e = _.pageYOffset || $.documentElement.scrollTop,
                o = u.offset(i).top,
                n = o - e;j = e < N ? "up" : "down", H = e - N, S < (N = e) ? w + g + (k ? h : 0) - (p.followScroll && k ? 0 : g) <= e + E - b - (m - (S - b) < E - b && p.followScroll && 0 < (t = E - m - b) ? t : 0) ? C.release({ position: "absolute", bottom: y + s.offsetHeight - w - g }) : k && p.followScroll ? "down" === j ? n + E + h <= m + .9 ? C.stick({ bottom: h }) : "fixed" === C.position && C.release({ position: "absolute", top: o - g - S - H + b }) : Math.ceil(n + b) < 0 && "fixed" === C.position ? C.release({ position: "absolute", top: o - g - S + b - H }) : e + g - b <= o && C.stick({ top: g - b }) : C.stick({ top: g - b }) : C.release({ stop: !0 });
        },
            A = !1,
            B = !1,
            I = function I() {
            A && (u.event.unbind(_, "scroll", R), A = !1);
        },
            q = function q() {
            null !== i.offsetParent && "none" !== u.getStyle(i, "display") ? (n(), c <= E ? I() : (R(), A || (u.event.bind(_, "scroll", R), A = !0))) : I();
        },
            F = function F() {
            i.style.position = "", i.style.left = "", i.style.top = "", i.style.bottom = "", i.style.width = "", i.classList ? i.classList.remove(p.stickyClass) : i.className = i.className.replace(new RegExp("(^|\\b)" + p.stickyClass.split(" ").join("|") + "(\\b|$)", "gi"), " "), C.css = {}, !(C.position = null) === z.isAttached && z.detach();
        },
            M = function M() {
            F(), e(), t() ? I() : q();
        },
            D = function D() {
            p.onBeforeResize && p.onBeforeResize.call(i, Object.assign({}, p)), M(), p.onResize && p.onResize.call(i, Object.assign({}, p));
        },
            P = p.resizeDebounce ? u.debounce(D, p.resizeDebounce) : D,
            W = function W() {
            B && (u.event.unbind(_, "resize", P), B = !1), I();
        },
            V = function V() {
            B || (u.event.bind(_, "resize", P), B = !0), e(), t() ? I() : q();
        };this.options = function (t) {
            return t ? p[t] : Object.assign({}, p);
        }, this.refresh = M, this.update = function (t) {
            O(t), f = Object.assign({}, f, t || {}), M();
        }, this.attach = V, this.detach = W, this.destroy = function () {
            W(), F();
        }, this.triggerMethod = function (t, e) {
            "function" == typeof o[t] && o[t](e);
        }, this.reinit = function () {
            Y("reinit", "refresh", "method"), M();
        }, O(f), V(), u.event.bind(_, "load", M);
    };if (void 0 !== _.jQuery) {
        var n = _.jQuery,
            i = "hcSticky";n.fn.extend({ hcSticky: function hcSticky(e, o) {
                return this.length ? "options" === e ? n.data(this.get(0), i).options() : this.each(function () {
                    var t = n.data(this, i);t ? t.triggerMethod(e, o) : (t = new Q(this, e), n.data(this, i, t));
                }) : this;
            } });
    }return _.hcSticky = _.hcSticky || Q, Q;
}), function (c) {
    "use strict";
    var t = c.hcSticky,
        f = c.document;"function" != typeof Object.assign && Object.defineProperty(Object, "assign", { value: function value(t, e) {
            if (null == t) throw new TypeError("Cannot convert undefined or null to object");for (var o = Object(t), n = 1; n < arguments.length; n++) {
                var i = arguments[n];if (null != i) for (var s in i) {
                    Object.prototype.hasOwnProperty.call(i, s) && (o[s] = i[s]);
                }
            }return o;
        }, writable: !0, configurable: !0 }), Array.prototype.forEach || (Array.prototype.forEach = function (t) {
        var e, o;if (null == this) throw new TypeError("this is null or not defined");var n = Object(this),
            i = n.length >>> 0;if ("function" != typeof t) throw new TypeError(t + " is not a function");for (1 < arguments.length && (e = arguments[1]), o = 0; o < i;) {
            var s;o in n && (s = n[o], t.call(e, s, o, n)), o++;
        }
    });var e = function () {
        var t = f.documentElement,
            e = function e() {};function n(t) {
            var e = c.event;return e.target = e.target || e.srcElement || t, e;
        }t.addEventListener ? e = function e(t, _e, o) {
            t.addEventListener(_e, o, !1);
        } : t.attachEvent && (e = function e(_e2, t, o) {
            _e2[t + o] = o.handleEvent ? function () {
                var t = n(_e2);o.handleEvent.call(o, t);
            } : function () {
                var t = n(_e2);o.call(_e2, t);
            }, _e2.attachEvent("on" + t, _e2[t + o]);
        });var o = function o() {};return t.removeEventListener ? o = function o(t, e, _o) {
            t.removeEventListener(e, _o, !1);
        } : t.detachEvent && (o = function o(e, _o2, n) {
            e.detachEvent("on" + _o2, e[_o2 + n]);try {
                delete e[_o2 + n];
            } catch (t) {
                e[_o2 + n] = void 0;
            }
        }), { bind: e, unbind: o };
    }(),
        r = function r(t, e) {
        return c.getComputedStyle ? e ? f.defaultView.getComputedStyle(t, null).getPropertyValue(e) : f.defaultView.getComputedStyle(t, null) : t.currentStyle ? e ? t.currentStyle[e.replace(/-\w/g, function (t) {
            return t.toUpperCase().replace("-", "");
        })] : t.currentStyle : void 0;
    },
        l = function l(t) {
        var e = t.getBoundingClientRect(),
            o = c.pageYOffset || f.documentElement.scrollTop,
            n = c.pageXOffset || f.documentElement.scrollLeft;return { top: e.top + o, left: e.left + n };
    };t.Helpers = { isEmptyObject: function isEmptyObject(t) {
            for (var e in t) {
                return !1;
            }return !0;
        }, debounce: function debounce(n, i, s) {
            var r;return function () {
                var t = this,
                    e = arguments,
                    o = s && !r;clearTimeout(r), r = setTimeout(function () {
                    r = null, s || n.apply(t, e);
                }, i), o && n.apply(t, e);
            };
        }, hasClass: function hasClass(t, e) {
            return t.classList ? t.classList.contains(e) : new RegExp("(^| )" + e + "( |$)", "gi").test(t.className);
        }, offset: l, position: function position(t) {
            var e = t.offsetParent,
                o = l(e),
                n = l(t),
                i = r(e),
                s = r(t);return o.top += parseInt(i.borderTopWidth) || 0, o.left += parseInt(i.borderLeftWidth) || 0, { top: n.top - o.top - (parseInt(s.marginTop) || 0), left: n.left - o.left - (parseInt(s.marginLeft) || 0) };
        }, getStyle: r, getCascadedStyle: function getCascadedStyle(t) {
            var e,
                o = t.cloneNode(!0);o.style.display = "none", Array.prototype.slice.call(o.querySelectorAll('input[type="radio"]')).forEach(function (t) {
                t.removeAttribute("name");
            }), t.parentNode.insertBefore(o, t.nextSibling), o.currentStyle ? e = o.currentStyle : c.getComputedStyle && (e = f.defaultView.getComputedStyle(o, null));var n = {};for (var i in e) {
                !isNaN(i) || "string" != typeof e[i] && "number" != typeof e[i] || (n[i] = e[i]);
            }if (Object.keys(n).length < 3) for (var s in n = {}, e) {
                isNaN(s) || (n[e[s].replace(/-\w/g, function (t) {
                    return t.toUpperCase().replace("-", "");
                })] = e.getPropertyValue(e[s]));
            }if (n.margin || "auto" !== n.marginLeft ? n.margin || n.marginLeft !== n.marginRight || n.marginLeft !== n.marginTop || n.marginLeft !== n.marginBottom || (n.margin = n.marginLeft) : n.margin = "auto", !n.margin && "0px" === n.marginLeft && "0px" === n.marginRight) {
                var r = t.offsetLeft - t.parentNode.offsetLeft,
                    l = r - (parseInt(n.left) || 0) - (parseInt(n.right) || 0),
                    a = t.parentNode.offsetWidth - t.offsetWidth - r - (parseInt(n.right) || 0) + (parseInt(n.left) || 0) - l;0 !== a && 1 !== a || (n.margin = "auto");
            }return o.parentNode.removeChild(o), o = null, n;
        }, event: e };
}(window);
jQuery(document).ready(function () {
    if (jQuery(window).width() > 767) {
        if (jQuery('.single-post').length) {
            jQuery('.single-post .vc_col-lg-4').hcSticky({
                stickTo: '.single-post .vc_col-lg-8',
                top: 180
            });
        }
    }
});
"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*
== malihu jquery custom scrollbar plugin == 
Version: 3.1.5 
Plugin URI: http://manos.malihu.gr/jquery-custom-content-scroller 
Author: malihu
Author URI: http://manos.malihu.gr
License: MIT License (MIT)
*/

/*
Copyright Manos Malihutsakis (email: manos@malihu.gr)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

/*
The code below is fairly long, fully commented and should be normally used in development. 
For production, use either the minified jquery.mCustomScrollbar.min.js script or 
the production-ready jquery.mCustomScrollbar.concat.min.js which contains the plugin 
and dependencies (minified). 
*/

(function (factory) {
	if (typeof define === "function" && define.amd) {
		define(["jquery"], factory);
	} else if (typeof module !== "undefined" && module.exports) {
		module.exports = factory;
	} else {
		factory(jQuery, window, document);
	}
})(function ($) {
	(function (init) {
		var _rjs = typeof define === "function" && define.amd,
		    /* RequireJS */
		_njs = typeof module !== "undefined" && module.exports,
		    /* NodeJS */
		_dlp = "https:" == document.location.protocol ? "https:" : "http:",
		    /* location protocol */
		_url = "cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js";
		if (!_rjs) {
			if (_njs) {
				require("jquery-mousewheel")($);
			} else {
				/* load jquery-mousewheel plugin (via CDN) if it's not present or not loaded via RequireJS 
    (works when mCustomScrollbar fn is called on window load) */
				$.event.special.mousewheel || $("head").append(decodeURI("%3Cscript src=" + _dlp + "//" + _url + "%3E%3C/script%3E"));
			}
		}
		init();
	})(function () {

		/* 
  ----------------------------------------
  PLUGIN NAMESPACE, PREFIX, DEFAULT SELECTOR(S) 
  ----------------------------------------
  */

		var pluginNS = "mCustomScrollbar",
		    pluginPfx = "mCS",
		    defaultSelector = ".mCustomScrollbar",


		/* 
  ----------------------------------------
  DEFAULT OPTIONS 
  ----------------------------------------
  */

		defaults = {
			/*
   set element/content width/height programmatically 
   values: boolean, pixels, percentage 
   	option						default
   	-------------------------------------
   	setWidth					false
   	setHeight					false
   */
			/*
   set the initial css top property of content  
   values: string (e.g. "-100px", "10%" etc.)
   */
			setTop: 0,
			/*
   set the initial css left property of content  
   values: string (e.g. "-100px", "10%" etc.)
   */
			setLeft: 0,
			/* 
   scrollbar axis (vertical and/or horizontal scrollbars) 
   values (string): "y", "x", "yx"
   */
			axis: "y",
			/*
   position of scrollbar relative to content  
   values (string): "inside", "outside" ("outside" requires elements with position:relative)
   */
			scrollbarPosition: "inside",
			/*
   scrolling inertia
   values: integer (milliseconds)
   */
			scrollInertia: 950,
			/* 
   auto-adjust scrollbar dragger length
   values: boolean
   */
			autoDraggerLength: true,
			/*
   auto-hide scrollbar when idle 
   values: boolean
   	option						default
   	-------------------------------------
   	autoHideScrollbar			false
   */
			/*
   auto-expands scrollbar on mouse-over and dragging
   values: boolean
   	option						default
   	-------------------------------------
   	autoExpandScrollbar			false
   */
			/*
   always show scrollbar, even when there's nothing to scroll 
   values: integer (0=disable, 1=always show dragger rail and buttons, 2=always show dragger rail, dragger and buttons), boolean
   */
			alwaysShowScrollbar: 0,
			/*
   scrolling always snaps to a multiple of this number in pixels
   values: integer, array ([y,x])
   	option						default
   	-------------------------------------
   	snapAmount					null
   */
			/*
   when snapping, snap with this number in pixels as an offset 
   values: integer
   */
			snapOffset: 0,
			/* 
   mouse-wheel scrolling
   */
			mouseWheel: {
				/* 
    enable mouse-wheel scrolling
    values: boolean
    */
				enable: true,
				/* 
    scrolling amount in pixels
    values: "auto", integer 
    */
				scrollAmount: "auto",
				/* 
    mouse-wheel scrolling axis 
    the default scrolling direction when both vertical and horizontal scrollbars are present 
    values (string): "y", "x" 
    */
				axis: "y",
				/* 
    prevent the default behaviour which automatically scrolls the parent element(s) when end of scrolling is reached 
    values: boolean
    	option						default
    	-------------------------------------
    	preventDefault				null
    */
				/*
    the reported mouse-wheel delta value. The number of lines (translated to pixels) one wheel notch scrolls.  
    values: "auto", integer 
    "auto" uses the default OS/browser value 
    */
				deltaFactor: "auto",
				/*
    normalize mouse-wheel delta to -1 or 1 (disables mouse-wheel acceleration) 
    values: boolean
    	option						default
    	-------------------------------------
    	normalizeDelta				null
    */
				/*
    invert mouse-wheel scrolling direction 
    values: boolean
    	option						default
    	-------------------------------------
    	invert						null
    */
				/*
    the tags that disable mouse-wheel when cursor is over them
    */
				disableOver: ["select", "option", "keygen", "datalist", "textarea"]
			},
			/* 
   scrollbar buttons
   */
			scrollButtons: {
				/*
    enable scrollbar buttons
    values: boolean
    	option						default
    	-------------------------------------
    	enable						null
    */
				/*
    scrollbar buttons scrolling type 
    values (string): "stepless", "stepped"
    */
				scrollType: "stepless",
				/*
    scrolling amount in pixels
    values: "auto", integer 
    */
				scrollAmount: "auto"
				/*
    tabindex of the scrollbar buttons
    values: false, integer
    	option						default
    	-------------------------------------
    	tabindex					null
    */
			},
			/* 
   keyboard scrolling
   */
			keyboard: {
				/*
    enable scrolling via keyboard
    values: boolean
    */
				enable: true,
				/*
    keyboard scrolling type 
    values (string): "stepless", "stepped"
    */
				scrollType: "stepless",
				/*
    scrolling amount in pixels
    values: "auto", integer 
    */
				scrollAmount: "auto"
			},
			/*
   enable content touch-swipe scrolling 
   values: boolean, integer, string (number)
   integer values define the axis-specific minimum amount required for scrolling momentum
   */
			contentTouchScroll: 25,
			/*
   enable/disable document (default) touch-swipe scrolling 
   */
			documentTouchScroll: true,
			/*
   advanced option parameters
   */
			advanced: {
				/*
    auto-expand content horizontally (for "x" or "yx" axis) 
    values: boolean, integer (the value 2 forces the non scrollHeight/scrollWidth method, the value 3 forces the scrollHeight/scrollWidth method)
    	option						default
    	-------------------------------------
    	autoExpandHorizontalScroll	null
    */
				/*
    auto-scroll to elements with focus
    */
				autoScrollOnFocus: "input,textarea,select,button,datalist,keygen,a[tabindex],area,object,[contenteditable='true']",
				/*
    auto-update scrollbars on content, element or viewport resize 
    should be true for fluid layouts/elements, adding/removing content dynamically, hiding/showing elements, content with images etc. 
    values: boolean
    */
				updateOnContentResize: true,
				/*
    auto-update scrollbars each time each image inside the element is fully loaded 
    values: "auto", boolean
    */
				updateOnImageLoad: "auto",
				/*
    auto-update scrollbars based on the amount and size changes of specific selectors 
    useful when you need to update the scrollbar(s) automatically, each time a type of element is added, removed or changes its size 
    values: boolean, string (e.g. "ul li" will auto-update scrollbars each time list-items inside the element are changed) 
    a value of true (boolean) will auto-update scrollbars each time any element is changed
    	option						default
    	-------------------------------------
    	updateOnSelectorChange		null
    */
				/*
    extra selectors that'll allow scrollbar dragging upon mousemove/up, pointermove/up, touchend etc. (e.g. "selector-1, selector-2")
    	option						default
    	-------------------------------------
    	extraDraggableSelectors		null
    */
				/*
    extra selectors that'll release scrollbar dragging upon mouseup, pointerup, touchend etc. (e.g. "selector-1, selector-2")
    	option						default
    	-------------------------------------
    	releaseDraggableSelectors	null
    */
				/*
    auto-update timeout 
    values: integer (milliseconds)
    */
				autoUpdateTimeout: 60
			},
			/* 
   scrollbar theme 
   values: string (see CSS/plugin URI for a list of ready-to-use themes)
   */
			theme: "light",
			/*
   user defined callback functions
   */
			callbacks: {
				/*
    Available callbacks: 
    	callback					default
    	-------------------------------------
    	onCreate					null
    	onInit						null
    	onScrollStart				null
    	onScroll					null
    	onTotalScroll				null
    	onTotalScrollBack			null
    	whileScrolling				null
    	onOverflowY					null
    	onOverflowX					null
    	onOverflowYNone				null
    	onOverflowXNone				null
    	onImageLoad					null
    	onSelectorChange			null
    	onBeforeUpdate				null
    	onUpdate					null
    */
				onTotalScrollOffset: 0,
				onTotalScrollBackOffset: 0,
				alwaysTriggerOffsets: true
				/*
    add scrollbar(s) on all elements matching the current selector, now and in the future 
    values: boolean, string 
    string values: "on" (enable), "once" (disable after first invocation), "off" (disable)
    liveSelector values: string (selector)
    	option						default
    	-------------------------------------
    	live						false
    	liveSelector				null
    */
			} },


		/* 
  ----------------------------------------
  VARS, CONSTANTS 
  ----------------------------------------
  */

		totalInstances = 0,
		    /* plugin instances amount */
		liveTimers = {},
		    /* live option timers */
		oldIE = window.attachEvent && !window.addEventListener ? 1 : 0,
		    /* detect IE < 9 */
		touchActive = false,
		    touchable,
		    /* global touch vars (for touch and pointer events) */
		/* general plugin classes */
		classes = ["mCSB_dragger_onDrag", "mCSB_scrollTools_onDrag", "mCS_img_loaded", "mCS_disabled", "mCS_destroyed", "mCS_no_scrollbar", "mCS-autoHide", "mCS-dir-rtl", "mCS_no_scrollbar_y", "mCS_no_scrollbar_x", "mCS_y_hidden", "mCS_x_hidden", "mCSB_draggerContainer", "mCSB_buttonUp", "mCSB_buttonDown", "mCSB_buttonLeft", "mCSB_buttonRight"],


		/* 
  ----------------------------------------
  METHODS 
  ----------------------------------------
  */

		methods = {

			/* 
   plugin initialization method 
   creates the scrollbar(s), plugin data object and options
   ----------------------------------------
   */

			init: function init(options) {

				var options = $.extend(true, {}, defaults, options),
				    selector = _selector.call(this); /* validate selector */

				/* 
    if live option is enabled, monitor for elements matching the current selector and 
    apply scrollbar(s) when found (now and in the future) 
    */
				if (options.live) {
					var liveSelector = options.liveSelector || this.selector || defaultSelector,
					    /* live selector(s) */
					$liveSelector = $(liveSelector); /* live selector(s) as jquery object */
					if (options.live === "off") {
						/* 
      disable live if requested 
      usage: $(selector).mCustomScrollbar({live:"off"}); 
      */
						removeLiveTimers(liveSelector);
						return;
					}
					liveTimers[liveSelector] = setTimeout(function () {
						/* call mCustomScrollbar fn on live selector(s) every half-second */
						$liveSelector.mCustomScrollbar(options);
						if (options.live === "once" && $liveSelector.length) {
							/* disable live after first invocation */
							removeLiveTimers(liveSelector);
						}
					}, 500);
				} else {
					removeLiveTimers(liveSelector);
				}

				/* options backward compatibility (for versions < 3.0.0) and normalization */
				options.setWidth = options.set_width ? options.set_width : options.setWidth;
				options.setHeight = options.set_height ? options.set_height : options.setHeight;
				options.axis = options.horizontalScroll ? "x" : _findAxis(options.axis);
				options.scrollInertia = options.scrollInertia > 0 && options.scrollInertia < 17 ? 17 : options.scrollInertia;
				if (_typeof(options.mouseWheel) !== "object" && options.mouseWheel == true) {
					/* old school mouseWheel option (non-object) */
					options.mouseWheel = { enable: true, scrollAmount: "auto", axis: "y", preventDefault: false, deltaFactor: "auto", normalizeDelta: false, invert: false };
				}
				options.mouseWheel.scrollAmount = !options.mouseWheelPixels ? options.mouseWheel.scrollAmount : options.mouseWheelPixels;
				options.mouseWheel.normalizeDelta = !options.advanced.normalizeMouseWheelDelta ? options.mouseWheel.normalizeDelta : options.advanced.normalizeMouseWheelDelta;
				options.scrollButtons.scrollType = _findScrollButtonsType(options.scrollButtons.scrollType);

				_theme(options); /* theme-specific options */

				/* plugin constructor */
				return $(selector).each(function () {

					var $this = $(this);

					if (!$this.data(pluginPfx)) {
						/* prevent multiple instantiations */

						/* store options and create objects in jquery data */
						$this.data(pluginPfx, {
							idx: ++totalInstances, /* instance index */
							opt: options, /* options */
							scrollRatio: { y: null, x: null }, /* scrollbar to content ratio */
							overflowed: null, /* overflowed axis */
							contentReset: { y: null, x: null }, /* object to check when content resets */
							bindEvents: false, /* object to check if events are bound */
							tweenRunning: false, /* object to check if tween is running */
							sequential: {}, /* sequential scrolling object */
							langDir: $this.css("direction"), /* detect/store direction (ltr or rtl) */
							cbOffsets: null, /* object to check whether callback offsets always trigger */
							/* 
       object to check how scrolling events where last triggered 
       "internal" (default - triggered by this script), "external" (triggered by other scripts, e.g. via scrollTo method) 
       usage: object.data("mCS").trigger
       */
							trigger: null,
							/* 
       object to check for changes in elements in order to call the update method automatically 
       */
							poll: { size: { o: 0, n: 0 }, img: { o: 0, n: 0 }, change: { o: 0, n: 0 } }
						});

						var d = $this.data(pluginPfx),
						    o = d.opt,

						/* HTML data attributes */
						htmlDataAxis = $this.data("mcs-axis"),
						    htmlDataSbPos = $this.data("mcs-scrollbar-position"),
						    htmlDataTheme = $this.data("mcs-theme");

						if (htmlDataAxis) {
							o.axis = htmlDataAxis;
						} /* usage example: data-mcs-axis="y" */
						if (htmlDataSbPos) {
							o.scrollbarPosition = htmlDataSbPos;
						} /* usage example: data-mcs-scrollbar-position="outside" */
						if (htmlDataTheme) {
							/* usage example: data-mcs-theme="minimal" */
							o.theme = htmlDataTheme;
							_theme(o); /* theme-specific options */
						}

						_pluginMarkup.call(this); /* add plugin markup */

						if (d && o.callbacks.onCreate && typeof o.callbacks.onCreate === "function") {
							o.callbacks.onCreate.call(this);
						} /* callbacks: onCreate */

						$("#mCSB_" + d.idx + "_container img:not(." + classes[2] + ")").addClass(classes[2]); /* flag loaded images */

						methods.update.call(null, $this); /* call the update method */
					}
				});
			},
			/* ---------------------------------------- */

			/* 
   plugin update method 
   updates content and scrollbar(s) values, events and status 
   ----------------------------------------
   usage: $(selector).mCustomScrollbar("update");
   */

			update: function update(el, cb) {

				var selector = el || _selector.call(this); /* validate selector */

				return $(selector).each(function () {

					var $this = $(this);

					if ($this.data(pluginPfx)) {
						/* check if plugin has initialized */

						var d = $this.data(pluginPfx),
						    o = d.opt,
						    mCSB_container = $("#mCSB_" + d.idx + "_container"),
						    mCustomScrollBox = $("#mCSB_" + d.idx),
						    mCSB_dragger = [$("#mCSB_" + d.idx + "_dragger_vertical"), $("#mCSB_" + d.idx + "_dragger_horizontal")];

						if (!mCSB_container.length) {
							return;
						}

						if (d.tweenRunning) {
							_stop($this);
						} /* stop any running tweens while updating */

						if (cb && d && o.callbacks.onBeforeUpdate && typeof o.callbacks.onBeforeUpdate === "function") {
							o.callbacks.onBeforeUpdate.call(this);
						} /* callbacks: onBeforeUpdate */

						/* if element was disabled or destroyed, remove class(es) */
						if ($this.hasClass(classes[3])) {
							$this.removeClass(classes[3]);
						}
						if ($this.hasClass(classes[4])) {
							$this.removeClass(classes[4]);
						}

						/* css flexbox fix, detect/set max-height */
						mCustomScrollBox.css("max-height", "none");
						if (mCustomScrollBox.height() !== $this.height()) {
							mCustomScrollBox.css("max-height", $this.height());
						}

						_expandContentHorizontally.call(this); /* expand content horizontally */

						if (o.axis !== "y" && !o.advanced.autoExpandHorizontalScroll) {
							mCSB_container.css("width", _contentWidth(mCSB_container));
						}

						d.overflowed = _overflowed.call(this); /* determine if scrolling is required */

						_scrollbarVisibility.call(this); /* show/hide scrollbar(s) */

						/* auto-adjust scrollbar dragger length analogous to content */
						if (o.autoDraggerLength) {
							_setDraggerLength.call(this);
						}

						_scrollRatio.call(this); /* calculate and store scrollbar to content ratio */

						_bindEvents.call(this); /* bind scrollbar events */

						/* reset scrolling position and/or events */
						var to = [Math.abs(mCSB_container[0].offsetTop), Math.abs(mCSB_container[0].offsetLeft)];
						if (o.axis !== "x") {
							/* y/yx axis */
							if (!d.overflowed[0]) {
								/* y scrolling is not required */
								_resetContentPosition.call(this); /* reset content position */
								if (o.axis === "y") {
									_unbindEvents.call(this);
								} else if (o.axis === "yx" && d.overflowed[1]) {
									_scrollTo($this, to[1].toString(), { dir: "x", dur: 0, overwrite: "none" });
								}
							} else if (mCSB_dragger[0].height() > mCSB_dragger[0].parent().height()) {
								_resetContentPosition.call(this); /* reset content position */
							} else {
								/* y scrolling is required */
								_scrollTo($this, to[0].toString(), { dir: "y", dur: 0, overwrite: "none" });
								d.contentReset.y = null;
							}
						}
						if (o.axis !== "y") {
							/* x/yx axis */
							if (!d.overflowed[1]) {
								/* x scrolling is not required */
								_resetContentPosition.call(this); /* reset content position */
								if (o.axis === "x") {
									_unbindEvents.call(this);
								} else if (o.axis === "yx" && d.overflowed[0]) {
									_scrollTo($this, to[0].toString(), { dir: "y", dur: 0, overwrite: "none" });
								}
							} else if (mCSB_dragger[1].width() > mCSB_dragger[1].parent().width()) {
								_resetContentPosition.call(this); /* reset content position */
							} else {
								/* x scrolling is required */
								_scrollTo($this, to[1].toString(), { dir: "x", dur: 0, overwrite: "none" });
								d.contentReset.x = null;
							}
						}

						/* callbacks: onImageLoad, onSelectorChange, onUpdate */
						if (cb && d) {
							if (cb === 2 && o.callbacks.onImageLoad && typeof o.callbacks.onImageLoad === "function") {
								o.callbacks.onImageLoad.call(this);
							} else if (cb === 3 && o.callbacks.onSelectorChange && typeof o.callbacks.onSelectorChange === "function") {
								o.callbacks.onSelectorChange.call(this);
							} else if (o.callbacks.onUpdate && typeof o.callbacks.onUpdate === "function") {
								o.callbacks.onUpdate.call(this);
							}
						}

						_autoUpdate.call(this); /* initialize automatic updating (for dynamic content, fluid layouts etc.) */
					}
				});
			},
			/* ---------------------------------------- */

			/* 
   plugin scrollTo method 
   triggers a scrolling event to a specific value
   ----------------------------------------
   usage: $(selector).mCustomScrollbar("scrollTo",value,options);
   */

			scrollTo: function scrollTo(val, options) {

				/* prevent silly things like $(selector).mCustomScrollbar("scrollTo",undefined); */
				if (typeof val == "undefined" || val == null) {
					return;
				}

				var selector = _selector.call(this); /* validate selector */

				return $(selector).each(function () {

					var $this = $(this);

					if ($this.data(pluginPfx)) {
						/* check if plugin has initialized */

						var d = $this.data(pluginPfx),
						    o = d.opt,

						/* method default options */
						methodDefaults = {
							trigger: "external", /* method is by default triggered externally (e.g. from other scripts) */
							scrollInertia: o.scrollInertia, /* scrolling inertia (animation duration) */
							scrollEasing: "mcsEaseInOut", /* animation easing */
							moveDragger: false, /* move dragger instead of content */
							timeout: 60, /* scroll-to delay */
							callbacks: true, /* enable/disable callbacks */
							onStart: true,
							onUpdate: true,
							onComplete: true
						},
						    methodOptions = $.extend(true, {}, methodDefaults, options),
						    to = _arr.call(this, val),
						    dur = methodOptions.scrollInertia > 0 && methodOptions.scrollInertia < 17 ? 17 : methodOptions.scrollInertia;

						/* translate yx values to actual scroll-to positions */
						to[0] = _to.call(this, to[0], "y");
						to[1] = _to.call(this, to[1], "x");

						/* 
      check if scroll-to value moves the dragger instead of content. 
      Only pixel values apply on dragger (e.g. 100, "100px", "-=100" etc.) 
      */
						if (methodOptions.moveDragger) {
							to[0] *= d.scrollRatio.y;
							to[1] *= d.scrollRatio.x;
						}

						methodOptions.dur = _isTabHidden() ? 0 : dur; //skip animations if browser tab is hidden

						setTimeout(function () {
							/* do the scrolling */
							if (to[0] !== null && typeof to[0] !== "undefined" && o.axis !== "x" && d.overflowed[0]) {
								/* scroll y */
								methodOptions.dir = "y";
								methodOptions.overwrite = "all";
								_scrollTo($this, to[0].toString(), methodOptions);
							}
							if (to[1] !== null && typeof to[1] !== "undefined" && o.axis !== "y" && d.overflowed[1]) {
								/* scroll x */
								methodOptions.dir = "x";
								methodOptions.overwrite = "none";
								_scrollTo($this, to[1].toString(), methodOptions);
							}
						}, methodOptions.timeout);
					}
				});
			},
			/* ---------------------------------------- */

			/*
   plugin stop method 
   stops scrolling animation
   ----------------------------------------
   usage: $(selector).mCustomScrollbar("stop");
   */
			stop: function stop() {

				var selector = _selector.call(this); /* validate selector */

				return $(selector).each(function () {

					var $this = $(this);

					if ($this.data(pluginPfx)) {
						/* check if plugin has initialized */

						_stop($this);
					}
				});
			},
			/* ---------------------------------------- */

			/*
   plugin disable method 
   temporarily disables the scrollbar(s) 
   ----------------------------------------
   usage: $(selector).mCustomScrollbar("disable",reset); 
   reset (boolean): resets content position to 0 
   */
			disable: function disable(r) {

				var selector = _selector.call(this); /* validate selector */

				return $(selector).each(function () {

					var $this = $(this);

					if ($this.data(pluginPfx)) {
						/* check if plugin has initialized */

						var d = $this.data(pluginPfx);

						_autoUpdate.call(this, "remove"); /* remove automatic updating */

						_unbindEvents.call(this); /* unbind events */

						if (r) {
							_resetContentPosition.call(this);
						} /* reset content position */

						_scrollbarVisibility.call(this, true); /* show/hide scrollbar(s) */

						$this.addClass(classes[3]); /* add disable class */
					}
				});
			},
			/* ---------------------------------------- */

			/*
   plugin destroy method 
   completely removes the scrollbar(s) and returns the element to its original state
   ----------------------------------------
   usage: $(selector).mCustomScrollbar("destroy"); 
   */
			destroy: function destroy() {

				var selector = _selector.call(this); /* validate selector */

				return $(selector).each(function () {

					var $this = $(this);

					if ($this.data(pluginPfx)) {
						/* check if plugin has initialized */

						var d = $this.data(pluginPfx),
						    o = d.opt,
						    mCustomScrollBox = $("#mCSB_" + d.idx),
						    mCSB_container = $("#mCSB_" + d.idx + "_container"),
						    scrollbar = $(".mCSB_" + d.idx + "_scrollbar");

						if (o.live) {
							removeLiveTimers(o.liveSelector || $(selector).selector);
						} /* remove live timers */

						_autoUpdate.call(this, "remove"); /* remove automatic updating */

						_unbindEvents.call(this); /* unbind events */

						_resetContentPosition.call(this); /* reset content position */

						$this.removeData(pluginPfx); /* remove plugin data object */

						_delete(this, "mcs"); /* delete callbacks object */

						/* remove plugin markup */
						scrollbar.remove(); /* remove scrollbar(s) first (those can be either inside or outside plugin's inner wrapper) */
						mCSB_container.find("img." + classes[2]).removeClass(classes[2]); /* remove loaded images flag */
						mCustomScrollBox.replaceWith(mCSB_container.contents()); /* replace plugin's inner wrapper with the original content */
						/* remove plugin classes from the element and add destroy class */
						$this.removeClass(pluginNS + " _" + pluginPfx + "_" + d.idx + " " + classes[6] + " " + classes[7] + " " + classes[5] + " " + classes[3]).addClass(classes[4]);
					}
				});
			}
			/* ---------------------------------------- */

		},


		/* 
  ----------------------------------------
  FUNCTIONS
  ----------------------------------------
  */

		/* validates selector (if selector is invalid or undefined uses the default one) */
		_selector = function _selector() {
			return _typeof($(this)) !== "object" || $(this).length < 1 ? defaultSelector : this;
		},

		/* -------------------- */

		/* changes options according to theme */
		_theme = function _theme(obj) {
			var fixedSizeScrollbarThemes = ["rounded", "rounded-dark", "rounded-dots", "rounded-dots-dark"],
			    nonExpandedScrollbarThemes = ["rounded-dots", "rounded-dots-dark", "3d", "3d-dark", "3d-thick", "3d-thick-dark", "inset", "inset-dark", "inset-2", "inset-2-dark", "inset-3", "inset-3-dark"],
			    disabledScrollButtonsThemes = ["minimal", "minimal-dark"],
			    enabledAutoHideScrollbarThemes = ["minimal", "minimal-dark"],
			    scrollbarPositionOutsideThemes = ["minimal", "minimal-dark"];
			obj.autoDraggerLength = $.inArray(obj.theme, fixedSizeScrollbarThemes) > -1 ? false : obj.autoDraggerLength;
			obj.autoExpandScrollbar = $.inArray(obj.theme, nonExpandedScrollbarThemes) > -1 ? false : obj.autoExpandScrollbar;
			obj.scrollButtons.enable = $.inArray(obj.theme, disabledScrollButtonsThemes) > -1 ? false : obj.scrollButtons.enable;
			obj.autoHideScrollbar = $.inArray(obj.theme, enabledAutoHideScrollbarThemes) > -1 ? true : obj.autoHideScrollbar;
			obj.scrollbarPosition = $.inArray(obj.theme, scrollbarPositionOutsideThemes) > -1 ? "outside" : obj.scrollbarPosition;
		},

		/* -------------------- */

		/* live option timers removal */
		removeLiveTimers = function removeLiveTimers(selector) {
			if (liveTimers[selector]) {
				clearTimeout(liveTimers[selector]);
				_delete(liveTimers, selector);
			}
		},

		/* -------------------- */

		/* normalizes axis option to valid values: "y", "x", "yx" */
		_findAxis = function _findAxis(val) {
			return val === "yx" || val === "xy" || val === "auto" ? "yx" : val === "x" || val === "horizontal" ? "x" : "y";
		},

		/* -------------------- */

		/* normalizes scrollButtons.scrollType option to valid values: "stepless", "stepped" */
		_findScrollButtonsType = function _findScrollButtonsType(val) {
			return val === "stepped" || val === "pixels" || val === "step" || val === "click" ? "stepped" : "stepless";
		},

		/* -------------------- */

		/* generates plugin markup */
		_pluginMarkup = function _pluginMarkup() {
			var $this = $(this),
			    d = $this.data(pluginPfx),
			    o = d.opt,
			    expandClass = o.autoExpandScrollbar ? " " + classes[1] + "_expand" : "",
			    scrollbar = ["<div id='mCSB_" + d.idx + "_scrollbar_vertical' class='mCSB_scrollTools mCSB_" + d.idx + "_scrollbar mCS-" + o.theme + " mCSB_scrollTools_vertical" + expandClass + "'><div class='" + classes[12] + "'><div id='mCSB_" + d.idx + "_dragger_vertical' class='mCSB_dragger' style='position:absolute;'><div class='mCSB_dragger_bar' /></div><div class='mCSB_draggerRail' /></div></div>", "<div id='mCSB_" + d.idx + "_scrollbar_horizontal' class='mCSB_scrollTools mCSB_" + d.idx + "_scrollbar mCS-" + o.theme + " mCSB_scrollTools_horizontal" + expandClass + "'><div class='" + classes[12] + "'><div id='mCSB_" + d.idx + "_dragger_horizontal' class='mCSB_dragger' style='position:absolute;'><div class='mCSB_dragger_bar' /></div><div class='mCSB_draggerRail' /></div></div>"],
			    wrapperClass = o.axis === "yx" ? "mCSB_vertical_horizontal" : o.axis === "x" ? "mCSB_horizontal" : "mCSB_vertical",
			    scrollbars = o.axis === "yx" ? scrollbar[0] + scrollbar[1] : o.axis === "x" ? scrollbar[1] : scrollbar[0],
			    contentWrapper = o.axis === "yx" ? "<div id='mCSB_" + d.idx + "_container_wrapper' class='mCSB_container_wrapper' />" : "",
			    autoHideClass = o.autoHideScrollbar ? " " + classes[6] : "",
			    scrollbarDirClass = o.axis !== "x" && d.langDir === "rtl" ? " " + classes[7] : "";
			if (o.setWidth) {
				$this.css("width", o.setWidth);
			} /* set element width */
			if (o.setHeight) {
				$this.css("height", o.setHeight);
			} /* set element height */
			o.setLeft = o.axis !== "y" && d.langDir === "rtl" ? "989999px" : o.setLeft; /* adjust left position for rtl direction */
			$this.addClass(pluginNS + " _" + pluginPfx + "_" + d.idx + autoHideClass + scrollbarDirClass).wrapInner("<div id='mCSB_" + d.idx + "' class='mCustomScrollBox mCS-" + o.theme + " " + wrapperClass + "'><div id='mCSB_" + d.idx + "_container' class='mCSB_container' style='position:relative; top:" + o.setTop + "; left:" + o.setLeft + ";' dir='" + d.langDir + "' /></div>");
			var mCustomScrollBox = $("#mCSB_" + d.idx),
			    mCSB_container = $("#mCSB_" + d.idx + "_container");
			if (o.axis !== "y" && !o.advanced.autoExpandHorizontalScroll) {
				mCSB_container.css("width", _contentWidth(mCSB_container));
			}
			if (o.scrollbarPosition === "outside") {
				if ($this.css("position") === "static") {
					/* requires elements with non-static position */
					$this.css("position", "relative");
				}
				$this.css("overflow", "visible");
				mCustomScrollBox.addClass("mCSB_outside").after(scrollbars);
			} else {
				mCustomScrollBox.addClass("mCSB_inside").append(scrollbars);
				mCSB_container.wrap(contentWrapper);
			}
			_scrollButtons.call(this); /* add scrollbar buttons */
			/* minimum dragger length */
			var mCSB_dragger = [$("#mCSB_" + d.idx + "_dragger_vertical"), $("#mCSB_" + d.idx + "_dragger_horizontal")];
			mCSB_dragger[0].css("min-height", mCSB_dragger[0].height());
			mCSB_dragger[1].css("min-width", mCSB_dragger[1].width());
		},

		/* -------------------- */

		/* calculates content width */
		_contentWidth = function _contentWidth(el) {
			var val = [el[0].scrollWidth, Math.max.apply(Math, el.children().map(function () {
				return $(this).outerWidth(true);
			}).get())],
			    w = el.parent().width();
			return val[0] > w ? val[0] : val[1] > w ? val[1] : "100%";
		},

		/* -------------------- */

		/* expands content horizontally */
		_expandContentHorizontally = function _expandContentHorizontally() {
			var $this = $(this),
			    d = $this.data(pluginPfx),
			    o = d.opt,
			    mCSB_container = $("#mCSB_" + d.idx + "_container");
			if (o.advanced.autoExpandHorizontalScroll && o.axis !== "y") {
				/* calculate scrollWidth */
				mCSB_container.css({ "width": "auto", "min-width": 0, "overflow-x": "scroll" });
				var w = Math.ceil(mCSB_container[0].scrollWidth);
				if (o.advanced.autoExpandHorizontalScroll === 3 || o.advanced.autoExpandHorizontalScroll !== 2 && w > mCSB_container.parent().width()) {
					mCSB_container.css({ "width": w, "min-width": "100%", "overflow-x": "inherit" });
				} else {
					/* 
     wrap content with an infinite width div and set its position to absolute and width to auto. 
     Setting width to auto before calculating the actual width is important! 
     We must let the browser set the width as browser zoom values are impossible to calculate.
     */
					mCSB_container.css({ "overflow-x": "inherit", "position": "absolute" }).wrap("<div class='mCSB_h_wrapper' style='position:relative; left:0; width:999999px;' />").css({ /* set actual width, original position and un-wrap */
						/* 
      get the exact width (with decimals) and then round-up. 
      Using jquery outerWidth() will round the width value which will mess up with inner elements that have non-integer width
      */
						"width": Math.ceil(mCSB_container[0].getBoundingClientRect().right + 0.4) - Math.floor(mCSB_container[0].getBoundingClientRect().left),
						"min-width": "100%",
						"position": "relative"
					}).unwrap();
				}
			}
		},

		/* -------------------- */

		/* adds scrollbar buttons */
		_scrollButtons = function _scrollButtons() {
			var $this = $(this),
			    d = $this.data(pluginPfx),
			    o = d.opt,
			    mCSB_scrollTools = $(".mCSB_" + d.idx + "_scrollbar:first"),
			    tabindex = !_isNumeric(o.scrollButtons.tabindex) ? "" : "tabindex='" + o.scrollButtons.tabindex + "'",
			    btnHTML = ["<a href='#' class='" + classes[13] + "' " + tabindex + " />", "<a href='#' class='" + classes[14] + "' " + tabindex + " />", "<a href='#' class='" + classes[15] + "' " + tabindex + " />", "<a href='#' class='" + classes[16] + "' " + tabindex + " />"],
			    btn = [o.axis === "x" ? btnHTML[2] : btnHTML[0], o.axis === "x" ? btnHTML[3] : btnHTML[1], btnHTML[2], btnHTML[3]];
			if (o.scrollButtons.enable) {
				mCSB_scrollTools.prepend(btn[0]).append(btn[1]).next(".mCSB_scrollTools").prepend(btn[2]).append(btn[3]);
			}
		},

		/* -------------------- */

		/* auto-adjusts scrollbar dragger length */
		_setDraggerLength = function _setDraggerLength() {
			var $this = $(this),
			    d = $this.data(pluginPfx),
			    mCustomScrollBox = $("#mCSB_" + d.idx),
			    mCSB_container = $("#mCSB_" + d.idx + "_container"),
			    mCSB_dragger = [$("#mCSB_" + d.idx + "_dragger_vertical"), $("#mCSB_" + d.idx + "_dragger_horizontal")],
			    ratio = [mCustomScrollBox.height() / mCSB_container.outerHeight(false), mCustomScrollBox.width() / mCSB_container.outerWidth(false)],
			    l = [parseInt(mCSB_dragger[0].css("min-height")), Math.round(ratio[0] * mCSB_dragger[0].parent().height()), parseInt(mCSB_dragger[1].css("min-width")), Math.round(ratio[1] * mCSB_dragger[1].parent().width())],
			    h = oldIE && l[1] < l[0] ? l[0] : l[1],
			    w = oldIE && l[3] < l[2] ? l[2] : l[3];
			mCSB_dragger[0].css({
				"height": h, "max-height": mCSB_dragger[0].parent().height() - 10
			}).find(".mCSB_dragger_bar").css({ "line-height": l[0] + "px" });
			mCSB_dragger[1].css({
				"width": w, "max-width": mCSB_dragger[1].parent().width() - 10
			});
		},

		/* -------------------- */

		/* calculates scrollbar to content ratio */
		_scrollRatio = function _scrollRatio() {
			var $this = $(this),
			    d = $this.data(pluginPfx),
			    mCustomScrollBox = $("#mCSB_" + d.idx),
			    mCSB_container = $("#mCSB_" + d.idx + "_container"),
			    mCSB_dragger = [$("#mCSB_" + d.idx + "_dragger_vertical"), $("#mCSB_" + d.idx + "_dragger_horizontal")],
			    scrollAmount = [mCSB_container.outerHeight(false) - mCustomScrollBox.height(), mCSB_container.outerWidth(false) - mCustomScrollBox.width()],
			    ratio = [scrollAmount[0] / (mCSB_dragger[0].parent().height() - mCSB_dragger[0].height()), scrollAmount[1] / (mCSB_dragger[1].parent().width() - mCSB_dragger[1].width())];
			d.scrollRatio = { y: ratio[0], x: ratio[1] };
		},

		/* -------------------- */

		/* toggles scrolling classes */
		_onDragClasses = function _onDragClasses(el, action, xpnd) {
			var expandClass = xpnd ? classes[0] + "_expanded" : "",
			    scrollbar = el.closest(".mCSB_scrollTools");
			if (action === "active") {
				el.toggleClass(classes[0] + " " + expandClass);scrollbar.toggleClass(classes[1]);
				el[0]._draggable = el[0]._draggable ? 0 : 1;
			} else {
				if (!el[0]._draggable) {
					if (action === "hide") {
						el.removeClass(classes[0]);scrollbar.removeClass(classes[1]);
					} else {
						el.addClass(classes[0]);scrollbar.addClass(classes[1]);
					}
				}
			}
		},

		/* -------------------- */

		/* checks if content overflows its container to determine if scrolling is required */
		_overflowed = function _overflowed() {
			var $this = $(this),
			    d = $this.data(pluginPfx),
			    mCustomScrollBox = $("#mCSB_" + d.idx),
			    mCSB_container = $("#mCSB_" + d.idx + "_container"),
			    contentHeight = d.overflowed == null ? mCSB_container.height() : mCSB_container.outerHeight(false),
			    contentWidth = d.overflowed == null ? mCSB_container.width() : mCSB_container.outerWidth(false),
			    h = mCSB_container[0].scrollHeight,
			    w = mCSB_container[0].scrollWidth;
			if (h > contentHeight) {
				contentHeight = h;
			}
			if (w > contentWidth) {
				contentWidth = w;
			}
			return [contentHeight > mCustomScrollBox.height(), contentWidth > mCustomScrollBox.width()];
		},

		/* -------------------- */

		/* resets content position to 0 */
		_resetContentPosition = function _resetContentPosition() {
			var $this = $(this),
			    d = $this.data(pluginPfx),
			    o = d.opt,
			    mCustomScrollBox = $("#mCSB_" + d.idx),
			    mCSB_container = $("#mCSB_" + d.idx + "_container"),
			    mCSB_dragger = [$("#mCSB_" + d.idx + "_dragger_vertical"), $("#mCSB_" + d.idx + "_dragger_horizontal")];
			_stop($this); /* stop any current scrolling before resetting */
			if (o.axis !== "x" && !d.overflowed[0] || o.axis === "y" && d.overflowed[0]) {
				/* reset y */
				mCSB_dragger[0].add(mCSB_container).css("top", 0);
				_scrollTo($this, "_resetY");
			}
			if (o.axis !== "y" && !d.overflowed[1] || o.axis === "x" && d.overflowed[1]) {
				/* reset x */
				var cx = dx = 0;
				if (d.langDir === "rtl") {
					/* adjust left position for rtl direction */
					cx = mCustomScrollBox.width() - mCSB_container.outerWidth(false);
					dx = Math.abs(cx / d.scrollRatio.x);
				}
				mCSB_container.css("left", cx);
				mCSB_dragger[1].css("left", dx);
				_scrollTo($this, "_resetX");
			}
		},

		/* -------------------- */

		/* binds scrollbar events */
		_bindEvents = function _bindEvents() {
			var $this = $(this),
			    d = $this.data(pluginPfx),
			    o = d.opt;
			if (!d.bindEvents) {
				/* check if events are already bound */
				_draggable.call(this);
				if (o.contentTouchScroll) {
					_contentDraggable.call(this);
				}
				_selectable.call(this);
				if (o.mouseWheel.enable) {
					/* bind mousewheel fn when plugin is available */
					var _mwt = function _mwt() {
						mousewheelTimeout = setTimeout(function () {
							if (!$.event.special.mousewheel) {
								_mwt();
							} else {
								clearTimeout(mousewheelTimeout);
								_mousewheel.call($this[0]);
							}
						}, 100);
					};

					var mousewheelTimeout;
					_mwt();
				}
				_draggerRail.call(this);
				_wrapperScroll.call(this);
				if (o.advanced.autoScrollOnFocus) {
					_focus.call(this);
				}
				if (o.scrollButtons.enable) {
					_buttons.call(this);
				}
				if (o.keyboard.enable) {
					_keyboard.call(this);
				}
				d.bindEvents = true;
			}
		},

		/* -------------------- */

		/* unbinds scrollbar events */
		_unbindEvents = function _unbindEvents() {
			var $this = $(this),
			    d = $this.data(pluginPfx),
			    o = d.opt,
			    namespace = pluginPfx + "_" + d.idx,
			    sb = ".mCSB_" + d.idx + "_scrollbar",
			    sel = $("#mCSB_" + d.idx + ",#mCSB_" + d.idx + "_container,#mCSB_" + d.idx + "_container_wrapper," + sb + " ." + classes[12] + ",#mCSB_" + d.idx + "_dragger_vertical,#mCSB_" + d.idx + "_dragger_horizontal," + sb + ">a"),
			    mCSB_container = $("#mCSB_" + d.idx + "_container");
			if (o.advanced.releaseDraggableSelectors) {
				sel.add($(o.advanced.releaseDraggableSelectors));
			}
			if (o.advanced.extraDraggableSelectors) {
				sel.add($(o.advanced.extraDraggableSelectors));
			}
			if (d.bindEvents) {
				/* check if events are bound */
				/* unbind namespaced events from document/selectors */
				$(document).add($(!_canAccessIFrame() || top.document)).unbind("." + namespace);
				sel.each(function () {
					$(this).unbind("." + namespace);
				});
				/* clear and delete timeouts/objects */
				clearTimeout($this[0]._focusTimeout);_delete($this[0], "_focusTimeout");
				clearTimeout(d.sequential.step);_delete(d.sequential, "step");
				clearTimeout(mCSB_container[0].onCompleteTimeout);_delete(mCSB_container[0], "onCompleteTimeout");
				d.bindEvents = false;
			}
		},

		/* -------------------- */

		/* toggles scrollbar visibility */
		_scrollbarVisibility = function _scrollbarVisibility(disabled) {
			var $this = $(this),
			    d = $this.data(pluginPfx),
			    o = d.opt,
			    contentWrapper = $("#mCSB_" + d.idx + "_container_wrapper"),
			    content = contentWrapper.length ? contentWrapper : $("#mCSB_" + d.idx + "_container"),
			    scrollbar = [$("#mCSB_" + d.idx + "_scrollbar_vertical"), $("#mCSB_" + d.idx + "_scrollbar_horizontal")],
			    mCSB_dragger = [scrollbar[0].find(".mCSB_dragger"), scrollbar[1].find(".mCSB_dragger")];
			if (o.axis !== "x") {
				if (d.overflowed[0] && !disabled) {
					scrollbar[0].add(mCSB_dragger[0]).add(scrollbar[0].children("a")).css("display", "block");
					content.removeClass(classes[8] + " " + classes[10]);
				} else {
					if (o.alwaysShowScrollbar) {
						if (o.alwaysShowScrollbar !== 2) {
							mCSB_dragger[0].css("display", "none");
						}
						content.removeClass(classes[10]);
					} else {
						scrollbar[0].css("display", "none");
						content.addClass(classes[10]);
					}
					content.addClass(classes[8]);
				}
			}
			if (o.axis !== "y") {
				if (d.overflowed[1] && !disabled) {
					scrollbar[1].add(mCSB_dragger[1]).add(scrollbar[1].children("a")).css("display", "block");
					content.removeClass(classes[9] + " " + classes[11]);
				} else {
					if (o.alwaysShowScrollbar) {
						if (o.alwaysShowScrollbar !== 2) {
							mCSB_dragger[1].css("display", "none");
						}
						content.removeClass(classes[11]);
					} else {
						scrollbar[1].css("display", "none");
						content.addClass(classes[11]);
					}
					content.addClass(classes[9]);
				}
			}
			if (!d.overflowed[0] && !d.overflowed[1]) {
				$this.addClass(classes[5]);
			} else {
				$this.removeClass(classes[5]);
			}
		},

		/* -------------------- */

		/* returns input coordinates of pointer, touch and mouse events (relative to document) */
		_coordinates = function _coordinates(e) {
			var t = e.type,
			    o = e.target.ownerDocument !== document && frameElement !== null ? [$(frameElement).offset().top, $(frameElement).offset().left] : null,
			    io = _canAccessIFrame() && e.target.ownerDocument !== top.document && frameElement !== null ? [$(e.view.frameElement).offset().top, $(e.view.frameElement).offset().left] : [0, 0];
			switch (t) {
				case "pointerdown":case "MSPointerDown":case "pointermove":case "MSPointerMove":case "pointerup":case "MSPointerUp":
					return o ? [e.originalEvent.pageY - o[0] + io[0], e.originalEvent.pageX - o[1] + io[1], false] : [e.originalEvent.pageY, e.originalEvent.pageX, false];
					break;
				case "touchstart":case "touchmove":case "touchend":
					var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0],
					    touches = e.originalEvent.touches.length || e.originalEvent.changedTouches.length;
					return e.target.ownerDocument !== document ? [touch.screenY, touch.screenX, touches > 1] : [touch.pageY, touch.pageX, touches > 1];
					break;
				default:
					return o ? [e.pageY - o[0] + io[0], e.pageX - o[1] + io[1], false] : [e.pageY, e.pageX, false];
			}
		},

		/* -------------------- */

		/* 
  SCROLLBAR DRAG EVENTS
  scrolls content via scrollbar dragging 
  */
		_draggable = function _draggable() {
			var $this = $(this),
			    d = $this.data(pluginPfx),
			    o = d.opt,
			    namespace = pluginPfx + "_" + d.idx,
			    draggerId = ["mCSB_" + d.idx + "_dragger_vertical", "mCSB_" + d.idx + "_dragger_horizontal"],
			    mCSB_container = $("#mCSB_" + d.idx + "_container"),
			    mCSB_dragger = $("#" + draggerId[0] + ",#" + draggerId[1]),
			    draggable,
			    dragY,
			    dragX,
			    rds = o.advanced.releaseDraggableSelectors ? mCSB_dragger.add($(o.advanced.releaseDraggableSelectors)) : mCSB_dragger,
			    eds = o.advanced.extraDraggableSelectors ? $(!_canAccessIFrame() || top.document).add($(o.advanced.extraDraggableSelectors)) : $(!_canAccessIFrame() || top.document);
			mCSB_dragger.bind("contextmenu." + namespace, function (e) {
				e.preventDefault(); //prevent right click
			}).bind("mousedown." + namespace + " touchstart." + namespace + " pointerdown." + namespace + " MSPointerDown." + namespace, function (e) {
				e.stopImmediatePropagation();
				e.preventDefault();
				if (!_mouseBtnLeft(e)) {
					return;
				} /* left mouse button only */
				touchActive = true;
				if (oldIE) {
					document.onselectstart = function () {
						return false;
					};
				} /* disable text selection for IE < 9 */
				_iframe.call(mCSB_container, false); /* enable scrollbar dragging over iframes by disabling their events */
				_stop($this);
				draggable = $(this);
				var offset = draggable.offset(),
				    y = _coordinates(e)[0] - offset.top,
				    x = _coordinates(e)[1] - offset.left,
				    h = draggable.height() + offset.top,
				    w = draggable.width() + offset.left;
				if (y < h && y > 0 && x < w && x > 0) {
					dragY = y;
					dragX = x;
				}
				_onDragClasses(draggable, "active", o.autoExpandScrollbar);
			}).bind("touchmove." + namespace, function (e) {
				e.stopImmediatePropagation();
				e.preventDefault();
				var offset = draggable.offset(),
				    y = _coordinates(e)[0] - offset.top,
				    x = _coordinates(e)[1] - offset.left;
				_drag(dragY, dragX, y, x);
			});
			$(document).add(eds).bind("mousemove." + namespace + " pointermove." + namespace + " MSPointerMove." + namespace, function (e) {
				if (draggable) {
					var offset = draggable.offset(),
					    y = _coordinates(e)[0] - offset.top,
					    x = _coordinates(e)[1] - offset.left;
					if (dragY === y && dragX === x) {
						return;
					} /* has it really moved? */
					_drag(dragY, dragX, y, x);
				}
			}).add(rds).bind("mouseup." + namespace + " touchend." + namespace + " pointerup." + namespace + " MSPointerUp." + namespace, function (e) {
				if (draggable) {
					_onDragClasses(draggable, "active", o.autoExpandScrollbar);
					draggable = null;
				}
				touchActive = false;
				if (oldIE) {
					document.onselectstart = null;
				} /* enable text selection for IE < 9 */
				_iframe.call(mCSB_container, true); /* enable iframes events */
			});
			function _drag(dragY, dragX, y, x) {
				mCSB_container[0].idleTimer = o.scrollInertia < 233 ? 250 : 0;
				if (draggable.attr("id") === draggerId[1]) {
					var dir = "x",
					    to = (draggable[0].offsetLeft - dragX + x) * d.scrollRatio.x;
				} else {
					var dir = "y",
					    to = (draggable[0].offsetTop - dragY + y) * d.scrollRatio.y;
				}
				_scrollTo($this, to.toString(), { dir: dir, drag: true });
			}
		},

		/* -------------------- */

		/* 
  TOUCH SWIPE EVENTS
  scrolls content via touch swipe 
  Emulates the native touch-swipe scrolling with momentum found in iOS, Android and WP devices 
  */
		_contentDraggable = function _contentDraggable() {
			var $this = $(this),
			    d = $this.data(pluginPfx),
			    o = d.opt,
			    namespace = pluginPfx + "_" + d.idx,
			    mCustomScrollBox = $("#mCSB_" + d.idx),
			    mCSB_container = $("#mCSB_" + d.idx + "_container"),
			    mCSB_dragger = [$("#mCSB_" + d.idx + "_dragger_vertical"), $("#mCSB_" + d.idx + "_dragger_horizontal")],
			    draggable,
			    dragY,
			    dragX,
			    touchStartY,
			    touchStartX,
			    touchMoveY = [],
			    touchMoveX = [],
			    startTime,
			    runningTime,
			    endTime,
			    distance,
			    speed,
			    amount,
			    durA = 0,
			    durB,
			    overwrite = o.axis === "yx" ? "none" : "all",
			    touchIntent = [],
			    touchDrag,
			    docDrag,
			    iframe = mCSB_container.find("iframe"),
			    events = ["touchstart." + namespace + " pointerdown." + namespace + " MSPointerDown." + namespace, //start
			"touchmove." + namespace + " pointermove." + namespace + " MSPointerMove." + namespace, //move
			"touchend." + namespace + " pointerup." + namespace + " MSPointerUp." + namespace //end
			],
			    touchAction = document.body.style.touchAction !== undefined && document.body.style.touchAction !== "";
			mCSB_container.bind(events[0], function (e) {
				_onTouchstart(e);
			}).bind(events[1], function (e) {
				_onTouchmove(e);
			});
			mCustomScrollBox.bind(events[0], function (e) {
				_onTouchstart2(e);
			}).bind(events[2], function (e) {
				_onTouchend(e);
			});
			if (iframe.length) {
				iframe.each(function () {
					$(this).bind("load", function () {
						/* bind events on accessible iframes */
						if (_canAccessIFrame(this)) {
							$(this.contentDocument || this.contentWindow.document).bind(events[0], function (e) {
								_onTouchstart(e);
								_onTouchstart2(e);
							}).bind(events[1], function (e) {
								_onTouchmove(e);
							}).bind(events[2], function (e) {
								_onTouchend(e);
							});
						}
					});
				});
			}
			function _onTouchstart(e) {
				if (!_pointerTouch(e) || touchActive || _coordinates(e)[2]) {
					touchable = 0;return;
				}
				touchable = 1;touchDrag = 0;docDrag = 0;draggable = 1;
				$this.removeClass("mCS_touch_action");
				var offset = mCSB_container.offset();
				dragY = _coordinates(e)[0] - offset.top;
				dragX = _coordinates(e)[1] - offset.left;
				touchIntent = [_coordinates(e)[0], _coordinates(e)[1]];
			}
			function _onTouchmove(e) {
				if (!_pointerTouch(e) || touchActive || _coordinates(e)[2]) {
					return;
				}
				if (!o.documentTouchScroll) {
					e.preventDefault();
				}
				e.stopImmediatePropagation();
				if (docDrag && !touchDrag) {
					return;
				}
				if (draggable) {
					runningTime = _getTime();
					var offset = mCustomScrollBox.offset(),
					    y = _coordinates(e)[0] - offset.top,
					    x = _coordinates(e)[1] - offset.left,
					    easing = "mcsLinearOut";
					touchMoveY.push(y);
					touchMoveX.push(x);
					touchIntent[2] = Math.abs(_coordinates(e)[0] - touchIntent[0]);touchIntent[3] = Math.abs(_coordinates(e)[1] - touchIntent[1]);
					if (d.overflowed[0]) {
						var limit = mCSB_dragger[0].parent().height() - mCSB_dragger[0].height(),
						    prevent = dragY - y > 0 && y - dragY > -(limit * d.scrollRatio.y) && (touchIntent[3] * 2 < touchIntent[2] || o.axis === "yx");
					}
					if (d.overflowed[1]) {
						var limitX = mCSB_dragger[1].parent().width() - mCSB_dragger[1].width(),
						    preventX = dragX - x > 0 && x - dragX > -(limitX * d.scrollRatio.x) && (touchIntent[2] * 2 < touchIntent[3] || o.axis === "yx");
					}
					if (prevent || preventX) {
						/* prevent native document scrolling */
						if (!touchAction) {
							e.preventDefault();
						}
						touchDrag = 1;
					} else {
						docDrag = 1;
						$this.addClass("mCS_touch_action");
					}
					if (touchAction) {
						e.preventDefault();
					}
					amount = o.axis === "yx" ? [dragY - y, dragX - x] : o.axis === "x" ? [null, dragX - x] : [dragY - y, null];
					mCSB_container[0].idleTimer = 250;
					if (d.overflowed[0]) {
						_drag(amount[0], durA, easing, "y", "all", true);
					}
					if (d.overflowed[1]) {
						_drag(amount[1], durA, easing, "x", overwrite, true);
					}
				}
			}
			function _onTouchstart2(e) {
				if (!_pointerTouch(e) || touchActive || _coordinates(e)[2]) {
					touchable = 0;return;
				}
				touchable = 1;
				e.stopImmediatePropagation();
				_stop($this);
				startTime = _getTime();
				var offset = mCustomScrollBox.offset();
				touchStartY = _coordinates(e)[0] - offset.top;
				touchStartX = _coordinates(e)[1] - offset.left;
				touchMoveY = [];touchMoveX = [];
			}
			function _onTouchend(e) {
				if (!_pointerTouch(e) || touchActive || _coordinates(e)[2]) {
					return;
				}
				draggable = 0;
				e.stopImmediatePropagation();
				touchDrag = 0;docDrag = 0;
				endTime = _getTime();
				var offset = mCustomScrollBox.offset(),
				    y = _coordinates(e)[0] - offset.top,
				    x = _coordinates(e)[1] - offset.left;
				if (endTime - runningTime > 30) {
					return;
				}
				speed = 1000 / (endTime - startTime);
				var easing = "mcsEaseOut",
				    slow = speed < 2.5,
				    diff = slow ? [touchMoveY[touchMoveY.length - 2], touchMoveX[touchMoveX.length - 2]] : [0, 0];
				distance = slow ? [y - diff[0], x - diff[1]] : [y - touchStartY, x - touchStartX];
				var absDistance = [Math.abs(distance[0]), Math.abs(distance[1])];
				speed = slow ? [Math.abs(distance[0] / 4), Math.abs(distance[1] / 4)] : [speed, speed];
				var a = [Math.abs(mCSB_container[0].offsetTop) - distance[0] * _m(absDistance[0] / speed[0], speed[0]), Math.abs(mCSB_container[0].offsetLeft) - distance[1] * _m(absDistance[1] / speed[1], speed[1])];
				amount = o.axis === "yx" ? [a[0], a[1]] : o.axis === "x" ? [null, a[1]] : [a[0], null];
				durB = [absDistance[0] * 4 + o.scrollInertia, absDistance[1] * 4 + o.scrollInertia];
				var md = parseInt(o.contentTouchScroll) || 0; /* absolute minimum distance required */
				amount[0] = absDistance[0] > md ? amount[0] : 0;
				amount[1] = absDistance[1] > md ? amount[1] : 0;
				if (d.overflowed[0]) {
					_drag(amount[0], durB[0], easing, "y", overwrite, false);
				}
				if (d.overflowed[1]) {
					_drag(amount[1], durB[1], easing, "x", overwrite, false);
				}
			}
			function _m(ds, s) {
				var r = [s * 1.5, s * 2, s / 1.5, s / 2];
				if (ds > 90) {
					return s > 4 ? r[0] : r[3];
				} else if (ds > 60) {
					return s > 3 ? r[3] : r[2];
				} else if (ds > 30) {
					return s > 8 ? r[1] : s > 6 ? r[0] : s > 4 ? s : r[2];
				} else {
					return s > 8 ? s : r[3];
				}
			}
			function _drag(amount, dur, easing, dir, overwrite, drag) {
				if (!amount) {
					return;
				}
				_scrollTo($this, amount.toString(), { dur: dur, scrollEasing: easing, dir: dir, overwrite: overwrite, drag: drag });
			}
		},

		/* -------------------- */

		/* 
  SELECT TEXT EVENTS 
  scrolls content when text is selected 
  */
		_selectable = function _selectable() {
			var $this = $(this),
			    d = $this.data(pluginPfx),
			    o = d.opt,
			    seq = d.sequential,
			    namespace = pluginPfx + "_" + d.idx,
			    mCSB_container = $("#mCSB_" + d.idx + "_container"),
			    wrapper = mCSB_container.parent(),
			    action;
			mCSB_container.bind("mousedown." + namespace, function (e) {
				if (touchable) {
					return;
				}
				if (!action) {
					action = 1;touchActive = true;
				}
			}).add(document).bind("mousemove." + namespace, function (e) {
				if (!touchable && action && _sel()) {
					var offset = mCSB_container.offset(),
					    y = _coordinates(e)[0] - offset.top + mCSB_container[0].offsetTop,
					    x = _coordinates(e)[1] - offset.left + mCSB_container[0].offsetLeft;
					if (y > 0 && y < wrapper.height() && x > 0 && x < wrapper.width()) {
						if (seq.step) {
							_seq("off", null, "stepped");
						}
					} else {
						if (o.axis !== "x" && d.overflowed[0]) {
							if (y < 0) {
								_seq("on", 38);
							} else if (y > wrapper.height()) {
								_seq("on", 40);
							}
						}
						if (o.axis !== "y" && d.overflowed[1]) {
							if (x < 0) {
								_seq("on", 37);
							} else if (x > wrapper.width()) {
								_seq("on", 39);
							}
						}
					}
				}
			}).bind("mouseup." + namespace + " dragend." + namespace, function (e) {
				if (touchable) {
					return;
				}
				if (action) {
					action = 0;_seq("off", null);
				}
				touchActive = false;
			});
			function _sel() {
				return window.getSelection ? window.getSelection().toString() : document.selection && document.selection.type != "Control" ? document.selection.createRange().text : 0;
			}
			function _seq(a, c, s) {
				seq.type = s && action ? "stepped" : "stepless";
				seq.scrollAmount = 10;
				_sequentialScroll($this, a, c, "mcsLinearOut", s ? 60 : null);
			}
		},

		/* -------------------- */

		/* 
  MOUSE WHEEL EVENT
  scrolls content via mouse-wheel 
  via mouse-wheel plugin (https://github.com/brandonaaron/jquery-mousewheel)
  */
		_mousewheel = function _mousewheel() {
			if (!$(this).data(pluginPfx)) {
				return;
			} /* Check if the scrollbar is ready to use mousewheel events (issue: #185) */
			var $this = $(this),
			    d = $this.data(pluginPfx),
			    o = d.opt,
			    namespace = pluginPfx + "_" + d.idx,
			    mCustomScrollBox = $("#mCSB_" + d.idx),
			    mCSB_dragger = [$("#mCSB_" + d.idx + "_dragger_vertical"), $("#mCSB_" + d.idx + "_dragger_horizontal")],
			    iframe = $("#mCSB_" + d.idx + "_container").find("iframe");
			if (iframe.length) {
				iframe.each(function () {
					$(this).bind("load", function () {
						/* bind events on accessible iframes */
						if (_canAccessIFrame(this)) {
							$(this.contentDocument || this.contentWindow.document).bind("mousewheel." + namespace, function (e, delta) {
								_onMousewheel(e, delta);
							});
						}
					});
				});
			}
			mCustomScrollBox.bind("mousewheel." + namespace, function (e, delta) {
				_onMousewheel(e, delta);
			});
			function _onMousewheel(e, delta) {
				_stop($this);
				if (_disableMousewheel($this, e.target)) {
					return;
				} /* disables mouse-wheel when hovering specific elements */
				var deltaFactor = o.mouseWheel.deltaFactor !== "auto" ? parseInt(o.mouseWheel.deltaFactor) : oldIE && e.deltaFactor < 100 ? 100 : e.deltaFactor || 100,
				    dur = o.scrollInertia;
				if (o.axis === "x" || o.mouseWheel.axis === "x") {
					var dir = "x",
					    px = [Math.round(deltaFactor * d.scrollRatio.x), parseInt(o.mouseWheel.scrollAmount)],
					    amount = o.mouseWheel.scrollAmount !== "auto" ? px[1] : px[0] >= mCustomScrollBox.width() ? mCustomScrollBox.width() * 0.9 : px[0],
					    contentPos = Math.abs($("#mCSB_" + d.idx + "_container")[0].offsetLeft),
					    draggerPos = mCSB_dragger[1][0].offsetLeft,
					    limit = mCSB_dragger[1].parent().width() - mCSB_dragger[1].width(),
					    dlt = o.mouseWheel.axis === "y" ? e.deltaY || delta : e.deltaX;
				} else {
					var dir = "y",
					    px = [Math.round(deltaFactor * d.scrollRatio.y), parseInt(o.mouseWheel.scrollAmount)],
					    amount = o.mouseWheel.scrollAmount !== "auto" ? px[1] : px[0] >= mCustomScrollBox.height() ? mCustomScrollBox.height() * 0.9 : px[0],
					    contentPos = Math.abs($("#mCSB_" + d.idx + "_container")[0].offsetTop),
					    draggerPos = mCSB_dragger[0][0].offsetTop,
					    limit = mCSB_dragger[0].parent().height() - mCSB_dragger[0].height(),
					    dlt = e.deltaY || delta;
				}
				if (dir === "y" && !d.overflowed[0] || dir === "x" && !d.overflowed[1]) {
					return;
				}
				if (o.mouseWheel.invert || e.webkitDirectionInvertedFromDevice) {
					dlt = -dlt;
				}
				if (o.mouseWheel.normalizeDelta) {
					dlt = dlt < 0 ? -1 : 1;
				}
				if (dlt > 0 && draggerPos !== 0 || dlt < 0 && draggerPos !== limit || o.mouseWheel.preventDefault) {
					e.stopImmediatePropagation();
					e.preventDefault();
				}
				if (e.deltaFactor < 5 && !o.mouseWheel.normalizeDelta) {
					//very low deltaFactor values mean some kind of delta acceleration (e.g. osx trackpad), so adjusting scrolling accordingly
					amount = e.deltaFactor;dur = 17;
				}
				_scrollTo($this, (contentPos - dlt * amount).toString(), { dir: dir, dur: dur });
			}
		},

		/* -------------------- */

		/* checks if iframe can be accessed */
		_canAccessIFrameCache = new Object(),
		    _canAccessIFrame = function _canAccessIFrame(iframe) {
			var result = false,
			    cacheKey = false,
			    html = null;
			if (iframe === undefined) {
				cacheKey = "#empty";
			} else if ($(iframe).attr("id") !== undefined) {
				cacheKey = $(iframe).attr("id");
			}
			if (cacheKey !== false && _canAccessIFrameCache[cacheKey] !== undefined) {
				return _canAccessIFrameCache[cacheKey];
			}
			if (!iframe) {
				try {
					var doc = top.document;
					html = doc.body.innerHTML;
				} catch (err) {/* do nothing */}
				result = html !== null;
			} else {
				try {
					var doc = iframe.contentDocument || iframe.contentWindow.document;
					html = doc.body.innerHTML;
				} catch (err) {/* do nothing */}
				result = html !== null;
			}
			if (cacheKey !== false) {
				_canAccessIFrameCache[cacheKey] = result;
			}
			return result;
		},

		/* -------------------- */

		/* switches iframe's pointer-events property (drag, mousewheel etc. over cross-domain iframes) */
		_iframe = function _iframe(evt) {
			var el = this.find("iframe");
			if (!el.length) {
				return;
			} /* check if content contains iframes */
			var val = !evt ? "none" : "auto";
			el.css("pointer-events", val); /* for IE11, iframe's display property should not be "block" */
		},

		/* -------------------- */

		/* disables mouse-wheel when hovering specific elements like select, datalist etc. */
		_disableMousewheel = function _disableMousewheel(el, target) {
			var tag = target.nodeName.toLowerCase(),
			    tags = el.data(pluginPfx).opt.mouseWheel.disableOver,

			/* elements that require focus */
			focusTags = ["select", "textarea"];
			return $.inArray(tag, tags) > -1 && !($.inArray(tag, focusTags) > -1 && !$(target).is(":focus"));
		},

		/* -------------------- */

		/* 
  DRAGGER RAIL CLICK EVENT
  scrolls content via dragger rail 
  */
		_draggerRail = function _draggerRail() {
			var $this = $(this),
			    d = $this.data(pluginPfx),
			    namespace = pluginPfx + "_" + d.idx,
			    mCSB_container = $("#mCSB_" + d.idx + "_container"),
			    wrapper = mCSB_container.parent(),
			    mCSB_draggerContainer = $(".mCSB_" + d.idx + "_scrollbar ." + classes[12]),
			    clickable;
			mCSB_draggerContainer.bind("mousedown." + namespace + " touchstart." + namespace + " pointerdown." + namespace + " MSPointerDown." + namespace, function (e) {
				touchActive = true;
				if (!$(e.target).hasClass("mCSB_dragger")) {
					clickable = 1;
				}
			}).bind("touchend." + namespace + " pointerup." + namespace + " MSPointerUp." + namespace, function (e) {
				touchActive = false;
			}).bind("click." + namespace, function (e) {
				if (!clickable) {
					return;
				}
				clickable = 0;
				if ($(e.target).hasClass(classes[12]) || $(e.target).hasClass("mCSB_draggerRail")) {
					_stop($this);
					var el = $(this),
					    mCSB_dragger = el.find(".mCSB_dragger");
					if (el.parent(".mCSB_scrollTools_horizontal").length > 0) {
						if (!d.overflowed[1]) {
							return;
						}
						var dir = "x",
						    clickDir = e.pageX > mCSB_dragger.offset().left ? -1 : 1,
						    to = Math.abs(mCSB_container[0].offsetLeft) - clickDir * (wrapper.width() * 0.9);
					} else {
						if (!d.overflowed[0]) {
							return;
						}
						var dir = "y",
						    clickDir = e.pageY > mCSB_dragger.offset().top ? -1 : 1,
						    to = Math.abs(mCSB_container[0].offsetTop) - clickDir * (wrapper.height() * 0.9);
					}
					_scrollTo($this, to.toString(), { dir: dir, scrollEasing: "mcsEaseInOut" });
				}
			});
		},

		/* -------------------- */

		/* 
  FOCUS EVENT
  scrolls content via element focus (e.g. clicking an input, pressing TAB key etc.)
  */
		_focus = function _focus() {
			var $this = $(this),
			    d = $this.data(pluginPfx),
			    o = d.opt,
			    namespace = pluginPfx + "_" + d.idx,
			    mCSB_container = $("#mCSB_" + d.idx + "_container"),
			    wrapper = mCSB_container.parent();
			mCSB_container.bind("focusin." + namespace, function (e) {
				var el = $(document.activeElement),
				    nested = mCSB_container.find(".mCustomScrollBox").length,
				    dur = 0;
				if (!el.is(o.advanced.autoScrollOnFocus)) {
					return;
				}
				_stop($this);
				clearTimeout($this[0]._focusTimeout);
				$this[0]._focusTimer = nested ? (dur + 17) * nested : 0;
				$this[0]._focusTimeout = setTimeout(function () {
					var to = [_childPos(el)[0], _childPos(el)[1]],
					    contentPos = [mCSB_container[0].offsetTop, mCSB_container[0].offsetLeft],
					    isVisible = [contentPos[0] + to[0] >= 0 && contentPos[0] + to[0] < wrapper.height() - el.outerHeight(false), contentPos[1] + to[1] >= 0 && contentPos[0] + to[1] < wrapper.width() - el.outerWidth(false)],
					    overwrite = o.axis === "yx" && !isVisible[0] && !isVisible[1] ? "none" : "all";
					if (o.axis !== "x" && !isVisible[0]) {
						_scrollTo($this, to[0].toString(), { dir: "y", scrollEasing: "mcsEaseInOut", overwrite: overwrite, dur: dur });
					}
					if (o.axis !== "y" && !isVisible[1]) {
						_scrollTo($this, to[1].toString(), { dir: "x", scrollEasing: "mcsEaseInOut", overwrite: overwrite, dur: dur });
					}
				}, $this[0]._focusTimer);
			});
		},

		/* -------------------- */

		/* sets content wrapper scrollTop/scrollLeft always to 0 */
		_wrapperScroll = function _wrapperScroll() {
			var $this = $(this),
			    d = $this.data(pluginPfx),
			    namespace = pluginPfx + "_" + d.idx,
			    wrapper = $("#mCSB_" + d.idx + "_container").parent();
			wrapper.bind("scroll." + namespace, function (e) {
				if (wrapper.scrollTop() !== 0 || wrapper.scrollLeft() !== 0) {
					$(".mCSB_" + d.idx + "_scrollbar").css("visibility", "hidden"); /* hide scrollbar(s) */
				}
			});
		},

		/* -------------------- */

		/* 
  BUTTONS EVENTS
  scrolls content via up, down, left and right buttons 
  */
		_buttons = function _buttons() {
			var $this = $(this),
			    d = $this.data(pluginPfx),
			    o = d.opt,
			    seq = d.sequential,
			    namespace = pluginPfx + "_" + d.idx,
			    sel = ".mCSB_" + d.idx + "_scrollbar",
			    btn = $(sel + ">a");
			btn.bind("contextmenu." + namespace, function (e) {
				e.preventDefault(); //prevent right click
			}).bind("mousedown." + namespace + " touchstart." + namespace + " pointerdown." + namespace + " MSPointerDown." + namespace + " mouseup." + namespace + " touchend." + namespace + " pointerup." + namespace + " MSPointerUp." + namespace + " mouseout." + namespace + " pointerout." + namespace + " MSPointerOut." + namespace + " click." + namespace, function (e) {
				e.preventDefault();
				if (!_mouseBtnLeft(e)) {
					return;
				} /* left mouse button only */
				var btnClass = $(this).attr("class");
				seq.type = o.scrollButtons.scrollType;
				switch (e.type) {
					case "mousedown":case "touchstart":case "pointerdown":case "MSPointerDown":
						if (seq.type === "stepped") {
							return;
						}
						touchActive = true;
						d.tweenRunning = false;
						_seq("on", btnClass);
						break;
					case "mouseup":case "touchend":case "pointerup":case "MSPointerUp":
					case "mouseout":case "pointerout":case "MSPointerOut":
						if (seq.type === "stepped") {
							return;
						}
						touchActive = false;
						if (seq.dir) {
							_seq("off", btnClass);
						}
						break;
					case "click":
						if (seq.type !== "stepped" || d.tweenRunning) {
							return;
						}
						_seq("on", btnClass);
						break;
				}
				function _seq(a, c) {
					seq.scrollAmount = o.scrollButtons.scrollAmount;
					_sequentialScroll($this, a, c);
				}
			});
		},

		/* -------------------- */

		/* 
  KEYBOARD EVENTS
  scrolls content via keyboard 
  Keys: up arrow, down arrow, left arrow, right arrow, PgUp, PgDn, Home, End
  */
		_keyboard = function _keyboard() {
			var $this = $(this),
			    d = $this.data(pluginPfx),
			    o = d.opt,
			    seq = d.sequential,
			    namespace = pluginPfx + "_" + d.idx,
			    mCustomScrollBox = $("#mCSB_" + d.idx),
			    mCSB_container = $("#mCSB_" + d.idx + "_container"),
			    wrapper = mCSB_container.parent(),
			    editables = "input,textarea,select,datalist,keygen,[contenteditable='true']",
			    iframe = mCSB_container.find("iframe"),
			    events = ["blur." + namespace + " keydown." + namespace + " keyup." + namespace];
			if (iframe.length) {
				iframe.each(function () {
					$(this).bind("load", function () {
						/* bind events on accessible iframes */
						if (_canAccessIFrame(this)) {
							$(this.contentDocument || this.contentWindow.document).bind(events[0], function (e) {
								_onKeyboard(e);
							});
						}
					});
				});
			}
			mCustomScrollBox.attr("tabindex", "0").bind(events[0], function (e) {
				_onKeyboard(e);
			});
			function _onKeyboard(e) {
				switch (e.type) {
					case "blur":
						if (d.tweenRunning && seq.dir) {
							_seq("off", null);
						}
						break;
					case "keydown":case "keyup":
						var code = e.keyCode ? e.keyCode : e.which,
						    action = "on";
						if (o.axis !== "x" && (code === 38 || code === 40) || o.axis !== "y" && (code === 37 || code === 39)) {
							/* up (38), down (40), left (37), right (39) arrows */
							if ((code === 38 || code === 40) && !d.overflowed[0] || (code === 37 || code === 39) && !d.overflowed[1]) {
								return;
							}
							if (e.type === "keyup") {
								action = "off";
							}
							if (!$(document.activeElement).is(editables)) {
								e.preventDefault();
								e.stopImmediatePropagation();
								_seq(action, code);
							}
						} else if (code === 33 || code === 34) {
							/* PgUp (33), PgDn (34) */
							if (d.overflowed[0] || d.overflowed[1]) {
								e.preventDefault();
								e.stopImmediatePropagation();
							}
							if (e.type === "keyup") {
								_stop($this);
								var keyboardDir = code === 34 ? -1 : 1;
								if (o.axis === "x" || o.axis === "yx" && d.overflowed[1] && !d.overflowed[0]) {
									var dir = "x",
									    to = Math.abs(mCSB_container[0].offsetLeft) - keyboardDir * (wrapper.width() * 0.9);
								} else {
									var dir = "y",
									    to = Math.abs(mCSB_container[0].offsetTop) - keyboardDir * (wrapper.height() * 0.9);
								}
								_scrollTo($this, to.toString(), { dir: dir, scrollEasing: "mcsEaseInOut" });
							}
						} else if (code === 35 || code === 36) {
							/* End (35), Home (36) */
							if (!$(document.activeElement).is(editables)) {
								if (d.overflowed[0] || d.overflowed[1]) {
									e.preventDefault();
									e.stopImmediatePropagation();
								}
								if (e.type === "keyup") {
									if (o.axis === "x" || o.axis === "yx" && d.overflowed[1] && !d.overflowed[0]) {
										var dir = "x",
										    to = code === 35 ? Math.abs(wrapper.width() - mCSB_container.outerWidth(false)) : 0;
									} else {
										var dir = "y",
										    to = code === 35 ? Math.abs(wrapper.height() - mCSB_container.outerHeight(false)) : 0;
									}
									_scrollTo($this, to.toString(), { dir: dir, scrollEasing: "mcsEaseInOut" });
								}
							}
						}
						break;
				}
				function _seq(a, c) {
					seq.type = o.keyboard.scrollType;
					seq.scrollAmount = o.keyboard.scrollAmount;
					if (seq.type === "stepped" && d.tweenRunning) {
						return;
					}
					_sequentialScroll($this, a, c);
				}
			}
		},

		/* -------------------- */

		/* scrolls content sequentially (used when scrolling via buttons, keyboard arrows etc.) */
		_sequentialScroll = function _sequentialScroll(el, action, trigger, e, s) {
			var d = el.data(pluginPfx),
			    o = d.opt,
			    seq = d.sequential,
			    mCSB_container = $("#mCSB_" + d.idx + "_container"),
			    once = seq.type === "stepped" ? true : false,
			    steplessSpeed = o.scrollInertia < 26 ? 26 : o.scrollInertia,
			    /* 26/1.5=17 */
			steppedSpeed = o.scrollInertia < 1 ? 17 : o.scrollInertia;
			switch (action) {
				case "on":
					seq.dir = [trigger === classes[16] || trigger === classes[15] || trigger === 39 || trigger === 37 ? "x" : "y", trigger === classes[13] || trigger === classes[15] || trigger === 38 || trigger === 37 ? -1 : 1];
					_stop(el);
					if (_isNumeric(trigger) && seq.type === "stepped") {
						return;
					}
					_on(once);
					break;
				case "off":
					_off();
					if (once || d.tweenRunning && seq.dir) {
						_on(true);
					}
					break;
			}

			/* starts sequence */
			function _on(once) {
				if (o.snapAmount) {
					seq.scrollAmount = !(o.snapAmount instanceof Array) ? o.snapAmount : seq.dir[0] === "x" ? o.snapAmount[1] : o.snapAmount[0];
				} /* scrolling snapping */
				var c = seq.type !== "stepped",
				    /* continuous scrolling */
				t = s ? s : !once ? 1000 / 60 : c ? steplessSpeed / 1.5 : steppedSpeed,
				    /* timer */
				m = !once ? 2.5 : c ? 7.5 : 40,
				    /* multiplier */
				contentPos = [Math.abs(mCSB_container[0].offsetTop), Math.abs(mCSB_container[0].offsetLeft)],
				    ratio = [d.scrollRatio.y > 10 ? 10 : d.scrollRatio.y, d.scrollRatio.x > 10 ? 10 : d.scrollRatio.x],
				    amount = seq.dir[0] === "x" ? contentPos[1] + seq.dir[1] * (ratio[1] * m) : contentPos[0] + seq.dir[1] * (ratio[0] * m),
				    px = seq.dir[0] === "x" ? contentPos[1] + seq.dir[1] * parseInt(seq.scrollAmount) : contentPos[0] + seq.dir[1] * parseInt(seq.scrollAmount),
				    to = seq.scrollAmount !== "auto" ? px : amount,
				    easing = e ? e : !once ? "mcsLinear" : c ? "mcsLinearOut" : "mcsEaseInOut",
				    onComplete = !once ? false : true;
				if (once && t < 17) {
					to = seq.dir[0] === "x" ? contentPos[1] : contentPos[0];
				}
				_scrollTo(el, to.toString(), { dir: seq.dir[0], scrollEasing: easing, dur: t, onComplete: onComplete });
				if (once) {
					seq.dir = false;
					return;
				}
				clearTimeout(seq.step);
				seq.step = setTimeout(function () {
					_on();
				}, t);
			}
			/* stops sequence */
			function _off() {
				clearTimeout(seq.step);
				_delete(seq, "step");
				_stop(el);
			}
		},

		/* -------------------- */

		/* returns a yx array from value */
		_arr = function _arr(val) {
			var o = $(this).data(pluginPfx).opt,
			    vals = [];
			if (typeof val === "function") {
				val = val();
			} /* check if the value is a single anonymous function */
			/* check if value is object or array, its length and create an array with yx values */
			if (!(val instanceof Array)) {
				/* object value (e.g. {y:"100",x:"100"}, 100 etc.) */
				vals[0] = val.y ? val.y : val.x || o.axis === "x" ? null : val;
				vals[1] = val.x ? val.x : val.y || o.axis === "y" ? null : val;
			} else {
				/* array value (e.g. [100,100]) */
				vals = val.length > 1 ? [val[0], val[1]] : o.axis === "x" ? [null, val[0]] : [val[0], null];
			}
			/* check if array values are anonymous functions */
			if (typeof vals[0] === "function") {
				vals[0] = vals[0]();
			}
			if (typeof vals[1] === "function") {
				vals[1] = vals[1]();
			}
			return vals;
		},

		/* -------------------- */

		/* translates values (e.g. "top", 100, "100px", "#id") to actual scroll-to positions */
		_to = function _to(val, dir) {
			if (val == null || typeof val == "undefined") {
				return;
			}
			var $this = $(this),
			    d = $this.data(pluginPfx),
			    o = d.opt,
			    mCSB_container = $("#mCSB_" + d.idx + "_container"),
			    wrapper = mCSB_container.parent(),
			    t = typeof val === "undefined" ? "undefined" : _typeof(val);
			if (!dir) {
				dir = o.axis === "x" ? "x" : "y";
			}
			var contentLength = dir === "x" ? mCSB_container.outerWidth(false) - wrapper.width() : mCSB_container.outerHeight(false) - wrapper.height(),
			    contentPos = dir === "x" ? mCSB_container[0].offsetLeft : mCSB_container[0].offsetTop,
			    cssProp = dir === "x" ? "left" : "top";
			switch (t) {
				case "function":
					/* this currently is not used. Consider removing it */
					return val();
					break;
				case "object":
					/* js/jquery object */
					var obj = val.jquery ? val : $(val);
					if (!obj.length) {
						return;
					}
					return dir === "x" ? _childPos(obj)[1] : _childPos(obj)[0];
					break;
				case "string":case "number":
					if (_isNumeric(val)) {
						/* numeric value */
						return Math.abs(val);
					} else if (val.indexOf("%") !== -1) {
						/* percentage value */
						return Math.abs(contentLength * parseInt(val) / 100);
					} else if (val.indexOf("-=") !== -1) {
						/* decrease value */
						return Math.abs(contentPos - parseInt(val.split("-=")[1]));
					} else if (val.indexOf("+=") !== -1) {
						/* inrease value */
						var p = contentPos + parseInt(val.split("+=")[1]);
						return p >= 0 ? 0 : Math.abs(p);
					} else if (val.indexOf("px") !== -1 && _isNumeric(val.split("px")[0])) {
						/* pixels string value (e.g. "100px") */
						return Math.abs(val.split("px")[0]);
					} else {
						if (val === "top" || val === "left") {
							/* special strings */
							return 0;
						} else if (val === "bottom") {
							return Math.abs(wrapper.height() - mCSB_container.outerHeight(false));
						} else if (val === "right") {
							return Math.abs(wrapper.width() - mCSB_container.outerWidth(false));
						} else if (val === "first" || val === "last") {
							var obj = mCSB_container.find(":" + val);
							return dir === "x" ? _childPos(obj)[1] : _childPos(obj)[0];
						} else {
							if ($(val).length) {
								/* jquery selector */
								return dir === "x" ? _childPos($(val))[1] : _childPos($(val))[0];
							} else {
								/* other values (e.g. "100em") */
								mCSB_container.css(cssProp, val);
								methods.update.call(null, $this[0]);
								return;
							}
						}
					}
					break;
			}
		},

		/* -------------------- */

		/* calls the update method automatically */
		_autoUpdate = function _autoUpdate(rem) {
			var $this = $(this),
			    d = $this.data(pluginPfx),
			    o = d.opt,
			    mCSB_container = $("#mCSB_" + d.idx + "_container");
			if (rem) {
				/* 
    removes autoUpdate timer 
    usage: _autoUpdate.call(this,"remove");
    */
				clearTimeout(mCSB_container[0].autoUpdate);
				_delete(mCSB_container[0], "autoUpdate");
				return;
			}
			upd();
			function upd() {
				clearTimeout(mCSB_container[0].autoUpdate);
				if ($this.parents("html").length === 0) {
					/* check element in dom tree */
					$this = null;
					return;
				}
				mCSB_container[0].autoUpdate = setTimeout(function () {
					/* update on specific selector(s) length and size change */
					if (o.advanced.updateOnSelectorChange) {
						d.poll.change.n = sizesSum();
						if (d.poll.change.n !== d.poll.change.o) {
							d.poll.change.o = d.poll.change.n;
							doUpd(3);
							return;
						}
					}
					/* update on main element and scrollbar size changes */
					if (o.advanced.updateOnContentResize) {
						d.poll.size.n = $this[0].scrollHeight + $this[0].scrollWidth + mCSB_container[0].offsetHeight + $this[0].offsetHeight + $this[0].offsetWidth;
						if (d.poll.size.n !== d.poll.size.o) {
							d.poll.size.o = d.poll.size.n;
							doUpd(1);
							return;
						}
					}
					/* update on image load */
					if (o.advanced.updateOnImageLoad) {
						if (!(o.advanced.updateOnImageLoad === "auto" && o.axis === "y")) {
							//by default, it doesn't run on vertical content
							d.poll.img.n = mCSB_container.find("img").length;
							if (d.poll.img.n !== d.poll.img.o) {
								d.poll.img.o = d.poll.img.n;
								mCSB_container.find("img").each(function () {
									imgLoader(this);
								});
								return;
							}
						}
					}
					if (o.advanced.updateOnSelectorChange || o.advanced.updateOnContentResize || o.advanced.updateOnImageLoad) {
						upd();
					}
				}, o.advanced.autoUpdateTimeout);
			}
			/* a tiny image loader */
			function imgLoader(el) {
				if ($(el).hasClass(classes[2])) {
					doUpd();return;
				}
				var img = new Image();
				function createDelegate(contextObject, delegateMethod) {
					return function () {
						return delegateMethod.apply(contextObject, arguments);
					};
				}
				function imgOnLoad() {
					this.onload = null;
					$(el).addClass(classes[2]);
					doUpd(2);
				}
				img.onload = createDelegate(img, imgOnLoad);
				img.src = el.src;
			}
			/* returns the total height and width sum of all elements matching the selector */
			function sizesSum() {
				if (o.advanced.updateOnSelectorChange === true) {
					o.advanced.updateOnSelectorChange = "*";
				}
				var total = 0,
				    sel = mCSB_container.find(o.advanced.updateOnSelectorChange);
				if (o.advanced.updateOnSelectorChange && sel.length > 0) {
					sel.each(function () {
						total += this.offsetHeight + this.offsetWidth;
					});
				}
				return total;
			}
			/* calls the update method */
			function doUpd(cb) {
				clearTimeout(mCSB_container[0].autoUpdate);
				methods.update.call(null, $this[0], cb);
			}
		},

		/* -------------------- */

		/* snaps scrolling to a multiple of a pixels number */
		_snapAmount = function _snapAmount(to, amount, offset) {
			return Math.round(to / amount) * amount - offset;
		},

		/* -------------------- */

		/* stops content and scrollbar animations */
		_stop = function _stop(el) {
			var d = el.data(pluginPfx),
			    sel = $("#mCSB_" + d.idx + "_container,#mCSB_" + d.idx + "_container_wrapper,#mCSB_" + d.idx + "_dragger_vertical,#mCSB_" + d.idx + "_dragger_horizontal");
			sel.each(function () {
				_stopTween.call(this);
			});
		},

		/* -------------------- */

		/* 
  ANIMATES CONTENT 
  This is where the actual scrolling happens
  */
		_scrollTo = function _scrollTo(el, to, options) {
			var d = el.data(pluginPfx),
			    o = d.opt,
			    defaults = {
				trigger: "internal",
				dir: "y",
				scrollEasing: "mcsEaseOut",
				drag: false,
				dur: o.scrollInertia,
				overwrite: "all",
				callbacks: true,
				onStart: true,
				onUpdate: true,
				onComplete: true
			},
			    options = $.extend(defaults, options),
			    dur = [options.dur, options.drag ? 0 : options.dur],
			    mCustomScrollBox = $("#mCSB_" + d.idx),
			    mCSB_container = $("#mCSB_" + d.idx + "_container"),
			    wrapper = mCSB_container.parent(),
			    totalScrollOffsets = o.callbacks.onTotalScrollOffset ? _arr.call(el, o.callbacks.onTotalScrollOffset) : [0, 0],
			    totalScrollBackOffsets = o.callbacks.onTotalScrollBackOffset ? _arr.call(el, o.callbacks.onTotalScrollBackOffset) : [0, 0];
			d.trigger = options.trigger;
			if (wrapper.scrollTop() !== 0 || wrapper.scrollLeft() !== 0) {
				/* always reset scrollTop/Left */
				$(".mCSB_" + d.idx + "_scrollbar").css("visibility", "visible");
				wrapper.scrollTop(0).scrollLeft(0);
			}
			if (to === "_resetY" && !d.contentReset.y) {
				/* callbacks: onOverflowYNone */
				if (_cb("onOverflowYNone")) {
					o.callbacks.onOverflowYNone.call(el[0]);
				}
				d.contentReset.y = 1;
			}
			if (to === "_resetX" && !d.contentReset.x) {
				/* callbacks: onOverflowXNone */
				if (_cb("onOverflowXNone")) {
					o.callbacks.onOverflowXNone.call(el[0]);
				}
				d.contentReset.x = 1;
			}
			if (to === "_resetY" || to === "_resetX") {
				return;
			}
			if ((d.contentReset.y || !el[0].mcs) && d.overflowed[0]) {
				/* callbacks: onOverflowY */
				if (_cb("onOverflowY")) {
					o.callbacks.onOverflowY.call(el[0]);
				}
				d.contentReset.x = null;
			}
			if ((d.contentReset.x || !el[0].mcs) && d.overflowed[1]) {
				/* callbacks: onOverflowX */
				if (_cb("onOverflowX")) {
					o.callbacks.onOverflowX.call(el[0]);
				}
				d.contentReset.x = null;
			}
			if (o.snapAmount) {
				/* scrolling snapping */
				var snapAmount = !(o.snapAmount instanceof Array) ? o.snapAmount : options.dir === "x" ? o.snapAmount[1] : o.snapAmount[0];
				to = _snapAmount(to, snapAmount, o.snapOffset);
			}
			switch (options.dir) {
				case "x":
					var mCSB_dragger = $("#mCSB_" + d.idx + "_dragger_horizontal"),
					    property = "left",
					    contentPos = mCSB_container[0].offsetLeft,
					    limit = [mCustomScrollBox.width() - mCSB_container.outerWidth(false), mCSB_dragger.parent().width() - mCSB_dragger.width()],
					    scrollTo = [to, to === 0 ? 0 : to / d.scrollRatio.x],
					    tso = totalScrollOffsets[1],
					    tsbo = totalScrollBackOffsets[1],
					    totalScrollOffset = tso > 0 ? tso / d.scrollRatio.x : 0,
					    totalScrollBackOffset = tsbo > 0 ? tsbo / d.scrollRatio.x : 0;
					break;
				case "y":
					var mCSB_dragger = $("#mCSB_" + d.idx + "_dragger_vertical"),
					    property = "top",
					    contentPos = mCSB_container[0].offsetTop,
					    limit = [mCustomScrollBox.height() - mCSB_container.outerHeight(false), mCSB_dragger.parent().height() - mCSB_dragger.height()],
					    scrollTo = [to, to === 0 ? 0 : to / d.scrollRatio.y],
					    tso = totalScrollOffsets[0],
					    tsbo = totalScrollBackOffsets[0],
					    totalScrollOffset = tso > 0 ? tso / d.scrollRatio.y : 0,
					    totalScrollBackOffset = tsbo > 0 ? tsbo / d.scrollRatio.y : 0;
					break;
			}
			if (scrollTo[1] < 0 || scrollTo[0] === 0 && scrollTo[1] === 0) {
				scrollTo = [0, 0];
			} else if (scrollTo[1] >= limit[1]) {
				scrollTo = [limit[0], limit[1]];
			} else {
				scrollTo[0] = -scrollTo[0];
			}
			if (!el[0].mcs) {
				_mcs(); /* init mcs object (once) to make it available before callbacks */
				if (_cb("onInit")) {
					o.callbacks.onInit.call(el[0]);
				} /* callbacks: onInit */
			}
			clearTimeout(mCSB_container[0].onCompleteTimeout);
			_tweenTo(mCSB_dragger[0], property, Math.round(scrollTo[1]), dur[1], options.scrollEasing);
			if (!d.tweenRunning && (contentPos === 0 && scrollTo[0] >= 0 || contentPos === limit[0] && scrollTo[0] <= limit[0])) {
				return;
			}
			_tweenTo(mCSB_container[0], property, Math.round(scrollTo[0]), dur[0], options.scrollEasing, options.overwrite, {
				onStart: function onStart() {
					if (options.callbacks && options.onStart && !d.tweenRunning) {
						/* callbacks: onScrollStart */
						if (_cb("onScrollStart")) {
							_mcs();o.callbacks.onScrollStart.call(el[0]);
						}
						d.tweenRunning = true;
						_onDragClasses(mCSB_dragger);
						d.cbOffsets = _cbOffsets();
					}
				}, onUpdate: function onUpdate() {
					if (options.callbacks && options.onUpdate) {
						/* callbacks: whileScrolling */
						if (_cb("whileScrolling")) {
							_mcs();o.callbacks.whileScrolling.call(el[0]);
						}
					}
				}, onComplete: function onComplete() {
					if (options.callbacks && options.onComplete) {
						if (o.axis === "yx") {
							clearTimeout(mCSB_container[0].onCompleteTimeout);
						}
						var t = mCSB_container[0].idleTimer || 0;
						mCSB_container[0].onCompleteTimeout = setTimeout(function () {
							/* callbacks: onScroll, onTotalScroll, onTotalScrollBack */
							if (_cb("onScroll")) {
								_mcs();o.callbacks.onScroll.call(el[0]);
							}
							if (_cb("onTotalScroll") && scrollTo[1] >= limit[1] - totalScrollOffset && d.cbOffsets[0]) {
								_mcs();o.callbacks.onTotalScroll.call(el[0]);
							}
							if (_cb("onTotalScrollBack") && scrollTo[1] <= totalScrollBackOffset && d.cbOffsets[1]) {
								_mcs();o.callbacks.onTotalScrollBack.call(el[0]);
							}
							d.tweenRunning = false;
							mCSB_container[0].idleTimer = 0;
							_onDragClasses(mCSB_dragger, "hide");
						}, t);
					}
				}
			});
			/* checks if callback function exists */
			function _cb(cb) {
				return d && o.callbacks[cb] && typeof o.callbacks[cb] === "function";
			}
			/* checks whether callback offsets always trigger */
			function _cbOffsets() {
				return [o.callbacks.alwaysTriggerOffsets || contentPos >= limit[0] + tso, o.callbacks.alwaysTriggerOffsets || contentPos <= -tsbo];
			}
			/* 
   populates object with useful values for the user 
   values: 
   	content: this.mcs.content
   	content top position: this.mcs.top 
   	content left position: this.mcs.left 
   	dragger top position: this.mcs.draggerTop 
   	dragger left position: this.mcs.draggerLeft 
   	scrolling y percentage: this.mcs.topPct 
   	scrolling x percentage: this.mcs.leftPct 
   	scrolling direction: this.mcs.direction
   */
			function _mcs() {
				var cp = [mCSB_container[0].offsetTop, mCSB_container[0].offsetLeft],
				    /* content position */
				dp = [mCSB_dragger[0].offsetTop, mCSB_dragger[0].offsetLeft],
				    /* dragger position */
				cl = [mCSB_container.outerHeight(false), mCSB_container.outerWidth(false)],
				    /* content length */
				pl = [mCustomScrollBox.height(), mCustomScrollBox.width()]; /* content parent length */
				el[0].mcs = {
					content: mCSB_container, /* original content wrapper as jquery object */
					top: cp[0], left: cp[1], draggerTop: dp[0], draggerLeft: dp[1],
					topPct: Math.round(100 * Math.abs(cp[0]) / (Math.abs(cl[0]) - pl[0])), leftPct: Math.round(100 * Math.abs(cp[1]) / (Math.abs(cl[1]) - pl[1])),
					direction: options.dir
				};
				/* 
    this refers to the original element containing the scrollbar(s)
    usage: this.mcs.top, this.mcs.leftPct etc. 
    */
			}
		},

		/* -------------------- */

		/* 
  CUSTOM JAVASCRIPT ANIMATION TWEEN 
  Lighter and faster than jquery animate() and css transitions 
  Animates top/left properties and includes easings 
  */
		_tweenTo = function _tweenTo(el, prop, to, duration, easing, overwrite, callbacks) {
			if (!el._mTween) {
				el._mTween = { top: {}, left: {} };
			}
			var callbacks = callbacks || {},
			    onStart = callbacks.onStart || function () {},
			    onUpdate = callbacks.onUpdate || function () {},
			    onComplete = callbacks.onComplete || function () {},
			    startTime = _getTime(),
			    _delay,
			    progress = 0,
			    from = el.offsetTop,
			    elStyle = el.style,
			    _request,
			    tobj = el._mTween[prop];
			if (prop === "left") {
				from = el.offsetLeft;
			}
			var diff = to - from;
			tobj.stop = 0;
			if (overwrite !== "none") {
				_cancelTween();
			}
			_startTween();
			function _step() {
				if (tobj.stop) {
					return;
				}
				if (!progress) {
					onStart.call();
				}
				progress = _getTime() - startTime;
				_tween();
				if (progress >= tobj.time) {
					tobj.time = progress > tobj.time ? progress + _delay - (progress - tobj.time) : progress + _delay - 1;
					if (tobj.time < progress + 1) {
						tobj.time = progress + 1;
					}
				}
				if (tobj.time < duration) {
					tobj.id = _request(_step);
				} else {
					onComplete.call();
				}
			}
			function _tween() {
				if (duration > 0) {
					tobj.currVal = _ease(tobj.time, from, diff, duration, easing);
					elStyle[prop] = Math.round(tobj.currVal) + "px";
				} else {
					elStyle[prop] = to + "px";
				}
				onUpdate.call();
			}
			function _startTween() {
				_delay = 1000 / 60;
				tobj.time = progress + _delay;
				_request = !window.requestAnimationFrame ? function (f) {
					_tween();return setTimeout(f, 0.01);
				} : window.requestAnimationFrame;
				tobj.id = _request(_step);
			}
			function _cancelTween() {
				if (tobj.id == null) {
					return;
				}
				if (!window.requestAnimationFrame) {
					clearTimeout(tobj.id);
				} else {
					window.cancelAnimationFrame(tobj.id);
				}
				tobj.id = null;
			}
			function _ease(t, b, c, d, type) {
				switch (type) {
					case "linear":case "mcsLinear":
						return c * t / d + b;
						break;
					case "mcsLinearOut":
						t /= d;t--;return c * Math.sqrt(1 - t * t) + b;
						break;
					case "easeInOutSmooth":
						t /= d / 2;
						if (t < 1) return c / 2 * t * t + b;
						t--;
						return -c / 2 * (t * (t - 2) - 1) + b;
						break;
					case "easeInOutStrong":
						t /= d / 2;
						if (t < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
						t--;
						return c / 2 * (-Math.pow(2, -10 * t) + 2) + b;
						break;
					case "easeInOut":case "mcsEaseInOut":
						t /= d / 2;
						if (t < 1) return c / 2 * t * t * t + b;
						t -= 2;
						return c / 2 * (t * t * t + 2) + b;
						break;
					case "easeOutSmooth":
						t /= d;t--;
						return -c * (t * t * t * t - 1) + b;
						break;
					case "easeOutStrong":
						return c * (-Math.pow(2, -10 * t / d) + 1) + b;
						break;
					case "easeOut":case "mcsEaseOut":default:
						var ts = (t /= d) * t,
						    tc = ts * t;
						return b + c * (0.499999999999997 * tc * ts + -2.5 * ts * ts + 5.5 * tc + -6.5 * ts + 4 * t);
				}
			}
		},

		/* -------------------- */

		/* returns current time */
		_getTime = function _getTime() {
			if (window.performance && window.performance.now) {
				return window.performance.now();
			} else {
				if (window.performance && window.performance.webkitNow) {
					return window.performance.webkitNow();
				} else {
					if (Date.now) {
						return Date.now();
					} else {
						return new Date().getTime();
					}
				}
			}
		},

		/* -------------------- */

		/* stops a tween */
		_stopTween = function _stopTween() {
			var el = this;
			if (!el._mTween) {
				el._mTween = { top: {}, left: {} };
			}
			var props = ["top", "left"];
			for (var i = 0; i < props.length; i++) {
				var prop = props[i];
				if (el._mTween[prop].id) {
					if (!window.requestAnimationFrame) {
						clearTimeout(el._mTween[prop].id);
					} else {
						window.cancelAnimationFrame(el._mTween[prop].id);
					}
					el._mTween[prop].id = null;
					el._mTween[prop].stop = 1;
				}
			}
		},

		/* -------------------- */

		/* deletes a property (avoiding the exception thrown by IE) */
		_delete = function _delete(c, m) {
			try {
				delete c[m];
			} catch (e) {
				c[m] = null;
			}
		},

		/* -------------------- */

		/* detects left mouse button */
		_mouseBtnLeft = function _mouseBtnLeft(e) {
			return !(e.which && e.which !== 1);
		},

		/* -------------------- */

		/* detects if pointer type event is touch */
		_pointerTouch = function _pointerTouch(e) {
			var t = e.originalEvent.pointerType;
			return !(t && t !== "touch" && t !== 2);
		},

		/* -------------------- */

		/* checks if value is numeric */
		_isNumeric = function _isNumeric(val) {
			return !isNaN(parseFloat(val)) && isFinite(val);
		},

		/* -------------------- */

		/* returns element position according to content */
		_childPos = function _childPos(el) {
			var p = el.parents(".mCSB_container");
			return [el.offset().top - p.offset().top, el.offset().left - p.offset().left];
		},

		/* -------------------- */

		/* checks if browser tab is hidden/inactive via Page Visibility API */
		_isTabHidden = function _isTabHidden() {
			var prop = _getHiddenProp();
			if (!prop) return false;
			return document[prop];
			function _getHiddenProp() {
				var pfx = ["webkit", "moz", "ms", "o"];
				if ("hidden" in document) return "hidden"; //natively supported
				for (var i = 0; i < pfx.length; i++) {
					//prefixed
					if (pfx[i] + "Hidden" in document) return pfx[i] + "Hidden";
				}
				return null; //not supported
			}
		};
		/* -------------------- */

		/* 
  ----------------------------------------
  PLUGIN SETUP 
  ----------------------------------------
  */

		/* plugin constructor functions */
		$.fn[pluginNS] = function (method) {
			/* usage: $(selector).mCustomScrollbar(); */
			if (methods[method]) {
				return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
			} else if ((typeof method === "undefined" ? "undefined" : _typeof(method)) === "object" || !method) {
				return methods.init.apply(this, arguments);
			} else {
				$.error("Method " + method + " does not exist");
			}
		};
		$[pluginNS] = function (method) {
			/* usage: $.mCustomScrollbar(); */
			if (methods[method]) {
				return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
			} else if ((typeof method === "undefined" ? "undefined" : _typeof(method)) === "object" || !method) {
				return methods.init.apply(this, arguments);
			} else {
				$.error("Method " + method + " does not exist");
			}
		};

		/* 
  allow setting plugin default options. 
  usage: $.mCustomScrollbar.defaults.scrollInertia=500; 
  to apply any changed default options on default selectors (below), use inside document ready fn 
  e.g.: $(document).ready(function(){ $.mCustomScrollbar.defaults.scrollInertia=500; });
  */
		$[pluginNS].defaults = defaults;

		/* 
  add window object (window.mCustomScrollbar) 
  usage: if(window.mCustomScrollbar){console.log("custom scrollbar plugin loaded");}
  */
		window[pluginNS] = true;

		$(window).bind("load", function () {

			$(defaultSelector)[pluginNS](); /* add scrollbars automatically on default selector */

			/* extend jQuery expressions */
			$.extend($.expr[":"], {
				/* checks if element is within scrollable viewport */
				mcsInView: $.expr[":"].mcsInView || function (el) {
					var $el = $(el),
					    content = $el.parents(".mCSB_container"),
					    wrapper,
					    cPos;
					if (!content.length) {
						return;
					}
					wrapper = content.parent();
					cPos = [content[0].offsetTop, content[0].offsetLeft];
					return cPos[0] + _childPos($el)[0] >= 0 && cPos[0] + _childPos($el)[0] < wrapper.height() - $el.outerHeight(false) && cPos[1] + _childPos($el)[1] >= 0 && cPos[1] + _childPos($el)[1] < wrapper.width() - $el.outerWidth(false);
				},
				/* checks if element or part of element is in view of scrollable viewport */
				mcsInSight: $.expr[":"].mcsInSight || function (el, i, m) {
					var $el = $(el),
					    elD,
					    content = $el.parents(".mCSB_container"),
					    wrapperView,
					    pos,
					    wrapperViewPct,
					    pctVals = m[3] === "exact" ? [[1, 0], [1, 0]] : [[0.9, 0.1], [0.6, 0.4]];
					if (!content.length) {
						return;
					}
					elD = [$el.outerHeight(false), $el.outerWidth(false)];
					pos = [content[0].offsetTop + _childPos($el)[0], content[0].offsetLeft + _childPos($el)[1]];
					wrapperView = [content.parent()[0].offsetHeight, content.parent()[0].offsetWidth];
					wrapperViewPct = [elD[0] < wrapperView[0] ? pctVals[0] : pctVals[1], elD[1] < wrapperView[1] ? pctVals[0] : pctVals[1]];
					return pos[0] - wrapperView[0] * wrapperViewPct[0][0] < 0 && pos[0] + elD[0] - wrapperView[0] * wrapperViewPct[0][1] >= 0 && pos[1] - wrapperView[1] * wrapperViewPct[1][0] < 0 && pos[1] + elD[1] - wrapperView[1] * wrapperViewPct[1][1] >= 0;
				},
				/* checks if element is overflowed having visible scrollbar(s) */
				mcsOverflow: $.expr[":"].mcsOverflow || function (el) {
					var d = $(el).data(pluginPfx);
					if (!d) {
						return;
					}
					return d.overflowed[0] || d.overflowed[1];
				}
			});
		});
	});
});
"use strict";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

jQuery(document).ready(function ($) {
    if ($('#countdown').length) {
        var second = 1000,
            minute = second * 60,
            hour = minute * 60,
            day = hour * 24;
        var today = new Date(),
            dd = String(today.getDate()).padStart(2, "0"),
            mm = String(today.getMonth() + 1).padStart(2, "0"),
            yyyy = today.getFullYear(),
            nextYear = yyyy + 1,
            dayData = document.getElementById("countdown").dataset.day,
            monthData = document.getElementById("countdown").dataset.month,
            dayMonth = monthData + "/" + dayData + "/",
            birthday = dayMonth + yyyy;
        today = mm + "/" + dd + "/" + yyyy;
        if (today > birthday) {
            birthday = dayMonth + nextYear;
        }
        var countDown = new Date(birthday).getTime(),
            x = setInterval(function () {
            var now = new Date().getTime(),
                distance = countDown - now;
            document.getElementById("days").innerText = Math.floor(distance / day), document.getElementById("hours").innerText = Math.floor(distance % day / hour), document.getElementById("minutes").innerText = Math.floor(distance % hour / minute), document.getElementById("seconds").innerText = Math.floor(distance % minute / second);
        }, 0);
    }
    if ($('progress').length) {
        var bar = document.querySelector("progress");
        addEventListener("scroll", function () {
            var max = document.querySelector('.company-description').scrollHeight - innerHeight;
            var percent = pageYOffset / max * 100;
            bar.setAttribute('value', percent);
        });
    }
    if ($('.single-courses').length) {
        setTimeout(function () {
            var wrapVcRow = $('.single-courses .vc_row[id]').parent();
            $('.banner-gradient').after(wrapVcRow);
            $('.single-courses .wpb-content-wrapper').remove();
        }, 1000);
    }
    if ($('.single-testimonials .vc_col-lg-8 img').length) {
        $('.single-testimonials .vc_col-lg-8 img').each(function () {
            var imgChild = $(this).parents('p').children('img').length;
            if ($(this).parents('p').children('img')) {
                $(this).parents('p').addClass('images-' + imgChild);
            }
            if ($(this).parents('p').hasClass('images-3')) {
                $(this).parents('p').addClass('gallery-one-two');
            }
        });
    }
    if (navigator.userAgent.match(/(iPod|iPhone|iPad|Macintosh)/)) {
        var string = navigator.userAgent;
        var pos = string.indexOf('Version/');
        var version = Number(string.slice(pos + 8, pos + 12));
        if (version < 14) {
            $('body').addClass('iphone');
        }
    }
    // SALARY EXPECTATIONS BLOCK START
    if ($('.icon-smile').length) {
        $('.block-of-salaries .range li:not(.icon-smile)').each(function () {
            var rangeEl = $(this).width();
            var rangeIc = $('.icon-smile'),
                rangeWidth = $(this).parent().width() - 29;
            var positionEl = rangeIc.offset().left;
            if ($(this).hasClass('active')) {
                var rangeElActive = $(this).data('price');
                $(this).parent().find('.icon-smile').addClass('left').attr('data-price', rangeElActive);
            }
            rangeIc.on('touchmove', function (e) {

                var touchLocation = e.originalEvent.touches[0].clientX - positionEl - 28;
                if (touchLocation > 0 && touchLocation < rangeWidth) {
                    rangeIc.css('left', touchLocation + 'px');
                    if (touchLocation <= rangeEl) {
                        $(this).removeClass('center').addClass('left');
                        $('.block-of-salaries .range li').removeClass('active');
                        $(this).parent().find('li:first').addClass('active');
                        var rangeElActive = $(this).parent().find('li.active').data('price');
                        $(this).parent().find('.icon-smile').attr('data-price', rangeElActive);
                        $(this).parent().find('li.active a').trigger("click");
                    } else if (touchLocation <= rangeEl * 2) {
                        $(this).removeClass('left').addClass('center');
                        $('.block-of-salaries .range li').removeClass('active');
                        $(this).parent().find('li:nth-of-type(2)').addClass('active');
                        var rangeElActive = $(this).parent().find('li.active').data('price');
                        $(this).parent().find('.icon-smile').attr('data-price', rangeElActive);
                        $(this).parent().find('li.active a').trigger("click");
                    } else if (touchLocation <= rangeEl * 3) {
                        $(this).removeClass('right').addClass('center');
                        $('.block-of-salaries .range li').removeClass('active');
                        $(this).parent().find('li:nth-of-type(3)').addClass('active');
                        var rangeElActive = $(this).parent().find('li.active').data('price');
                        $(this).parent().find('.icon-smile').attr('data-price', rangeElActive);
                        $(this).parent().find('li.active a').trigger("click");
                    } else if (touchLocation <= rangeEl * 4) {
                        $('.block-of-salaries .range li').removeClass('active');
                        $(this).removeClass('center').addClass('right');
                        $(this).parent().find('li:nth-of-type(4)').addClass('active');
                        var rangeElActive = $(this).parent().find('li.active').data('price');
                        $(this).parent().find('.icon-smile').attr('data-price', rangeElActive);
                        $(this).parent().find('li.active a').trigger("click");
                    }
                }
            });
            rangeIc.draggable({
                containment: ".block-of-salaries .range",
                axis: "x",
                drag: function drag(event, ui) {
                    if (ui.position.left <= rangeEl) {
                        $(this).removeClass('center').addClass('left');
                        $('.block-of-salaries .range li').removeClass('active');
                        $(this).parent().find('li:first').addClass('active');
                        var rangeElActive = $(this).parent().find('li.active').data('price');
                        $(this).parent().find('.icon-smile').attr('data-price', rangeElActive);
                        $(this).parent().find('li.active a').trigger("click");
                    } else if (ui.position.left <= rangeEl * 2) {
                        $(this).removeClass('left').addClass('center');
                        $('.block-of-salaries .range li').removeClass('active');
                        $(this).parent().find('li:nth-of-type(2)').addClass('active');
                        var rangeElActive = $(this).parent().find('li.active').data('price');
                        $(this).parent().find('.icon-smile').attr('data-price', rangeElActive);
                        $(this).parent().find('li.active a').trigger("click");
                    } else if (ui.position.left <= rangeEl * 3) {
                        $(this).removeClass('right').addClass('center');
                        $('.block-of-salaries .range li').removeClass('active');
                        $(this).parent().find('li:nth-of-type(3)').addClass('active');
                        var rangeElActive = $(this).parent().find('li.active').data('price');
                        $(this).parent().find('.icon-smile').attr('data-price', rangeElActive);
                        $(this).parent().find('li.active a').trigger("click");
                    } else if (ui.position.left <= rangeEl * 4) {
                        $('.block-of-salaries .range li').removeClass('active');
                        $(this).removeClass('center').addClass('right');
                        $(this).parent().find('li:nth-of-type(4)').addClass('active');
                        var rangeElActive = $(this).parent().find('li.active').data('price');
                        $(this).parent().find('.icon-smile').attr('data-price', rangeElActive);
                        $(this).parent().find('li.active a').trigger("click");
                    }
                }
            });
        });
    };
    // SALARY EXPECTATIONS BLOCK END
    var courseAccordion = function courseAccordion() {
        $('.accordion-item .dfr').each(function () {
            var title = $(this).find('h4'),
                ulEl = $(this).find('ul').width() + 60;
            title.css('width', 'calc(100% - ' + ulEl + 'px');
        });
    };
    //    var paymentBlock = function(){
    //        var descPayment = $('.payment-block .description[id]');
    //        if($(window).width() < 768){
    //            $('.payment-block form').prepend(descPayment);
    //        }else{
    //            $('.payment-block .dfr:not(.radio-buttons)').prepend(descPayment);
    //        }
    //    }
    //     if($('.payment-block').length){
    //        paymentBlock();
    //     }
    if ($('.course-content .accordion-item').length) {
        courseAccordion();
    }
    if ($('.vc_btn3-container[class*="icon-"]').length) {
        var ButtonEl = $('.vc_btn3-container[class*="icon-"]').attr('class'),
            classButton = ButtonEl.toString().match(/icon-[a-zA-Z0-9-]+/gm).toString();
        $('.vc_btn3-container[class*="icon-"]').each(function () {
            $(this).children().addClass(classButton);
            $(this).removeClass(classButton);
        });
    }
    // DELITE EMPTY P TAG START
    $('p:not([class])').filter(function () {
        if ($(this).children().length == 0) {
            $(this).text($(this).text().replace(/\s{2,}/g, ' '));
            if (this.innerHTML == '&nbsp;') {
                return this.innerHTML == '&nbsp;';
            } else if (this.innerHTML == ' ') {
                return this.innerHTML == ' ';
            } else if (this.innerHTML == '') {
                return this.innerHTML == '';
            }
        }
    }).remove();
    // DELITE EMPTY P TAG END

    // SLIDER START
    // SCROLL PLUGIN START
    if ($(window).width() > 767) {
        $(".wpb_text_column.description .wpb_wrapper").each(function () {
            if ($(this).height() >= 200) {
                $(this).mCustomScrollbar({
                    axis: "y"
                });
            }
        });
    }
    // SCROLL PLUGIN END
    // COURSES SLICK SLIDER START
    if ($('.courses-slider').length) {
        var _$$slick;

        $('.courses-slider').slick((_$$slick = {
            slidesToShow: 3,
            slidesToScroll: 1,
            prevArrow: '<i class="icon-arrow-left"></i>',
            nextArrow: '<i class="icon-arrow-right"></i>',
            autoplay: true,
            autoplaySpeed: 2000
        }, _defineProperty(_$$slick, "slidesToShow", 1), _defineProperty(_$$slick, "variableWidth", true), _$$slick));
    }
    // COURSES SLICK SLIDER END
    // LOGO SLICK SLIDER START
    if ($('.logo-slider').length) {
        $('.logo-slider').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 2000,
            draggable: true,
            prevArrow: '<i class="icon-arrow-left"></i>',
            nextArrow: '<i class="icon-arrow-right"></i>',
            responsive: [{
                breakpoint: 1199,
                settings: {
                    slidesToShow: 5
                }
            }, {
                breakpoint: 991,
                settings: {
                    slidesToShow: 4
                }
            }, {
                breakpoint: 767,
                settings: {
                    slidesToShow: 3,
                    arrows: false
                }
            }]
        });
    }
    // LOGO SLICK SLIDER END
    // EVENT SLICK SLIDER START
    if ($('.events-block').length) {
        $('.events-block:not(.no-slider)').slick({
            prevArrow: '<i class="icon-arrow-left"></i>',
            nextArrow: '<i class="icon-arrow-right"></i>',
            autoplay: true,
            autoplaySpeed: 2000,
            responsive: [{
                breakpoint: 992,
                settings: {
                    adaptiveHeight: true
                }
            }]
        });
    }
    // EVENT SLICK SLIDER END
    // SLIDER END
    // GALLERY IMAGE START
    if ($('.gallery-one-two').length) {
        $('.gallery-one-two').each(function () {
            var figureEl = $(this).children('img');
            for (var i = 0; i < figureEl.length; i += 3) {
                if (i < 2) {
                    figureEl.slice(i, i + 1).wrapAll('<div class="left-block"></div>');
                }
                var figureEl = $(this).children('img');
                if (i < 3) {
                    figureEl.slice(i, i + 2).wrapAll('<div class="right-block"></div>');
                }
            }
        });
    }
    // GALLERY IMAGE END
    var prevScrollpos = window.pageYOffset;
    var topHeadHight = $('.top-head').outerHeight();
    var adminbar = $('#wpadminbar').height();
    window.onscroll = function () {
        // HEADER SCROLL HIDE TOP HEADER START
        if ($(document).scrollTop() > topHeadHight) {
            if ($('.admin-bar').length) {
                $("header").css({
                    top: "-4px",
                    background: "#F1F5F9"
                }).addClass('fix');
                $("header .hide, .fix-menu").addClass('show');
            } else {
                $("header").css({
                    top: -topHeadHight + "px",
                    background: "#F1F5F9"
                }).addClass('fix');
                $("header .hide, .fix-menu").addClass('show');
            }
        } else {
            if ($('.admin-bar').length) {
                $("header").css({
                    top: adminbar + 'px',
                    background: "transparent"
                }).removeClass('fix');
                $("header .hide, .fix-menu").removeClass('show');
            } else {
                $("header").css({
                    top: "0",
                    background: "transparent"
                }).removeClass('fix');
                $("header .hide, .fix-menu").removeClass('show');
            }
        }
        // HEADER SCROLL HIDE TOP HEADER END
        // NAVIGATION POSITION TOP START
        var currentScrollPos = window.pageYOffset;
        if (prevScrollpos > currentScrollPos) {
            if ($('.navigation').length) {
                if ($(window).width() < 1025) {
                    if ($('.admin-bar').length) {
                        $('.navigation').css('top', "87px");
                    } else {
                        $('.navigation').css('top', "60px");
                    }
                } else {
                    if ($('.admin-bar').length) {
                        $('.navigation').css('top', "107px");
                    } else {
                        $('.navigation').css('top', "80px");
                    }
                }
            }
        } else {
            if ($('.navigation').length) {
                if ($(window).width() < 1025) {
                    if ($('.admin-bar').length) {
                        $('.navigation').css('top', "87px");
                    } else {
                        $('.navigation').css('top', "60px");
                    }
                } else {
                    if ($('.admin-bar').length) {
                        $('.navigation').css('top', "107px");
                    } else {
                        $('.navigation').css('top', "80px");
                    }
                }
            }
        }
        prevScrollpos = currentScrollPos;
        // NAVIGATION POSITION TOP 
    };
    // NAVIGATION
    if ($('.navigation').length) {
        var nav = $('.navigation .menu'),
            navHeight = nav.outerHeight(),
            headerHeight = $('header').height() + 30;
        if ($(window).width() < 1025) {
            var navigation = $('.navigation').offset().top + 60;
        } else {
            var navigation = $('.navigation').offset().top + 60;
        }

        var navigationHeight = $('.navigation').outerHeight(true);
        $(window).on('scroll', function () {
            var position = $(this).scrollTop();
            if ($(window).scrollTop() > navigation - headerHeight) {
                $('.navigation').addClass('fixed');
                $('#courses').css('padding-top', navigationHeight + 'px');
            } else {
                $('.navigation').removeClass('fixed');
                $('#courses').css('padding-top', '0');
            }
        });
    }
    // NAVIGATION
    $('body').css('padding-top', headerHeight + 'px');
    // COURSES MIX IT UP PLUGIN START
    if ($('.sidebar').length) {
        $('.courses').mixItUp({
            selectors: {
                target: '.mix',
                filter: '[data-filter]'
            },
            animation: {
                "duration": 1,
                "nudge": false,
                "reverseOut": false,
                "effects": ""
            }
        });
    }
    // COURSES MIX IT UP PLUGIN END
    // ACCORDION START
    if ($('.accordion, .faq').length) {
        $('.accordion-item').click(function () {
            if ($(this).hasClass('open')) {
                $(this).removeClass('open');
            } else {
                $(this).addClass('open');
            }
        });
    }
    // ACCORDION END
    // TABS START
    var tabsClick = function tabsClick() {
        $('[data-toggle="tab"]').click(function (e) {
            e.preventDefault();
            var hrefEl = $(this).attr('href');
            $(this).parents('ul').children().removeClass('active');
            $(hrefEl).parents('.tabs').children('div').removeClass('active');
            $(this).parent().addClass('active');
            $(hrefEl).addClass('active');
            //            $(this)[0].scrollIntoView({block: "nearest"}); 
        });
    };
    tabsClick();
    // TABS END
    // SELECT PLUGIN START
    $('.select select').select2({
        minimumResultsForSearch: Infinity
    });
    // SELECT PLUGIN END
    // LANGUAGE CHECK START
    $('.language input').click(function () {
        var valEl = $(this).val();
        $('[value="' + valEl + '"]').prop("checked", true);
    });
    // LANGUAGE CHECK END
    //    if ($('.leave-review').length) {
    //        $('.leave-review .next').click(function (e) {
    //            e.preventDefault();
    //            $(this).parents('.modal-body').removeClass('show').next().addClass('show');
    //            $(this).parents('.modal-content').find('.modal-header p').hide();
    //            $(this).parents('.modal-content').find('.steps li.active').removeClass('active').next().addClass('active');
    //        });
    //        $('.leave-review .prev').click(function (e) {
    //            e.preventDefault();
    //            $(this).parents('.modal-body').removeClass('show').prev().addClass('show');
    //            $(this).parents('.modal-content').find('.steps li.active').removeClass('active').prev().addClass('active');
    //            if ($(this).parents('.modal-content').find('.steps').children("li:first").hasClass('active')) {
    //                $(this).parents('.modal-content').find('.steps').next().show();
    //            }
    //        });
    //    }
    // BOTTOM FIXED MENU START
    if ($(window).width() < 1200) {
        $(window).on('scroll', function () {
            var footerPosition = $('footer').offset().top,
                footerHeight = $('footer').outerHeight();
            if ($(window).scrollTop() > footerPosition - footerHeight - 55) {
                $('.fix-menu').hide();
            } else {
                $('.fix-menu').show();
            }
        });
    }
    // BOTTOM FIXED MENU END
    // HEDER TRANSFORM DESCTOP TO MOBILE START
    var header = function header() {
        if ($('.admin-bar').length) {
            var adminbar = $('#wpadminbar').height();
            if ($('.home').length) {
                var headerHeight = $('header').outerHeight() + adminbar;
            } else {
                var headTop = $('.top-head').outerHeight();
                var headerHeight = adminbar + headTop;
            }
            $('header').css('top', adminbar + 'px');
        } else if ($('.banner-gradient').length) {
            var topHeadHeight = $('.top-head').height();
            $('body').css('padding-top', topHeadHeight + 'px');
        } else {
            var headerHeight = $('header').outerHeight();
        }
        $('body').css('padding-top', headerHeight + 'px');
        if ($(window).width() < 1025) {
            var burgerEl = $('.burger-menu').clone(),
                blockHide = $('header .hide'),
                buttonService = $('.top-head .call-back').clone(),
                logoblock = $('.bottom-head .logo').clone();
            if ($('.bottom-head .wpb_wrapper > .call-back').length == 0) {
                $('header .menu').wrap('<div class="menu-block"></div>');
                $('header .menu-block').prepend(burgerEl);
                $('header .menu-block').prepend(logoblock);
                $('header .menu-block').append(blockHide);
                $('header .menu-block').after(buttonService);
                $('.burger-menu').click(function () {
                    $('.burger-menu').toggleClass('close');
                    $('header .menu-block').toggleClass('open');
                    $('body').toggleClass('modal-open');
                });
            }
        } else {
            var blockHide = $('header .hide');
            if ($('header .menu-block').length) {
                $('.bottom-head .wpb_wrapper  > .call-back').remove();
                $('.menu-block .burger-menu').remove();
                $('.menu-block .logo').remove();
                $('.bottom-head .menu').unwrap('.menu-block');
                $('.bottom-head .menu').before(blockHide);
            }
        }
    };
    $(window).on('resize', function () {
        if ($(window).width() > 991) {
            if ($('.course-content .accordion-item').length) {
                courseAccordion();
            }
        }
        header();
    });
    header();
    // HEDER TRANSFORM DESCTOP TO MOBILE END
    // SIDEBAR MOBILE TRANSFER CHOOSE BLOCK START
    if ($(window).width() < 768) {
        if ($('.sidebar').length) {
            var chooseBlock = $('.sidebar .choose');
            $('.sidebar').next().after(chooseBlock);
        }
    }
    // SIDEBAR MOBILE TRANSFER CHOOSE BLOCK END
    $(document).mouseup(function (e) {
        var folder = $("header .menu-block"),
            headerButtonCallback = $('header .icon-phone');
        if (!folder.is(e.target) && folder.has(e.target).length === 0) {
            if ($('.menu-block').hasClass('open')) {
                $('.burger-menu').removeClass('close');
                folder.removeClass('open');
                $('body').removeClass('modal-open');
            }
        }
        if (!headerButtonCallback.is(e.target) && headerButtonCallback.has(e.target).length === 0) {
            if (headerButtonCallback.hasClass('open')) {
                headerButtonCallback.removeClass('open');
            }
        }
    });
    //MODAL START
    // CROP IMAGE MODAL START
    var $image_crop = $('#crop-image').croppie({
        enableExif: true,
        viewport: {
            width: 150,
            height: 150
        },
        boundary: {
            width: 300,
            height: 300
        }
    });
    $('input[type="file"]').on('change', function () {
        var reader = new FileReader();
        reader.onload = function (event) {
            $image_crop.croppie('bind', {
                url: event.target.result
            }).then(function () {});
        };
        reader.readAsDataURL(this.files[0]);
        $('.crop-image-modal').modal('show');
        $('.crop-image-modal .name-file').html(this.files[0].name);
    });
    $('#crop-save-image').click(function (event) {
        $image_crop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {

            var contLength = $("#preview").children().length;
            var nemeFile = $('.crop-image-modal .name-file').text();
            var templImg = '<div class="item-photo"><img class="photo" src="' + response + '" alt=""><p class="name-file">' + nemeFile + '<i class="icon-close"></i>' + '</p></div>';
            $('#preview').append(templImg);
            $(".icon-close").click(function () {
                $(this).parents('.item-photo').remove();
            });
            $('.crop-image-modal').modal('hide');
            return;
        });
    });
    // CROP IMAGE MODAL END
    // MODAL SOC START
    $('.modal .soc label').click(function () {
        var idEl = $(this).attr('for');
        if ($(this).children('input').length == 0) {
            $(this).parents('li').addClass('active');
            $(this).append('<input id="' + idEl + '" placeholder="Посилання">');
            $(this).parent().append('<i class="icon-close"></i>');
            $('.modal .soc .icon-close').click(function () {
                $(this).parents('li').find('input').remove();
                $(this).parents('li').removeClass('active');
                $(this).remove();
            });
        }
    });
    // MODAL SOC END
    // PAYMENT BLOCK RADIO BUTTON TAB START
    var paymentSlider = function paymentSlider() {
        $('.radio-buttons').slick({
            responsive: [{
                breakpoint: 480,
                settings: {
                    dots: false,
                    arrows: false,
                    infinite: false,
                    centerPadding: '0',
                    speed: 300,
                    slidesToShow: 1,
                    variableWidth: true
                }
            }]
        });
    };
    if ($('.radio-buttons:not(.slick-slider)').length) {
        if ($(window).width() < 480) {
            paymentSlider();
            $('.payment-block .radio-button input[checked]').trigger('click');
            $('#payment_policy').prop('checked', 'true');
        }
    }
    if ($('.payment-block').length) {
        var idEl = $(this).attr("checked");
        $('.payment-block .radio-button input[type="radio"]').change(function () {
            var idEl = $(this).attr('id');
            $('.payment-block .description').removeClass('active');
            $('.payment-block .description[id*="' + idEl + '"]').addClass('active');
        });
        $('.payment-block .radio-button input[type="radio"]').each(function () {
            if ($(this).prop('checked') === true) {
                var idEl = $(this).attr('id');
                $('.payment-block .description[id*="' + idEl + '"]').addClass('active');
            }
        });
    }
    // PAYMENT BLOCK RADIO BUTTON TAB END
    //MODAL END
    //HISTORY IMAGE START
    if ($('.history-images').length) {
        $('.history-images').Mosaic({
            innerGap: 16,
            maxRowHeight: 176,
            maxRowHeightPolicy: 'crop',
            refitOnResizeDelay: false
        });
    }
    //HISTORY IMAGE END
    // DATE PICKER START
    if ($('input.date').length) {
        $('input.date').datetimepicker({
            showClose: true,
            format: "MM/DD/YYYY",
            locale: "uk",
            daysOfWeekDisabled: [6, 7]
        });
    }
    // DATE PICKER END
    // TIME PICKER START
    if ($('input.clock').length) {
        $('input.clock').datetimepicker({
            showClose: true,
            showTodayButton: true,
            format: "H:mm"
        });
    }
    // TIME PICKER END


    //MASK TEL START

    //MASK TEL END

    $('header .icon-phone').click(function () {
        $(this).toggleClass('open');
    });
});
jQuery(function ($) {
    $(document).ready(function () {
        setTimeout(function () {
            $('input[type="tel"]').each(function () {
                $(this).addClass('error');
            });
            var input = document.querySelectorAll('input[type="tel"]');
            var iti_el = $('.iti.iti--allow-dropdown.iti--separate-dial-code');
            if (iti_el.length) {
                iti.destroy();
            }
            for (var i = 0; i < input.length; i++) {
                var iti = intlTelInput(input[i], {
                    autoHideDialCode: false,
                    intlTelInput: 'ua',
                    autoPlaceholder: "aggressive",
                    InitialCountry: "ua",
                    separateDialCode: false,
                    nationalMode: false,
                    preferredCountries: ['ua', 'pl', 'de', 'by', 'kz', 'uz'],
                    customPlaceholder: function customPlaceholder(selectedCountryPlaceholder, selectedCountryData) {
                        return '' + selectedCountryPlaceholder.replace(/[0-9]/g, 'X');
                    },
                    //                    geoIpLookup: function (callback) {
                    //                        $.get('//ipinfo.io', function () {}, "jsonp").always(function (resp) {
                    //                            var countryCode = (resp && resp.country) ? resp.country : "";
                    //                            callback(countryCode);
                    //                        });
                    //                    },
                    utilsScript: "//cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.0/js/utils.js" // just for 
                });
                $('input[type="tel"]').on("focus click countrychange", function (e, countryData) {
                    var pl = $(this).attr('placeholder') + '';
                    var res = pl.replace(/X/g, '9');
                    if (res != 'undefined') {
                        $(this).inputmask(res, {
                            mask: res,
                            placeholder: "X",
                            clearMaskOnLostFocus: false
                        });
                    }
                    if ($(this).val().indexOf('X') > 0) {
                        $(this).addClass('error');
                    }
                });
                $('input[type="tel"]').keyup(function () {
                    if ($(this).val().indexOf('X') < 1) {
                        $(this).removeClass('error');
                        $(this).css('box-shadow', 'none');
                    } else {
                        $(this).addClass('error');
                        $(this).css('box-shadow', 'inset 0 0 0 1px #f00');
                    }
                    if ($(this).val().length === 0) {
                        $(this).addClass('error');
                        $(this).css('box-shadow', 'inset 0 0 0 1px #f00');
                    }
                });
                $('input[type="tel"]').blur(function () {
                    if ($(this).val().indexOf('X') < 1) {
                        $(this).removeClass('error');
                        $(this).css('box-shadow', 'none');
                    } else {
                        $(this).addClass('error');
                        $(this).css('box-shadow', 'inset 0 0 0 1px #f00');
                    }
                    if ($(this).val().length === 0) {
                        $(this).addClass('error');
                        $(this).css('box-shadow', 'inset 0 0 0 1px #f00');
                    }
                });
                $('input[type="tel"]').on("focusout", function (e, countryData) {
                    var intlNumber = iti.getNumber();
                });
            }
            $('#iti-item-ru').remove();
        }, 500);
    });
});
'use strict';

jQuery(document).ready(function ($) {
    if ($('.navigation').length) {
        var navLinks = document.querySelectorAll('.navigation a');
        var activeLink = document.querySelector('.active');
        var section = document.querySelectorAll(' .vc_row[id]');
        var navHeight = document.querySelector('.navigation .menu').offsetHeight;
        var headerHeight = document.querySelector('header').offsetHeight;
        var flag = true;
        window.addEventListener("scroll", function (event) {
            section.forEach(function (element) {
                var position = document.documentElement.scrollTop;
                var top = element.offsetTop;
                var bottom = top + element.offsetHeight;
                if (position >= top && position <= bottom) {
                    var idSection = element.id;
                    section.forEach(function (element) {
                        element.classList.remove('active');
                        navLinks.forEach(function (link) {
                            link.classList.remove('active');
                        });
                    });
                    document.getElementById(idSection).classList.add('active');
                    document.querySelector('.navigation a[href="#' + idSection + '"]').classList.add('active');
                    if ($(window).width() < 768) {
                        document.querySelector('.navigation a[href="#' + idSection + '"]').scrollIntoView({
                            behavior: "smooth",
                            inline: "center"
                        });
                    }
                }
            });
        });
        navLinks.forEach(function (link) {
            link.addEventListener("click", function (event) {
                var hrefEl = link.getAttribute('href');
                navLinks.forEach(function (link) {
                    link.classList.remove('active');
                });
                link.classList.add('active');
                link.scrollIntoView({
                    behavior: "smooth",
                    inline: "center"
                });
                $('html, body').animate({
                    scrollTop: $(hrefEl).offset().top - navHeight - headerHeight + 50
                }, 500);
                return false;
            });
        });
    }
});
// SCROLL NAVIGATION BLOCK
//nav.find('a:not([data-toggle="tab"])').on('click', function () {
//    var id = $(this).attr('href');
//    $('html, body').animate({
//        scrollTop: $(id).offset().top - navHeight - headerHeight + 30
//    }, 500);
//    return false;
//});
// SCROLL NAVIGATION BLOCK