/* global courseObject, Swal */

/**
 * @param courseObject.url
 * @param courseObject.actionNamesCourseRegister
 * @param courseObject.ajaxBeforeSendTitle
 */

jQuery( document ).ready( function( $ ) {
	$( '#consultation-modal' ).submit( function( e ) {
		e.preventDefault()

		let data = {
			action: courseObject.actionNamesCourseRegister,
			name: $( '#consultation-name' ).val(),
			email: $( '#consultation-email' ).val(),
			phone: $( '#consultation-phone' ).val(),
			course: $( '#consultation-course' ).val(),
			message: $( '#consultation-message' ).val(),
			nonce: $( '#alv_register_on_course' ).val()
		};

		let error = $( '#consultation-modal .error' );

		if ( ! error.length ) {
			$.ajax( {
				type: 'POST',
				url: courseObject.url,
				data: data,
				beforeSend: function() {
					loader_ajax( false );
				},
				success: function( res ) {
					if ( res.success ) {
						const form = document.getElementById( 'consultation-modal' );
						form.reset();
						$( '.events-wrapper .event-item' ).remove()
						$( '.events-wrapper' ).append( res.data.event );
						$( '.consultation.modal' ).modal( 'hide' );
						$( '.consultation-thanks' ).modal( 'show' );
						loader_ajax( true );
					} else {
						loader_ajax( true );
						Swal.fire( {
							icon: 'error',
							title: 'Oops...',
							text: res.data.message,
						} )
					}

				},
				error: function( xhr, ajaxOptions, thrownError ) {
					console.log( 'error...', xhr );
					//error logging
				},
			} );
		}

	} );
} );