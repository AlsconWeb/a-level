/* global blogObject, Swal */

/**
 * @param blogObject.url
 * @param blogObject.actionNamesCategory
 * @param blogObject.alvBlogNonce
 * @param blogObject.ajaxBeforeSendTitle
 */

jQuery( document ).ready( function( $ ) {
	$( '.category-item' ).click( function( e ) {
		e.preventDefault();

		let activeEl = $( this );

		let data = {
			action: blogObject.actionNamesCategory,
			category_slug: $( this ).data( 'category' ),
			nonce: blogObject.alvBlogNonce
		}

		$.ajax( {
			type: 'POST',
			url: blogObject.url,
			data: data,
			cache: false,
			beforeSend: function() {
				loader( false );
			},
			success: function( res ) {
				if ( res.success ) {
					loader( true );
					$( '.blog-items .blog-item' ).remove();
					$( '.blog-items' ).append( res.data.posts );
					$( '.tags-menu li' ).removeClass( 'active' );
					$( activeEl ).parent().addClass( 'active' );
					Swal.close();
				} else {
					Swal.fire( {
						icon: 'error',
						title: 'Oops...',
						text: res.data.message,
					} )
				}
			},
			error: function( xhr, ajaxOptions, thrownError ) {
				console.log( 'error...', xhr );
				//error logging
			}
		} );
	} );
} );