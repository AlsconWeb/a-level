jQuery( document ).ready( function( $ ) {
	$( '.language input[type=radio]' ).change( function( e ) {
		let url = $( this ).data( 'url_lang' );

		window.location.href = url;
	} );

	countdownTimer( $( '.countdown_timer' ).data( 'start' ) );

	let wpcf7Elm = document.querySelector( '.wpcf7' );

	if ( wpcf7Elm ) {
		$( '.wpcf7-submit' ).click( function( e ) {
			loader_ajax( false );
		} );

		wpcf7Elm.addEventListener( 'wpcf7submit', function( event ) {
			loader_ajax( true );
			$( '.thanks' ).modal( 'show' );

		}, false );
	}

	$( '#local-city, #city-header, #city-footer' ).change( function( e ) {
		e.preventDefault();

		window.location.href = $( this ).val();
	} );

	const salesModal = $( '#sales-modal' );

	if ( salesModal.length ) {
		let openModalCookie = getCookie( 'alv_sales_pop_up' );

		if ( ! openModalCookie ) {
			salesModal.modal( 'show' );
		}

		$( '#sales-modal .all-course, #sales-modal .close' ).click( function( e ) {
			setCookie( 'alv_sales_pop_up', true )
		} );

	}

} );

/**
 * Timer function.
 *
 * @param date
 */
function countdownTimer( date ) {
	const { _n, } = wp.i18n;
	let countDownDate = new Date( date ).getTime();

	let x = setInterval( function() {

			let now = new Date().getTime();
			let distance = countDownDate - now;

			let days = Math.floor( distance / ( 1000 * 60 * 60 * 24 ) );
			let hours = Math.floor( ( distance % ( 1000 * 60 * 60 * 24 ) ) / ( 1000 * 60 * 60 ) );
			let minutes = Math.floor( ( distance % ( 1000 * 60 * 60 ) ) / ( 1000 * 60 ) );
			let seconds = Math.floor( ( distance % ( 1000 * 60 ) ) / 1000 );

			jQuery( '.countdown_timer' ).text( days + _n( ' день ', ' днiв ', days, 'alevel' ) + hours + ':' + minutes + ':' + seconds );

			if ( distance < 0 ) {
				clearInterval( x );
				jQuery( '.countdown_timer' ).text( 'Акция закончилась' );
			}
		},
		1000
	);
}

/**
 * Loader from ajax.
 *
 * @param success
 */
function loader_ajax( success ) {
	var obj = document.querySelector( '.mail-preloader' ),
		line = document.querySelector( '.form-line' ),
		num = document.querySelector( '.preloader-num' ),
		bodyEl = document.querySelector( 'body' ),
		w = 0;

	bodyEl.classList.add( 'overflow' )
	obj.style.display = 'flex';
	line.style.width = w + '%';

	if ( ! success ) {
		t = setInterval( function() {
			w = w + 1;
			line.style.width = w + '%';
			num.textContent = w + '%';
		}, 10 );
	} else {
		obj.style.display = 'none';
		bodyEl.classList.remove( 'overflow' );
		w = 0;
		line.style.width = w + '%';
		num.textContent = w + '%';
		clearInterval( t );
	}
}

/**
 * Set cookie.
 *
 * @param name cookie name.
 * @param value cookie value.
 * @param days cookie date expires.
 */
function setCookie( name, value, days ) {
	var expires = '';
	if ( days ) {
		var date = new Date();
		date.setTime( date.getTime() + ( days * 24 * 60 * 60 * 1000 ) );
		expires = '; expires=' + date.toUTCString();
	}
	document.cookie = name + '=' + ( value || '' ) + expires + '; path=/';
}

/**
 * Get cookie.
 *
 * @param name cookie name.
 * @returns {null|string}
 */
function getCookie( name ) {
	var nameEQ = name + '=';
	var ca = document.cookie.split( ';' );
	for ( var i = 0; i < ca.length; i++ ) {
		var c = ca[ i ];
		while ( c.charAt( 0 ) == ' ' ) c = c.substring( 1, c.length );
		if ( c.indexOf( nameEQ ) == 0 ) return c.substring( nameEQ.length, c.length );
	}
	return null;
}