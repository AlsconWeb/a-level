/* global partnerObject, Swal */

/**
 * @param partnerObject.url
 * @param partnerObject.actionNamesEventRegister
 * @param partnerObject.ajaxBeforeSendTitle
 */

jQuery( document ).ready( function( $ ) {

	$( '#become-partner-modal' ).submit( function( e ) {
		e.preventDefault();

		let data = {
			action: partnerObject.actionNamesEventRegister,
			partner_name: $( '#partner-name' ).val(),
			partner_last_mane: $( '#partner-last-mane' ).val(),
			partner_email: $( '#partner-email' ).val(),
			partner_phone: $( '#partner-phone' ).val(),
			partner_company: $( '#partner-company' ).val(),
			partner_web: $( '#partner-web' ).val(),
			partner_linkedin: $( '#partner-linkedin' ).val(),
			partner_message: $( '#partner-message' ).val(),
			partner_nonce: $( '#alv_register_partner' ).val()
		};

		let error = $( '#become-partner-modal .error' );

		if ( ! error.length ) {
			$.ajax( {
				type: 'POST',
				url: partnerObject.url,
				data: data,
				beforeSend: function() {
					loader_ajax( false );
				},
				success: function( res ) {
					if ( res.success ) {
						const form = document.getElementById( 'become-partner-modal' );
						form.reset();
						loader_ajax( true );
						$( '.medea-items .medea-item, .partners .reviews-item, .partners .button, .medea-items .button' ).remove()
						$( '.partners' ).append( res.data.review );
						$( '.become-partner' ).modal( 'hide' );
						$( '.become-partner-thanks' ).modal( 'show' );
					} else {
						loader_ajax( true );
						Swal.fire( {
							icon: 'error',
							title: 'Oops...',
							text: res.data.message,
						} )
					}
				},
				error: function( xhr, ajaxOptions, thrownError ) {
					console.log( 'error...', xhr );
					//error logging
				},
			} );
		}
	} );

} );