/* global callPartnerObj, Swal */

/**
 * @param callPartnerObj.url
 * @param callPartnerObj.actionNamesPartner
 * @param callPartnerObj.noncePartner
 * @param callPartnerObj.ajaxBeforeSendTitle
 */

jQuery( document ).ready( function( $ ) {
	let filter = $( '.partners-filter .active' );
	if ( filter.length ) {
		$( filter )[ 0 ].scrollIntoView( { block: 'nearest' } )
	}

	$( '.partners-filter a' ).click( function( e ) {
		e.preventDefault();

		let data = {
			action: callPartnerObj.actionNamesPartner,
			nonce: callPartnerObj.noncePartner,
			partner_type: $( this ).data( 'type' )
		}

		$.ajax( {
			type: 'POST',
			url: callPartnerObj.url,
			data: data,
			success: function( res ) {
				if ( res.success ) {
					let baseUrl = window.location.protocol + '//' + window.location.host + window.location.pathname;
					location.href = baseUrl + '?' + res.data.params;
				} else {
					console.log( res.data.message );
				}
			},
			error: function( xhr, ajaxOptions, thrownError ) {
				console.log( 'error...', xhr );
				//error logging
			},
		} );
	} );
} );