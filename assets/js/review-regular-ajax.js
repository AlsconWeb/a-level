/* global reviewObject, Swal */

/**
 * @param reviewObject.url
 * @param reviewObject.actionNamesTestimonialsRegular
 * @param reviewObject.alvTestimonialsRegularNonce
 * @param reviewObject.ajaxBeforeSendTitle
 */

jQuery( document ).ready( function( $ ) {
	$( '.link.regular-review, .link-pop-up' ).click( function( e ) {
		e.preventDefault();

		let data = {
			action: reviewObject.actionNamesTestimonialsRegular,
			nonce: reviewObject.alvTestimonialsRegularNonce,
			review_id: $( this ).data( 'course_id' )
		};

		let filter = $( '.filter-menu .active' );

		$.ajax( {
			type: 'POST',
			url: reviewObject.url,
			data: data,
			success: function( res ) {
				if ( res.success ) {
					$( '.review-modal.modal .modal-dialog .modal-content' ).remove();
					$( '.review-modal.modal .modal-dialog' ).append( res.data.review );
					$( '.review-modal' ).modal( 'show' );
					if ( filter.length ) {
						$( '.filter-menu .active' )[ 0 ].scrollIntoView( { block: 'nearest' } )
					}
				} else {
					Swal.fire( {
						icon: 'error',
						title: 'Oops...',
						text: res.data.message,
					} )
				}
			},
			error: function( xhr, ajaxOptions, thrownError ) {
				console.log( 'error...', xhr );
				//error logging
			}
		} );
	} );
} );