/* global paymentFormObject, Swal */

/**
 * @param paymentFormObject.url
 * @param paymentFormObject.actionNamesPaymentForm
 * @param paymentFormObject.ajaxBeforeSendTitle
 */
jQuery( document ).ready( function( $ ) {
	$( '[name=method_payment]' ).change( function( e ) {
		let sales = $( '.form-wrapper .discount' );

		if ( sales.length === 0 ) {
			let value = $( this ).attr( 'id' );
			if ( value === 'installment' || value === 'mono' ) {
				$( '.price' ).hide();
				$( '.full-price' ).show();
			} else {
				$( '.full-price' ).hide();
				$( '.price' ).show();
			}
		}
	} );

	$( '#payment-form' ).submit( function( e ) {
		e.preventDefault();

		let data = {
			action: paymentFormObject.actionNamesPaymentForm,
			nonce: $( '#alv_course_payment' ).val(),
			payment_from_firs_name: $( '#payment_from_firs_name' ).val(),
			payment_from_last_name: $( '#payment_from_last_name' ).val(),
			payment_from_phone: $( '#payment_from_phone' ).val(),
			payment_phone_country_code: $( '.iti__country-list' ).attr( 'aria-activedescendant' ),
			payment_from_email: $( '#payment_from_email' ).val(),
			method_payment: $( 'input[name=method_payment]:checked' ).val() ?? 'Сплата за весь курс',
			payment_from_count_payment: $( '#payment_from_count_payment' ).val(),
			course_name: $( '#course_name' ).val()
		};

		let error = $( '#payment-form .error' );

		if ( ! error.length ) {
			$.ajax( {
				type: 'POST',
				url: paymentFormObject.url,
				data: data,
				beforeSend: function() {
					loader_ajax( false );
				},
				success: function( res ) {
					if ( res.success ) {
						loader_ajax( true );

						const form = document.getElementById( 'payment-form' );
						form.reset();
						$( '.thanks .events-wrapper .event-item' ).remove();
						$( '.thanks .events-wrapper' ).append( res.data.event );
						$( '.thanks' ).modal( 'show' );

					} else {
						loader_ajax( true );
						Swal.fire( {
							icon: 'error',
							title: 'Oops...',
							text: res.data.message,
						} );
					}

				},
				error: function( xhr, ajaxOptions, thrownError ) {
					console.log( 'error...', xhr );
					//error logging
				},
			} );
		}

	} );
} );