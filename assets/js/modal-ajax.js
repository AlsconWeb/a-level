/* global ModalObject, Swal */

/**
 * @param ModalObject.actionNamesNewTeacher
 * @param ModalObject.actionNameNewReview
 * @param ModalObject.url
 * @param ModalObject.beforeSendNewTeacher
 * @param ModalObject.validateFormText.first_name
 * @param ModalObject.validateFormText.last_name
 * @param ModalObject.validateFormText.email
 * @param ModalObject.validateFormText.phone
 * @param ModalObject.validateFormText.avatar_file
 * @param ModalObject.validateFormText.leave_review_teachers
 * @param ModalObject.validateFormText.message
 * @param ModalObject.validateFormText.rating
 * @param ModalObject.validateFormText.social
 * @param ModalObject.actionNameGetTeachers
 * @param ModalObject.nonceGetTeachers
 */

const EMAIL_REGEXP = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/iu;
const SOCIAL_URL = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;

jQuery( document ).ready( function( $ ) {

	if ( $( '.leave-review' ).length ) {
		$( '.leave-review .next' ).click( function( e ) {
			e.preventDefault();
			let validateStepOne = formValidate( '#alv-leave-review-step-one' );
			let validateStepTwo = formValidate( '#alv-leave-review-step-two' )
			let validateStepThree = formValidate( '#alv-leave-review-step-three' )
			let currentForm = $( this ).parents( 'form' ).attr( 'id' );
			let currentStep = $( this ).parents( '.modal-body' );


			switch ( currentForm ) {
				case 'alv-leave-review-step-one':
					if ( validateStepOne === true ) {
						nextFormStep( currentStep );
					} else {
						fieldsErrorValidate( validateStepOne );
					}
					break;
				case 'alv-leave-review-step-two':
					if ( validateStepTwo === true ) {
						nextFormStep( currentStep );
					} else {
						fieldsErrorValidate( validateStepTwo );
					}
					break;
				case 'alv-leave-review-step-three':
					if ( validateStepThree === true ) {
						console.log( 'Send' );
					} else {
						fieldsErrorValidate( validateStepThree );
					}
					break;
			}

		} );
		$( '.leave-review .prev' ).click( function( e ) {
			e.preventDefault();
			$( this ).parents( '.modal-body' ).removeClass( 'show' ).prev().addClass( 'show' );
			$( this ).parents( '.modal-content' ).find( '.steps li.active' ).removeClass( 'active' ).prev().addClass( 'active' );
			if ( $( this ).parents( '.modal-content' ).find( '.steps' ).children( 'li:first' ).hasClass( 'active' ) ) {
				$( this ).parents( '.modal-content' ).find( '.steps' ).next().show();
			}
		} );

		let courseSelect = $( '#review-course' );

		if ( courseSelect.length ) {
			$( courseSelect ).change( function( e ) {
				$( '#course-id' ).val( $( this ).val() );

				let data = {
					action: ModalObject.actionNameGetTeachers,
					nonce: ModalObject.nonceGetTeachers,
					course_id: $( this ).val()
				};

				$.ajax( {
					type: 'POST',
					url: ModalObject.url,
					data: data,
					success: function( res ) {
						if ( res.success ) {
							$( '#alv-leave-review-step-two .teachers li' ).remove();
							$( '#alv-leave-review-step-two .teachers' ).append( res.data.teachers );
						}
					},
					error: function( xhr, ajaxOptions, thrownError ) {
						console.log( 'error...', xhr );
						//error logging
					},
				} );
			} );
		}
	}

	function nextFormStep( form ) {
		$( form ).removeClass( 'show' ).next().addClass( 'show' );
		$( form ).find( '.modal-header p' ).hide();
		$( form ).parents( '.modal-content' ).find( '.steps li.active' ).removeClass( 'active' ).next().addClass( 'active' );
	}

	$( '.icon-instagram, .icon-facebook-f,  .icon-linkedin, .icon-behance, .icon-telegram' ).click( function( e ) {
		setTimeout( function() {
			$( '#facebook, #instagram, #linkedin, #behance, #telegram' ).change( function( e ) {
				let element = $( this );
				let value = $( this ).val();

				if ( SOCIAL_URL.test( value ) ) {
					console.log( 'if', SOCIAL_URL.test( value ) )
				} else {
					console.log( 'else', SOCIAL_URL.test( value ) )
					Swal.fire( {
						icon: 'error',
						title: 'Oops...',
						text: ModalObject.validateFormText.social,
					} ).then( ( result ) => {
						if ( result.isConfirmed ) {
							$( element ).val( '' );
						}
					} );
				}

			} );
		}, 100 );
	} )

	$( '#alv-become-teacher #become-teacher-submit' ).click( function( e ) {
		e.preventDefault()

		let validate = formValidate( '#alv-become-teacher' );

		let data = {
			action: ModalObject.actionNamesNewTeacher,
			first_name: $( '#first_name' ).val(),
			last_name: $( '#last_name' ).val(),
			email: $( '#email' ).val(),
			phone: $( '#phone' ).val(),
			company: $( '#company' ).val(),
			position: $( '#position' ).val(),
			linkedin: $( '#linkedin_link' ).val(),
			message: $( '#message' ).val(),
			course_id: $( '#course-id' ).val(),
			nonce: $( '#alv_become_teacher' ).val()
		};
		if ( validate === true ) {
			$.ajax( {
				type: 'POST',
				url: ModalObject.url,
				data: data,
				beforeSend: function() {
					loader_ajax( false );
				},
				success: function( res ) {
					if ( res.success ) {
						$( '.error' ).remove();
						$( '.become-teacher' ).modal( 'hide' );
						$( '.thanks .events-wrapper' ).append( res.data.events )
						$( '.thanks' ).modal( 'show' );
						clearForm( '#alv-become-teacher' );
						loader_ajax( true );
					} else {
						loader_ajax( true );
						Swal.fire( {
							icon: 'error',
							title: 'Oops...',
							text: res.data.message,
						} )
					}
				},
				error: function( xhr, ajaxOptions, thrownError ) {
					console.log( 'error...', xhr );

				},
			} );
		} else {
			fieldsErrorValidate( validate );
		}
	} );

	$( '#new-review' ).click( function( e ) {
		e.preventDefault();
		let social = {};

		if ( $( '#facebook' ).val() ) {
			social.facebook = $( '#facebook' ).val()
		}

		if ( $( '#instagram' ).val() ) {
			social.instagram = $( '#instagram' ).val()
		}

		if ( $( '#linkedin' ).val() ) {
			social.linkedin = $( '#linkedin' ).val()
		}

		if ( $( '#behance' ).val() ) {
			social.behance = $( '#behance' ).val()
		}

		if ( $( '#telegram' ).val() ) {
			social.telegram = $( '#telegram' ).val()
		}
		let teacher = [];

		$.each( $( '[id^=teacher-]:checked' ), function( i, el ) {
			teacher.push( $( el ).val() );
		} );

		let data = {
			action: ModalObject.actionNameNewReview,
			firs_name: $( '#leave-review-fist-name' ).val(),
			last_name: $( '#leave-review-last-name' ).val(),
			email: $( '#leave-review-email' ).val(),
			phone: $( '#leave-review-phone' ).val(),
			social: JSON.stringify( social ),
			photo: $( '#leave-review-add-file' ).prop( 'files' )[ 0 ],
			teacher: teacher,
			message: $( '#leave-review-message' ).val(),
			star_rating: $( '[name=rating]:checked' ).val(),
			course_id: $( '#course-id' ).val(),
			nonce: $( '#alv_course_review' ).val()
		};

		let ajax_data = new FormData();

		$.each( data, function( i, el ) {
			ajax_data.append( i, el );
		} );


		$.ajax( {
			type: 'POST',
			url: ModalObject.url,
			data: ajax_data,
			processData: false,
			contentType: false,
			beforeSend: function() {
				loader_ajax( false );
			},
			success: function( res ) {
				if ( res.success ) {
					$( '.leave-review' ).modal( 'hide' )
					loader_ajax( true );
				} else {
					loader_ajax( true );
					Swal.fire( {
						icon: 'error',
						title: 'Oops...',
						text: res.data.message,
					} )
				}
			},
			error: function( xhr, ajaxOptions, thrownError ) {
				console.log( 'error...', xhr );
				//error logging
			},
		} );

	} );


	/**
	 * Add error message to field.
	 *
	 * @param errorFields.fieldName String ID field.
	 * @param errorFields.message String message.
	 */
	function fieldsErrorValidate( errorFields ) {
		Swal.close();
		if ( errorFields.length ) {
			$.each( errorFields, function( i, el ) {
				$( '#' + el.fieldName ).parent().append( '<p class="error">' + el.message + '</p>' );
			} );
		}
	}

	/**
	 * Clear fields.
	 *
	 * @param formID ID selector from.
	 */
	function clearForm( formID ) {
		let from = $( formID + ' input:not([type="submit"]):not([type=hidden]), ' + formID + ' textarea' );

		$.each( from, function( i, el ) {
			$( el ).val( '' );
		} );
	}

	function formValidate( formSelector ) {
		let form = $( formSelector + ' input:not([type="submit"]):not([type=hidden]), ' + formSelector + ' chackbox' );
		let errorFields = [];
		let elCaked;
		let elRadio;
		let elID;
		$( formSelector + ' p.error' ).remove();

		$.each( form, function( i, el ) {
			let elValue = $( el ).val().trim();

			switch ( $( el ).attr( 'name' ) ) {
				case 'first_name':
					if ( elValue.length === 0 ) {
						errorFields.push( {
							message: ModalObject.validateFormText.first_name,
							fieldName: $( el ).attr( 'id' )
						} );
					}
					break;
				case 'last_name':
					if ( elValue.length === 0 ) {
						errorFields.push( {
							message: ModalObject.validateFormText.last_name,
							fieldName: $( el ).attr( 'id' )
						} );
					}
					break;
				case 'email':
					if ( ! EMAIL_REGEXP.test( elValue ) )
						errorFields.push( {
							message: ModalObject.validateFormText.email,
							fieldName: $( el ).attr( 'id' )
						} );
					break;
				case 'avatar-file':
					if ( elValue.length === 0 ) {
						errorFields.push( {
							message: ModalObject.validateFormText.avatar_file,
							fieldName: $( el ).attr( 'id' )
						} );
					}
					break;
				case 'leave-review-teachers[]':
					elID = $( el ).attr( 'id' );
					elCaked = $( '[id^=teacher-]:checked' ).val();


					if ( undefined === elCaked ) {
						errorFields.push( {
							message: ModalObject.validateFormText.leave_review_teachers,
							fieldName: $( el ).attr( 'id' )
						} );
					}
					break;
				case 'leave-review-message':
					if ( elValue.length === 0 ) {
						errorFields.push( {
							message: ModalObject.validateFormText.message,
							fieldName: $( el ).attr( 'id' )
						} );
					}
					break;
			}
		} );

		if ( '#alv-leave-review-step-three' === formSelector ) {
			elRadio = $( '[name=rating]:checked' ).val();

			if ( undefined === elRadio ) {
				errorFields.push( {
					message: ModalObject.validateFormText.rating,
					fieldName: 'rating_fields'
				} );
			}
		}

		if ( errorFields.length === 0 ) {
			return true;
		}

		return errorFields;
	}
} );