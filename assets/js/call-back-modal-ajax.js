/* global callBackModalObject, Swal */

/**
 * @param callBackModalObject.url
 * @param callBackModalObject.actionNamesCallBack
 * @param callBackModalObject.ajaxBeforeSendTitle
 */

jQuery( document ).ready( function( $ ) {
	$( '#call_back_form' ).submit( function( e ) {
		e.preventDefault();

		let data = {
			action: callBackModalObject.actionNamesCallBack,
			name: $( '#callback-name' ).val(),
			phone: $( '#callback-phone' ).val(),
			date: $( '#callback-date' ).val(),
			time: $( '#callback-time' ).val(),
			nonce: $( '#alv_call_back_modal' ).val(),
		};

		let error = $( '#call_back_form .error' );

		if ( ! error.length ) {
			$.ajax( {
				type: 'POST',
				url: callBackModalObject.url,
				data: data,
				beforeSend: function() {
					loader_ajax( false );
				},
				success: function( res ) {
					if ( res.success ) {
						const form = document.getElementById( 'call_back_form' );
						form.reset();
						$( '.call-back ' ).modal( 'hide' );
						$( '.call-back-thanks .events .event-item' ).remove();
						$( '.call-back-thanks .events' ).append( res.data.events );
						$( '.call-back-thanks' ).modal( 'show' )
						loader_ajax( true );

					} else {
						Swal.fire( {
							icon: 'error',
							title: 'Oops...',
							text: res.data.message,
						} )
					}

				},
				error: function( xhr, ajaxOptions, thrownError ) {
					console.log( 'error...', xhr );
					//error logging
				},
			} );
		}

	} );
} );