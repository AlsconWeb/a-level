<?php
/**
 * Single event template.
 *
 * @package iwpdev/alevel
 */

get_header();
$event_id       = get_the_ID();
$event_start    = carbon_get_post_meta( $event_id, 'alv_event_start' );
$date           = date_create( $event_start );
$event_category = wp_get_post_terms( $event_id, 'event-category' );
$event_type     = carbon_get_post_meta( $event_id, 'alv_event_type' );

if ( have_posts() ) {
	?>
	<section style="background:#F9F6F1;">
		<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex banner-gradient"
			 style="--var-gradient: linear-gradient(82.27deg, #9D82C9 0%, #F5C2E5 100%);">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<div class="vc_col-lg-12 vc_col-md-12 vc_col-sm-12 vc_col-xs-12">
							<a class="back" href="<?php echo esc_url( get_post_type_archive_link( 'events' ) ); ?>">
								<i class="icon-arrow-left"></i>
								<?php esc_html_e( 'Усi заходи', 'alevel' ); ?>
							</a>
							<ul class="tag dfr">
								<?php
								if ( ! empty( $event_category ) ) {
									foreach ( $event_category as $category ) {
										?>
										<li><?php echo esc_html( $category->name ); ?></li>
										<?php
									}
								}
								?>
								<li><?php echo esc_html( date_format( $date, 'd.m.Y' ) ); ?></li>
							</ul>
							<div class="event-block">
								<h1 class="title"><?php the_title(); ?></h1>
								<?php
								$url = 'firm' === $event_type ? carbon_get_post_meta( $event_id, 'alv_event_firm_link' ) : '#';
								if ( 'firm' === $event_type ) {
									?>
									<a class="button" href="<?php echo esc_url( $url ); ?>">
										<?php esc_html_e( 'Записаться на захiд', 'alevel' ); ?>
									</a>
								<?php } else { ?>
									<a
											class="button"
											href="#"
											rel="noreferrer nofollow"
											data-toggle="modal"
											data-event_id="<?php echo esc_attr( $event_id ); ?>"
											data-target=".registration-event">
										<?php esc_html_e( 'Записаться на захiд', 'alevel' ); ?>
									</a>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
		while ( have_posts() ) {
			the_post();
			$events_course  = carbon_get_post_meta( $event_id, 'alv_course_event' );
			$events_mentors = carbon_get_post_meta( $event_id, 'alv_mentors' );
			$event_plan     = carbon_get_post_meta( $event_id, 'alv_event_plan' );
			?>
			<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex company-description">
				<div class="wpb_column vc_column_container vc_col-sm-12">
					<div class="vc_column-inner">
						<div class="wpb_wrapper">
							<div class="vc_col-lg-8 vc_col-md-8 vc_col-sm-8 vc_col-xs-12">
								<?php
								if ( has_post_thumbnail( $event_id ) ) {
									the_post_thumbnail( 'alv-event', [ 'class' => 'thumbnail' ] );
								} else {
									?>
									<img
											class="thumbnail"
											src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/Media.png' ); ?>"
											alt="No image">
									<?php
								}

								get_template_part( 'template-parts/courses', 'block', [ 'course' => $events_course ] );

								the_content();
								?>
							</div>
							<div class="vc_col-lg-4 vc_col-md-4 vc_col-sm-4 vc_col-xs-12">
								<?php if ( ! empty( $events_mentors ) ) { ?>
									<div class="mentors-info">
										<h4><?php esc_html_e( 'Спікери', 'alevel' ); ?></h4>
										<ul>
											<?php
											foreach ( $events_mentors as $mentor ) {
												$avatar = wp_get_attachment_image_url( $mentor['photo'], 'alv-teacher-avatar' );
												?>
												<li>
													<img
															src="<?php echo esc_url( $avatar ); ?>"
															alt="<?php echo esc_attr( get_the_title( $mentor['photo'] ) ); ?>">
													<p><?php echo esc_html( $mentor['name'] ); ?>
														<span><?php echo esc_html( $mentor['position'] ); ?></span>
													</p>
												</li>
											<?php } ?>
										</ul>
									</div>
								<?php } ?>
								<div class="info-blog">
									<?php if ( $event_plan ) { ?>
										<h3><?php esc_html_e( 'Що на Вас чекає', 'alevel' ); ?></h3>
										<ol>
											<?php foreach ( $event_plan as $item ) { ?>
												<li><?php echo esc_html( $item['item_plan'] ); ?></li>
											<?php } ?>
										</ol>
									<?php } ?>
									<div class="soc">
										<p><?php esc_html_e( 'Подiлитися з друзями:', 'alevel' ); ?></p>
										<?php echo do_shortcode( '[spbsm-share-buttons]' ); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>

		<?php
		get_template_part( 'template-parts/short-code-review' );
		get_template_part( 'template-parts/short-code-faq' );
		?>
	</section>
	<?php
}
get_footer();
