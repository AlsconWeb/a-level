<?php
/**
 * Template Name:Blog
 *
 * @package iwpdev/alevel
 */

$sticky     = get_option( 'sticky_posts' );
$arg_sticky = [
	'posts_per_page' => 1,
	'post__in'       => $sticky,
];

$sticky_query = new WP_Query( $arg_sticky );

$arg_events = [
	'post_type'      => 'events',
	'post_status'    => 'publish',
	'posts_per_page' => 3,
	'order'          => 'DESC',
	'orderby'        => 'date',
];

$event_query = new WP_Query( $arg_events );

$blog_paged = get_query_var( 'paged' ) ?: 1;

$blog_arg = [
	'post_type'           => [ 'post', 'testimonials' ],
	'ignore_sticky_posts' => true,
	'paged'               => $blog_paged,
	'meta_query'          => [
		'relation' => 'OR',
		[
			'key'         => '_alv_type_review',
			'compare_key' => 'NOT EXISTS',
		],
		[
			'key'   => '_alv_type_review',
			'value' => 'history',
		],
	],
	'order'               => 'DESC',
	'orderby'             => 'date',
];

$blog_query = new WP_Query( $blog_arg );

get_header();

$gradient_class = carbon_get_post_meta( get_the_ID(), 'alv_line_gradient_class' );
?>
	<section style="background:#F9F6F1;">
		<?php
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();
				$custom_title = carbon_get_the_post_meta( 'alv_blog_custom_title' );
				?>
				<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex banner-gradient <?php echo esc_attr( $gradient_class ) ?? ''; ?>">
					<div class="wpb_column vc_column_container vc_col-sm-12">
						<div class="vc_column-inner">
							<div class="wpb_wrapper">
								<div class="vc_col-lg-8 vc_col-md-8 vc_col-sm-8 vc_col-xs-12">
									<h1 class="title"><?php the_title(); ?></h1>
									<p><?php echo wp_kses_post( wpautop( $custom_title ?? '' ) ); ?></p>
								</div>
								<div class="vc_col-lg-4 vc_col-md-4 vc_col-sm-4 vc_col-xs-12">
									<a class="button" href="<?php bloginfo( 'url' ); ?>">
										<?php esc_html_e( 'На головну', 'alevel' ); ?>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex blog-block">
					<div class="wpb_column vc_column_container vc_col-sm-12">
						<div class="vc_column-inner">
							<div class="wpb_wrapper">
								<div class="vc_col-lg-8 vc_col-md-7 vc_col-sm-6 vc_col-xs-12">
									<?php
									if ( $sticky_query->have_posts() ) {
										while ( $sticky_query->have_posts() ) {
											$sticky_query->the_post();
											$sticky_id       = get_the_ID();
											$sticky_post_tag = wp_get_post_terms( $sticky_id, 'post_tag' );
											?>
											<div class="blog-item">
												<a
														class="link" href="<?php the_permalink(); ?>"
														title="<?php the_title(); ?>"></a>
												<div class="dfr">
													<div class="img">
														<?php
														if ( has_post_thumbnail( $sticky_id ) ) {
															the_post_thumbnail( 'alv-blog-sticky' );
														} else {
															?>
															<img
																	src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/thumbnail-medium.png' ); ?>"
																	alt="No Image">
														<?php } ?>
														<?php if ( ! empty( $sticky_post_tag ) ) { ?>
															<ul class="tags dfr">
																<?php foreach ( $sticky_post_tag as $item ) { ?>
																	<li><?php echo esc_html( $item->name ); ?></li>
																<?php } ?>
															</ul>
														<?php } ?>
													</div>
													<div class="desc">
														<h3 class="title"><?php the_title(); ?></h3>
														<p>
															<?php the_excerpt(); ?>
														</p>
														<?php get_template_part( 'template-parts/author', 'info', [ 'post_id' => $sticky_id ] ); ?>

													</div>
												</div>
											</div>
											<?php
										}
										wp_reset_postdata();
									}
									?>
								</div>
								<div class="vc_col-lg-4 vc_col-md-5 vc_col-sm-6 vc_col-xs-12">
									<?php
									if ( $event_query->have_posts() ) {
										?>
										<div class="events">
											<h5 class="title"><?php esc_html_e( 'Майбутнi заходи', 'alevel' ); ?></h5>
											<?php
											while ( $event_query->have_posts() ) {
												$event_query->the_post();
												$event_id    = get_the_ID();
												$event_start = carbon_get_post_meta( $event_id, 'alv_event_start' );
												$date        = date_create( $event_start );
												$speakers    = carbon_get_post_meta( $event_id, 'alv_mentors' );
												?>
												<div class="event-item <?php echo empty( $speakers ) ? 'no-speakers' : ''; ?>">
													<a
															class="link" href="<?php the_permalink(); ?>"
															title="<?php the_title(); ?>"></a>
													<?php
													if ( has_post_thumbnail( $event_id ) ) {
														the_post_thumbnail( 'alv-thmb_mini' );
													} else {
														?>
														<img
																src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/thumbnail-mini.png' ); ?>"
																alt="No image">
													<?php } ?>
													<div class="desc">
														<h3 class="title"><?php the_title(); ?></h3>
														<p class="date"><?php echo esc_html( date_format( $date, 'd.m.Y' ) ); ?></p>
														<?php get_template_part( 'template-parts/speakers', 'info', [ 'speakers' => $speakers ] ); ?>
													</div>
												</div>
												<?php
											}
											wp_reset_postdata();
											?>
											<a
													class="button"
													href="<?php echo esc_url( get_post_type_archive_link( 'events' ) ); ?>">
												<?php esc_html_e( 'Усi заходи', 'alevel' ); ?>
											</a>
										</div>
										<?php
									}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex tags-block">
					<div class="wpb_column vc_column_container vc_col-sm-12">
						<div class="vc_column-inner">
							<div class="wpb_wrapper">
								<div class="vc_col-lg-12 vc_col-md-12 vc_col-sm-12 vc_col-xs-12">
									<h3 class="title"><?php esc_html_e( 'Теми публiкацiй', 'alevel' ); ?></h3>
									<?php get_template_part( 'template-parts/category', 'menu', [ 'teacher_post' => false ] ); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex blog-block">
					<div class="wpb_column vc_column_container vc_col-sm-12">
						<div class="vc_column-inner">
							<div class="wpb_wrapper">
								<div class="vc_col-lg-12 vc_col-md-12 vc_col-sm-12 vc_col-xs-12">
									<?php if ( $blog_query->have_posts() ) { ?>
										<div class="blog-items dfr">
											<?php
											while ( $blog_query->have_posts() ) {
												$blog_query->the_post();
												$article_id = get_the_ID();
												get_template_part( 'template-parts/blog', 'item', [ 'article_id' => $article_id ] );
											}
											?>
										</div>
										<div class="page_navigation_wrapper">
											<?php
											if ( function_exists( 'wp_pagenavi' ) ) {
												wp_pagenavi( [ 'query' => $blog_query ] );
											}
											?>
										</div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
			}
		}
		get_template_part( 'template-parts/short-code-events' );
		get_template_part( 'template-parts/short-code-faq' );
		?>
	</section>
<?php
get_footer();
