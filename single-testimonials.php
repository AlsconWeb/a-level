<?php
/**
 * Events single template.
 *
 * @package iwpdev/alevel
 */

get_header();
?>
	<section style="background:#F8EEFC;">
		<?php
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();
				$review_id         = get_the_ID();
				$review_type       = carbon_get_the_post_meta( 'alv_type_review' );
				$related_course    = carbon_get_the_post_meta( 'alv_course_event' );
				$review_title      = carbon_get_the_post_meta( 'alev_review_title' );
				$reviewer_position = carbon_get_the_post_meta( 'alv_reviewer_position' );
				$reviewer_social   = carbon_get_the_post_meta( 'alv_social_media_reviewer' );
				?>
				<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex banner-gradient"
					 style="--var-gradient: linear-gradient(135deg, rgba(72,14,193,1) 0%,rgba(72,14,193,1) 40%,rgba(153,18,201,1) 80%,rgba(153,18,201,1) 100%);">

					<div class="wpb_column vc_column_container vc_col-sm-12">
						<div class="vc_column-inner">
							<div class="wpb_wrapper">
								<div class="vc_col-lg-12 vc_col-md-12 vc_col-sm-12 vc_col-xs-12">
									<a
											class="back"
											href="<?php echo esc_attr( get_post_type_archive_link( 'testimonials' ) ); ?>">
										<i class="icon-arrow-left"></i>
										<?php esc_html_e( 'Усi вiдгуки', 'alevel' ); ?>
									</a>
									<?php if ( 'history' === $review_type ) { ?>
										<ul class="tag dfr">
											<li><?php esc_html_e( 'Iсторiя успiху', 'alevel' ); ?></li>
										</ul>
									<?php } ?>
									<?php if ( 'firm' === $review_type ) { ?>
										<ul class="tag dfr">
											<li><?php esc_html_e( 'Вiдгук компанii', 'alevel' ); ?></li>
										</ul>
										<?php
									}

									if ( 'regular' === $review_type || 'history' === $review_type ) {
										?>
										<div class="dfr">
											<h1 class="title"><?php echo esc_html( $review_title ?? '' ); ?></h1>
										</div>
										<?php
									}

									if ( 'firm' === $review_type ) {
										?>
										<div class="dfr">
											<?php
											if ( has_post_thumbnail( $review_id ) ) {
												echo wp_kses_post( get_the_post_thumbnail( $review_id, 'alv-teacher-avatar' ) );
											} else {
												?>
												<img
														src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/thumbnail-mini-2.png' ); ?>"
														alt="No Avatar">
											<?php } ?>
											<h1 class="title"><?php the_title(); ?></h1>
										</div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex company-description">
					<div class="wpb_column vc_column_container vc_col-sm-12">
						<div class="vc_column-inner">
							<div class="wpb_wrapper">
								<div class="vc_col-lg-8 vc_col-md-7 vc_col-sm-6 vc_col-xs-12">
									<?php
									get_template_part( 'template-parts/courses', 'block', [ 'course' => $related_course ] );

									the_content();
									?>
								</div>
								<div class="vc_col-lg-4 vc_col-md-5 vc_col-sm-6 vc_col-xs-12">
									<div class="user-info">
										<?php
										if ( has_post_thumbnail( $review_id ) ) {
											echo wp_kses_post( get_the_post_thumbnail( $review_id, 'alv-teacher-avatar' ) );
										} else {
											?>
											<img
													src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/thumbnail-mini-2.png' ); ?>"
													alt="No Avatar">
										<?php } ?>
										<div class="user-desc">
											<h4><?php the_title(); ?></h4>
											<?php if ( 'regular' === $review_type || 'history' === $review_type ) { ?>
												<p><?php echo esc_html( $reviewer_position ?? '' ); ?></p>
												<?php
											}

											if ( 'firm' === $review_type ) {
												$text_link = carbon_get_post_meta( $review_id, 'alv_firm_text_link' );
												$firm_link = carbon_get_post_meta( $review_id, 'alv_firm_link' );
												?>
												<p>
													<a
															href="<?php echo esc_url( $firm_link ?? '#' ); ?>"
															target="_blank" rel="nofollow">
														<?php echo esc_html( $text_link ?? '' ); ?>
													</a>
												</p>
											<?php } ?>
										</div>
										<?php get_template_part( 'template-parts/social', '', [ 'social' => $reviewer_social ] ); ?>
										<?php
										if ( 'firm' === $review_type ) {
											$firm_link = carbon_get_post_meta( $review_id, 'alv_firm_link' );
											?>
											<a
													class="button"
													rel="nofollow"
													target="_blank"
													href="<?php echo esc_url( $firm_link ?? '#' ); ?>">
												<?php esc_html_e( 'Сторiнка компанii', 'alevel' ); ?>
											</a>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
			}
		}
		?>
		<?php
		get_template_part( 'template-parts/short-code-review' );
		get_template_part( 'template-parts/short-code-events' );
		get_template_part( 'template-parts/short-code-faq' );
		?>
	</section>
<?php
get_footer();
