<?php
/**
 * Archive testimonials template.
 *
 * @package iwpdev/alevel
 */

get_header();

$type_review = isset( $_GET['type'] ) ? filter_var( wp_unslash( $_GET['type'] ), FILTER_SANITIZE_STRING ) : null;

?>
	<section style="background:#F8EEFC;">
		<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex banner-gradient"
			 style="--var-gradient: linear-gradient(135deg, rgba(72,14,193,1) 0%,rgba(72,14,193,1) 40%,rgba(153,18,201,1) 80%,rgba(153,18,201,1) 100%); ">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<div class="vc_col-lg-8 vc_col-md-7 vc_col-sm-6 vc_col-xs-12">
							<h1 class="title"><?php esc_html_e( 'Вiдгуки', 'alevel' ); ?></h1>
							<p><?php echo wp_kses_post( wpautop( carbon_get_theme_option( 'alv_archive_custom_title_testimonials' ) ) ); ?></p>
						</div>
						<div class="vc_col-lg-4 vc_col-md-5 vc_col-sm-6 vc_col-xs-12">
							<a class="button" data-toggle="modal" data-target=".leave-review" href="#">
								<?php esc_html_e( '+Додати вiдгук', 'alevel' ); ?>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php if ( 'firm' !== $type_review || empty( $type_review ) ) { ?>
			<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex tags-block">
				<div class="wpb_column vc_column_container vc_col-sm-12">
					<div class="vc_column-inner">
						<div class="wpb_wrapper">
							<div class="vc_col-lg-12 vc_col-md-12 vc_col-sm-12 vc_col-xs-12">
								<?php get_template_part( 'template-parts/testimonials-menu-tag' ); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
		<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex filter">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<div class="vc_col-lg-12 vc_col-md-12 vc_col-sm-12 vc_col-xs-12">
							<?php get_template_part( 'template-parts/testimonials-filter-menu' ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex reviews">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<div class="vc_col-lg-12 vc_col-md-12 vc_col-sm-12 vc_col-xs-12">
							<?php if ( have_posts() ) { ?>
								<div class="reviews-items dfr">
									<?php
									while ( have_posts() ) {
										the_post();

										$testimonial_id = get_the_ID();
										$review_social  = carbon_get_post_meta( $testimonial_id, 'alv_social_media_reviewer' );
										$course_id      = carbon_get_post_meta( $testimonial_id, 'alv_course_event' )[0]['id'] ?? 0;
										$teachers       = carbon_get_post_meta( $testimonial_id, 'alv_reviewer_teachers' );
										$type_review    = carbon_get_post_meta( $testimonial_id, 'alv_type_review' );

										if ( 'firm' === $type_review ) {
											get_template_part(
												'template-parts/reviews',
												'firm',
												[
													'testimonials_id' => $testimonial_id,
													'course_id'       => $course_id,
												]
											);
										} else {
											get_template_part(
												'template-parts/reviews',
												'item',
												[
													'testimonials_id' => $testimonial_id,
													'course_id'       => $course_id,
												]
											);
										}
									}
									?>
								</div>
							<?php } else { ?>
								<h3 class="no-testimonials"><?php esc_html_e( 'Цей напрям новий, відгуків ще немає, будь першим хто закінчить цей курс та залишить відгук.', 'alevel' ); ?></h3>
							<?php } ?>
							<div class="page_navigation_wrapper">
								<?php
								if ( function_exists( 'wp_pagenavi' ) ) {
									wp_pagenavi();
								}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
		get_template_part( 'template-parts/short-code-events' );
		get_template_part( 'template-parts/short-code-faq' );
		?>
	</section>
<?php
get_footer();
