<?php
/**
 * Function themes.
 *
 * @package iwpdev/alevel
 */

use Alevel\Main;

define( 'ALV_URL_PATH', get_stylesheet_directory_uri() );

require_once __DIR__ . '/vendor/autoload.php';

new Main();

