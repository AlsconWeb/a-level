<?php
/**
 * Footer template.
 *
 * @package iwpdev/alevel
 */

use Alevel\Helpers\Helper;
use Alevel\Main;

$logo_url         = get_theme_mod( 'alv_logo_footer' );
$text             = carbon_get_theme_option( 'alv_footer_text' );
$address          = carbon_get_theme_option( 'alv_address' );
$phone_one        = carbon_get_theme_option( 'alv_phone_one' );
$phone_two        = carbon_get_theme_option( 'alv_phone_two' );
$social_facebook  = carbon_get_theme_option( 'alv_facebook_link' );
$social_instagram = carbon_get_theme_option( 'alv_instagram_link' );
$social_linkedin  = carbon_get_theme_option( 'alv_linkedin_link' );
$social_telegram  = carbon_get_theme_option( 'alv_telegram_link' );
$social_dou       = carbon_get_theme_option( 'alv_dou_link' );
$social_youtube   = carbon_get_theme_option( 'alv_youtube_link' );
$social_twitter   = carbon_get_theme_option( 'alv_twitter_link' );
$social_behance   = carbon_get_theme_option( 'alv_behance_link' );
?>
<footer>
	<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex">
		<div class="wpb_column vc_column_container vc_col-sm-12">
			<div class="vc_column-inner">
				<div class="wpb_wrapper">
					<div class="vc_col-lg-4 vc_col-md-3 vc_col-sm-3 vc_col-xs-12">
						<a class="logo" href="<?php bloginfo( 'url' ); ?>">
							<img src="<?php echo esc_url( $logo_url ); ?>" alt="White logo">
						</a>
						<?php echo wp_kses_post( wpautop( $text ) ) ?? ''; ?>
					</div>
					<div class="vc_col-lg-8 vc_col-md-9 vc_col-sm-9 vc_col-xs-12">
						<div class="dfr">
							<div class="courses-menu">
								<h3 class="title"><?php esc_html_e( 'Курси', 'alevel' ); ?></h3>
								<?php Helper::show_course_footer_menu(); ?>
							</div>
							<div class="school-menu">
								<h3 class="title"><?php echo esc_html_e( 'Школа', 'alevel' ); ?></h3>
								<?php
								wp_nav_menu(
									[
										'menu'       => 'Footer menu',
										'container'  => '',
										'menu_class' => 'menu',
										'echo'       => true,
										'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
									]
								);
								?>
							</div>
							<div class="contacts-block">
								<h3 class="title"><?php esc_html_e( 'Контакти', 'alevel' ); ?></h3>
								<?php echo wp_kses_post( wpautop( $address ) ) ?? ''; ?>
								<a href="tel:<?php echo filter_var( $phone_one, FILTER_SANITIZE_NUMBER_INT ) ?? ''; ?>">
									<?php echo esc_html( $phone_one ) ?? ''; ?>
								</a>
								<a href="tel:<?php echo filter_var( $phone_two, FILTER_SANITIZE_NUMBER_INT ) ?? ''; ?>">
									<?php echo esc_html( $phone_two ) ?? ''; ?>
								</a>
								<a class="button" data-toggle="modal" data-target=".become-teacher" href="#">
									<?php esc_html_e( 'Стати викладачем', 'alevel' ); ?>
								</a>
								<a class="button" data-toggle="modal" data-target=".become-partner" href="#">
									<?php esc_html_e( 'Стати партнером', 'alevel' ); ?>
								</a>
							</div>
						</div>
					</div>
					<div class="vc_col-lg-12 vc_col-md-12 vc_col-sm-12 vc_col-xs-12">
						<div class="dfr">
							<?php
							wp_nav_menu(
								[
									'menu'       => 'Footer submenu',
									'container'  => '',
									'menu_class' => 'menu dfr',
									'echo'       => true,
									'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
								]
							);
							?>
							<div class="select">
								<?php Main::get_city_in_select( 'footer' ); ?>
							</div>
							<?php //Main::show_switch_language( 'footer' ); ?>
						</div>
					</div>
					<div class="vc_col-lg-12 vc_col-md-12 vc_col-sm-12 vc_col-xs-12">
						<div class="dfr">
							<?php
							echo sprintf(
								'<p class="copyright">Copyright %s A-Level Ukraine</p>',
								esc_html( gmdate( 'Y' ) )
							);
							?>
							<ul class="soc dfr">
								<?php if ( ! empty( $social_facebook ) ) { ?>
									<li class="icon-facebook-f">
										<a
												href="<?php echo esc_url( $social_facebook ); ?>"
												rel="nofollow noreferrer"
												target="_blank"></a>
									</li>
									<?php
								}
								if ( ! empty( $social_instagram ) ) {
									?>
									<li class="icon-instagram">
										<a
												href="<?php echo esc_url( $social_instagram ); ?>"
												rel="nofollow noreferrer"
												target="_blank"></a>
									</li>
									<?php
								}

								if ( ! empty( $social_linkedin ) ) {
									?>
									<li class="icon-linkedin">
										<a
												href="<?php echo esc_url( $social_linkedin ); ?>"
												rel="nofollow noreferrer"
												target="_blank"></a>
									</li>
									<?php
								}

								if ( ! empty( $social_telegram ) ) {
									?>
									<li class="icon-telegram">
										<a
												href="<?php echo esc_url( $social_telegram ); ?>"
												rel="nofollow noreferrer"
												target="_blank"></a>
									</li>
									<?php
								}

								if ( ! empty( $social_dou ) ) {
									?>
									<li class="icon-dou">
										<a
												href="<?php echo esc_url( $social_dou ); ?>"
												rel="nofollow noreferrer"
												target="_blank"></a>
									</li>
									<?php
								}

								if ( ! empty( $social_youtube ) ) {
									?>
									<li class="icon-youtube">
										<a
												href="<?php echo esc_url( $social_youtube ); ?>"
												rel="nofollow noreferrer"
												target="_blank"></a>
									</li>
									<?php
								}

								if ( ! empty( $social_twitter ) ) {
									?>
									<li class="icon-twitter">
										<a
												href="<?php echo esc_url( $social_twitter ); ?>"
												rel="nofollow noreferrer"
												target="_blank"></a>
									</li>
									<?php
								}

								if ( ! empty( $social_behance ) ) {
									?>
									<li class="icon-behance">
										<a
												href="<?php echo esc_url( $social_behance ); ?>"
												rel="nofollow noreferrer"
												target="_blank"></a>
									</li>
								<?php } ?>

							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<?php
wp_nav_menu(
	[
		'theme_location' => 'scroll_menu',
		'container'      => '',
		'menu_class'     => 'fix-menu dfr',
		'echo'           => true,
		'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
	]
);

global $post;

if ( Helper::alv_has_shortcode_array( [ 'alv_testimonials_course' ] ) ) {
	get_template_part( 'template-parts/modals/leave-review', null, [ 'course_id' => $post->ID ] );
}

if ( is_tax( 'teacher-specialization' ) || is_post_type_archive( 'teachers' ) || is_post_type_archive( 'testimonials' ) ) {
	get_template_part( 'template-parts/modals/leave-review' );
}

if ( is_post_type_archive( 'events' ) ) {
	get_template_part( 'template-parts/modals/registration-event' );
}

if (
	is_post_type_archive( 'testimonials' ) ||
	is_singular( 'teachers' ) ||
	Helper::alv_has_shortcode_array( Main::ALV_SHORT_CODE_MODAL_NAME ) ||
	is_front_page() ||
	is_page(
		[
			553,
			1613,
		]
	)
) {
	get_template_part( 'template-parts/modals/review-regular' );
}

get_template_part( 'template-parts/modals/become-teacher' );
get_template_part( 'template-parts/modals/become-partner' );
get_template_part( 'template-parts/modals/registration-event' );
get_template_part( 'template-parts/modals/consultation' );
get_template_part( 'template-parts/modals/call-back' );
get_template_part( 'template-parts/modals/thanks-modal' );

if ( is_multisite() ) {
	$modal_show = carbon_get_network_option( 1, 'alv_pop_up_sales_start', 'carbon_fields_container_crb_network_container' );

	if ( 'on' === $modal_show ) {
		get_template_part( 'template-parts/modals/new-year-sales' );
	}
}

wp_footer();
?>
</body>
</html>
