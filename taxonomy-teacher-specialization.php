<?php
/**
 * Taxonomy template teacher specialization.
 *
 * @package iwpdev/alevel
 */

get_header();
?>
	<section style="background:#F9F6F1;">
		<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex banner-gradient"
			 style="--var-gradient: linear-gradient(82.27deg, #FED68F 0%, #F2773A 100%);">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<div class="vc_col-lg-12 vc_col-md-12 vc_col-sm-12 vc_col-xs-12">
							<h1 class="title"><?php esc_html_e( 'Викладачі A-Level', 'alevel' ); ?></h1>
							<p>
								<?php echo wp_kses_post( wpautop( carbon_get_theme_option( 'alv_archive_custom_title_teacher' ) ) ); ?>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex teachers-block">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<div class="vc_col-lg-12 vc_col-md-12 vc_col-sm-12 vc_col-xs-12">
							<h2 class="title">
								<?php esc_html_e( 'Ментори по дисциплiнам', 'alevel' ); ?>
							</h2>
							<?php
							get_template_part( 'template-parts/teachers', 'specialization' );
							if ( have_posts() ) {
								?>
								<div class="teachers dfr">
									<?php
									while ( have_posts() ) {
										the_post();

										$teacher_id = get_the_ID();

										get_template_part( 'template-parts/teacher', 'cart', [ 'teacher_id' => $teacher_id ] );
									}
									?>
									<a
											class="button" data-toggle="modal" data-target=".leave-review"
											href="#">
										<i class="icon-plus"><?php esc_html_e( 'Залишити вiдгук', 'alevel' ); ?></i>
									</a>
								</div>
							<?php } ?>
							<div class="page_navigation_wrapper">
								<?php
								if ( function_exists( 'wp_pagenavi' ) ) {
									wp_pagenavi();
								}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
		get_template_part( 'template-parts/shortcode-teachers-title' );
		get_template_part( 'template-parts/short-code-faq' );
		?>
	</section>
<?php
get_footer();

