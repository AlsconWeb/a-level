<?php
/**
 * Template Name: Sitemap HTML.
 *
 * @package iwpdev/alevel
 */

get_header();

$gradient_class = carbon_get_post_meta( get_the_ID(), 'alv_line_gradient_class' );

const ALV_POST_TYPES_ARRAY = [
	'page',
	'courses',
	'teachers',
	'post',
];

?>
	<section style="background:#F9F6F1;">
		<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex banner-gradient <?php echo esc_attr( $gradient_class ) ?? ''; ?>">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<?php if ( ! is_page( 'kontakti' ) ) { ?>
							<div class="vc_col-lg-12 vc_col-md-12 vc_col-sm-12 vc_col-xs-12">
								<h1 class="title"><?php the_title(); ?></h1>
								<p><?php echo wp_kses_post( wpautop( carbon_get_the_post_meta( 'alv_partners_custom_title' ) ) ); ?></p>
							</div>
							<?php
						} else {
							get_template_part( 'template-parts/header', 'contact' );
						}
						?>
					</div>
				</div>
			</div>
		</div>
		<?php
		foreach ( ALV_POST_TYPES_ARRAY as $alv_post_type ) {
			get_template_part(
				'template-parts/sitemap',
				'item',
				[
					'post_type' => $alv_post_type,
				]
			);
		}
		?>
	</section>
<?php
get_footer();
