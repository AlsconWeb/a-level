<?php
/**
 * Index template.
 *
 * @package iwpdev/alevel
 */

get_header();
?>
	<section>
		<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<div class="vc_col-lg-12 vc_col-md-12 vc_col-sm-12 vc_col-xs-12">
							<h1><?php echo wp_kses_post( wpautop( carbon_get_theme_option( 'alv_404_error_title' ) ) ?? '' ); ?></h1>
							<div class="dfr">
								<div class="desc">
									<h3><?php echo wp_kses_post( wpautop( carbon_get_theme_option( 'alv_404_error_sub_title' ) ) ?? '' ); ?></h3>
									<h2>404</h2>
								</div>
								<p>
									<?php echo wp_kses_post( wpautop( carbon_get_theme_option( 'alv_404_error_text' ) ) ?? '' ); ?>
								</p>
							</div>
							<?php
							wp_nav_menu(
								[
									'theme_location' => 'scroll_menu',
									'container'      => '',
									'menu_class'     => 'buttons dfr',
									'echo'           => true,
									'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
								]
							);
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer();
