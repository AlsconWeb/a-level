<?php
/**
 * Single post template.
 *
 * @package iwpdev/alevel
 */

use Alevel\Helpers\Helper;

get_header();
?>
	<section style="background:#F9F6F1;">
		<?php
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();
				$article_id     = get_the_ID();
				$article_tag    = wp_get_post_terms( $article_id, 'post_tag' );
				$article_course = carbon_get_the_post_meta( 'alv_course_post' );
				$author_type    = carbon_get_the_post_meta( 'alv_author_teachers' );
				if ( 'yes' === $author_type ) {
					$author            = carbon_get_the_post_meta( 'alv_post_author_teacher' );
					$author_avatar_url = isset( $author[0]['id'] ) && has_post_thumbnail( $author[0]['id'] ) ? get_the_post_thumbnail_url( $author[0]['id'], 'alv-teacher-avatar' ) : 'https://via.placeholder.com/80x80';
					$author_name       = isset( $author[0]['id'] ) ? get_the_title( $author[0]['id'] ) : '';
					$author_position   = isset( $author[0]['id'] ) ? carbon_get_post_meta( $author[0]['id'], 'alv_teacher_job_title' ) : '';
					$author_link       = isset( $author[0]['id'] ) ? get_the_permalink( $author[0]['id'] ) : '';
					$author_post_count = isset( $author[0]['id'] ) ? Helper::alv_get_count_post_teacher( $author[0]['id'] ) : '';
				} else {
					$author = carbon_get_the_post_meta( 'alv_post_author_no_teacher' )[0] ?? null;
					if ( $author ) {
						$author_avatar_url = wp_get_attachment_image_url( $author['photo'], 'alv-teacher-avatar' ) ?: 'https://via.placeholder.com/80x80';
						$author_name       = $author['name'];
						$author_position   = $author['position'];
						$author_link       = $author['link'];
						$author_post_count = null;
					}
				}

				?>
				<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex banner-gradient"
					 style="--var-gradient: radial-gradient(193.23% 436.88% at 75.5% 179.02%, #FFB800 0%, #FF9878 100%);">
					<div class="wpb_column vc_column_container vc_col-sm-12">
						<div class="vc_column-inner">
							<div class="wpb_wrapper">
								<div class="vc_col-lg-12 vc_col-md-12 vc_col-sm-12 vc_col-xs-12">
									<a
											class="back"
											href="<?php echo esc_attr( get_permalink( 40 ) ); ?>">
										<i class="icon-arrow-left"></i>
										<?php esc_html_e( 'Усi статтi', 'alevel' ); ?>
									</a>
									<ul class="tag dfr">
										<?php
										if ( ! empty( $article_tag ) ) {
											foreach ( $article_tag as $item ) {
												?>
												<li><?php echo esc_html( $item->name ); ?></li>
												<?php
											}
										}
										?>
										<li><?php the_time( 'd.m.Y' ); ?></li>
									</ul>
									<div class="company-block dfr">
										<h1 class="title"><?php the_title(); ?></h1>
										<p>
											<?php the_excerpt(); ?>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex company-description">
					<div class="wpb_column vc_column_container vc_col-sm-12">
						<div class="vc_column-inner">
							<div class="wpb_wrapper">
								<div class="vc_col-lg-8 vc_col-md-8 vc_col-sm-8 vc_col-xs-12">
									<?php
									if ( has_post_thumbnail( $article_id ) ) {
										the_post_thumbnail( 'alv-blog', [ 'class' => 'thumbnail' ] );
									} else {
										?>
										<img
												class="thumbnail"
												src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/Media.png' ); ?>"
												alt="No image">
										<?php
									}

									the_content();

									get_template_part( 'template-parts/courses', 'block', [ 'course' => $article_course ] );
									?>
								</div>
								<div class="vc_col-lg-4 vc_col-md-4 vc_col-sm-4 vc_col-xs-12">
									<?php if ( ! empty( $author ) ) { ?>
										<div class="user-info">
											<?php if ( ! empty( $author_avatar_url ) ) { ?>
												<img
														class="user" src="<?php echo esc_url( $author_avatar_url ); ?>"
														alt="<?php echo esc_html( $author_name ); ?>">
											<?php } else { ?>
												<img
														class="user" src="<?php echo esc_url( $author_avatar_url ); ?>"
														alt="<?php echo esc_html( $author_name ); ?>">
											<?php } ?>
											<h5><?php esc_html_e( 'Автор статтi', 'alevel' ); ?></h5>
											<div class="user-desc">
												<h4><?php echo esc_html( $author_name ); ?></h4>
												<p><?php echo esc_html( $author_position ); ?></p>
											</div>
											<?php
											if ( 'yes' === $author_type ) {
												echo sprintf(
													'<p>%d %s</p>',
													esc_html( $author_post_count ),
													esc_html( _n( 'статя на сайтi', 'статей на сайтi', $author_post_count, 'alevel' ) )
												);
											}
											?>
											<a class="button" href="<?php echo esc_url( $author_link ); ?>">
												<?php esc_html_e( 'Сторiнка автора', 'alevel' ); ?>
											</a>
										</div>
									<?php } ?>
									<div class="info-blog">
										<?php
										echo sprintf(
											'<p class="icon-clock">%s %s</p>',
											/* translators: %s text line */
											esc_html( __( 'Читати', 'alevel' ) ),
											esc_html( Helper::alv_show_read_min( get_the_content() ) )
										);
										?>
										<progress max="100" value="0"></progress>
										<?php echo do_shortcode( '[ez-toc]' ); ?>
										<div class="soc">
											<p><?php esc_html_e( 'Подiлитися з друзями:', 'alevel' ); ?></p>
											<?php echo do_shortcode( '[spbsm-share-buttons]' ); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
			}
		}
		?>
		<?php
		get_template_part( 'template-parts/short-code-review' );
		get_template_part( 'template-parts/short-code-faq' );
		?>
	</section>
<?php
get_footer();
