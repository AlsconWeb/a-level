<?php
/**
 * Template Name: Partners.
 *
 * @package iwpdev/alevel
 */

get_header();

$custom_title       = carbon_get_the_post_meta( 'alv_partners_custom_title' ) ?? '';
$testimonials_paged = get_query_var( 'paged' ) ?: 1;
$type               = ! empty( $_GET['type'] ) ? filter_var( wp_unslash( $_GET['type'] ), FILTER_SANITIZE_STRING ) : null;

if ( empty( $type ) || 'partner' === $type ) {
	$arg = [
		'post_type'      => 'testimonials',
		'post_status'    => 'publish',
		'posts_per_page' => 8,
		'orderby'        => 'rand',
		'paged'          => $testimonials_paged,
		'meta_query'     => [
			'relation' => 'AND',
			[
				'key'   => '_alv_type_review',
				'value' => 'firm',
			],
			[
				'key'     => '_alv_company_no_review',
				'value'   => 'true',
				'compare' => '!=',
			],
		],
	];
} else {
	$arg = [
		'post_type'      => 'testimonials',
		'post_status'    => 'publish',
		'posts_per_page' => 8,
		'orderby'        => 'rand',
		'paged'          => $testimonials_paged,
		'meta_query'     => [
			[
				'key'     => '_alv_company_no_review',
				'value'   => 'true',
				'compare' => '=',
			],
		],
	];
}

$testimonials_obj = new WP_Query( $arg );
$gradient_class   = carbon_get_post_meta( get_the_ID(), 'alv_line_gradient_class' );
?>
	<section style="background:#F8EEFC;">
		<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex banner-gradient <?php echo esc_attr( $gradient_class ) ?? ''; ?>">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<div class="vc_col-lg-8 vc_col-md-8 vc_col-sm-8 vc_col-xs-12">
							<h1 class="title"><?php the_title(); ?></h1>
							<p><?php echo esc_html( $custom_title ); ?></p>
						</div>
						<div class="vc_col-lg-4 vc_col-md-4 vc_col-sm-4 vc_col-xs-12">
							<a class="button" data-toggle="modal" data-target=".become-partner" href="#">
								<?php esc_html_e( 'Стати партнером', 'alevel' ); ?>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex filter">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<div class="vc_col-lg-12 vc_col-md-12 vc_col-sm-12 vc_col-xs-12">
							<ul class="partners-filter dfr">
								<li class="<?php echo empty( $type ) || 'partner' === $type ? 'active' : ''; ?>">
									<a href="#" data-type="partner">
										<?php esc_html_e( 'З відгуками', 'alevel' ); ?>
									</a>
								</li>
								<li class="<?php echo 'no-review' === $type ? 'active' : ''; ?>">
									<a href="#" data-type="no-review">
										<?php esc_html_e( 'Без відгуків', 'alevel' ); ?>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex partners-block">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<div class="vc_col-lg-12 vc_col-md-12 vc_col-sm-12 vc_col-xs-12">
							<?php if ( $testimonials_obj->have_posts() ) { ?>
								<div class="partners dfr">
									<?php
									while ( $testimonials_obj->have_posts() ) {
										$testimonials_obj->the_post();

										$testimonial_id = get_the_ID();
										$course_id      = carbon_get_post_meta( $testimonial_id, 'alv_course_event' );
										$teachers       = carbon_get_post_meta( $testimonial_id, 'alv_reviewer_teachers' );

										get_template_part(
											'template-parts/reviews',
											'firm',
											[
												'testimonials_id' => $testimonial_id,
												'course_id'       => $course_id,
											]
										);
									}

									wp_reset_postdata();
									?>
								</div>
							<?php } ?>
						</div>
						<div class="vc_col-lg-12 vc_col-md-12 vc_col-sm-12 vc_col-xs-12">
							<div class="page_navigation_wrapper">
								<?php
								if ( function_exists( 'wp_pagenavi' ) ) {
									wp_pagenavi( [ 'query' => $testimonials_obj ] );
								}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
		get_template_part( 'template-parts/short-code-reviews-block' );
		get_template_part( 'template-parts/short-code-faq' );
		?>
	</section>
<?php
get_footer();
