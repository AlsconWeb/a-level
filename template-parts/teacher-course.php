<?php
/**
 * Template part: teacher course.
 *
 * @package iwpdev/alevel
 */

$teacher_course_id       = ! empty( $args['course_id'] ) ? $args['course_id'] : null;
$teacher_courses         = ! empty( $args['courses'] ) ? $args['courses'] : null;
$teacher_courses_archive = ! empty( $args['courses_archive'] ) ? $args['courses_archive'] : null;
if ( ! empty( $teacher_course_id ) ) { ?>
	<ul>
		<?php
		$data_title  = __( 'Курс:', 'alevel' );
		$course_logo = carbon_get_post_meta( $teacher_course_id, 'alv_course_icon' );
		?>
		<li data-title="<?php echo esc_html( $data_title ); ?>">
			<a href="<?php echo esc_url( get_permalink( $teacher_course_id ) ); ?>" class="link">
				<img
						src="<?php echo esc_url( $course_logo ); ?>"
						alt="<?php echo esc_html( get_the_title( $teacher_course_id ) ); ?>">
				<p><?php echo esc_html( get_the_title( $teacher_course_id ) ); ?></p>
			</a>
		</li>
	</ul>
	<?php
}

if ( isset( $teacher_courses ) ) {
	?>
	<ul>
		<?php
		foreach ( $teacher_courses as $key => $course ) {
			$data_title  = 0 === $key ? __( 'Курс:', 'alevel' ) : '';
			$course_logo = carbon_get_post_meta( $course['id'], 'alv_course_icon' );
			?>
			<li data-title="<?php echo esc_html( $data_title ); ?>">
				<a href="<?php echo esc_url( get_permalink( $course['id'] ) ); ?>" class="link">
					<img
							src="<?php echo esc_url( $course_logo ); ?>"
							alt="<?php echo esc_html( get_the_title( $course['id'] ) ); ?>">
					<p><?php echo esc_html( get_the_title( $course['id'] ) ); ?></p>
				</a>
			</li>
			<?php
		}
		?>
	</ul>
	<?php
}

if ( isset( $teacher_courses_archive ) ) {
	?>
	<ul>
		<?php
		foreach ( $teacher_courses_archive as $key => $course ) {
			$data_title  = 0 === $key ? __( 'Курс:', 'alevel' ) : '';
			$course_logo = carbon_get_post_meta( $course['id'], 'alv_course_icon' );
			if ( $key === 0 || count( $teacher_courses_archive ) < 3 ) {
				?>
				<li data-title="<?php echo esc_html( $data_title ); ?>">
					<a href="<?php echo esc_url( get_permalink( $course['id'] ) ); ?>" class="link">
						<img
								src="<?php echo esc_url( $course_logo ); ?>"
								alt="<?php echo esc_html( get_the_title( $course['id'] ) ); ?>">
						<p><?php echo esc_html( get_the_title( $course['id'] ) ); ?></p>
					</a>
				</li>
				<?php
			}
		}
		if ( count( $teacher_courses_archive ) > 2 ) {
			$count = count( $teacher_courses_archive ) - 1;
			?>
			<li>
				<span class="count-course"><?php echo esc_html( $count ); ?></span>
				<p><?php echo esc_html( '+' . $count . __( ' курси', 'alevel' ) ); ?></p>
			</li>
		<?php } ?>
	</ul>
	<?php
}
