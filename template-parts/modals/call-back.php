<?php
/**
 * Template part: call back.
 *
 * @package iwpdev/alevel
 */

use Alevel\Ajax\ModalHandler;

?>
<div class="call-back modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: linear-gradient(82.27deg, #658B94 0%, #F0722F 100%);">
				<h2><?php echo esc_html( carbon_get_theme_option( 'alv_modal_callback_title' ) ?? '' ); ?></h2>
				<p><?php echo esc_html( carbon_get_theme_option( 'alv_modal_callback_sub_title' ) ?? '' ); ?></p>
				<button class="close" type="button" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<form id="call_back_form" method="post">
					<div class="dfr">
						<div class="input">
							<label for="callback-name" class="hidden"></label>
							<input
									type="text"
									name="callback_name"
									id="callback-name"
									placeholder="<?php esc_html_e( 'Iм\'я', 'alevel' ); ?>"
									required>
						</div>
						<div class="input">
							<label for="callback-phone" class="hidden"></label>
							<input
									type="tel"
									name="callback_phone"
									id="callback-phone"
									placeholder="<?php esc_html_e( 'Телефон', 'alevel' ); ?>"
									required>
						</div>
					</div>
					<p><?php echo esc_html( carbon_get_theme_option( 'alv_modal_callback_middle_text' ) ?? '' ); ?></p>
					<div class="dfr">
						<div class="input">
							<label for="callback-date" class="hidden"></label>
							<input
									class="date"
									type="text"
									id="callback-date"
									name="callback_date"
									placeholder="<?php esc_html_e( 'Дата', 'alevel' ); ?>"
									required>
						</div>
						<div class="input">
							<label for="callback-time" class="hidden"></label>
							<input
									class="clock"
									type="text"
									name="callback_time"
									id="callback-time"
									placeholder="<?php esc_html_e( 'Час', 'alevel' ); ?>"
									required>
						</div>
					</div>
					<?php wp_nonce_field( ModalHandler::CALL_BACK_MODAL_ACTION, ModalHandler::CALL_BACK_MODAL_ACTION ); ?>
					<input type="submit" value="<?php esc_html_e( 'Залишити заявку', 'alevel' ); ?>">
					<?php
					echo sprintf(
						'<label class="label-policy" for="callback_policy"><input id="callback_policy" type="checkbox" name="callback_policy" checked><p>%s <a href="%s">%s</a></p></label>',
						esc_html( __( 'Натискаючи на кнопку я погоджуюсь на', 'alevel' ) ),
						esc_url( get_privacy_policy_url() ),
						esc_html( __( 'обробку персональних даних', 'alevel' ) )
					);
					?>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="call-back-thanks modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: linear-gradient(82.27deg, #658B94 0%, #F0722F 100%);">
				<i class="icon-like"></i>
				<h2><?php echo esc_html( carbon_get_theme_option( 'alv_modal_thanks_callback_title' ) ); ?></h2>
				<p><?php echo esc_html( carbon_get_theme_option( 'alv_modal_thanks_callback_sub_title' ) ); ?></p>
				<button class="close" type="button" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<h5><?php echo esc_html( carbon_get_theme_option( 'alv_modal_thanks_callback_text_title' ) ); ?></h5>
				<p><?php echo esc_html( carbon_get_theme_option( 'alv_modal_thanks_callback_text_sub_title' ) ); ?></p>
				<p><?php echo esc_html( carbon_get_theme_option( 'alv_modal_thanks_callback_text_center' ) ); ?></p>
				<div class="events">

				</div>
				<a class="button dark-grey" href="#" data-dismiss="modal" aria-hidden="true">
					<?php esc_html_e( 'Закрити', 'alevel' ); ?>
				</a>
			</div>
		</div>
	</div>
</div>
