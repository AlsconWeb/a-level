<?php
/**
 * Template parts modal leave-review.
 *
 * @package iwpdev/alevel.
 */

use Alevel\Ajax\ModalHandler;
use Alevel\Helpers\Helper;

?>
<div class="leave-review modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: linear-gradient(97.71deg, #4BD2FD -16.21%, #406BD9 77.81%);">
				<h2><?php echo esc_html( carbon_get_theme_option( 'alv_modal_leave_review_title' ) ?? '' ); ?></h2>
				<ul class="steps">
					<li class="active"></li>
					<li></li>
					<li></li>
				</ul>
				<p><?php echo wp_kses_post( wpautop( carbon_get_theme_option( 'alv_modal_leave_review_sub_title' ) ?? '' ) ); ?></p>
				<button class="close" type="button" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body show">
				<form id="alv-leave-review-step-one" method="post">
					<div class="dfr">
						<div class="input">
							<input
									type="text" id="leave-review-fist-name" name="first_name"
									placeholder="<?php esc_html_e( 'Iм\'я', 'alevel' ); ?>">
						</div>
						<div class="input">
							<input
									type="text" id="leave-review-last-name" name="last_name"
									placeholder="<?php esc_html_e( 'Прiзвище', 'alevel' ); ?>">
						</div>
						<div class="input">
							<input
									type="email" name="email" id="leave-review-email"
									placeholder="<?php esc_html_e( 'Email', 'alevel' ); ?>">
						</div>
						<div class="input">
							<input
									type="tel" name="phone" id="leave-review-phone"
									placeholder="<?php esc_html_e( 'Телефон: +380', 'alevel' ); ?>">
						</div>
					</div>
					<hr class="sline">
					<p><?php echo esc_html( carbon_get_theme_option( 'alv_modal_leave_review_social_text' ) ?? '' ); ?></p>
					<ul class="soc dfr">
						<li>
							<label class="icon-facebook-f" for="facebook" href="#"></label>
						</li>
						<li>
							<label class="icon-instagram" for="instagram" href="#"></label>
						</li>
						<li>
							<label class="icon-linkedin" for="linkedin" href="#"></label>
						</li>
						<li>
							<label class="icon-behance" for="behance" href="#"></label>
						</li>
						<li>
							<label class="icon-telegram" for="telegram" href="#"></label>
						</li>
					</ul>
					<hr class="sline">
					<p><?php esc_html_e( 'Ваше фото:', 'alevel' ); ?></p>
					<div class="file">
						<div id="preview"></div>
						<input id="leave-review-add-file" name="avatar-file" accept="image/*" type="file">
						<label for="leave-review-add-file">
							<?php esc_html_e( '+ Завантажити фото', 'alevel' ); ?>
						</label>
						<div id="uploaded_image"></div>
					</div>
					<a class="button next" href="#"><?php esc_html_e( 'Далi', 'alevel' ); ?></a>
					<?php
					echo sprintf(
						'<label  for="review_policy"><input type="checkbox" name="review_policy" checked><p>%s <a href="%s">%s</a></p></label>',
						esc_html( __( 'Натискаючи на кнопку я погоджуюсь на', 'alevel' ) ),
						esc_url( get_privacy_policy_url() ),
						esc_html( __( 'обробку персональних даних', 'alevel' ) )
					);
					?>

				</form>
			</div>
			<div class="modal-body">
				<form method="post" id="alv-leave-review-step-two">

					<div class="dfr">
						<?php
						if ( ! isset( $args['course_id'] ) ) {
							?>
							<div class="select" id="course_select">
								<?php Helper::show_course_select( 'review-course' ); ?>
							</div>
						<?php } ?>
					</div>
					<hr class="sline">
					<p><?php esc_html_e( 'Викладачi:', 'alevel' ); ?></p>
					<ul class="teachers">
						<?php
						$teachers_ids = get_post_meta( get_the_ID(), 'alv_course_teachers', true );

						if ( ! empty( $teachers_ids ) ) {
							foreach ( $teachers_ids as $teacher ) {
								?>
								<li>
									<?php
									if ( has_post_thumbnail( $teacher ) ) {
										echo get_the_post_thumbnail( $teacher, 'alv-teacher-avatar' );
									} else {
										?>
										<img
												src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/thumbnail-mini-2.png' ); ?>"
												alt="No Avatar">
									<?php } ?>
									<div class="checkbox">
										<input
												id="teacher-<?php echo esc_attr( $teacher ); ?>"
												name="leave-review-teachers[]"
												value="<?php echo esc_attr( $teacher ); ?>" type="checkbox">
										<label
												class="icon-check"
												for="teacher-<?php echo esc_attr( $teacher ); ?>">
											<?php echo esc_html( get_the_title( $teacher ) ); ?>
										</label>
									</div>
								</li>
								<?php
							}
						}
						?>
					</ul>
					<a class="button prev" href="#"><?php esc_html_e( 'Назад', 'alevel' ); ?></a>
					<a class="button next" href="#"><?php esc_html_e( 'Далi', 'alevel' ); ?></a>
				</form>
			</div>
			<div class="modal-body">
				<form action="" method="post" id="alv-leave-review-step-three">
					<div class="textarea">
					<textarea
							name="message" id="leave-review-message"
							placeholder="<?php esc_html_e( 'Вiдгук', 'alevel' ); ?>"></textarea>
					</div>
					<p>Оцініть курс</p>
					<fieldset class="rating" id="rating_fields">
						<input id="star5" type="radio" name="rating" value="5">
						<label class="full" for="star5"></label>
						<input id="star4half" type="radio" name="rating" value="4.5">
						<label class="half" for="star4half"></label>
						<input id="star4" type="radio" name="rating" value="4">
						<label class="full" for="star4"></label>
						<input id="star3half" type="radio" name="rating" value="3.5">
						<label class="half" for="star3half"></label>
						<input id="star3" type="radio" name="rating" value="3">
						<label class="full" for="star3"></label>
						<input id="star2half" type="radio" name="rating" value="2.5">
						<label class="half" for="star2half"></label>
						<input id="star2" type="radio" name="rating" value="2">
						<label class="full" for="star2"></label>
						<input id="star1half" type="radio" name="rating" value="1.5">
						<label class="half" for="star1half"></label>
						<input id="star1" type="radio" name="rating" value="1">
						<label class="full" for="star1"></label>
						<input id="starhalf" type="radio" name="rating" value="half">
						<label class="half" for="starhalf"></label>
					</fieldset>
					<?php wp_nonce_field( ModalHandler::NEW_REVIEW, ModalHandler::NEW_REVIEW ); ?>
					<input
							type="hidden" name="course_id" id="course-id"
							value="<?php echo esc_attr( get_the_ID() ); ?>">
					<a class="button prev" href="#"><?php esc_html_e( 'Назад', 'alevel' ); ?></a>
					<button type="submit"
							id="new-review"
							class="button next"><?php esc_html_e( 'Залишити вiдгук', 'alevel' ); ?></button>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="crop-image-modal modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: linear-gradient(82.27deg, #1E3F37 0%, #9AEECF 100%);">
				<h2><?php esc_html_e( 'Обрізати зображення' ); ?></h2>
				<button class="close" type="button" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<p class="name-file"></p>
				<div id="crop-image"></div>
				<a class="button" href="#" data-dismiss="modal">
					<?php esc_html_e( 'Закрити', 'alevel' ); ?>
				</a>
				<a class="button btn-success" href="#" id="crop-save-image">
					<?php esc_html_e( 'Обрізати зображення', 'alevel' ); ?>
				</a>
			</div>
		</div>
	</div>
</div>