<?php
/**
 * Template part: Registration event modal.
 *
 * @package iwpdev/alevel
 */

use Alevel\Ajax\ModalHandler;

?>
<div class="registration-event modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: linear-gradient(82.27deg, #658B94 0%, #F0722F 100%);">
				<h2><?php echo esc_html( carbon_get_theme_option( 'alv_modal_register_event_title' ) ); ?></h2>
				<p><?php echo esc_html( carbon_get_theme_option( 'alv_modal_register_event_title' ) ); ?></p>
				<button class="close" type="button" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<form id="register-to-event" method="post">
					<div class="dfr">
						<div class="input">
							<label for="event-name" class="hidden"></label>
							<input
									type="text"
									id="event-name"
									name="event_name"
									placeholder="<?php esc_html_e( 'Iм\'я', 'alevel' ); ?>"
									required/>
						</div>
						<div class="input">
							<label for="event-email" class="hidden"></label>
							<input
									type="email"
									id="event-email"
									name="event-email"
									placeholder="<?php esc_html_e( 'Email', 'alevel' ); ?>"
									required/>
						</div>
						<div class="input">
							<label for="event-phone" class="hidden"></label>
							<input
									type="tel"
									id="event-phone"
									name="event_phone"
									placeholder="<?php esc_html_e( 'Телефон', 'alevel' ); ?>"
									required/>
						</div>
					</div>
					<input type="hidden" name="event_id" id="event-id" value="">
					<input type="submit" value="<?php esc_html_e( 'Залишити заявку', 'alevel' ); ?>"/>
					<?php
					wp_nonce_field( ModalHandler::REGISTER_ON_EVENT, ModalHandler::REGISTER_ON_EVENT );
					echo sprintf(
						'<label class="label-policy" for="event_policy"><input type="checkbox" id="event_policy" name="event_policy" checked><p>%s <a href="%s">%s <br/>%s </a></p></label>',
						esc_html( __( 'Натискаючи на кнопку я погоджуюсь на', 'alevel' ) ),
						esc_url( get_privacy_policy_url() ),
						esc_html( __( 'обробку', 'alevel' ) ),
						esc_html( __( 'персональних даних', 'alevel' ) )
					);
					?>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="registration-event-thanks modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: linear-gradient(82.27deg, #658B94 0%, #F0722F 100%);">
				<i class="icon-like"></i>
				<h2><?php echo esc_html( carbon_get_theme_option( 'alv_modal_thanks_event_title' ) ); ?></h2>
				<p><?php echo esc_html( carbon_get_theme_option( 'alv_modal_thanks_event_sub_title' ) ); ?></p>
				<button class="close" type="button" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<h5><?php echo esc_html( carbon_get_theme_option( 'alv_modal_thanks_event_text_title' ) ); ?></h5>
				<p><?php echo esc_html( carbon_get_theme_option( 'alv_modal_thanks_event_text_sub_title' ) ); ?></p>
				<div class="events-wrapper">
				</div>
				<a class="button dark-grey" href="#" data-dismiss="modal" aria-hidden="true">
					<?php esc_html_e( 'Закрити', 'alevel' ); ?>
				</a>
			</div>
		</div>
	</div>
</div>
