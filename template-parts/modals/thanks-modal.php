<?php
/**
 * Template parts modal thanks-modal.
 *
 * @package iwpdev/alevel.
 */

use Alevel\Helpers\Helper;

?>
<div class="thanks modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: linear-gradient(82.27deg, #1E3F37 0%, #9AEECF 100%);">
				<i class="icon-like"></i>
				<h2><?php echo esc_html( carbon_get_theme_option( 'alv_modal_thanks_consultation_title' ) ); ?></h2>
				<p><?php echo esc_html( carbon_get_theme_option( 'alv_modal_thanks_consultation_sub_title' ) ); ?></p>
				<button class="close" type="button" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<h5><?php echo esc_html( carbon_get_theme_option( 'alv_modal_thanks_consultation_text_title' ) ); ?></h5>
				<p><?php echo esc_html( carbon_get_theme_option( 'alv_modal_thanks_consultation_text_sub_title' ) ); ?></p>
				<div class="events-wrapper">
					<?php
					$event = Helper::get_event_to_madal_thnas();

					echo wp_kses_post( $event );
					?>
				</div>
				<a class="button dark-grey" href="#" data-dismiss="modal" aria-hidden="true">
					<?php esc_html_e( 'Закрити', 'alevel' ); ?>
				</a>
			</div>
		</div>
	</div>
</div>
