<?php
/**
 * Template part: New year pop-up.
 *
 * @package iwpdev/alevel
 */


$title     = carbon_get_network_option( 1, 'alv_pop_up_sales_title', 'carbon_fields_container_crb_network_container' );
$sub_title = carbon_get_network_option( 1, 'alv_pop_up_sales_sub_title', 'carbon_fields_container_crb_network_container' );
$text      = carbon_get_network_option( 1, 'alv_pop_up_sales_text_sales', 'carbon_fields_container_crb_network_container' );
$date_end  = carbon_get_network_option( 1, 'alv_pop_up_sales_end_date', 'carbon_fields_container_crb_network_container' );
$date      = strtotime( $date_end );
$day       = date( 'd', $date );
$month     = date( 'm', $date );
?>

<div class="action modal fade" id="sales-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<img
						src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/hat.svg' ); ?>"
						alt="Santa hat">
				<button class="close" type="button" data-dismiss="modal" aria-hidden="true">×</button>
				<h3><?php echo esc_html( $title ); ?></h3>
				<p><?php echo esc_html( $sub_title ); ?></p>
				<h4><?php echo wp_kses_post( $text ); ?></h4>
				<p><?php esc_html_e( 'до завершення акції:', 'alevel' ); ?></p>
				<div
						id="countdown" data-day="<?php echo esc_attr( $day ); ?>"
						data-month="<?php echo esc_attr( $month ); ?>">
					<ul>
						<li><span id="days"></span><?php esc_html_e( 'днi', 'alevel' ); ?></li>
						<li><span id="hours"></span><?php esc_html_e( 'год.', 'alevel' ); ?></li>
						<li><span id="minutes"></span><?php esc_html_e( 'хв.', 'alevel' ); ?></li>
						<li><span id="seconds"></span><?php esc_html_e( 'сек.', 'alevel' ); ?></li>
					</ul>
				</div>
				<a class="button all-course" href="<?php echo esc_url( get_post_type_archive_link( 'courses' ) ); ?>">
					<?php esc_html_e( 'Усі курси', 'alevel' ); ?>
				</a>
			</div>
		</div>
	</div>
</div>