<?php
/**
 * Template parts: modal become-partner.
 *
 * @package iwpdev/alevel
 */

use Alevel\Ajax\ModalHandler;

?>
<div class="become-partner modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: linear-gradient(82.27deg, #658B94 0%, #F0722F 100%);">
				<h2><?php echo esc_html( carbon_get_theme_option( 'alv_modal_partner_title' ) ); ?></h2>
				<p><?php echo esc_html( carbon_get_theme_option( 'alv_modal_partner_sub_title' ) ); ?></p>
				<button class="close" type="button" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<form id="become-partner-modal" method="post">
					<div class="dfr">
						<div class="input">
							<label for="partner-name" class="hide"></label>
							<input
									type="text"
									id="partner-name"
									name="partner_name"
									placeholder="<?php esc_html_e( 'Iм\'я представника', 'alevel' ); ?>"
									required>
						</div>
						<div class="input">
							<label for="partner-last-mane" class="hide"></label>
							<input
									type="text"
									name="partner_last_mane"
									id="partner-last-mane"
									placeholder="<?php esc_html_e( 'Прiзвище представника', 'alevel' ); ?>"
									required>
						</div>
						<div class="input">
							<label for="partner-email" class="hide"></label>
							<input
									type="email"
									name="partner_email"
									id="partner-email"
									placeholder="<?php esc_html_e( 'Email', 'alevel' ); ?>"
									required>
						</div>
						<div class="input">
							<label for="partner-phone" class="hide"></label>
							<input
									type="tel"
									id="partner-phone"
									name="partner_phone"
									placeholder="<?php esc_html_e( 'Телефон', 'alevel' ); ?>"
									required>
						</div>
					</div>
					<hr class="sline">
					<div class="dfr">
						<div class="input">
							<label for="partner-company" class="hide"></label>
							<input
									type="text"
									id="partner-company"
									name="partner_company"
									placeholder="<?php esc_html_e( 'Компанiя', 'alevel' ); ?>"
									required>
						</div>
						<div class="input">
							<label for="partner-web" class="hide"></label>
							<input
									type="text"
									id="partner-web"
									name="partner_web"
									placeholder="<?php esc_html_e( 'Веб сайт', 'alevel' ); ?>"
									required>
						</div>
						<div class="input">
							<label for="partner-linkedin" class="hide"></label>
							<input
									type="text"
									id="partner-linkedin"
									name="partner_linkedin"
									placeholder="<?php esc_html_e( 'Linkedin', 'alevel' ); ?>">
						</div>
					</div>
					<hr class="sline">
					<div class="textarea">
						<label for="partner-message" class="hide"></label>
						<textarea
								id="partner-message"
								name="partner_message"
								placeholder="<?php esc_html_e( 'Супровiдний лист (не обов’язково)', 'alevel' ); ?>"></textarea>
					</div>
					<input type="submit" value="<?php esc_html_e( 'Залишити заявку', 'alevel' ); ?>">
					<?php
					wp_nonce_field( ModalHandler::REGISTER_PARTNERS, ModalHandler::REGISTER_PARTNERS );
					echo sprintf(
						'<label class="label-policy" for="partner_policy"><input id="partner_policy" type="checkbox" name="partner_policy" checked><p>%s <a href="%s">%s <br/>%s </a></p></label>',
						esc_html( __( 'Натискаючи на кнопку я погоджуюсь на', 'alevel' ) ),
						esc_url( get_privacy_policy_url() ),
						esc_html( __( 'обробку', 'alevel' ) ),
						esc_html( __( 'персональних даних', 'alevel' ) )
					);
					?>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="become-partner-thanks modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: linear-gradient(82.27deg, #658B94 0%, #F0722F 100%);">
				<i class="icon-like"></i>
				<h2><?php echo esc_html( carbon_get_theme_option( 'alv_modal_thanks_partner_title' ) ); ?></h2>
				<p><?php echo esc_html( carbon_get_theme_option( 'alv_modal_thanks_partner_sub_title' ) ); ?></p>
				<button class="close" type="button" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<h5><?php echo esc_html( carbon_get_theme_option( 'alv_modal_thanks_partner_text_title' ) ); ?></h5>
				<div class="partners dfr">
				</div>
				<a class="button dark-grey" href="#" data-dismiss="modal" aria-hidden="true">
					<?php esc_html_e( 'Закрити', 'alevel' ); ?>
				</a>
			</div>
		</div>
	</div>
</div>
