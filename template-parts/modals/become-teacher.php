<?php
/**
 * Template parts modal become-teacher.
 *
 * @package iwpdev/alevel.
 */

use Alevel\Ajax\ModalHandler;

?>

<div class="become-teacher modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: linear-gradient(262.27deg, #2EBDE3 0%, #EB5563 100%);">
				<h2><?php esc_html_e( 'Стати викладачем', 'alevel' ); ?></h2>
				<p><?php esc_html_e( 'Залиште ваші контактні дані, і ми вам обов\'язково зателефонуємо!', 'alevel' ); ?></p>
				<button class="close" type="button" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<form method="post" name="alv_become_teacher" id="alv-become-teacher">
					<div class="dfr">
						<div class="input">
							<input
									type="text" id="first_name" name="first_name"
									placeholder="<?php esc_html_e( 'Iм\'я', 'alevel' ); ?>">
						</div>
						<div class="input">
							<input
									type="text" id="last_name" name="last_name"
									placeholder="<?php esc_html_e( 'Прiзвище', 'alevel' ); ?>">
						</div>
						<div class="input">
							<input
									type="email" id="email" name="email"
									placeholder="<?php esc_html_e( 'Email', 'alevel' ); ?>">
						</div>
						<div class="input">
							<input
									type="tel" id="phone" name="phone"
									placeholder="<?php esc_html_e( 'Телефон: +380', 'alevel' ); ?>">
						</div>
					</div>
					<div class="dfr">
						<div class="input">
							<input
									type="text" id="company" name="company"
									placeholder="<?php esc_html_e( 'Компанiя', 'alevel' ); ?>">
						</div>
						<div class="input">
							<input
									type="text" id="position" name="position"
									placeholder="<?php esc_html_e( 'Посада', 'alevel' ); ?>">
						</div>
						<div class="input">
							<input
									type="text" id="linkedin_link" name="linkedin_link"
									placeholder="<?php esc_html_e( 'Linkedin', 'alevel' ); ?>">
						</div>
					</div>
					<div class="dfr">
						<div class="textarea">
						<textarea
								name="description"
								id="message"
								placeholder="<?php esc_html_e( 'Коментар (не обов’язково)', 'alevel' ); ?>"></textarea>
						</div>
						<?php wp_nonce_field( ModalHandler::NEW_TEACHER, ModalHandler::NEW_TEACHER ); ?>
						<input type="hidden" name="course_name" id="course-name" value="<?php the_title(); ?>">
						<input
								type="submit" id="become-teacher-submit"
								value="<?php esc_html_e( 'Залишити заявку', 'alevel' ); ?>">
						<?php
						echo sprintf(
							'<label class="label-policy" for="teacher_policy"><input id="teacher_policy" type="checkbox" name="teacher_policy" checked><p>%s <a href="%s">%s</a></p></label>',
							esc_html( __( 'Натискаючи на кнопку я погоджуюсь на', 'alevel' ) ),
							esc_url( get_privacy_policy_url() ),
							esc_html( __( 'обробку персональних даних', 'alevel' ) )
						);
						?>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
