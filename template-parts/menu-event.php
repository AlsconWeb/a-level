<?php
/**
 * Menu event archive.
 *
 * @package iwpdev/alevel
 */

global $post;

$event_end_page = get_page_by_path( 'events-end' );


?>
<div class="nav-block">
	<ul class="nav dfr">
		<li class="<?php echo $post->ID !== $event_end_page->ID ? 'active' : ''; ?>">
			<a href="<?php echo esc_url( get_post_type_archive_link( 'events' ) ); ?>">
				<?php esc_html_e( 'Майбутні заходи', 'alevel' ); ?>
			</a>
		</li>
		<li class="<?php echo $post->ID === $event_end_page->ID ? 'active' : ''; ?>">
			<a href="<?php echo esc_url( get_the_permalink( $event_end_page->ID ) ); ?>">
				<?php esc_html_e( 'Архів заходів', 'alevel' ); ?>
			</a>
		</li>
	</ul>
</div>
