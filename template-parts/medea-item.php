<?php
/**
 * Template part: medea item.
 *
 * @package iwpdev/alevel
 */

$blog_post_id = ! empty( $args['post_id'] ) ? $args['post_id'] : null;
$post_term    = wp_get_post_terms( $blog_post_id, 'category' )[0];
$post_tag     = wp_get_post_terms( $blog_post_id, 'post_tag' );

if ( ! empty( $blog_post_id ) ) {
	?>
	<div class="medea-item">
		<a class="link" href="<?php the_permalink(); ?>"></a>
		<div class="img">
			<?php
			if ( has_post_thumbnail( $blog_post_id ) ) {
				the_post_thumbnail( 'alv-blog-min' );
			} else {
				?>
				<img
						src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/thumbnail-medium.png' ); ?>"
						alt="No image">
			<?php } ?>
			<p class="tag"><?php echo esc_html( $post_term->name ); ?></p>
			<p class="icon-file">
				<i class="icon-arrow-right"><?php esc_html_e( 'Стаття повністю', 'alevel' ); ?></i>
			</p>
		</div>
		<?php if ( ! empty( $post_tag ) ) { ?>
			<ul class="tags dfr">
				<?php foreach ( $post_tag as $item ) { ?>
					<li><?php echo esc_html( $item->name ); ?></li>
				<?php } ?>
			</ul>
		<?php } ?>
		<h3 class="title"><?php the_title(); ?></h3>
		<p><?php the_excerpt(); ?></p>
		<?php get_template_part( 'template-parts/author', 'info', [ 'post_id' => $blog_post_id ] ); ?>
	</div>
	<?php
}
