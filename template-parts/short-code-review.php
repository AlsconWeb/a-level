<?php
/**
 * Template Part: Reviews.
 *
 * @package iwpdev/alevel.
 */

?>
<div class="reviews short-code">
	<?php
	if ( is_multisite() ) {
		$site_id     = get_current_blog_id();
		$template_id = carbon_get_network_option( 1, 'alv_short_code' . $site_id . '_review', 'carbon_fields_container_crb_network_container' );
		echo do_shortcode( '[templatera id="' . $template_id . '"]' );
	} else {
		echo do_shortcode( '[templatera id="481"]' );
	}
	?>
</div>
