<?php
/**
 * Template part: testimonials menu tag.
 *
 * @package iwpdev/alevel
 */

$all_courses =
	[
		'post_type'      => 'courses',
		'posts_per_page' => - 1,
		'post_status'    => 'publish',
	];
$courses     = new WP_Query( $all_courses );

// phpcs:disable
$param_filter = ! empty( $_GET['course_id'] ) ? filter_var( wp_unslash( $_GET['course_id'] ), FILTER_SANITIZE_NUMBER_INT ) : 0;
// phpcs:enable

?>
<div class="menu-tag-wrapper">
	<h3 class="title"><?php esc_html_e( 'Вiдгуки за напрямками', 'alevel' ); ?></h3>
	<ul class="tags-menu dfr">
		<li class="<?php echo 0 === $param_filter ? esc_attr( 'active' ) : ''; ?>">
			<a href="#" class="review-category" data-courses_id="0">
				<?php esc_html_e( 'Усi напрямки', 'alevel' ); ?>
			</a>
		</li>
		<?php
		if ( ! empty( $courses->have_posts() ) ) {
			foreach ( $courses->posts as $item ) {
				?>
				<li class="<?php echo $item->ID === (int) $param_filter ? esc_attr( 'active' ) : ''; ?>">
					<a
							href="#" class="review-category"
							data-courses_id="<?php echo esc_attr( $item->ID ); ?>">
						<?php echo esc_html( $item->post_title ); ?>
					</a>
				</li>
				<?php
			}
		}
		?>
	</ul>
</div>
