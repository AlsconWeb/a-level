<?php
/**
 * Template part: testimonials filter menu.
 *
 * @package iwpdev/alevel
 */

use Alevel\Ajax\Testimonials;

// phpcs:disable
$param_filter = ! empty( $_GET['type'] ) ? filter_var( wp_unslash( $_GET['type'] ), FILTER_SANITIZE_STRING ) : 'all';
// phpcs:enable

?>
<ul class="filter-menu dfr">
	<?php
	foreach ( Testimonials::get_testimonials_type() as $key => $review_type ) {
		?>
		<li class="<?php echo $key === $param_filter ? esc_attr( 'active' ) : ''; ?>">
			<a href="#" data-type="<?php echo esc_attr( $key ); ?>">
				<?php echo esc_html( $review_type ); ?>
			</a>
		</li>
		<?php
	}
	?>
</ul>
