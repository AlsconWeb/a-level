<?php
/**
 * Template part: Blog item.
 *
 * @package iwpdev/alevel
 */

global $post;
$article_id = ! empty( $args['article_id'] ) ? $args['article_id'] : null;
$post_term  = ! empty( wp_get_post_terms( $article_id, 'category' ) ) ? wp_get_post_terms( $article_id, 'category' )[0] : null;
$post_tag   = wp_get_post_terms( $article_id, 'post_tag' );
$class_item = ! empty( $args['class_name'] ) ? $args['class_name'] : 'blog-item';
?>
<div class="<?php echo esc_attr( $class_item ); ?>">
	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="link"></a>
	<div class="img">
		<?php
		if ( 'testimonials' === $post->post_type ) {
			$url = carbon_get_post_meta( $article_id, 'alv_history_banner' );
			if ( ! empty( $url ) ) {
				?>
				<img
						src="<?php echo esc_url( $url ); ?>"
						alt="History banner">
				<?php
			} else {
				?>
				<img
						src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/thumbnail-medium.png' ); ?>"
						alt="No image">
				<?php
			}
		} else {
			if ( has_post_thumbnail( $article_id ) ) {
				the_post_thumbnail( 'alv-blog-min' );
			} else {
				?>
				<img
						src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/thumbnail-medium.png' ); ?>"
						alt="No image">
				<?php
			}
		}
		?>
		<?php if ( ! empty( $post_term ) ) { ?>
			<p class="tag"><?php echo esc_html( $post_term->name ); ?></p>
		<?php } ?>
		<p class="icon-file">
			<i class="icon-arrow-right"><?php esc_html_e( 'Стаття повністю', 'alevel' ); ?></i>
		</p>
	</div>
	<?php if ( ! empty( $post_tag ) ) { ?>
		<ul class="tags dfr">
			<?php foreach ( $post_tag as $item ) { ?>
				<li><?php echo esc_html( $item->name ); ?></li>
			<?php } ?>
		</ul>
	<?php } ?>
	<h3 class="title"><?php the_title(); ?></h3>
	<p><?php the_excerpt(); ?></p>
	<?php get_template_part( 'template-parts/author', 'info', [ 'post_id' => $article_id ] ); ?>
</div>
