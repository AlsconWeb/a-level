<?php
/**
 * History block templetera.
 *
 * @package iwpdev/alevel
 */

$class_name = is_post_type_archive( 'courses' ) ? 'white' : null;
?>
<div class="reviews short-code <?php echo esc_attr( $class_name ); ?>">
	<?php
	if ( is_multisite() ) {
		$site_id     = get_current_blog_id();
		$template_id = carbon_get_network_option( 1, 'alv_short_code' . $site_id . '_review_block', 'carbon_fields_container_crb_network_container' );
		echo do_shortcode( '[templatera id="' . $template_id . '"]' );
	} else {
		echo do_shortcode( '[templatera id="879"]' );
	}
	?>
</div>
