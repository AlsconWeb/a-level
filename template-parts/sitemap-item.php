<?php
/**
 * Template part: sitemap item.
 *
 * @package iwpdev/alevel
 */

$post_type_item = ! empty( $args['post_type'] ) ? $args['post_type'] : null;

$arg = [
	'post_type'      => $post_type_item,
	'posts_per_page' => - 1,
];

$site_map_obj = new WP_Query( $arg );

if ( $site_map_obj->have_posts() ) {
	$post_type_name = '';
	if ( 'post' === $post_type_item ) {
		$post_type_name = __( 'Записи', 'alevel' );
	}

	if ( 'page' === $post_type_item ) {
		$post_type_name = __( 'Сторінки', 'alevel' );
	}

	?>
	<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex">
		<div class="wpb_column vc_column_container vc_col-sm-12">
			<div class="vc_column-inner">
				<div class="wpb_wrapper">
					<h3><?php echo esc_html( $site_map_obj->get_queried_object()->label ?? $post_type_name ); ?></h3>
					<ul>
						<?php
						while ( $site_map_obj->have_posts() ) {
							$site_map_obj->the_post();
							?>
							<li>
								<a href="<?php the_permalink(); ?>">
									<?php the_title(); ?>
								</a>
							</li>
							<?php
						}
						wp_reset_postdata();
						?>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<?php
}