<?php
/**
 * Speaker info block.
 *
 * @package iwpdev/alevel
 */

$speakers = ! empty( $args['speakers'] ) ? $args['speakers'] : null;

if ( ! empty( $speakers ) ) {
	?>
	<ul>
		<?php
		foreach ( $speakers as $speaker ) {
			$avatar = wp_get_attachment_image_url( $speaker['photo'], 'alv-teacher-avatar' ) ?? get_template_directory_uri() . '/assets/img/basic.png';
			?>
			<li>
				<img
						src="<?php echo esc_url( $avatar ); ?>"
						alt="<?php echo esc_html( get_the_title( $speaker['photo'] ) ); ?>">
				<p>
					<?php echo esc_html( $speaker['name'] ); ?>
					<span><?php echo esc_html( $speaker['position'] ); ?></span>
				</p>
			</li>
		<?php } ?>
	</ul>
<?php } ?>
