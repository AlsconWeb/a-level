<?php
/**
 * Social media block.
 *
 * @package iwpdev/alevel
 */

$socials = ! empty( $args['social'] ) ? $args['social'] : null;

if ( ! empty( $socials ) ) { ?>
	<ul class="soc dfr">
		<?php foreach ( $socials as $item ) { ?>
			<li>
				<a
						class="icon-<?php echo esc_attr( $item['social_name'] ?? '' ); ?>"
						target="_blank"
						rel="nofollow"
						href="<?php echo esc_url( $item['social_link'] ?? '' ); ?>"></a>
			</li>
		<?php } ?>
	</ul>
	<?php
}
