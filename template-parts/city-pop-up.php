<?php
/**
 * Template part: City pop-up.
 *
 * @package iwpdev/alevel
 */

use Alevel\CityByIP\CityByIPPopUp;

$blog_name = (int) $args['blog_id'];
$city_name = (int) $args['city_name']

?>
<div class="located">
	<h3>
		<?php if ( $city_name !== 0 ) { ?>
			<?php esc_html_e( 'Ви знаходитесь в', 'alevel' ); ?>
			<span><?php echo esc_html( CityByIPPopUp::get_blogs_name()[ $city_name ]['ua'] ); ?>?</span>
		<?php } else { ?>
			<?php esc_html_e( 'Вашого міста немає у списку оберете місто або онлайн', 'alevel' ); ?>
		<?php } ?>
	</h3>
	<p>
		<?php
		echo sprintf(
			esc_html(
			/* translators: City name translate. %s: city name. */
				__( 'На даний момент ви на сайті A-Level %s та доступні курси можуть відрізнятися.', 'alevel' )
			),
			esc_html( CityByIPPopUp::get_blogs_name()[ $blog_name ]['ua'] )
		);
		?>
	</p>
	<div class="buttons dfr">
		<div class="select">
			<select id="local-city">
				<?php foreach ( CityByIPPopUp::get_blogs_name() as $key => $city ) { ?>
					<option value="<?php echo esc_url( is_multisite() ? get_blog_option( $key, 'siteurl' ) : '#' ); ?>">
						<?php echo esc_html( $city['ua'] ); ?>
					</option>
				<?php } ?>
			</select>
		</div>
		<a class="button closed" href="#">
			<?php esc_html_e( 'Залишитись тут', 'alevel' ); ?>
		</a>
	</div>
</div>
