<?php
/**
 * Template part: teacher cart.
 *
 * @package iwpdev/alevel
 */

use Alevel\Helpers\Helper;

$teacher_id = ! empty( $args['teacher_id'] ) ? $args['teacher_id'] : null;
$course_id  = ! empty( $args['course_id'] ) ? $args['course_id'] : null;

$author_avatar_url = isset( $teacher_id ) && has_post_thumbnail( $teacher_id ) ? get_the_post_thumbnail_url( $teacher_id, 'alv-teacher-avatar' ) : 'https://via.placeholder.com/80x80';
$author_name       = isset( $teacher_id ) ? get_the_title( $teacher_id ) : '';
$author_position   = isset( $teacher_id ) ? carbon_get_post_meta( $teacher_id, 'alv_teacher_job_title' ) : '';
$author_link       = isset( $teacher_id ) ? get_the_permalink( $teacher_id ) : '';
$author_post_count = isset( $teacher_id ) ? Helper::alv_get_count_post_teacher( $teacher_id ) : '';
$teacher_social    = carbon_get_post_meta( $teacher_id, 'alv_social_media_teacher' );
$teacher_course    = carbon_get_post_meta( $teacher_id, 'alv_teacher_course' );

?>
<div class="teacher">
	<a href="<?php the_permalink(); ?>" class="full-link"></a>
	<img
			class="user"
			src="<?php echo esc_url( $author_avatar_url ); ?>"
			alt="<?php echo esc_html( $author_name ); ?>">
	<div class="user-desc">
		<h4><?php echo esc_html( $author_name ); ?></h4>
		<h5><?php echo esc_html( $author_position ); ?></h5>
	</div>
	<p><?php echo esc_html( get_the_excerpt( $teacher_id ) ); ?></p>
	<?php
	if ( ! is_post_type_archive( 'teachers' ) && ! is_tax( 'teacher-specialization' ) ) {
		get_template_part(
			'template-parts/teacher',
			'course',
			[
				'course_id' => $course_id,
			]
		);
	} else {
		get_template_part(
			'template-parts/teacher',
			'course',
			[
				'courses_archive' => $teacher_course,
			]
		);
	}
	?>
	<a class="link icon-arrow-right" href="<?php the_permalink(); ?>">
		<?php esc_html_e( 'Дивитись повністю', 'alevel' ); ?>
	</a>
	<?php get_template_part( 'template-parts/social', '', [ 'social' => $teacher_social ] ); ?>
</div>
