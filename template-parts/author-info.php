<?php
/**
 * Author info block.
 *
 * @package iwpdev/alevel
 */

use Alevel\Helpers\Helper;

$post_id = $args['post_id'] ?? null;

if ( ! empty( $post_id ) ) {

	$author_type = carbon_get_post_meta( $post_id, 'alv_author_teachers' );

	if ( 'yes' === $author_type ) {
		$author            = carbon_get_the_post_meta( 'alv_post_author_teacher' );
		$author_avatar_url = isset( $author[0]['id'] ) && has_post_thumbnail( $author[0]['id'] ) ? get_the_post_thumbnail_url( $author[0]['id'], 'alv-teacher-avatar' ) : 'https://via.placeholder.com/80x80';
		$author_name       = isset( $author[0]['id'] ) ? get_the_title( $author[0]['id'] ) : '';
		$author_position   = isset( $author[0]['id'] ) ? carbon_get_post_meta( $author[0]['id'], 'alv_teacher_job_title' ) : '';
		$author_link       = isset( $author[0]['id'] ) ? get_the_permalink( $author[0]['id'] ) : '';
		$author_post_count = isset( $author[0]['id'] ) ? Helper::alv_get_count_post_teacher( $author[0]['id'] ) : '';
	} else {
		$author            = carbon_get_the_post_meta( 'alv_post_author_no_teacher' )[0];
		$author_avatar_url = wp_get_attachment_image_url( $author['photo'], 'alv-teacher-avatar' ) ?: 'https://via.placeholder.com/80x80';
		$author_name       = $author['name'];
		$author_position   = $author['position'];
		$author_link       = $author['link'];
		$author_post_count = null;
	}
}
?>
<ul>
	<?php if ( ! empty( $post_id ) && ! empty( $author ) ) { ?>
		<li data-title="<?php esc_html_e( 'Автор статтi:', 'alevel' ); ?>">
			<a href="<?php echo esc_url( $author_link ?: get_bloginfo( 'url' ) ); ?>">
				<?php if ( ! empty( $author_avatar_url ) ) { ?>
					<img
							class="user"
							src="<?php echo esc_url( $author_avatar_url ); ?>"
							alt="<?php echo esc_html( $author_name ); ?>">
				<?php } else { ?>
					<img
							class="user"
							src="<?php echo esc_url( $author_avatar_url ); ?>"
							alt="<?php echo esc_html( $author_name ); ?>">
				<?php } ?>
				<p>
					<?php echo esc_html( $author_name ); ?>
					<span><?php echo esc_html( $author_position ); ?></span>
				</p>
			</a>
		</li>
	<?php } ?>
</ul>
