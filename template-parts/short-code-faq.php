<?php
/**
 * Template Part: FAQ.
 *
 * @package iwpdev/alevel
 */

?>
<div class="faq short-code">
	<?php
	if ( is_multisite() ) {
		$site_id     = get_current_blog_id();
		$name        = 'alv_short_code' . $site_id . '_faq';
		$template_id = carbon_get_network_option( 1, $name, 'carbon_fields_container_crb_network_container' );
		echo do_shortcode( '[templatera id="' . $template_id . '"]' );
	} else {
		echo do_shortcode( '[templatera id="488"]' );
	}
	?>
</div>
