<?php
/**
 * Template part: Header contact template.
 *
 * @package iwpdev/alevel
 */

$social_facebook  = carbon_get_theme_option( 'alv_facebook_link' );
$social_instagram = carbon_get_theme_option( 'alv_instagram_link' );
$social_linkedin  = carbon_get_theme_option( 'alv_linkedin_link' );
$social_telegram  = carbon_get_theme_option( 'alv_telegram_link' );
$social_dou       = carbon_get_theme_option( 'alv_dou_link' );
?>

<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex">
	<div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-6 vc_col-md-6 vc_col-xs-12">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">
				<h1 style="text-align: left" class="vc_custom_heading title">
					<?php the_title(); ?>
				</h1>
				<div class="wpb_text_column wpb_content_element ">
					<div class="wpb_wrapper">
						<?php echo wp_kses_post( wpautop( carbon_get_the_post_meta( 'alv_partners_custom_title' ) ) ); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-6 vc_col-md-6 vc_col-xs-12">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">
				<div class="wpb_raw_code wpb_content_element wpb_raw_html">
					<div class="wpb_wrapper">
						<ul class="soc">
							<?php if ( ! empty( $social_facebook ) ) { ?>
								<li class="icon-facebook-f">
									<a
											href="<?php echo esc_url( $social_facebook ); ?>"
											rel="nofollow noreferrer"
											target="_blank"></a>
								</li>
								<?php
							}
							if ( ! empty( $social_instagram ) ) {
								?>
								<li class="icon-instagram">
									<a
											href="<?php echo esc_url( $social_instagram ); ?>"
											rel="nofollow noreferrer"
											target="_blank"></a>
								</li>
								<?php
							}

							if ( ! empty( $social_linkedin ) ) {
								?>
								<li class="icon-linkedin">
									<a
											href="<?php echo esc_url( $social_linkedin ); ?>"
											rel="nofollow noreferrer"
											target="_blank"></a>
								</li>
								<?php
							}

							if ( ! empty( $social_telegram ) ) {
								?>
								<li class="icon-telegram">
									<a
											href="<?php echo esc_url( $social_telegram ); ?>"
											rel="nofollow noreferrer"
											target="_blank"></a>
								</li>
								<?php
							}

							if ( ! empty( $social_dou ) ) {
								?>
								<li class="icon-dou">
									<a
											href="<?php echo esc_url( $social_dou ); ?>"
											rel="nofollow noreferrer"
											target="_blank"></a>
								</li>
							<?php } ?>
						</ul>
					</div>
				</div>
				<div class="vc_btn3-container  button vc_btn3-inline">
					<a
							class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-modern vc_btn3-color-grey"
							href="<?php bloginfo( 'url' ); ?>"
							title="<?php esc_html_e( 'На головну', 'alevel' ); ?>">
						<?php esc_html_e( 'На головну', 'alevel' ); ?>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
