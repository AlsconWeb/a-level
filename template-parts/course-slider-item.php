<?php
/**
 * Template part course slider item.
 *
 * @package iwpdev/alevel
 */

$course_id            = ! empty( $args['course_id'] ) ? $args['course_id'] : null;
$course_icon_url      = carbon_get_post_meta( $course_id, 'alv_course_icon' );
$course_category      = wp_get_post_terms( $course_id, 'category-courses' );
$date_start           = carbon_get_post_meta( $course_id, 'alv_course_start' ) ?? '';
$course_duration      = carbon_get_post_meta( $course_id, 'alv_course_duration' ) ?? '';
$course_discount_text = carbon_get_post_meta( $course_id, 'alv_course_discount_text' );
$course_type          = wp_get_post_terms( $course_id, 'type-courses' );

if ( ! empty( $course_id ) ) {
	?>
	<div class="item">
		<div class="dfr">
			<?php if ( ! empty( $course_icon_url ) ) { ?>
				<img
						src="<?php echo esc_url( $course_icon_url ); ?>"
						alt="<?php echo esc_html( get_the_title( $course_id ) ); ?> icon">
			<?php } else { ?>
				<img
						src="https://via.placeholder.com/80x80"
						alt="<?php echo esc_html( get_the_title( $course_id ) ); ?> no image">
				<?php
			} ?>
			<ul>
				<?php
				if ( ! empty( $course_type ) ) {
					?>
					<li>
						<?php echo esc_html( $course_type[0]->name ); ?>
					</li>
					<?php
				}
				?>
				<?php
				if ( ! empty( $course_discount_text ) ) {
					?>
					<li class="stock">
						<?php echo esc_html( $course_discount_text ); ?>
					</li>
					<?php
				}
				?>
			</ul>
		</div>
		<ul class="tags dfr">
			<li>
				<a href="#" class="icon-arrow-right"><?php echo esc_html( $course_category[0]->name ?? '' ); ?></a>
			</li>
		</ul>
		<h3 class="title"><?php the_title(); ?></h3>

		<ul class="metadate">
			<li class="start">
				<?php esc_html_e( 'Старт:', 'alevel' ); ?>
				<span><?php echo esc_html( $date_start ); ?></span>
			</li>
			<li class="duration">
				<?php esc_html_e( 'Тривалість:', 'alevel' ); ?>
				<span>
				<?php
				echo esc_html( $course_duration );
				?>
			</span>
			</li>
		</ul>
		<a class="link" href="<?php the_permalink(); ?>"></a>
	</div>
	<?php
}
