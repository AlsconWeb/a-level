<?php
/**
 * Template part: reviews item.
 *
 * @package iwpdev/alevel
 */

$course_id       = ! empty( $args['course_id'] ) ? $args['course_id'] : null;
$testimonials_id = ! empty( $args['testimonials_id'] ) ? $args['testimonials_id'] : null;
if ( ! empty( $course_id ) ) {
	$review_social = carbon_get_post_meta( $testimonials_id, 'alv_social_media_reviewer' );
	$course_icon   = carbon_get_post_meta( $course_id, 'alv_course_icon' );
	$teachers      = carbon_get_post_meta( $testimonials_id, 'alv_reviewer_teachers' );
	$position      = carbon_get_post_meta( $testimonials_id, 'alv_reviewer_position' );
	$review_type   = get_post_meta( $testimonials_id, '_alv_type_review', true ) ?: 'regular';

	if ( 'history' === $review_type ) {
		$text    = __( 'Icторiя успiху', 'alevel' );
		$history = sprintf( "data-history='%s'", esc_html( $text ) );
	}
	?>
	<div class="reviews-item <?php echo 'firm' === $review_type ? esc_attr( 'company' ) : ''; ?>">
		<?php if ( 'history' === $review_type || 'firm' === $review_type ) { ?>
			<a href="<?php the_permalink(); ?>" class="full-link"></a>
		<?php } else { ?>
			<a data-course_id="<?php echo esc_attr( $testimonials_id ); ?>" class="full-link link-pop-up"></a>
		<?php } ?>
		<div class="dfr <?php echo isset( $history ) ? 'history-el' : ''; ?>">
			<?php
			if ( has_post_thumbnail( $testimonials_id ) ) {
				the_post_thumbnail( 'alv-teacher-avatar', [ 'class' => 'user' ] );
			} else {
				?>
				<img
						class="user"
						src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/thumbnail-mini-2.png' ); ?>"
						alt="No Avatar">
				<?php
			}
			?>
			<div class="user-desc" <?php echo $history ?? ''; ?>>
				<h4><?php the_title(); ?></h4>
			</div>
		</div>
		<?php if ( ! empty( $review_social ) ) { ?>
			<ul class="soc dfr">
				<?php foreach ( $review_social as $social ) { ?>
					<li>
						<a
								target="_blank"
								rel="nofollow"
								class="icon-<?php echo esc_attr( $social['social_name'] ); ?>"
								href="<?php echo esc_url( $social['social_link'] ); ?>"> </a>
					</li>
				<?php } ?>
			</ul>
		<?php } ?>
		<p><?php the_excerpt(); ?></p>
		<?php if ( 'history' === $review_type || 'firm' === $review_type ) { ?>
			<a class="link icon-arrow-right" href="<?php the_permalink(); ?>">
				<?php esc_html_e( 'Вiдгук повнiстю', 'alevel' ); ?>
			</a>
		<?php } else { ?>
			<a
					class="link regular-review icon-arrow-right"
					data-course_id="<?php echo esc_attr( $testimonials_id ); ?>"
					href="#">
				<?php esc_html_e( 'Вiдгук повнiстю', 'alevel' ); ?>
			</a>
		<?php } ?>
		<?php if ( 'firm' !== $review_type ) { ?>
			<ul>
				<li data-title="<?php esc_html_e( 'Курс:', 'alevel' ); ?>">
					<?php if ( ! empty( $course_icon ) ) { ?>
					<a href="<?php echo esc_url( get_permalink( $course_id ) ); ?>" class="link">
						<img
								src="<?php echo esc_url( $course_icon ); ?>"
								alt="<?php the_title(); ?> icon">
						<?php } else { ?>
							<img
									src="https://via.placeholder.com/80x80"
									alt="<?php the_title(); ?> no image">
						<?php } ?>
						<p><?php echo esc_html( get_the_title( $course_id ) ); ?></p>
					</a>
				</li>
				<?php if ( ! empty( $teachers ) ) { ?>
					<li data-title="<?php esc_html_e( 'Викладач:', 'alevel' ); ?>">
						<?php foreach ( $teachers as $teacher ) { ?>
							<div class="teacher">
								<a href="<?php echo esc_url( get_permalink( $teacher['id'] ) ); ?>" class="link">
									<?php
									if ( has_post_thumbnail( $teacher['id'] ) ) {
										echo wp_kses_post( get_the_post_thumbnail( $teacher['id'], 'alv-teacher-avatar' ) );
									} else {
										?>
										<img
												src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/thumbnail-mini-2.png' ); ?>"
												alt="No Avatar">
									<?php } ?>
									<p><?php echo esc_html( get_the_title( $teacher['id'] ) ); ?></p>
								</a>
							</div>
						<?php } ?>
					</li>
				<?php } ?>
			</ul>
		<?php } ?>
	</div>
	<?php
}
