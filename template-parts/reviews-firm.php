<?php
/**
 * Template part reviews firm.
 *
 * @package iwpdev/alevel
 */

$course_id       = ! empty( $args['course_id'] ) ? $args['course_id'] : null;
$testimonials_id = ! empty( $args['testimonials_id'] ) ? $args['testimonials_id'] : null;

$count = carbon_get_post_meta( $testimonials_id, 'alv_course_count_students' );
if ( ! empty( $course_id ) ) {
	$course_icon = carbon_get_post_meta( $course_id, 'alv_course_icon' );
}
$type_testimonials = carbon_get_post_meta( $testimonials_id, 'alv_company_no_review' );
$url               = carbon_get_post_meta( $testimonials_id, 'alv_firm_link' );
$text_url          = carbon_get_post_meta( $testimonials_id, 'alv_firm_text_link' );
$reviewer_social   = carbon_get_post_meta( $testimonials_id, 'alv_social_media_reviewer' );
?>

<div class="partner <?php echo 'true' === $type_testimonials ? 'no-review' : ''; ?>">
	<?php if ( 'false' === $type_testimonials ) { ?>
		<a href="<?php the_permalink(); ?>" class="full-link"></a>
	<?php } ?>
	<div class=" dfr">
		<?php
		if ( has_post_thumbnail( $testimonials_id ) ) {
			the_post_thumbnail( 'alv-teacher-avatar', [ 'class' => 'user' ] );
		} else {
			?>
			<img
					class="user"
					src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/thumbnail-mini-2.png' ); ?>"
					alt="No Avatar">
			<?php
		}
		?>
		<div class="desc">
			<?php if ( 'false' === $type_testimonials ) { ?>
				<h3><?php the_title(); ?></h3>
			<?php } else { ?>
				<div class="desc">
					<h3>
						<a href="<?php echo esc_url( $url ?: '#' ); ?>">
							<?php the_title(); ?>
						</a>
					</h3>
				</div>
			<?php } ?>
			<?php
			if ( ! empty( $count ) ) {
				echo sprintf(
					'<p>%s<br>%s %s</p>',
					esc_html( __( 'Працевлаштовано', 'alevel' ) ),
					esc_html( __( 'випускників до компанії:', 'alevel' ) ),
					esc_attr( $count ),
				);
			}
			?>
			<a href="<?php echo esc_url( $url ?? '#' ); ?>">
				<?php echo esc_html( $text_url ); ?>
			</a>
			<?php get_template_part( 'template-parts/social', '', [ 'social' => $reviewer_social ] ); ?>
		</div>
	</div>
	<?php if ( 'false' === $type_testimonials ) { ?>
		<p class="icon-quote"><?php echo esc_html( get_the_excerpt( $testimonials_id ) ); ?></p>
		<a class="link" href="<?php the_permalink(); ?>">
			<?php esc_html_e( 'Читати повний вiдгук', 'alevel' ); ?>
		</a>
	<?php } ?>
</div>
