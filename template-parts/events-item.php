<?php
/**
 * Template part: Events item.
 *
 * @package iwpdev/alevel
 */

$event_id = ! empty( $args['event_id'] ) ? $args['event_id'] : null;

if ( ! empty( $event_id ) ) {

	$event_tag          = wp_get_post_terms( $event_id, 'event-tag' );
	$event_mentors      = carbon_get_the_post_meta( 'alv_mentors' );
	$event_company_link = carbon_get_the_post_meta( 'alv_company_web_link' );
	$event_type         = carbon_get_post_meta( $event_id, 'alv_event_type' );
	?>

	<div class="events-item">
		<?php
		if ( has_post_thumbnail( $event_id ) ) {
			the_post_thumbnail( 'alv-thumbnail-big' );
		} else {
			?>
			<img
					src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/thumbnail-big.png"' ); ?>"
					alt="No image">
		<?php } ?>
		<div class="desc">
			<?php if ( ! empty( $event_tag ) ) { ?>
				<ul class="tags dfr">
					<?php foreach ( $event_tag as $item_tag ) { ?>
						<li><?php echo esc_html( $item_tag->name ); ?></li>
					<?php } ?>
				</ul>
			<?php } ?>
			<h3 class="title"><?php the_title(); ?></h3>
			<p><?php the_excerpt(); ?></p>
			<ul>
				<?php if ( ! empty( $event_mentors ) ) { ?>
					<li data-title="<?php esc_html_e( 'Спiкер:', 'alevel' ); ?>">
						<?php
						foreach ( $event_mentors as $mentor ) {
							$photo_url = wp_get_attachment_image_url( $mentor['photo'], 'alv-teacher-avatar' );
							?>
							<div class="mentors">
								<?php if ( ! empty( $photo_url ) ) { ?>
									<img
											src="<?php echo esc_url( $photo_url ); ?>"
											alt="<?php echo esc_html( get_the_title( $mentor['photo'] ) ); ?>">
								<?php } else { ?>
									<img
											src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/thumbnail-mini-2.png' ); ?>"
											alt="No image">
								<?php } ?>
								<p><?php echo esc_html( $mentor['name'] ); ?></p>
							</div>
						<?php } ?>
					</li>
					<?php
				}
				$support_name = carbon_get_the_post_meta( 'alv_company_name' );
				?>
				<li data-title="<?php esc_html_e( 'За підтримки', 'alevel' ); ?>">
					<a
							href="<?php echo esc_url( $event_company_link ) ?: '#'; ?>"
							target="_blank"
							rel="nofollow"
							class="link">
						<?php echo esc_html( $support_name ?? '' ); ?>
					</a>
				</li>
			</ul>
			<div class="buttons dfr">
				<?php
				$url = 'firm' === $event_type ? carbon_get_post_meta( $event_id, 'alv_event_firm_link' ) : '#';
				if ( 'firm' === $event_type ) {
					?>
					<a class="button" target="_blank" rel="nofollow" href="<?php echo esc_url( $url ); ?>">
						<?php esc_html_e( 'Записатись', 'alevel' ); ?>
					</a>
				<?php } else { ?>
					<a
							class="button"
							href="#"
							rel="noreferrer nofollow"
							data-toggle="modal"
							data-event_id="<?php echo esc_attr( $event_id ); ?>"
							data-target=".registration-event">
						<?php esc_html_e( 'Записатись', 'alevel' ); ?>
					</a>
				<?php } ?>
				<a
						class="button"
						href="<?php the_permalink(); ?>"
						title="<?php the_title(); ?>">
					<?php esc_html_e( 'Дізнатись більше', 'alevel' ); ?>
				</a>
			</div>
		</div>
	</div>
	<?php
}
