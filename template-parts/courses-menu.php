<?php
/**
 * Template part courses menu
 *
 * @package iwpdev/alevel
 */

$courses_category = get_categories(
	[
		'taxonomy'   => 'category-courses',
		'type'       => 'courses',
		'hide_empty' => 1,
	]
);
$term             = get_queried_object();
if ( ! is_post_type_archive() ) {
	$description = term_description( $term->term_id, 'category-courses' );
	?>
	<p>
		<?php echo wp_kses_post( wpautop( $description ?? '' ) ); ?>
	</p>
	<?php
}
if ( ! empty( $courses_category ) ) { ?>
	<ul class="tag dfr">
		<li class="<?php echo 'courses' === $term->name ? 'current-menu-item' : ''; ?>">
			<a href="<?php echo esc_url( get_post_type_archive_link( 'courses' ) ); ?>">
				<?php esc_attr_e( 'Усi курси', 'alevel' ); ?>
			</a>
		</li>
		<?php foreach ( $courses_category as $item ) { ?>
			<li class="<?php echo isset( $term->slug ) && $term->slug === $item->slug ? 'current-menu-item' : ''; ?>">
				<a href="<?php echo esc_url( get_term_link( $item, 'category-courses' ) ); ?>">
					<?php echo esc_html( $item->name ); ?>
				</a>
			</li>
		<?php } ?>
	</ul>
	<?php
}
