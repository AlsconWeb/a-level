<?php
/**
 * Global preloader.
 *
 * @package iwpdev/alevel
 */

?>
<style>
	@media only screen and (max-width: 1024px) {
		body.overflow header .hide,
		body.overflow header .menu {
			display: none !important;
		}

	}

	body.overflow .top-head,
	body.overflow .bottom-head,
	body.overflow section,
	body.overflow footer {
		filter: blur(3px);
		pointer-events: none;
	}

	body.overflow header {
		top: 8px;
	}

	body.overflow.admin-bar header {
		top: 40px !important;
	}

	.preloader-head {
		position: absolute;
		left: 0;
		right: 0;
		top: 0px;
		transform: translateY(-100%);
		height: 8px;
		z-index: 999999;
	}

	.preloader-head .line-head {
		background: linear-gradient(97.71deg, #4BD2FD -16.21%, #406BD9 77.81%);
		height: 100%;
		width: 0;
	}
</style>
<div class="preloader-head">
	<div class="line-head"></div>
</div>
<script>
	function loader( _success ) {
		var obj = document.querySelector( '.preloader-head' ),
			line = document.querySelector( '.line-head' ),
			bodyEl = document.querySelector( 'body' ),
			w = 0;
		bodyEl.classList.add( 'overflow' );
		t = setInterval( function() {
			w = w + 1.25;
			line.style.width = w + 'vw';
			if ( w === 100 ) {
				obj.style.display = 'none';
				clearInterval( t );
				w = 0;
				line.style.width = w + 'vw';
				bodyEl.classList.remove( 'overflow' );
				if ( _success ) {
					return _success();
				}
			}
		}, 20 );
	};
	loader();
</script>
