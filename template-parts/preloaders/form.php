<?php
/**
 * Preloader by forms.
 *
 * @package iwpdev/alevel
 */

?>
<div class="mail-preloader" style="display: none">
	<div class="preloader-content">
		<img
				src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/mail-preloader.svg' ); ?>"
				alt="Mail icon">
		<p style="margin-top:22px;">
			<?php esc_html_e( 'Відправлення Вашої заявки' ); ?>
		</p>
		<div class="preloader-num">0</div>
		<div class="preloader-line" style="margin-top:16px;">
			<div class="line form-line"></div>
		</div>
	</div>
</div>
