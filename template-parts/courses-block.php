<?php
/**
 * Releted course block.
 *
 * @package
 */

$related_course = ! empty( $args['course'][0] ) ? $args['course'][0] : null;

if ( ! empty( $related_course ) ) {
	$course_icon_url = carbon_get_post_meta( $related_course['id'], 'alv_course_icon' );
	?>
	<div class="courses-block">
		<div class="dfr">
			<div class="img">
				<?php if ( ! empty( $course_icon_url ) ) { ?>
					<img
							src="<?php echo esc_url( $course_icon_url ); ?>"
							alt="<?php echo esc_html( get_the_title( $related_course['id'] ) ); ?> icon">
				<?php } else { ?>
					<img
							src="https://via.placeholder.com/80x80"
							alt="<?php echo esc_html( get_the_title( $related_course['id'] ) ); ?> no image">
				<?php } ?>
			</div>
			<h3><?php echo esc_html( get_the_title( $related_course['id'] ) ); ?></h3>
			<a class="button" href="<?php echo esc_url( get_the_permalink( $related_course['id'] ) ); ?>">
				<?php esc_html_e( 'Дiзнатися бiльше', 'alevel' ); ?>
			</a>
		</div>
	</div>
<?php } ?>
