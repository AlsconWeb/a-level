<?php
/**
 * Template part: teachers specialization.
 *
 * @package iwpdev/alevel
 */

$query_object = get_queried_object();
?>
<ul class="tags dfr">
	<?php
	$all_category = get_categories(
		[
			'taxonomy'   => 'teacher-specialization',
			'type'       => 'teachers',
			'hide_empty' => 1,
		]
	);

	if ( ! empty( $all_category ) ) {
		foreach ( $all_category as $item ) {
			?>
			<li class="<?php echo esc_attr( isset( $query_object->slug ) && $query_object->slug === $item->slug ? 'active' : '' ); ?>">
				<a
						href="<?php echo esc_url( get_term_link( $item->term_id, 'teacher-specialization' ) ); ?>"
						class="teacher-specialization"
						data-category="<?php echo esc_attr( $item->slug ); ?>">
					<?php echo esc_html( $item->name ); ?>
				</a>
			</li>
			<?php
		}
	}
	?>
</ul>
