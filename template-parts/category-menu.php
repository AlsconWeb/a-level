<?php
/**
 * Template part: Blog category tag.
 *
 * @package iwpdev/alevel
 */

$teacher_id = ! empty( $args['teacher_post'] ) && $args['teacher_post'] ? 'data-teacher=' . get_the_ID() : null;
?>
<ul class="tags-menu dfr">
	<li class="active" data-category="all">
		<a href="#" class="category-item" data-category="all">
			<?php esc_html_e( 'Усi напрямки', 'alevel' ); ?>
		</a>
	</li>
	<?php
	$all_category = get_categories(
		[
			'taxonomy'   => 'category',
			'type'       => 'post',
			'exclude'    => [ 1 ],
			'hide_empty' => 1,
		]
	);

	if ( ! empty( $all_category ) ) {
		foreach ( $all_category as $item ) {
			?>
			<li>
				<a
						href="#" class="category-item"
						data-category="<?php echo esc_attr( $item->slug ); ?>">
					<?php echo esc_html( $item->name ); ?>
				</a>
			</li>
			<?php
		}
		?>
		<li>
			<a href="#" class="category-item" data-category="history">
				<?php esc_html_e( 'Історія успіху', 'alevel' ); ?>
			</a>
		</li>
		<?php
	}
	?>
</ul>
