<?php
/**
 * Header template.
 *
 * @package iwpdev/alevel
 */

use Alevel\Main;

$page_class = '';

if ( is_front_page() ) {
	$page_class = 'home';
}
$social_telegram              = carbon_get_theme_option( 'alv_telegram_link' );
$phone_one                    = carbon_get_theme_option( 'alv_phone_one' );
$phone_two                    = carbon_get_theme_option( 'alv_phone_two' );
$email                        = carbon_get_theme_option( 'alv_email' );
$alv_header_sales_text        = carbon_get_theme_option( 'alv_header_sales_text' );
$alv_header_sales_height      = carbon_get_theme_option( 'alv_height' );
$alv_header_sales_bg_color    = carbon_get_theme_option( 'alv_bg_color' );
$alv_header_sales_text_color  = carbon_get_theme_option( 'alv_text_color' );
$alv_header_sales_text_weight = carbon_get_theme_option( 'alv_text_weight' );
$alv_header_sales_text_size   = carbon_get_theme_option( 'alv_text_size' );

?>
<!DOCTYPE html>
<html style="margin-top:0 !important" <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<title><?php bloginfo( 'name' ); ?> | <?php bloginfo( 'description' ); ?></title>
	<?php wp_head(); ?>

</head>
<body <?php body_class( $page_class ); ?>>
<?php
get_template_part( 'template-parts/preloaders/form' );
?>

<header>
	<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex top-head">
		<div class="wpb_column vc_column_container vc_col-sm-12">
			<div class="vc_column-inner">
				<div class="wpb_wrapper">
					<div class="wpb_column vc_column_container vc_col-sm-12">
						<div class="dfr">
							<?php the_custom_logo(); ?>
							<?php
							wp_nav_menu(
								[
									'theme_location' => 'top_bar_menu',
									'container'      => '',
									'menu_class'     => 'city dfr',
									'echo'           => true,
									'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
								]
							);
							?>
							<div class="dfr">
								<?php
								if ( ! empty( $social_telegram ) ) {
									?>
									<a
											class="icon-telegram"
											href="<?php echo esc_url( $social_telegram ); ?>"
											rel="nofollow noreferrer"
											target="_blank"></a>
									<?php
								}
								?>
								<div class="call-back">
									<p class="icon-phone"><?php esc_html_e( 'Подзвонити', 'alevel' ); ?></p>
									<div class="call-back-contacts">
										<a
												class="tel"
												href="tel:<?php echo filter_var( $phone_one, FILTER_SANITIZE_NUMBER_INT ) ?? ''; ?>">
											<?php echo esc_html( $phone_one ) ?? ''; ?>
										</a>
										<a
												class="tel"
												href="tel:<?php echo filter_var( $phone_two, FILTER_SANITIZE_NUMBER_INT ) ?? ''; ?>">
											<?php echo esc_html( $phone_two ) ?? ''; ?>
										</a>
										<p><?php esc_html_e( 'Дзвінки в межах України безкоштовні', 'alevel' ); ?></p>
										<a
												class="icon-mail"
												href="mailto:<?php echo esc_html( $email ?: '' ); ?>">
											<?php echo esc_html( $email ?: '' ); ?>
										</a>
									</div>
								</div>
								<a
										class="consultation icon-service"
										data-toggle="modal"
										data-target=".consultation"
										href="#">
									<?php esc_html_e( 'Безкоштовна консультацiя', 'alevel' ); ?>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if ( ! empty( $alv_header_sales_text ) ) { ?>
		<div
				class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex banner-head"
				style="background-color: <?php echo esc_attr( $alv_header_sales_bg_color ); ?>; min-height:<?php echo esc_attr( $alv_header_sales_height ) ?? 40; ?>px;">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper dfr">
						<p style="font-weight: <?php echo esc_attr( $alv_header_sales_text_weight ); ?>; color: <?php echo esc_attr( $alv_header_sales_text_color ); ?>; font-size: <?php echo esc_attr( $alv_header_sales_text_size ) ?? 25; ?>px;">
							<?php echo esc_html( $alv_header_sales_text ); ?>
						</p>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
	<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex bottom-head">
		<div class="wpb_column vc_column_container vc_col-sm-12">
			<div class="vc_column-inner">
				<div class="wpb_wrapper dfr">
					<div class="burger-menu">
						<span></span>
						<span></span>
						<span></span>
					</div>
					<?php the_custom_logo(); ?>
					<div class="hide">
						<div class="dfr">
							<div class="select">
								<?php Main::get_city_in_select(); ?>
							</div>
							<div class="button">
								<p class="icon-phone"><?php esc_html_e( 'Подзвонити', 'alevel' ); ?></p>
								<div class="call-back-contacts">
									<a
											class="tel"
											href="tel:<?php echo filter_var( $phone_one, FILTER_SANITIZE_NUMBER_INT ) ?? ''; ?>">
										<?php echo esc_html( $phone_one ) ?? ''; ?>
									</a>
									<a
											class="tel"
											href="tel:<?php echo filter_var( $phone_two, FILTER_SANITIZE_NUMBER_INT ) ?? ''; ?>">
										<?php echo esc_html( $phone_two ) ?? ''; ?>
									</a>
									<a
											class="icon-mail"
											href="mailto:<?php echo esc_html( $email ?: '' ); ?>">
										<?php echo esc_html( $email ?: '' ); ?>
									</a>
								</div>
							</div>
							<a
									class="button icon-service"
									data-toggle="modal"
									data-target=".consultation"
									href="#">
								<?php esc_html_e( 'Безкоштовна консультацiя', 'alevel' ); ?>
							</a>
						</div>
					</div>
					<?php
					wp_nav_menu(
						[
							'theme_location'  => 'main_menu',
							'container'       => '',
							'container_class' => '',
							'menu_class'      => 'menu dfr',
							'echo'            => true,
							'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						]
					);
					?>

				</div>
			</div>
		</div>
	</div>
	<?php get_template_part( 'template-parts/preloaders/global' ); ?>
</header>
